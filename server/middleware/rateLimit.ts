import { createStorage } from "unstorage";

const storage = createStorage()

type StorageItem = {
  value: number;
  date: number;
}

const setStorageItem = async (ip: string, rateLimiterConfig: any) => {
  const rateLimitedObject: StorageItem = {
    value: rateLimiterConfig?.tokensPerInterval,
    date: Date.now(),
  };

  await storage.setItem(ip, rateLimitedObject);
}

const getIP = (event: any) =>  {
  const req = event?.node?.req
  let xForwardedFor = getRequestHeader(event, 'x-forwarded-for')

  if (xForwardedFor === '::1') {
    xForwardedFor = '127.0.0.1'
  }

  const transformedXForwardedFor = xForwardedFor?.split(',')?.pop()?.trim() || ''
  const remoteAddress = req?.socket?.remoteAddress || ''
  let ip = transformedXForwardedFor || remoteAddress

  if (ip) {
    ip = ip.split(':')[0]
  }

  return ip
}

export default defineEventHandler(async (event) => {
  const rateLimiterConfig = {
    tokensPerInterval: 100,
    interval: 1000 * 5,
    throwError: true,
    headers: true,
  };

  return;
  
  const ip = getIP(event);
  let storageItem = (await storage.getItem(ip)) as StorageItem;
  if (!storageItem) {
    await setStorageItem(ip, rateLimiterConfig);
  } else {
    if (typeof storageItem !== "object") {
      return;
    }

    const timeSinceFirstRateLimit = storageItem.date;
    const timeForInterval = storageItem.date + rateLimiterConfig?.interval;

    if (Date.now() >= timeForInterval) {
      await setStorageItem(ip, rateLimiterConfig);
      storageItem = (await storage.getItem(ip)) as StorageItem;
    }

    const isLimited =
      timeSinceFirstRateLimit <= timeForInterval && storageItem.value === 0;
    
    if (isLimited) {
      const tooManyRequestsError = {
        statusCode: 429,
        statusMessage: "Too Many Requests",
      };

      if (rateLimiterConfig.headers) {
        setHeader(event, "x-ratelimit-remaining", 0);
        setHeader(
          event,
          "x-ratelimit-limit",
          rateLimiterConfig?.tokensPerInterval
        );
        setHeader(event, "x-ratelimit-reset", timeForInterval);
      }

      if (rateLimiterConfig.throwError === false) {
        return tooManyRequestsError;
      }
      throw createError(tooManyRequestsError);
    }

    const newItemDate =
      timeSinceFirstRateLimit > timeForInterval ? Date.now() : storageItem.date;

    const newStorageItem = {
      value: storageItem.value - 1,
      date: newItemDate,
    };

    await storage.setItem(ip, newStorageItem);
    const currentItem = (await storage.getItem(ip)) as StorageItem;

    if (currentItem && rateLimiterConfig.headers) {
      setHeader(event, "x-ratelimit-remaining", currentItem.value);
      setHeader(
        event,
        "x-ratelimit-limit",
        rateLimiterConfig?.tokensPerInterval
      );
      setHeader(event, "x-ratelimit-reset", timeForInterval);
    }
  }
})