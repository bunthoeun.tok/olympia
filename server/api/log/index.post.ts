import fs from 'fs';
import path from 'path';

export default defineEventHandler( async(event) => {
    const body = await readBody(event);
    const writeLog = (log: string) => {
        const folderPath = 'D:/Log/YongYuan/Olympia/';
        // const folderPath = 'Log/';
        const fileName = `error-${new Date().toISOString().split('T')[0]}.log`;
        const fullPath = folderPath+fileName;
        const fileContent = `[${ new Date()}]${log}`;
        if (!fs.existsSync(folderPath)) {
            fs.mkdirSync(folderPath);
            fs.chmodSync(folderPath, '0777');
        }
        if (fs.existsSync(fullPath)) {
            const maxSize = 5 * 1024 * 1024;
            if (fs.existsSync(fullPath) && fs.statSync(fullPath).size >= maxSize) {
                const fileDir = path.dirname(fullPath);
                const fileName = path.basename(fullPath, '.log');
                let suffix = 1;
                let newFilePath;
                do {
                    newFilePath = path.join(fileDir, `${fileName}(${suffix}).log`);
                    suffix++;
                } while (fs.existsSync(newFilePath));
                const existingFilePath = path.join(fileDir, `${fileName}.log`);
                fs.renameSync(existingFilePath, newFilePath);
            }
           fs.appendFileSync(fullPath, '\n'+fileContent);
        } else {
            fs.writeFileSync(fullPath, fileContent);
        }
        return fileContent;
    };
    return { log:  writeLog(body) };
});