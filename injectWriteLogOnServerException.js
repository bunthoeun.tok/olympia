const fs = require('fs');

function addHelloToFileLine(filePath, arg) {
  fs.readFile(filePath, 'utf8', (err, data) => {
    if (err) {
      console.error(err);
      return;
    }
    const lines = data.split('\n');
    const modifiedLines = lines.map((line) => {
        if (line.includes(arg[0].searchString)) {
            return line + arg[0].stringToAdd;
        }
        return line;
    });
    const modifiedData = modifiedLines.join('\n');
    fs.writeFile(filePath, modifiedData, (err) => {
      if (err) {
        console.error(err);
        return;
      }
      console.log('inject write log script on server error exception successfully')
    });
  });
}
const filePath = `./.output/server/chunks/node-server.mjs`;
const data  = [
  {
    searchString: `const { stack, statusCode, statusMessage, message } = normalizeError(error);`,
    stringToAdd: `\n\tfetch('http://localhost:3000/api/log', { method: 'post', body: '[host:'+event.node.req.headers?.host+'][ip:'+event.node.req.headers?.['x-forwarded-for']+']\\n[uagent:'+event.node.req.headers?.['user-agent']+']\\n['+error.stack+']'});`
  },
];
addHelloToFileLine(filePath, data);