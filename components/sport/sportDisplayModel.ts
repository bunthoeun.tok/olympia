import _ from "lodash";
import useModifyGameName from "../../composables/useModifyGameName";
import { IGameBsi } from "@/models/bsiGameList";
import textHelper from "@/utils/formatHelpers/textFormatHelper";
import commonHelper from "@/utils/commonHelper";
import { store } from "~~/store";

const { convertString } = textHelper;
const { getAssetImage } = commonHelper;
class SportDisplayModel implements IGameBsi {
  GameProviderShortName!: string;
  IsHotGame!: boolean;
  Rank!: number;
  IsNewGame!: boolean;
  IsJackpot!: boolean;
  Device!: number | null;
  TabType!: string;
  GameProviderName!: string;
  GameProviderId!: number;
  GameId!: number;
  Category!: number;
  CategoryName!: string;
  GameType!: string;
  GameName!: string;
  EnglishGameName?: string;
  IsEnabled!: boolean;
  IsMaintainByGameProvider!: boolean;
  IsMaintain!: boolean;
  ForMobile!: boolean;
  ForDesktop!: boolean;
  IsPromotionGame!: boolean;
  BsiPriority!: number;
  AsiPriority!: number;
  GameIconUrl!: string;
  EnabledCurrency!: string;
  GameCode!: string | null;
  Platform?: number | null;
  APIGameProviderName!: string;
  APIGameProviderId!: number;
  DisplayOrder!: number;
  IsEnabledByGameProvider!: boolean;
  LinkOption?: string;
  Named?: string;
  GameProviderDisplayName!: string;

  get StarImagePath(): string {
    const path = `${convertString(this.CategoryName)}/${this.Named}/star.png`;
    return getAssetImage(`assets/theme/common/entry/${path}`);
  }

  get ProviderLogoPath(): string {
    const path = `${convertString(this.CategoryName)}/${this.Named}/logo.png`;
    return getAssetImage(`assets/theme/common/entry/${path}`);
  }
  get ProviderLogoCenterPath(): string {
    const path = `${convertString(this.CategoryName)}/${this.Named}/logo.png`;
    return getAssetImage(`assets/theme/common/entry/center/${path}`);
  }

  get FloatBackground(): string {
    let path = `${convertString(this.CategoryName)}/float_background.png`;
    if (
      store.state.selectedThemeName === "peru" &&
      convertString(this.CategoryName) !== "live_casino"
    ) {
      path = `${convertString(this.CategoryName)}/${this.Named}/float/float_bg.png`;
    } else if (convertString(this.CategoryName) === "live_casino") {
      path = `${convertString(this.CategoryName)}/${this.Named}/float_bg.png`;
    }
    return getAssetImage(`assets/theme/common/entry/${path}`);
  }

  get BackgroundImage(): string {
    const path = `${convertString(this.CategoryName)}/background.png`;
    return getAssetImage(`assets/theme/common/entry/${path}`);
  }

  get BackgroundGeometryImage(): string {
    const location = window.location.pathname?.replace(/\/|-/g, '');
    const path = `bg-${location}.png`;
    return getAssetImage(`assets/common/geometry/${path}`);
  }

  get GameNameForDisplay(): string {
    return useModifyGameName(this);
  }

  constructor(init: IGameBsi) {
    Object.assign(this, init);
    const replacement: Record<string, string> = {
      _live_casino_lobby: "",
      _live_casino: "",
      _livecasino: "",
      live_casino: "",
      livecasino: "",
      _casino: "",
      casino: "",
      _lobby: "",
      lobby: "",
    };
    const expression = new RegExp(Object.keys(replacement).join("|"), "g");
    this.Named = _.replace(
      _.toLower(this.EnglishGameName),
      /(\s)+/g,
      "_"
    ).replace(expression, (matched) => replacement[matched]);
    if (this.GameProviderId === 1052) this.Named = this.Named.concat("new");
  }
}

export default SportDisplayModel;
