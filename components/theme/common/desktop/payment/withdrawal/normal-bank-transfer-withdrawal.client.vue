<template>
  <div class="space-y-2 p-6">
    <div
      v-if="bankTransferWithdrawalTextInfo?.Text.length > 0"
      class="p-4 text-sm rounded bg-background5 --text4 font-normal mb-4 txt-info"
      v-html="bankTransferWithdrawalTextInfo?.Text"
    ></div>
    <h5 class="--text4">{{ $t("bank") }}</h5>
    <div class="pb-5">
      <ui-radio-group
        v-model="selectedBank"
        class="flex flex-column flex-wrap gap-2 select-bank py-2 depsit-select-bank"
        group-name="select-company-bank-name"
        id="withdrawalBankGroup"
      >
        <ui-radio-button
          v-for="bank in bankRadios"
          :key="bank.PlayerBankName"
          :label="bank.PlayerBankName"
          :value="bank.Id"
          active-class="selected"
          class="w-fit h-full"
        >
          <ui-check-button
            v-if="!bank.BankLogo || bank.BankLogo == undefined"
            variant="ICON_AND_TEXT"
            :text="bank.PlayerBankName"
          />
          <ui-check-button
            v-else
            class="min-h-[3rem]"
            style="--height: 3rem"
            :img="getImagePathBankLogo(bank.BankLogo)"
          />
        </ui-radio-button>
      </ui-radio-group>
    </div>
  </div>
  <div class="p-6">
    <el-form
      ref="bankTransferWithdrawalFormRef"
      label-width="210px"
      :model="bankTransferWithdrawalFormData"
      :rules="bankTransferWithdrawalRules"
      label-position="left"
      :validate-on-rule-change="false"
    >
      <el-form-item
        v-if="isBankNameEnabled || selectedBank === 'Other'"
        :label="$t('bank_name')"
        prop="customizeBankName"
      >
        <el-input v-model="bankTransferWithdrawalFormData.customizeBankName" id="withdrawalBankName" />
      </el-form-item>
      <el-form-item
        v-show="branchNameSetting"
        :label="$t('BranchName')"
        prop="branchName"
      >
        <el-input v-model="bankTransferWithdrawalFormData.branchName" id="withdrawalBranchName" />
      </el-form-item>
      <el-form-item
        v-if="bankAccountNameSetting"
        :label="$t('bank_account_name')" 
        prop="bankAccountName"
      >
        <el-input
          v-model="bankTransferWithdrawalFormData.bankAccountName"
          :disabled="!isFirstTimeDeposit && selectedBank !== 'Other' && !isFirstTimeBankAccountNameEmpty"
          id="withdrawalBankAccountName"
        />
      </el-form-item>
      <el-form-item
        v-if="bankAccountNumberSetting"
        :label="$t('bank_account_number')" 
        prop="bankAccountNumber"
      >
        <el-input
          v-model="bankTransferWithdrawalFormData.bankAccountNumber"
          :disabled="!isFirstTimeDeposit && selectedBank !== 'Other' && !isFirstTimeBankAccountNumberEmpty"
          @input="onInputValue($event, bankTransferWithdrawalFormData, 'bankAccountNumber')"
          id="withdrawalBankAccountNumber"
        />
      </el-form-item>
      <el-form-item
        v-if="cpfNumberSetting"
        :label="$t('CPF Number')"
        prop="cpfNumber"
      >
        <el-input 
          maxlength="11" 
          v-model="bankTransferWithdrawalFormData.cpfNumber" id="withdrawalCPFNumber" 
          @input="onInputValue($event, bankTransferWithdrawalFormData, 'cpfNumber')"
        />
      </el-form-item>
      <el-form-item
        v-if="bsbSetting"
        :label="$t('BSB')"
        prop="bsb"
      >
        <el-input
          maxlength="6"
          v-model="bankTransferWithdrawalFormData.bsb" id="withdrawalBSB" 
          @input="onInputValue($event, bankTransferWithdrawalFormData, 'bsb')"
        />
      </el-form-item>
      <el-form-item
        v-if="payIdSetting"
        :label="$t('PayID')"
        prop="payId"
      >
        <el-input 
          v-model="bankTransferWithdrawalFormData.payId" id="withdrawalPayId"
        />
      </el-form-item>
      <el-form-item :label="$t('balance')" class="--text4">
        {{ playerCreditBalance }}
      </el-form-item>
      <template v-if="isWithdrawalLimitEnabled">
        <el-form-item v-if="isAllowEligibleWithdrawal" :label="$t('eligible_withdrawal')" class="--text4">
          <div class="fc-primary font-bold">{{ eligibleWithdrawal }}</div>
        </el-form-item>
        <el-form-item
          prop="withdrawalLimit"
          :label="$t('remaining_rollover')"
          class="--text4"
        >
          {{ remainingRollover }}
        </el-form-item>
      </template>
      <el-form-item :label="$t('amount')" prop="amountValue">
        <el-input
          v-model="bankTransferWithdrawalFormData.amountValue"
          @input="onInputValue($event, bankTransferWithdrawalFormData, 'amountValue')"
          :placeholder="amountPlaceholder"
          id="withdrawalAmount"
        >
          <template v-if="checkedCurrency" #append>
            <span>000</span>
          </template>
        </el-input>
        <span v-if="checkedCurrency" class="order-1 text-xs fc-hint mt-3">{{
          $t(
            `withdrawal_amount_shown_by_the_system_${profile.Currency.toLowerCase()}`
          )
        }}</span>
      </el-form-item>
      <el-form-item
        v-if="isPaymentPasswordEnable"
        :label="$t('payment_password')"
        prop="paymentPasswordValue"
      >
        <el-input
          v-model="bankTransferWithdrawalFormData.paymentPasswordValue"
          type="password"
          id="withdrawalPaymentPassword"
        />
      </el-form-item>
      <div class="flex justify-center mb-4">
        <el-button
          id="withdrawalSubmitBtn"
          class="!py-2 !px-4"
          type="primary"
          :disabled="isWithdrawProcessing"
          round
          @click="submitWithdrawalBankTransfer(bankTransferWithdrawalFormRef)"
        >
          {{ $t("submit") }}
        </el-button>
      </div>
    </el-form>
  </div>
</template>
<script lang="ts">
import { defineComponent, PropType, toRefs } from "vue";
import { IGetTransactionBankListResponse } from "@/models/getTransactionBankListResponse";
import { IGetPaymentGatewayBanksResponse } from "@/models/getPaymentGatewayBanksRespone";
import { ProfileDto } from "@/models/getProfileResponse";
import depositBankImageHelper from "@/utils/depositBankImageHelper";

export default defineComponent({
  name: "NormalBankTransferWithdrawal",
  props: {
    normalBanks: {
      type: Object as PropType<IGetTransactionBankListResponse>,
      required: true,
    },
    paymentBanks: {
      type: Object as PropType<IGetPaymentGatewayBanksResponse>,
      required: true,
    },
    profile: {
      type: Object as PropType<ProfileDto>,
      required: true,
    },
    isPaymentPasswordEnable: {
      type: Boolean,
      default: false,
    },
    isWithdrawalLimitEnabled: {
      type: Boolean,
      default: false,
    },
  },
  setup(props) {
    const {
      normalBanks,
      paymentBanks,
      profile,
      isPaymentPasswordEnable,
      isWithdrawalLimitEnabled,
    } = toRefs(props);
    const {
      isFirstTimeDeposit,
      playerCreditBalance,
      withdrawalMin,
      withdrawalMax,
      bankRadios,
      selectedBank,
      branchNameSetting,
      isBankNameEnabled,
      submitWithdrawalBankTransfer,
      bankTransferWithdrawalFormRef,
      bankTransferWithdrawalFormData,
      bankTransferWithdrawalRules,
      isWithdrawProcessing,
      checkedCurrency,
      remainingRollover,
      eligibleWithdrawal,
      amountPlaceholder,
      bankTransferWithdrawalTextInfo,
      cpfNumberSetting,
      bsbSetting,
      payIdSetting,
      bankAccountNumberSetting,
      bankAccountNameSetting,
      isFirstTimeBankAccountNameEmpty,
      isFirstTimeBankAccountNumberEmpty
    } = useNormalBankTransferWithdrawal(
      normalBanks,
      paymentBanks,
      profile,
      isPaymentPasswordEnable,
      isWithdrawalLimitEnabled
    );
    const { getAssetImage, onInputValue, allowCompany, isAllowEligibleWithdrawal } = commonHelper;
    const { getImagePathBankLogo } = depositBankImageHelper;
    return {
      isFirstTimeDeposit,
      playerCreditBalance,
      withdrawalMin,
      withdrawalMax,
      bankRadios,
      selectedBank,
      branchNameSetting,
      isBankNameEnabled,
      submitWithdrawalBankTransfer,
      bankTransferWithdrawalFormRef,
      bankTransferWithdrawalFormData,
      bankTransferWithdrawalRules,
      isWithdrawProcessing,
      checkedCurrency,
      remainingRollover,
      eligibleWithdrawal,
      amountPlaceholder,
      bankTransferWithdrawalTextInfo,
      getAssetImage,
      getImagePathBankLogo,
      onInputValue,
      allowCompany,
      cpfNumberSetting,
      bsbSetting,
      payIdSetting,
      isAllowEligibleWithdrawal,
      bankAccountNumberSetting,
      bankAccountNameSetting,
      isFirstTimeBankAccountNameEmpty,
      isFirstTimeBankAccountNumberEmpty
    };
  },
});
</script>
<style scoped>
:deep(.ui-radio-group .check-btn__img img) {
  max-height: 32px;
}
:deep(.ui-radio-group .check-btn__img) {
  max-width: max-content;
}
:deep(.ui-radio-button .check-btn__content) {
  padding-right: 16px;
}
:deep(.ui-radio-button.selected .check-btn__content) {
  padding-right: 40px;
}
.txt-info {
  word-break: break-word;
}
</style>
<style>
[layout="bk8"][data-device="desktop"] {
  .el-input.is-disabled .el-input__inner {
    -webkit-text-fill-color: unset !important;
  }
}
</style>
