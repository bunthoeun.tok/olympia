import { resolve } from "path";

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  nitro: {
    sourceMap: false,
  },
  runtimeConfig: {
    SERVER_END_POINT: process.env.SERVER_END_POINT,
    END_POINT_1: process.env.END_POINT_1,
    END_POINT_2: process.env.END_POINT_2,
    END_POINT_3: process.env.END_POINT_3,
    END_POINT_4: process.env.END_POINT_4,
    END_POINT_5: process.env.END_POINT_5,
    END_POINT_6: process.env.END_POINT_6,
    END_POINT_7: process.env.END_POINT_7,
    END_POINT_8: process.env.END_POINT_8,
    END_POINT_9: process.env.END_POINT_9,
    END_POINT_10: process.env.END_POINT_10,

    SA_END_POINT: process.env.SA_END_POINT,
    SA_END_POINT_1: process.env.SA_END_POINT_1,
    SA_SERVER_END_POINT: process.env.SA_SERVER_END_POINT,
    SA_ASSET_IMAGE_CDN: process.env.SA_ASSET_IMAGE_CDN,
    SA_THEME_PROPERTY_CDN: process.env.SA_THEME_PROPERTY_CDN,
    SA_APP_DOWNLOAD: process.env.SA_APP_DOWNLOAD,
    SA_RESOURCE_VERSION: process.env.SA_RESOURCE_VERSION,
    SA_SPORT_URL: process.env.SA_SPORT_URL,

    public: {
      NODE_ENV: process.env.NODE_ENV,
      CURRENTLY_ENV: process.env.CURRENTLY_ENV,
      END_POINT: process.env.END_POINT,
      ASSET_IMAGE_CDN: process.env.ASSET_IMAGE_CDN,
      CHINA_ASSET_IMAGE_CDN: process.env.CHINA_ASSET_IMAGE_CDN,
      THEME_PROPERTY_CDN: process.env.THEME_PROPERTY_CDN,
      CHINA_THEME_PROPERTY_CDN: process.env.CHINA_THEME_PROPERTY_CDN,
      APP_DOWNLOAD: process.env.APP_DOWNLOAD,
      RESOURCE_VERSION: process.env.RESOURCE_VERSION,
      API_KEY: process.env.API_KEY,
      SPORT_URL: process.env.SPORT_URL,

      SA_END_POINT: process.env.SA_END_POINT,
      SA_END_POINT_1: process.env.SA_END_POINT_1,
      SA_SERVER_END_POINT: process.env.SA_SERVER_END_POINT,
      SA_ASSET_IMAGE_CDN: process.env.SA_ASSET_IMAGE_CDN,
      SA_THEME_PROPERTY_CDN: process.env.SA_THEME_PROPERTY_CDN,
      SA_APP_DOWNLOAD: process.env.SA_APP_DOWNLOAD,
      SA_RESOURCE_VERSION: process.env.SA_RESOURCE_VERSION,
      SA_SPORT_URL: process.env.SA_SPORT_URL,
    },
    SPORT_URL_1: process.env.SPORT_URL_1,
    SPORT_URL_2: process.env.SPORT_URL_2,
    SPORT_URL_3: process.env.SPORT_URL_3,
    SPORT_URL_4: process.env.SPORT_URL_4,
    SPORT_URL_5: process.env.SPORT_URL_5,
    SPORT_URL_6: process.env.SPORT_URL_6,
    SPORT_URL_7: process.env.SPORT_URL_7,
    SPORT_URL_8: process.env.SPORT_URL_8,
  },
  build: {
    transpile: ['unFocus', 'intersectionObserver']
  },
  vite: {
    css: {
      preprocessorOptions: {
        scss: {
          includePaths: ["./assets/styles"],
        },
      },
    },
    build: {
      target: ["esnext", "ios12"],
    }
  },
  modules: [
    // "@element-plus/nuxt",
    "@nuxtjs/tailwindcss",
    "@nuxtjs/i18n",
    "nuxt-swiper",
    "@nuxtjs/device",
    "nuxt-svgo",
  ],
  svgo: {
    global: false,
    // Cannot do auto import due to confliction
    autoImportPath: false,
  },
  css: [
    "@fortawesome/fontawesome-svg-core/styles.css",
    "vue3-marquee/dist/style.css",
  ],
  i18n: {
    strategy: "no_prefix",
    locales: [
      {
        code: "en",
        file: "en.json",
      },
      {
        code: "km_KH",
        file: "km_KH.json",
      },
      {
        code: "cn_MY",
        file: "cn_MY.json",
      },
      {
        code: "id_ID",
        file: "id_ID.json",
      },
      {
        code: "ja_JP",
        file: "ja_JP.json",
      },
      {
        code: "ko_KR",
        file: "ko_KR.json",
      },
      {
        code: "my_MM",
        file: "my_MM.json",
      },
      {
        code: "th_TH",
        file: "th_TH.json",
      },
      {
        code: "vi_VN",
        file: "vi_VN.json",
      },
      {
        code: "zh_CN",
        file: "zh_CN.json",
      },
      {
        code: "zh_TW",
        file: "zh_TW.json",
      },
      {
        code: "bn_BD",
        file: "bn_BD.json",
      },
      {
        code: "es_ES",
        file: "es_ES.json",
      },
      {
        code: "pt_BR",
        file: "pt_BR.json",
      },
    ],
    langDir: "lang",
    lazy: true,
    detectBrowserLanguage: {
      alwaysRedirect: false,
      cookieCrossOrigin: false,
      cookieDomain: null,
      cookieKey: "site_locale",
      cookieSecure: false,
      fallbackLocale: "",
      redirectOn: "root",
      useCookie: true,
    },
    vueI18n: {
      legacy: false,
    },
  },
  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
    },
  },
  tailwindcss: {
    config: {
      content: [
        "./components/**/*.{js,vue,ts}",
        "./layouts/**/*.vue",
        "./pages/**/*.vue",
        "./plugins/**/*.{js,ts}",
        "./nuxt.config.{js,ts}",
        "./app.vue",
      ],
      theme: {
        extend: {},
      },
      plugins: [],
    },
  },
});
