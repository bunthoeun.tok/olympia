import formRulesHelper from '../utils/elementUiHelpers/formRulesHelper'

describe('Validate Login Field', () => {
  test('given value to check password length not less than 6', () => {
    const password = '123'
    expect(formRulesHelper.passwordFormat(password, true)).toEqual('passwordlenght')
  });
  test('given value to check password length is not more than 20', () => {
    const password = '12356dguheugerehguehguuheuhgrgheuhguehguehguegh'
    expect(formRulesHelper.passwordFormat(password, true)).toEqual('passwordlenght')
  });
  test('given value to check password must contain some letter', () => {
    const password = '12234444'
    expect(formRulesHelper.passwordFormat(password, true)).toEqual('passwordlater')
  });
  test('given value to check password must contain some number', () => {
    const password = 'asfujruef'
    expect(formRulesHelper.passwordFormat(password, true)).toEqual('passwordnumber')
  });
  test('given value to check password must be only letter and number not others special character', () => {
    const password = 'fjjf%e4'
    expect(formRulesHelper.passwordFormat(password, true)).toEqual('password_only_letter_number')
  })
});
