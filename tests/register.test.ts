import formRulesHelper from '../utils/elementUiHelpers/formRulesHelper'

describe('Validate Register Username Field', () => {
  test('given value to check username length not less than 6', () => {
    const username = '123'
    expect(formRulesHelper.userNameFormat(username)).toEqual('must_be_in_6-20_characters_use_only_letters_(a-z)_and_numbers_(0-9)_username_is_not_case_sensitive')
  });
  test('given value to check username length not more than 20', () => {
    const username = '12356dguheugerehguehguuheuhgrgheuhguehguehguegh'
    expect(formRulesHelper.userNameFormat(username)).toEqual('must_be_in_6-20_characters_use_only_letters_(a-z)_and_numbers_(0-9)_username_is_not_case_sensitive')
  });
  test('given value to check username must contain some letter', () => {
    const username = '123459'
    expect(formRulesHelper.userNameFormat(username)).toEqual('username_must_be_contain_letter')
  });
  test('given value to check username must not contain with space or symbol', () => {
    const username = 'jh23#@459'
    expect(formRulesHelper.userNameFormat(username)).toEqual('username_must_not_contain_with_space_or_symbol')
  });
});
describe('Validate Register Password Field', () => {
  test('given value to check password length not less than 6', () => {
    const password = '123'
    expect(formRulesHelper.passwordFormat(password)).toEqual('must be_in_6-20_characters_use_both_letters_(a-z)_and_numbers_(0-9)_password_is_case_sensitive')
  });
  test('given value to check password length is not more than 20', () => {
    const password = '12356dguheugerehguehguuheuhgrgheuhguehguehguegh'
    expect(formRulesHelper.passwordFormat(password)).toEqual('must be_in_6-20_characters_use_both_letters_(a-z)_and_numbers_(0-9)_password_is_case_sensitive')
  });
  test('given value to check password must contain some letter', () => {
    const password = '12234444'
    expect(formRulesHelper.passwordFormat(password)).toEqual('password_must_contain_with_letter')
  });
  test('given value to check password must contain some number', () => {
    const password = 'asfujruef'
    expect(formRulesHelper.passwordFormat(password)).toEqual('password_must_contain_with_number')
  });
  test('given value to check password must be only letter and number not others special character', () => {
    const password = 'fjjf%e4'
    expect(formRulesHelper.passwordFormat(password, true)).toEqual('password_only_letter_number')
  })
});
describe('Validate Register Confirm Password', () => {
  test('given value to check confirm password not match', () => {
    const password = '123'
    const confirmPassword = '124'
    expect(formRulesHelper.confirmPasswordFormat(password, confirmPassword)).toEqual('password_does_not_match')
  });
  test('given value to check confirm password match', () => {
    const password = '123'
    const confirmPassword = '123'
    expect(formRulesHelper.confirmPasswordFormat(password, confirmPassword)).toEqual('')
  });
});
describe('Validate Register Email Format', () => {
  test('given value to check email is not valid ', () => {
    const email = '123.com'
    expect(formRulesHelper.emailFormat(email)).toEqual('must_be_valid_email')
  });
  test('given value to check email is valid ', () => {
    const email = '123@yahoo.com'
    expect(formRulesHelper.emailFormat(email)).toEqual('')
  });
});
describe('Validate Phone Number', () => {
  test('given value to check Phone Number is number only ', () => {
    const phone = '123.com'
    expect(formRulesHelper.numbersOnly(phone)).toEqual('should be numbers only')
  });
});
