export interface IWebId {
  WebId: number;
  GTMId: string;
  IsSuccess: boolean;
}
