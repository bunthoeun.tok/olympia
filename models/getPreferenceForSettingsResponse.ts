import { IBaseResponse } from "./baseResponse";
import EnumDisplayTimeOption from "./enums/enumDisplayTimeOption";

export default interface IGetPreferenceForSettingsResponse
  extends IBaseResponse {
  DisplayTime: EnumDisplayTimeOption;
}
