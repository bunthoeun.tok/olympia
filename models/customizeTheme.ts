export interface ILobbySettingsText {
  SelectedModule: string;
  SelectedStyle: string;
  Position: string[];
  Title?: string;
}
