import numberFormatHelper from "@/utils/formatHelpers/numberFormatHelper";
import textHelper from "@/utils/formatHelpers/textFormatHelper";

interface IStatementRequest {
  StartTime: Date | string;
  EndTime: Date | string;
}

interface IStatementListResponse {
  Date: string;
  StatementType: string;
  Total: number;
  Comm: number;
  Running_Total: number;
}

class StatementListResponse implements IStatementListResponse {
  Date: string;
  StatementType: string;
  Total: number;
  Comm: number;
  Running_Total: number;

  get DateForDisplay(): string {
    return textHelper.getDate(this.Date, true);
  }

  get TotalColorForDisplay(): string {
    if (this.Total > 0) {
      return "success";
    }
    if (this.Total < 0) {
      return "failure";
    }
    return "text";
  }

  get TotalCommas(): string {
    return numberFormatHelper.addAccountingFormat(this.Total);
  }

  get CommissionCommas(): string {
    return numberFormatHelper.addAccountingFormat(this.Comm);
  }

  get RunningTotalCommas(): string {
    return numberFormatHelper.addAccountingFormat(this.Running_Total);
  }

  constructor(init: IStatementListResponse) {
    Object.assign(this, init);
  }
}

interface IStatementResponse {
  StatementList: Array<IStatementListResponse>;
  IsSuccess: boolean;
}

class StatementResponse {
  StatementList: Array<IStatementListResponse>;
  IsSuccess: boolean;

  constructor(init: IStatementResponse) {
    Object.assign(this, init);
    this.StatementList = init.StatementList.map(
      (statement) => new StatementListResponse(statement)
    );
  }
}

export {
  IStatementRequest,
  IStatementResponse,
  IStatementListResponse,
  StatementListResponse,
  StatementResponse,
};
