export interface IToggle {
  WebId: number;
  IsUsingPaymentGateway: boolean;
  IsAppEnabled: boolean;
}

export interface IFeaturesToggleRespone {
  IsSuccess: boolean;
  Toggles: IToggle[];
}
