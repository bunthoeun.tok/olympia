export interface IUpdateTelegramSsoTokenRequest {
  SsoKey: string
  OnlineId: number
}

export interface IUpdateTelegramSsoTokenResponse {
  IsConsumed: boolean
}