export enum EPlayerSearchGameDevice {
  Desktop = 1,
  Mobile = 2,
}

export interface IPlayerSearchGameRequest {
  WebId: number;
  Username: string;
  SearchContent: string;
  SearchPage: string;
  DeviceType: EPlayerSearchGameDevice;
  OnlineId: number;
}
