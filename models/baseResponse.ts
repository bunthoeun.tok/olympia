export interface IBaseResponse {
  ErrorCode: number;
  ErrorMessage: string;
  IsSuccess: boolean;
}

export interface INullDataResponse {
  ErrorCode: number;
  Message: string;
}

export interface IBaseResponsePromotion {
  TraceId: number;
  ErrorCode: number;
  ErrorMessage: string;
  IsSuccess: boolean;
}
