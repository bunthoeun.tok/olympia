export interface IRadio {
  name: string;
  radios: Array<string>;
}
