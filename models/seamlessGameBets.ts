import BetStatus from "./enums/enumBetStatus";
import { IBet } from "./bet";
import textHelper from "@/utils/formatHelpers/textFormatHelper";
import numberFormatHelper from "@/utils/formatHelpers/numberFormatHelper";

export interface ISeamlessGameBet extends IBet {
  OrderTime: string;
  Stake: number;
  Turnover: number;
  NetTurnover: number;
  Status: string;
  Currency: string;
  WinLost: number;
  PlayerCommission: number;
  GameType: number | string | null;
  OrderDetail: any;
  IsShowBetDetailButton: boolean;
  MjpCommission: number;
  MjpWinlose: number;
  IsShowMjp: boolean;
  MaxPage: number;
  RowNumber: number;
  Url?: string;
}

export class SeamlessGameBet implements ISeamlessGameBet {
  RefNo: string;
  OrderTime: string;
  Stake: number;
  Turnover: number;
  NetTurnover: number;
  Status: string;
  Currency: string;
  WinLost: number;
  PlayerCommission: number;
  GameType: any;
  OrderDetail: any;
  IsShowBetDetailButton: boolean;
  MjpCommission: number;
  MjpWinlose: number;
  IsShowMjp: boolean;
  GameProviderId: number;
  GameProviderType: number;
  GameProviderName: string;
  MaxPage: number;
  RowNumber: number;
  Url?: string;

  get WinLostForDisplay(): string {
    if (this.Status === "running") {
      return "--";
    }
    return numberFormatHelper.addAccountingFormat(this.WinLost);
  }

  get WinLoseColorForDisplay(): string {
    if (this.Status === "running") {
      return "text";
    }
    if (this.WinLost > 0) {
      return "success";
    }
    if (this.WinLost < 0) {
      return "failure";
    }
    return "text";
  }

  get StatusForDisplay(): string {
    const { $t } = useNuxtApp();
    return $t(this.Status);
  }
  get StatusClassColorForDisplay(): string {
    if (this.Status === BetStatus.Won) {
      return "success";
    }
    if (this.Status === BetStatus.Lose) {
      return "failure";
    }
    return "text";
  }
  get StatusColorForMobileDisplay(): string {
    if (this.Status === BetStatus.Won) {
      return "success";
    }
    if (this.Status === BetStatus.Lose) {
      return "failure";
    }
    return "border";
  }
  get StakeForDisplay(): string {
    return numberFormatHelper.addAccountingFormat(this.Stake);
  }
  get NetTurnoverForDisplay(): string {
    if (this.Status === BetStatus.Running) {
      return "--";
    }
    return numberFormatHelper.addAccountingFormat(this.NetTurnover);
  }
  get PlayerCommissionForDisplay(): string {
    if (this.Status === BetStatus.Running) {
      return "--";
    }
    return numberFormatHelper.addAccountingFormat(this.PlayerCommission);
  }
  constructor(init: ISeamlessGameBet) {
    init.OrderTime = textHelper.dateStringTo12HourClock(init.OrderTime);
    init.Status = String(init.Status).toLowerCase();
    Object.assign(this, init);
  }
}
