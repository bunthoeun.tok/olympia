import EnumDisplayTimeOption from "./enums/enumDisplayTimeOption";

interface ITimeZone {
  IsUsingLocalTime: boolean;
  IsHidden: boolean;
  RegionTimeZone: keyof typeof EnumDisplayTimeOption;
}

export default interface IGetPlayerPreference {
  TimeZone: ITimeZone;
  ErrorCode: number;
  ErrorMessage: string;
  ThemeMode: number;
}
