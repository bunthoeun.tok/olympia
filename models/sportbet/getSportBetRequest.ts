export default interface IGetSportBetRequest {
  IsRunningBet: boolean;
  Language: string;
  GameProviderId: number;
  IsGetFromAllProviders: boolean;
  WinLoseDateStart: string;
  WinLoseDateEnd: string;
  RowCountPerPage: number;
  Page: number;
}
