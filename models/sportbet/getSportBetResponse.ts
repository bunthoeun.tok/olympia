import { ISportBet, SportBet } from "./sportbet";

export interface IGetSportBetResponse {
  SportBets: Array<ISportBet>;
  TotalWinLose: number;
  TotalCommission: number;
  IsSuccess: boolean;
}

export class GetSportBetResponse implements IGetSportBetResponse {
  SportBets: Array<SportBet>;
  TotalWinLose: number;
  TotalCommission: number;
  IsSuccess: boolean;
  constructor(init: IGetSportBetResponse) {
    this.SportBets = init.SportBets.map((sportbet) => new SportBet(sportbet));
    this.TotalCommission = init.TotalCommission;
    this.TotalWinLose = init.TotalWinLose;
    this.IsSuccess = init.IsSuccess;
  }
}
