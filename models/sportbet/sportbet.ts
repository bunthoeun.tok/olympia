import _ from "lodash";
import BetStatus from "../enums/enumBetStatus";
import EnumSportBetDisplayType from "../enums/enumSportBetDisplayType";
import EnumProductType from "../enums/enumProductType";
import { IBet } from "../bet";
import textHelper from "@/utils/formatHelpers/textFormatHelper";
import numberFormatHelper from "@/utils/formatHelpers/numberFormatHelper";

const toOddsDisplayFormat = (odds: number) =>
  odds.toFixed(Math.max(2, (odds.toString().split(".")[1] || []).length));

export interface ISportBetDetail {
  BetOption: string;
  SubBetOdds: number;
  HandicapPoint: number;
  SportType: string;
  MarketType: string;
  SubBetStatus: string;
  BetType: number;
  Match: string;
  League: string | null;
  WinLoseDate: string;
  LiveScore: string;
  HalfTimeScore: string;
  FullTimeScore: string;
  CustomizedBetType: string;
  KickOffTime: string;
  HomeTeam: string;
  AwayTeam: string;
  Favourite: string;
}

export interface ISportBet extends IBet {
  MainBetSportsType: string;
  DisplayType?: number;
  OrderTime: string;
  MainBetOdds: number;
  Stake: number;
  ActualStake: number;
  PlayerCommission: number;
  Winlose: number;
  Status: string;
  IsCashOut: boolean;
  OddsStyle: string;
  IsLiveBet: boolean;
  Currency: string;
  IsShowBetDetailButton: boolean;
  IsResettled: boolean;
  RollbackTime: string;
  ResettleTime: string;
  BetDetails: Array<SportBetDetail>;
  VoidReason: string;
  MaxPage: number;
  RowNumber: number;
  Url?: string | undefined;
  OperationStatus: string;
  GameRoundId: string;
  GamePeriodId: string;
}

export class SportBetDetail implements ISportBetDetail {
  BetOption: string;
  SubBetOdds: number;
  HandicapPoint: number;
  SportType: string;
  MarketType: string;
  SubBetStatus: string;
  BetType: number;
  Match: string;
  League: string | null;
  WinLoseDate: string;
  LiveScore: string;
  HalfTimeScore: string;
  FullTimeScore: string;
  CustomizedBetType: string;
  KickOffTime: string;
  HomeTeam: string;
  AwayTeam: string;
  Favourite: string;

  get MarketTypeForDisplay(): string {
    const { $t, $te } = useNuxtApp();
    if (!this.MarketType) return '';
    if ($te(this.MarketType)) {
      return $t(this.MarketType);
    }
    return this.MarketType;
  }

  get SubBetStatusForDisplay(): string {
    const { $t } = useNuxtApp();
    return $t(this.SubBetStatus.replaceAll(" ", "_"));
  }
  get kickOffTimeForDisplay(): string {
    return textHelper.dateStringTo12HourClock(this.KickOffTime);
  }
  get SubBetStatusColorForDisplay(): string {
    if (
      this.SubBetStatus === BetStatus.Won ||
      this.SubBetStatus === BetStatus.HaflWon
    ) {
      return "success";
    }
    if (
      this.SubBetStatus === BetStatus.Lose ||
      this.SubBetStatus === BetStatus.HaflLose
    ) {
      return "failure";
    }
    return "text";
  }

  get SubBetStatusColorForMobileDisplay(): string {
    if (
      this.SubBetStatus === BetStatus.Won ||
      this.SubBetStatus === BetStatus.HaflWon
    ) {
      return "success";
    }
    if (
      this.SubBetStatus === BetStatus.Lose ||
      this.SubBetStatus === BetStatus.HaflLose
    ) {
      return "failure";
    }
    return "border";
  }
  get HandicapPointForDisplay(): string {
    return numberFormatHelper.addAccountingFormat(this.HandicapPoint);
  }
  get SubBetOddsForDisplay(): string | number {
    return toOddsDisplayFormat(this.SubBetOdds);
  }
  constructor(init: SportBetDetail) {
    const status = _.toLower(init.SubBetStatus);
    Object.assign(this, init);
    this.League = textHelper.addDefaultValue(init.League);
    this.WinLoseDate = textHelper.getDate(init.WinLoseDate);
    this.Match = textHelper.addDefaultValue(init.Match);
    this.MarketType = this.MarketType;
    this.KickOffTime = init.KickOffTime;
    this.SubBetStatus = status === "half lost" ? "half lose" : status;
  }
}

export class SportBet implements ISportBet {
  RefNo: string;
  MainBetSportsType: string;
  DisplayType?: number;
  OrderTime: string;
  MainBetOdds: number;
  Stake: number;
  ActualStake: number;
  PlayerCommission: number;
  Winlose: number;
  Status: string;
  IsCashOut: boolean;
  OddsStyle: string;
  IsLiveBet: boolean;
  Currency: string;
  IsShowBetDetailButton: boolean;
  IsResettled: boolean;
  RollbackTime: string;
  ResettleTime: string;
  BetDetails: Array<SportBetDetail>;
  VoidReason: string;
  GameProviderId: number;
  GameProviderType: number;
  GameProviderName: string;
  MaxPage: number;
  RowNumber: number;
  OperationStatus: string;
  Url?: string | undefined;
  Payout: number;
  GameRoundId: string;
  GamePeriodId: string;

  get StatusForDisplay(): string {
    const { $t } = useNuxtApp();
    return $t(this.Status.replaceAll(" ", "_"));
  }
  get StatusColorForDisplay(): string {
    if (this.Status === BetStatus.Won || this.Status === BetStatus.HaflWon) {
      return "success";
    }
    if (this.Status === BetStatus.Lose || this.Status === BetStatus.HaflLose) {
      return "failure";
    }
    return "border";
  }
  get StatusColorForMobileDisplay(): string {
    if (this.Status === BetStatus.Won || this.Status === BetStatus.HaflWon) {
      return "success";
    }
    if (this.Status === BetStatus.Lose || this.Status === BetStatus.HaflLose) {
      return "failure";
    }
    return "border";
  }
  get MainBetOddsForDisplay(): string {
    return toOddsDisplayFormat(this.MainBetOdds);
  }
  get StakeForDisplay(): string {
    return numberFormatHelper.addAccountingFormat(this.Stake);
  }
  get ActualStakeForDisplay(): string {
    return numberFormatHelper.addAccountingFormat(this.ActualStake);
  }
  get NoWinLose(): boolean {
    if (this.GameProviderType === EnumProductType.ThirdPartySports) {
      return (
        this.Status === "running" ||
        this.DisplayType === EnumSportBetDisplayType.LiveCoin
      );
    }
    return this.Status === "running";
  }
  get WinLoseForDisplay(): string {
    if (this.NoWinLose) {
      return "--";
    }
    return numberFormatHelper.addAccountingFormat(this.Winlose);
  }
  get PayoutForDisplay(): string {
    return numberFormatHelper.addAccountingFormat(this.Payout);
  }
  get WinLoseColorForDisplay(): string {
    if (this.NoWinLose) {
      return "text";
    }
    if (this.Winlose > 0) {
      return "success";
    }
    if (this.Winlose < 0) {
      return "failure";
    }
    return "text";
  }
  get PlayerCommissionForDisplay(): string {
    if (this.NoWinLose) {
      return "--";
    }
    return numberFormatHelper.addAccountingFormat(this.PlayerCommission);
  }

  constructor(init: ISportBet) {
    const status = _.toLower(init.Status);
    Object.assign(this, init);
    this.BetDetails = init.BetDetails ? init.BetDetails.map(
      (betdetail) => new SportBetDetail(betdetail)
    ) : [];
    // this.kickOffTime = textHelper.dateStringTo12HourClock(this.kickOffTime);
    this.RollbackTime = textHelper.dateStringTo24HourClock(init.RollbackTime);
    this.OrderTime = textHelper.dateStringTo12HourClock(init.OrderTime);
    this.OddsStyle = textHelper.addDefaultValue(init.OddsStyle);
    this.Status = status === "half lost" ? "half lose" : status;
  }
}
