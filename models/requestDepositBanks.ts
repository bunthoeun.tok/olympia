export interface IRequestDepositBankTransfer {
  PlayerBankName: string;
  PlayerBankAccountName: string;
  PlayerBankAccountNumber: string;
  BankReferenceCode: string;
  BranchName: string;
  CustomizeBankName: string;
  CompanyBankId: number | string;
  BSB: string;
  PayId: string;
  Amount: number;
  DepositTime: Date | string;
  SlipImage: string;
  Url: string;
  FingerPrint: string;
  OnlineId: number;
}

export interface IRequestAutoDepositBankTransfer {
  PlayerBankName: string;
  PlayerBankAccountName: string;
  PlayerBankAccountNumber: string;
  BankReferenceCode: string;
  BranchName: string;
  CustomizeBankName: string;
  CompanyBankId: number;
  Amount: string;
  DepositTime: string;
  Domain: string;
  IsAutoDeposit: boolean;
  OnlineId: number;
}
export interface IRequestDepositInternetBank {
  PaymentMethod: string;
  BankCode: string;
  PpId: number;
  Amount: number;
  HomeUrl: string;
  ReturnUrl: string;
  FailedUrl: string;
  CryptoCurrencyNetwork: string;
  OnlineId: number;
}
export interface IPaymentInfo {
  Amount: number;
  Description: string;
  QRCodeUrl: string;
  WalletAddress: string;
}

export interface IGetDepositInternetBankReponse {
  AfterAmount: number;
  IsSuccess: boolean;
  PaymentInfo: IPaymentInfo;
  RedirectUrl: string;
  RedirectView: string;
  TransactionFee: number;
}
