import { IBaseResponse } from "./baseResponse";

export interface IGetTransactionFeeResponse extends IBaseResponse {
  TotalFee: number;
}
