export interface ICurrencyWhiteList {
  WebId: number;
  Currency: string;
  CountryCode: string;
}

export interface IGetAccountCurrencyByUsernameRequest {
  Domain: string;
  Username: string;
}

export interface IGetAccountCurrencyByUsernameResponse {
  Currency: string;
  IsSuccess: boolean;
}

export interface IGetCurrencyWhiteListResponse {
  CurrencyWhiteList: Array<ICurrencyWhiteList>;
  IsSuccess: boolean;
}
