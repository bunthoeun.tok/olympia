import { IBaseResponse } from "./baseResponse";
import StatusEnum from "./enums/enumBetStatus";
import ColorEnum from "./enums/enumColor";
import textHelper from "@/utils/formatHelpers/textFormatHelper";

export interface IBetListDetails {
  Provider: string;
  BetTime: string;
  RefNo: string;
  Stake: number;
  NetTurnover: number;
  Winloss: number;
  Commission: number;
  Status: string;
  BetDetailUrl: string;
}

export interface IGameBetDetailResponse extends IBaseResponse {
  BetListDetails: Array<IBetListDetails>;
}
export class BetListDetails implements IBetListDetails {
  Provider: string;
  GameName: string;
  BetTime: string;
  RefNo: string;
  Stake: number;
  NetTurnover: number;
  Winloss: number;
  Commission: number;
  Currency: string;
  Status: string;
  BetDetailUrl: string;
  StatusColor: ColorEnum;
  constructor(init: IBetListDetails) {
    Object.assign(this, init);
    this.StatusColor =
      this.Status === StatusEnum.Lose
        ? ColorEnum.ColorFailure
        : ColorEnum.ColorSuccess;
    this.BetTime = textHelper.dateStringTo12HourClock(this.BetTime);
  }
}
export class GameBetDetailResponseDto implements IGameBetDetailResponse {
  BetListDetails: Array<IBetListDetails>;
  ErrorCode: number;
  ErrorMessage: string;
  IsSuccess: boolean;
  constructor(init: IGameBetDetailResponse) {
    Object.assign(this, init);
    this.BetListDetails = init.BetListDetails.map(
      (data) => new BetListDetails(data)
    );
  }
}
