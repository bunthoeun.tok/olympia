import EnumDisplayTimeOption from "@/models/enums/enumDisplayTimeOption";

export interface IUpdatePlayerPreferenceRequest {
  DisplayTime: EnumDisplayTimeOption;
  ThemeMode: number;
}
