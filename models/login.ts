import _ from "lodash";
import { IBaseResponse } from "./baseResponse";
import { IGameBsi, ISeamlessProviderList } from "@/models/bsiGameList";
import { ITabInfo } from "@/models/IGetPlayableGamesResponse";

export class IRequestLogin {
  Username: string;
  Password: string;
  OnlineId: number;
  WebId: number;
  Ip: string;
  Country: string;
  Url: string;
  Region: string;
  City: string;
  Platform: string;
  Browser: string;
  Lang: string;
  Captcha: string;
  Fingerprint:string;
}

interface IGameProvidersDisplayOrder {
  ApiGameProviderId: number;
  ApiGameProviderName: string;
  GameProviderId: number;
  GameProviderName: string;
  DisplayOrder: number;
}

export interface IGetLoginResponse extends IBaseResponse {
  Username: string;
  LoginName: string;
  CustomerId: number;
  ParentId: number;
  Currency: string;
  Token: string;
  BetCredit: number;
  IsPasswordExpired: boolean;
  IsCashPlayer: boolean;
  IsStockPlayer: boolean;
  OnlineId: number;
  IsLoginNameUpdated: boolean;
  AccountType: number;
  Games: Array<IGameBsi>;
  SeamlessGameGameTypeList: Array<string>;
  SeamlessGameTabList: Array<string>;
  SeamlessProviderList: Array<ISeamlessProviderList>;
  TabInfos: Array<ITabInfo>;
  GameProvidersDisplayOrders: Array<IGameProvidersDisplayOrder>;
  Notifications: Array<string>;
  IsPaymentPasswordEnable: boolean;
}

export class GetLoginResponse implements IGetLoginResponse {
  Username: string;
  LoginName: string;
  CustomerId: number;
  ParentId: number;
  Currency: string;
  Token: string;
  BetCredit: number;
  IsPasswordExpired: boolean;
  IsCashPlayer: boolean;
  IsStockPlayer: boolean;
  OnlineId: number;
  IsLoginNameUpdated: boolean;
  AccountType: number;
  Games: Array<IGameBsi>;
  SeamlessGameGameTypeList: Array<string>;
  SeamlessGameTabList: Array<string>;
  SeamlessProviderList: Array<ISeamlessProviderList>;
  TabInfos: Array<ITabInfo>;
  GameProvidersDisplayOrders: Array<IGameProvidersDisplayOrder>;
  Notifications: Array<string>;
  IsPaymentPasswordEnable: boolean;
  ErrorCode: number;
  ErrorMessage: string;
  IsSuccess: boolean;

  get GamesPriority(): IGameBsi[] {
    return _.sortBy(this.Games, [
      (game) => (game.AsiPriority ? game.AsiPriority : undefined),
      "GameName",
      "DisplayOrder",
    ]);
  }

  constructor(init: GetLoginResponse) {
    Object.assign(this, init);
  }
}

export interface IWhiteListedIps {
  Username: string;
  CustomerId: number;
  WebId: number;
  Ip: string;
}
export interface IGetWhiteListedIps {
  Ips: Array<IWhiteListedIps>;
  IsSuccess: boolean;
}
export interface ICurrencies {
  DepositMin: number;
  DepositMax: number;
  WithdrawalMin: number;
  WithdrawalMax: number;
  IsEnabled: boolean;
  IsDecimalAllow: boolean;
  WebId: number;
  Currency: string;
  ExchangeRate: number;
}
export interface IGetCurrencies {
  Currencies: Array<ICurrencies>;
  IsSuccess: boolean;
}
export interface IRequestPlayerIsOnline {
  OnlineId: number;
}
