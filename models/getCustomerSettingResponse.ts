export interface ICustomerSettingResponse {
  Value: string;
  IsSuccess: boolean;
}
export interface IAllCustomerSettingsData {
  WebId: number;
  Description: string;
  PossibleValues: string;
  Responsible: string;
  ModifiedBy: string;
  ModifiedOn: string;
  IsEnabled: true;
  Id: string;
  Value: string;
}
export interface IAllCustomerSettingResponse {
  Results: Array<IAllCustomerSettingsData>;
  ErrorCode: number;
  Message: string;
}
