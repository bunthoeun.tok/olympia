import { IGame } from "@/models/IGetPlayableGamesResponse";
import commonHelper from "@/utils/commonHelper";
import { IThemePropertiesFromApi } from "~~/models/companyThemePropertiesRespone";
import { store } from "~~/store";

const { getAssetImage } = commonHelper;
const convertString = (param: string): string => {
  try {
    let result = param.trim().replaceAll(" ", "_").toLowerCase();
    result = result.replaceAll("(", "_").replaceAll(")", "");
    return result;
  } catch (error) {
    return "";
  }
};
const themeProperties = computed(
  () => store.state.companyThemePropertiesFromApi.ThemeProperties
);
const productListFeature = computed(
  (): IThemePropertiesFromApi =>
    themePropertyHelper.getSingleThemeProperty(
      themeProperties.value,
      "product_list_feature"
    )
);
export default class GameForDisplay implements IGame {
  GameProviderName: string;
  GameProviderId: number;
  GameId: number;
  Category: number;
  CategoryName: string;
  GameType: string;
  GameName: string;
  EnglishGameName: string;
  IsEnabled: boolean;
  IsMaintainByGameProvider: boolean;
  IsMaintain: boolean;
  ForMobile: boolean;
  ForDesktop: boolean;
  IsPromotionGame: boolean;
  BsiPriority: number;
  AsiPriority: number;
  GameIconUrl: string;
  EnabledCurrency: string;
  GameCode: string | null;
  Platform?: number | null;
  APIGameProviderName: string;
  APIGameProviderId: number;
  DisplayOrder: number;
  IsEnabledByGameProvider: boolean;
  IsHotGame: boolean;
  IsNewGame: boolean;
  LinkOption?: string;
  TabType?: string;
  IsHotGameProvider: boolean;
  IsNewGameProvider: boolean;

  get IsNeedFloatBackground(): boolean {
    return [2, 3].includes(this.Category);
  }

  get IsNeedLightBackground(): boolean {
    try {
      return ["sports", "virtual sports", "lottery", "games"].includes(
        (this.TabType ?? "").toLowerCase()
      );
    } catch (e) {
      return false;
    }
  }

  get FloatBackgroundPath(): string {
    try {
      const name = convertString(this.EnglishGameName);
      const category = convertString(this.TabType ?? "");
      const providerName = convertString(this.GameProviderName);
      if ((this.TabType ?? "").toLowerCase() === "games") {
        return getAssetImage(
          `assets/theme/common/provider_icon/${category}/${providerName}/float_bg.png`
        );
      }
      return getAssetImage(
        `assets/theme/common/provider_icon/${category}/${name}/float_bg.png`
      );
    } catch (e) {
      return "";
    }
  }

  get BackgroundLogo(): string {
    const bgSubfix =
      productListFeature.value.Text === "" || productListFeature.value.Text?.toLowerCase().includes('_draf_1')
        ? ""
        : `_${productListFeature.value.Text}`;
    try {
      const category = convertString(this.TabType ?? "");
      if (
        ["live casino", "cock fight"].includes(
          (this.TabType ?? "").toLowerCase()
        )
      ) {
        const name = convertString(this.EnglishGameName);
        return getAssetImage(
          `assets/theme/common/provider_icon/${category}/${name}/background${bgSubfix}.png`
        );
      }
      return getAssetImage(
        `assets/theme/common/provider_icon/${category}/background${bgSubfix}.png`
      );
    } catch (e) {
      return "";
    }
  }

  get MainLogoPath(): string {
    try {
      const name = convertString(this.EnglishGameName);
      const category = convertString(this.TabType ?? "");
      const providerName = convertString(this.GameProviderName);
      if ((this.TabType ?? "").toLowerCase() === "games") {
        if (providerName.includes("sbo_")) {
          return getAssetImage(
            `assets/theme/common/provider_icon/${category}/_sbo_slots/star.png`
          );
        }
        return getAssetImage(
          `assets/theme/common/provider_icon/${category}/${providerName}/star.png`
        );
      }
      return getAssetImage(
        `assets/theme/common/provider_icon/${category}/${name}/star.png`
      );
    } catch (e) {
      return "";
    }
  }

  get LabelLogoPath(): string {
    // TODO: remap the assets again using providers, maybe game name for virtual sports only
    const logoSubfix =
      productListFeature.value.Text === "" || productListFeature.value.Text?.toLowerCase().includes('_draf_1')
        ? ""
        : `_${productListFeature.value.Text}`;
        
    try {
      const name = convertString(this.EnglishGameName);
      const category = convertString(this.TabType ?? "");
      const providerName = convertString(this.GameProviderName);
      if ((this.TabType ?? "").toLowerCase() === "games") {
        if (providerName.includes("sbo_")) {
          return getAssetImage(
            `assets/theme/common/provider_icon/${category}/_sbo_slots/label${logoSubfix}.png`
          );
        }
        return getAssetImage(
          `assets/theme/common/provider_icon/${category}/${providerName}/label${logoSubfix}.png`
        );
      }
      if(category === "live_casino"){
        if(providerName === "dream_gaming") {
          return getAssetImage(
            `assets/theme/common/provider_icon/${category}/${providerName}/label${logoSubfix}.png`
          );
        }
      }
      return getAssetImage(
        `assets/theme/common/provider_icon/${category}/${name}/label${logoSubfix}.png`
      );
    } catch (e) {
      return "";
    }
  }

  constructor(init: IGame) {
    try {
      Object.assign(this, init);
    } catch (e) {
      // error here
    }
  }
}
