import { format } from "date-fns";

interface IHandiCap {
  HomeOdds: number;
  AwayOdds: number;
  Point: number;
}

interface IOverUnder {
  HomeOdds: number;
  AwayOdds: number;
  Point: number;
}

interface ILoginByMatchIdRequest {
  MatchId: number;
  IsFromMobile: boolean;
  FromUrl: string;
  Lang: string | undefined;
  OnlineId: number;
}

interface ILoginByMatchIdResponse {
  Url: string;
}

interface IRecommendMatch {
  LeagueName: string;
  MatchId: number;
  HomeName: string;
  AwayName: string;
  KickOffTime: string;
  GameTime: string;
  HomeScore: number;
  AwayScore: number;
  HomeTeamIconUrl: string;
  AwayTeamIconUrl: string;
  Handicap: IHandiCap;
  OverUnder: IOverUnder;
}

class RecommendMatch implements IRecommendMatch {
  LeagueName!: string;
  MatchId!: number;
  HomeName!: string;
  AwayName!: string;
  KickOffTime!: string;
  GameTime!: string;
  HomeScore!: number;
  AwayScore!: number;
  HomeTeamIconUrl!: string;
  AwayTeamIconUrl!: string;
  Handicap!: IHandiCap;
  OverUnder!: IOverUnder;

  get KickOffTimeForDisplay(): string {
    try {
      return format(new Date(this.KickOffTime), "MM/dd HH:mm");
    } catch (e) {
      return "";
    }
  }

  get HandicapHomeOddsForDisplay(): string {
    return Number(this.Handicap.HomeOdds).toFixed(2);
  }

  get HandicapAwayOddsForDisplay(): string {
    return Number(this.Handicap.AwayOdds).toFixed(2);
  }

  get HandicapPointForDisplay(): string {
    return Number(this.Handicap.Point).toFixed(2);
  }

  constructor(init: IRecommendMatch) {
    Object.assign(this, init);
  }
}

interface IRecommendMatchResponse {
  RecommendMatches: Array<IRecommendMatch>;
}

export {
  IHandiCap,
  IOverUnder,
  IRecommendMatch,
  IRecommendMatchResponse,
  RecommendMatch,
  ILoginByMatchIdRequest,
  ILoginByMatchIdResponse,
};
