import textHelper from "@/utils/formatHelpers/textFormatHelper";

interface IRequestBetList {
  StartDate: Date;
  EndDate: Date;
}
interface IResponseBetDetail {
  BetOption: string;
  SubBetOdds: number;
  HandicapPoint: number;
  SportType: string;
  MarketType: string;
  SubBetStatus: string;
  BetType: number;
  Match: string;
  League: string;
  WinLoseDate: Date;
  LiveScore: string;
  HalfTimeScore: string;
  FullTimeScore: string;
  CustomizedBetType: string;
  KickOffTime: Date;
  HomeTeam: string;
  AwayTeam: string;
  Favourite: string;
}

interface IResponseSportBets {
  RefNo: number;
  Token: string;
  IsRedeem: boolean;
  RedeemTime: Date;
  PrintCount: 0;
  MainBetSportsType: string;
  OrderTime: Date;
  MainBetOdds: number;
  Stake: number;
  ActualStake: number;
  PlayerCommission: number;
  Winlose: number;
  Status: string;
  OddsStyle: string;
  IsLiveBet: boolean;
  Currency: string;
  IsShowBetDetailButton: boolean;
  IsResettled: boolean;
  RollbackTime: Date;
  ResettleTime: Date;
  BetDetails: Array<IResponseBetDetail>;
  VoidReason: string | null;
  GameProviderId: number;
  MaxPage: number;
  RowNumber: number;
}

class ResponseSportBets implements IResponseSportBets {
  RefNo: number;
  Token: string;
  IsRedeem: boolean;
  RedeemTime: Date;
  PrintCount: 0;
  MainBetSportsType: string;
  OrderTime: Date;
  MainBetOdds: number;
  Stake: number;
  ActualStake: number;
  PlayerCommission: number;
  Winlose: number;
  Status: string;
  OddsStyle: string;
  IsLiveBet: boolean;
  Currency: string;
  IsShowBetDetailButton: boolean;
  IsResettled: boolean;
  RollbackTime: Date;
  ResettleTime: Date;
  BetDetails: Array<IResponseBetDetail>;
  VoidReason: string | null;
  GameProviderId: number;
  MaxPage: number;
  RowNumber: number;

  get OrderTimeForDisplay(): string {
    let result = "";
    result = textHelper.dateStringTo12HourClock(
      String(new Date(this.OrderTime)),
      true
    );

    return result;
  }
  constructor(init: IResponseSportBets) {
    Object.assign(this, init);
  }
}
interface IResponseBetList {
  SportBets: Array<IResponseSportBets>;
  TotalWinLose: number;
  TotalCommission: number;
  IsSuccess: boolean;
}

export {
  IRequestBetList,
  IResponseBetDetail,
  IResponseSportBets,
  IResponseBetList,
  ResponseSportBets,
};
