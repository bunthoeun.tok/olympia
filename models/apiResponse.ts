import EnumApiErrorCode from "./enums/enumApiErrorCode";

export interface IApiResponse<T = any> {
  Data: T;
  ErrorCode: EnumApiErrorCode;
  Message: string;
}

export default class ApiResponse<T = any> implements IApiResponse {
  Data: T;
  ErrorCode: EnumApiErrorCode;
  Message: string;

  get ErrorMessageForDisplay(): string {
    const { $t } = useNuxtApp();
    if (
      EnumApiErrorCode[this.ErrorCode] &&
      $t(EnumApiErrorCode[this.ErrorCode])
    ) {
      return $t(EnumApiErrorCode[this.ErrorCode]);
    }
    return this.Message;
  }

  get isSuccessful(): boolean {
    return this.ErrorCode === EnumApiErrorCode.success;
  }

  constructor(init: IApiResponse) {
    Object.assign(this, init);
  }
}
