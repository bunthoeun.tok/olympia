import { IAvailablePropertyListDto } from "./getTransactionBankListResponse";
import textHelper from "@/utils/formatHelpers/textFormatHelper";

export interface ICompanyFlowSettingRequest {
  Language: string;
  Type: string;
  Domain: string;
  IsSettingForApp: boolean;
}

export interface ICompanyFlowSettingResponse {
  AvailablePropertyList: Array<IAvailablePropertyListDto>;
  IsSuccess: boolean;
}

export class CompanyFlowSetting implements IAvailablePropertyListDto {
  DisplayOrder!: number;
  PropertyName!: string;
  PropertyStatus!: string;
  get PropertyNameForDisplay(): string {
    const { $t } = useNuxtApp();
    return $t(
      textHelper.removeAllSpace(
        textHelper.convertCamelCaseToSnakeCase(this.PropertyName)
      )
    );
  }
  constructor(params: IAvailablePropertyListDto) {
    Object.assign(this, params);
  }
}

export class CompanyFlowSettingResponse implements ICompanyFlowSettingResponse {
  AvailablePropertyList: Array<IAvailablePropertyListDto>;
  IsSuccess: boolean;
  constructor(params: CompanyFlowSettingResponse) {
    Object.assign(this, params);
    this.AvailablePropertyList = params?.AvailablePropertyList.map(
      (property) => new CompanyFlowSetting(property)
    );
    this.AvailablePropertyList?.sort((a, b) => a.DisplayOrder - b.DisplayOrder);
    this.IsSuccess = params.IsSuccess;
  }
}
