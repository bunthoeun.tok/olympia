import { IApiResponse } from "./apiResponse";

// support apiFake
export interface IAxiosResponse<T = any> {
  data: IApiResponse<T>;
}

export type IAxiosPromise<T = any> = Promise<IAxiosResponse<T>>;

export type IFetchPromise<T = any> = Promise<IApiResponse<T>>;
