enum EnumOperationStatus {
    unpaid = "unpaid",
    paid = "paid",
    redeemed = "redeemed",
    refund = "refunded",
}

export default EnumOperationStatus;