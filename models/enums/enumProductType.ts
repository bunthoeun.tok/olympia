enum EnumProductType {
  Sports = 1,
  LiveCasino = 2,
  Games = 3,
  VirtualSports = 4,
  SeamlessGame = 5,
  ThirdPartySports = 6,
  LiveCoin = 7,
}

export default EnumProductType;
