enum EnumTransactionStatusForDisplay {
  Waiting = "waiting",
  Verified = "verified",
  Approved = "approved",
  Rejected = "rejected",
  Canceled = "cancelled",
  Processing = "processing",
  Succeed = "success",
  Error = "error",
  Completed = "completed",
  Pending = "pending",
}

export default EnumTransactionStatusForDisplay;
