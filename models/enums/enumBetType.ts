enum hasHandicapPoint {
  Handicap = 1,
  OverUnder = 3,
  FH_Handicap = 7,
  FH_OverUnder = 9,
  OverUnder1X2 = 900000,
  OverUnderDoubleChance = 900001,
  HomeOrOver = 900010,
  XOrOver = 900011,
  AwayOrOver = 900012,
  HomeOrUnder = 900013,
  XOrUnder = 900014,
  AwayOrUnder = 900015,
  HomeOrBothTeamScore = 900016,
  AwayOrBothTeamScore = 900018,
  Handicap1X2 = 900020,
}

export default hasHandicapPoint;
