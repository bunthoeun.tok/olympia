enum EnumDisplayTimeOption {
  HongKongTime = 0,
  MyComputerTime = 1,
  PacificEfateTime = 2,
}

enum EnumDisplayTimeZone {
  HongKongTime = "HongKongTime",
  MyComputerTime = "MyComputerTime",
  PacificEfateTime = "PacificEfateTime",
}

export { EnumDisplayTimeZone };
export default EnumDisplayTimeOption;
