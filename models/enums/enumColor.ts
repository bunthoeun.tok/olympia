enum EnumColor {
  ColorText = "text",
  ColorFailure = "failure",
  ColorSuccess = "success",
}
export default EnumColor;
