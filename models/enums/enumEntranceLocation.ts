enum EnumEntranceLocation {
  Other = "Other",
  Tab = "Tab",
  Hover = "Hover",
  GameLobby = "GameLobby",
}

export default EnumEntranceLocation;
