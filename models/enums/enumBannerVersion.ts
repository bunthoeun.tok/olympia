enum EnumBannerVersion {
  Tall = "tall",
  Compact = "compact",
}

export default EnumBannerVersion;
