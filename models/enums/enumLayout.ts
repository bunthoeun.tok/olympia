export enum EnumTemplate {
    main = "main",
    sbo = "sbo",
    modern = "modern",
    peru = "peru",
    cool88 = "cool88",
    black = "black",
    china = "china",
    common = "common",
    classic = "classic",
    global = "global",
    playful = "playful",
    footer_1 = "footer_1",
    footer_2 = "footer_2",
}
export enum EnumLayout {
    desktop = "desktop",
    mobile = "mobile"
}