enum BetStatus {
  Void = "void",
  Success = "success",
  Won = "won",
  Lose = "lose",
  Canceled = "canceled",
  Running = "running",
  HaflWon = "half won",
  HaflLose = "half lose",
}
export default BetStatus;
