enum EnumPromotionStatus {
  Completed = "Completed",
  Rejected = "Rejected",
  Approved = "Approved",
  Waiting = "Waiting",
  Other = "Other",
}

export default EnumPromotionStatus;
