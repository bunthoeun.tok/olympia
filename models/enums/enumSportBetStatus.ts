enum EnumSportBetStatus {
    waiting = "waiting", 
    void = "void",
    running = "running",
    draw = "draw",
    won = "won",
    halfwon = "half won",
    lost = "lose",
    halflose = "half lose",
}

export default EnumSportBetStatus;