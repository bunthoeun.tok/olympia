export enum EnumMessage {
  Unread = 0,
  Announcement = 1,
  Notification = 2,
  Promotion = 3,
  PersonalMessage = 4,
}

export enum EnumMessageStatus {
  Read = 1,
  Delete = 2,
}

export enum EnumUnreadInfoMapper {
  Unread = "TotalCount",
  Announcement = "AnnouncementCount",
  Notification = "NotificationCount",
  Promotion = "PromotionCount",
  PersonalMessage = "PersonalMessageCount",
}
