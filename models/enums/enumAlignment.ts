enum Alignment {
  Left = "left",
  Center = "center",
  Right = "right",
}
export default Alignment;
