enum EnumCountryCodeLocale {
  EN = "en",
  CN = "zh_CN",
  TW = "zh_TW",
  MY = "cn_MY",
  ID = "id_ID",
  JP = "ja_JP",
  KH = "km_KH",
  KR = "ko_KR",
  TH = "th_TH",
  VN = "vi_VN",
  MM = "my_MM",
  BD = "bn_BD",
  ES = "es_ES",
  BR = "pt_BR"
}
export enum EnumIsoToShortCountryCode {
  bn_BD = "BGD",
  cn_MY = "MYS",
  en = "ENG",
  es_ES = "ESP",
  id_ID = "IDN",
  ja_JP = "JPN",
  km_KH = "KHM",
  ko_KR = "KOR",
  my_MM = "MMR",
  pt_BR = "BRA",
  th_TH = "THA",
  vi_VN = "VNM",
  zh_CN = "CHN",
  zh_TW = "TWN"
};
export default EnumCountryCodeLocale;
