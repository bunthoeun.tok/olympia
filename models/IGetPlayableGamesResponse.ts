import _ from "lodash";
import textHelper from "@/utils/formatHelpers/textFormatHelper";

interface IGame {
  GameProviderShortName?: string;
  GameProviderName: string;
  GameProviderId: number | null;
  GameId: number;
  Category: number;
  CategoryName: string;
  GameType: string;
  GameName: string;
  EnglishGameName?: string;
  IsEnabled: boolean;
  IsMaintainByGameProvider: boolean;
  IsMaintain: boolean;
  ForMobile: boolean;
  ForDesktop: boolean;
  IsPromotionGame: boolean;
  BsiPriority: number;
  AsiPriority: number;
  GameIconUrl: string;
  EnabledCurrency: string;
  GameCode: string | null;
  Platform?: number | null;
  APIGameProviderName: string;
  APIGameProviderId: number;
  DisplayOrder: number;
  IsEnabledByGameProvider: boolean;
  IsHotGame: boolean;
  IsNewGame: boolean;
  LinkOption?: string;
  TabType?: string;
  IsHotGameProvider?: boolean;
  IsNewGameProvider?: boolean;
  IsSupportIframe?: boolean;
  GameProviderDisplayName?: string;
}
interface ITabInfo {
  TabType: string;
  Order: number;
  IsFilter: boolean;
}

interface IGetPlayableGamesResponse {
  Games: Array<IGame>;
  TabInfos: Array<ITabInfo>;
  IsSuccess: boolean;
}

interface IGetGameProviderListRequest {
  WebId: number;
  Currency: string;
  Language: string;
  IsBsi: boolean;
}

interface IGetGameProviderListResponse {
  Games: Array<IGame>;
  TabInfos: Array<ITabInfo>;
  IsSuccess: boolean;
}
interface IGetTopGameListResponse {
  Games: IGame[];
  ErrorCode: number;
  ErrorMessage: string;
  IsSuccess: string;
}

export class GameProviderList implements IGetGameProviderListResponse {
  Games: IGame[];
  IsSuccess: boolean;
  TabInfos: ITabInfo[];

  get GameProviderByCategoryName(): IGame[] {
    const games = this.Games ?? [];
    const filtered = games.filter(
      (item) => _.toLower(item.CategoryName) === "games"
    );
    return _.sortBy(
      filtered.map((game) => {
        if (game.GameProviderId === 46) {
          game.CategoryName = "COCK_FIGHT";
        }
        return game;
      }),
      ["DisplayOrder"]
    ).slice(0, 6);
  }
  get GameProviderForDisplay(): IGame[] {
    const games = this.Games ?? [];
    return _.sortBy(
      games.map((game) => {
        if (game.GameProviderId === 46) {
          game.CategoryName = "COCK_FIGHT";
        }
        return game;
      }),
      ["DisplayOrder"]
    ).slice(0, 6);
  }

  constructor(params: IGetGameProviderListResponse) {
    Object.assign(this, params);
    this.Games = textHelper.sortedGames(params.Games);
  }
}

export class TabInfo implements ITabInfo {
  TabType: string;
  Order: number;
  IsFilter: boolean;
  TabLanguage?: string;
  TabURL?: string;
  TabIconURL?: string;

  constructor(init: TabInfo) {
    Object.assign(this, init);
  }
}

export {
  IGame,
  ITabInfo,
  IGetPlayableGamesResponse,
  IGetGameProviderListRequest,
  IGetGameProviderListResponse,
  IGetTopGameListResponse,
};
