interface IPagination {
  CurrentPage?: number;
  MaxPage: number;
  RowNumber: number;
}

export class Pagination implements IPagination {
  CurrentPage = 1;
  MaxPage!: number;
  RowNumber!: number;

  constructor(data: IPagination) {
    Object.assign(this, {
      CurrentPage: 1,
      ...data,
    });
  }

  get MaxRows(): Number {
    return this.MaxPage * this.RowNumber;
  }
}
