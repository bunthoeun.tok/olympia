import _ from "lodash";
import numberFormatHelp from "@/utils/formatHelpers/numberFormatHelper";
import { store } from "@/store";

const peruShop = store.state.isPerushopEnabled ? store.state.isPerushopEnabled : false;
interface IWallet {
  WalletName: string;
  BetCredit: number;
  Category: number;
  Outstanding: number;
  MinTurnoverToWithdrawal: number;
  CurrentTurnover: number;
  PromotionGameProviderIds: string;
  PromotionGameProviders: Array<string>;
}

class Wallet implements IWallet {
  WalletName: string;
  BetCredit: number;
  Category: number;
  Outstanding: number;
  MinTurnoverToWithdrawal: number;
  CurrentTurnover: number;
  PromotionGameProviderIds: string;
  PromotionGameProviders: Array<string>;

  getWalletNameForDisplay(): string {
    const { $t } = useNuxtApp();
    return $t(_.toLower(this.WalletName.replace(/ /g, "_").replace(/\./g, "")));
  }

  getBetCreditForDisplay(): string {
    return numberFormatHelp.addAccountingFormat(this.BetCredit, 2, false);
  }

  getTotalBalanceForDisplay(): string {
    return numberFormatHelp.addAccountingFormat(this.BetCredit + this.Outstanding, 2, false);
  }

  getOutstandingForDisplay(): string {
    return numberFormatHelp.addAccountingFormat(this.Outstanding, 2, false);
  }

  getTurnoverProgressForDisplay(): number {
    if (this.CurrentTurnover === 0 ) return 0;
    return (this.CurrentTurnover * 100) / this.MinTurnoverToWithdrawal;
  }

  getProviderNameForDisplay(): string {
    return this.PromotionGameProviders.join(", ");
  }

  constructor(init: IWallet) {
    Object.assign(this, init);
  }
}

interface IGetMultiWalletBalaceResponse {
  Wallets: Array<IWallet>;
  IsSuccess: boolean;
}

class GetMultiWalletBalaceResponse implements IGetMultiWalletBalaceResponse {
  Wallets: Array<IWallet>;
  IsSuccess: boolean;

  get mainBalanceForDisplay(): string {
    let mainBalance = 0;
    if (peruShop === true) {
      this.Wallets.forEach((item) => {
        if (item.Category === 0) {
          mainBalance = item.BetCredit;
        }
      });
    } else {
      this.Wallets.forEach((item) => {
        mainBalance += item.BetCredit;
      });
    }
    return numberFormatHelp.addAccountingFormat(mainBalance, 2, false);
  }

  constructor(init: IGetMultiWalletBalaceResponse) {
    Object.assign(this, init);
  }
}

export { IWallet, Wallet, IGetMultiWalletBalaceResponse, GetMultiWalletBalaceResponse };
