interface ILanguage {
  ISO: string;
  LanguageLocalName: string;
  Status: boolean;
  ModifiedBy: string;
  ModifiedOn: string;
  ShortCountryCode: string[];
}

interface IGetLanguageSettingResponse {
  Language: Array<ILanguage>;
  IsSuccess: boolean;
}

export { ILanguage, IGetLanguageSettingResponse };
