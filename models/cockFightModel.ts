import _ from "lodash";
import { IGameBsi } from "@/models/bsiGameList";
import textHelper from "@/utils/formatHelpers/textFormatHelper";
import commonHelper from "@/utils/commonHelper";
import { store } from "~~/store";
import EnumProductType from "~~/models/enums/enumProductType";

const { convertString } = textHelper;
const { getAssetImage } = commonHelper;
class CockFightDisplayModel implements IGameBsi {
  GameProviderShortName: string;
  IsHotGame: boolean;
  Rank: number;
  IsNewGame: boolean;
  IsJackpot: boolean;
  Device: number | null;
  TabType: string;
  GameProviderName: string;
  GameProviderId: number;
  GameId: number;
  Category: number;
  CategoryName: string;
  GameType: string;
  GameName: string;
  EnglishGameName?: string;
  IsEnabled: boolean;
  IsMaintainByGameProvider: boolean;
  IsMaintain: boolean;
  ForMobile: boolean;
  ForDesktop: boolean;
  IsPromotionGame: boolean;
  BsiPriority: number;
  AsiPriority: number;
  GameIconUrl: string;
  EnabledCurrency: string;
  GameCode: string | null;
  Platform?: number | null;
  APIGameProviderName: string;
  APIGameProviderId: number;
  DisplayOrder: number;
  IsEnabledByGameProvider: boolean;
  LinkOption?: string;
  Named?: string;
  get StarImagePath(): string {
    const gameProviderName = _.replace(
      _.toLower(this.GameProviderName),
      /(\s)+/g,
      "_"
    );
    const path = `${convertString(this.TabType)}/${gameProviderName}/star.png`;
    return getAssetImage(`assets/theme/common/entry/${path}`);
  }

  get backgroundMobilePath(): string {
    const path = `${convertString(this.TabType)}/entry_image.png`;
    return getAssetImage(`assets/theme/common/entry/${path}`);
  }

  
  constructor(init: IGameBsi) {
    Object.assign(this, init);
  }
}


export default CockFightDisplayModel;
