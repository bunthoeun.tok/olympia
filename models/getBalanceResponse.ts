import numberFormatHelper from "@/utils/formatHelpers/numberFormatHelper";

export interface IRequestGetBalance {
  WebId: number;
  Username: string;
}
export interface IGetBalanceResponse {
  CustomerId: number;
  WebId: number;
  Username: string;
  BetCredit: number;
  OutStanding: number;
  ErrorCode: number;
  Message: string;
}

export class GetBalanceResponse implements IGetBalanceResponse {
  CustomerId: number;
  WebId: number;
  Username: string;
  BetCredit: number;
  OutStanding: number;
  public rawBetCredit: number;
  public rawOutstanding: number;
  ErrorCode: number;
  Message: string;

  get Balance(): string {
    return numberFormatHelper.addAccountingFormat(
      this.rawBetCredit + this.rawOutstanding,
      2,
      false
    );
  }

  get BetCreditWithCommas(): string {
    return numberFormatHelper.addAccountingFormat(this.rawBetCredit, 2, false);
  }

  get OutStandingWithCommas(): string {
    return numberFormatHelper.addAccountingFormat(
      this.rawOutstanding,
      2,
      false
    );
  }

  constructor(init: GetBalanceResponse) {
    Object.assign(this, init);

    this.rawBetCredit = this.BetCredit;
    this.BetCredit = numberFormatHelper.floorTo2Digit(this.BetCredit);

    this.rawOutstanding = this.OutStanding;
    this.OutStanding = numberFormatHelper.floorTo2Digit(this.OutStanding);
  }
}
