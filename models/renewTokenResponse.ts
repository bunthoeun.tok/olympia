import { IBaseResponse } from "./baseResponse";

export interface IRenewTokenResponse extends IBaseResponse {
  Token: string;
}
