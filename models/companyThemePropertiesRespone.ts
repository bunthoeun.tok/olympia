import { IBaseResponse } from "./baseResponse";
import themePropertyHelper from "@/utils/themePropertyHelper";
import { store } from "~~/store";

export interface ICompanyThemeOption {
  BrandName: string;
  DomainId: number;
  WebId: number;
  Domain: string;
  IsEnabled: boolean;
}

export interface IThemePropertiesFromApi {
  HtmlId: string;
  ThemeId: number;
  ThemeName: string;
  Type: number;
  Path: string;
  Height: string;
  Width: string;
  HyperLink: string;
  Text: string;
  ResourceKey: string;
  IsHidden: boolean;
  IsDefault: boolean;
  Language: string;
  ImagePath: string;
  ContactOrder?: number;
}

export interface ICompanyThemePropertiesFromApi extends IBaseResponse {
  ThemeProperties: IThemePropertiesFromApi[];
}

export interface GetThemePropertiesRequestDto {
  IsMobileTheme: boolean;
  Domain: string;
  IsAppTheme: boolean;
  Language: string;
}

export class ThemePropertiesFromApiDto implements IThemePropertiesFromApi {
  HtmlId!: string;
  ThemeId!: number;
  ThemeName!: string;
  Type!: number;
  Path!: string;
  Height!: string;
  Width!: string;
  HyperLink!: string;
  Text!: string;
  ResourceKey!: string;
  IsHidden!: true;
  IsDefault!: boolean;
  Language!: string;
  ImagePath: string;
  ContactOrder?: number;

  constructor(init: IThemePropertiesFromApi, webId: Number = 0, cdn = "") {
    Object.assign(this, init);
    let stateWebIdByDomian = webId;
    if (!process.server) {
      stateWebIdByDomian = store.state.webId;
      // eslint-disable-next-line no-param-reassign
      cdn = commonHelper.getThemePropertiesCdn();
    }
    this.ImagePath = themePropertyHelper.getImagePath(
      this,
      stateWebIdByDomian,
      cdn
    );
  }
}
