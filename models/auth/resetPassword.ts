export interface IRequestResetPassword {
  Domain: string;
  Email: string;
}
