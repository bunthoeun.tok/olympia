export type AccountTabType = {
  name: string;
  label: string;
  link: string;
  disabled: boolean;
};
