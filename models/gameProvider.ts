export interface IGameProvider {
  GameProviderName: string;
  GameProviderId: number;
}
export interface IGameProviders {
  GameProviders: Array<IGameProvider>;
}

export class GameProvider implements IGameProvider {
  GameProviderName = "";
  GameProviderId = 0;

  constructor(init: IGameProvider) {
    Object.assign(this, init);
  }
}
