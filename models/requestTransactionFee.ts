export interface IRequestTransactionFee {
  Amount: number;
  CryptoCurrencyNetwork: string;
}
