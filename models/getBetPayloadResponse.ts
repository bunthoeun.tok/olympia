export default interface IGetBetPayloadResponse {
  Url: string;
  IsSuccess: boolean;
}
