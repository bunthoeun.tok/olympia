import _ from "lodash";
import { IBaseResponse } from "./baseResponse";
import commonHelper from "@/utils/commonHelper";

const { getAssetImage } = commonHelper;
export interface IAvailableBanksForDeposit {
  BankName: string;
  BankCode: string;
  Currency: string;
  CountryCode: string;
  PpId: number;
  DepositMin: number;
  DepositMax: number;
  WithdrawalMin: number;
  WithdrawalMax: number;
  PPDepositMin: number;
  PPDepositMax: number;
  PPWithdrawalMin: number;
  PPWithdrawalMax: number;
  IsEnabled: number;
  PaymentMethod: string;
  PaymentMethodDisplayName: string;
  IsDeposit: boolean;
  IsWithdrawal: boolean;
  CryptoCurrencyNetwork: Array<string>;
}
export interface IPlayerPaymentWithdrawalBankInfo {
  BankCode: string;
  BankName: string;
  BankAccountName: string;
  BankAccountNumber: string;
}
export interface IPaymentMethodsForDepositAndWithdrawal {
  DisplayOrder: number;
  PaymentMethodId: number;
  PaymentMethod: string;
  PaymentMethodDisplayName: string;
  PaymentMethodIconUrl: string;
}
export interface IAvailableBanksForWithdrawal {
  BankCode: string;
  BankName: string;
  WithdrawalMin: number;
  WithdrawalMax: number;
  PaymentMethod: string;
  CryptoCurrencyNetwork: Array<string>;
  PpId: number;
}
export interface IGetPaymentGatewayBanksResponse extends IBaseResponse {
  AvailableBanksForDeposit: Array<IAvailableBanksForDeposit>;
  PlayerPaymentWithdrawalBankInfo: IPlayerPaymentWithdrawalBankInfo;
  PaymentMethodsForDeposit: Array<IPaymentMethodsForDepositAndWithdrawal>;
  PaymentMethodsForWithdrawal: Array<IPaymentMethodsForDepositAndWithdrawal>;
  AvailableBanksForWithdrawal: Array<IAvailableBanksForWithdrawal>;
  WithdrawalLimit: number;
  AvailableWithdrawAmount: number;
  UsdtErcWithdrawalExchangeRate: number;
  UsdtTrc20WithdrawalExchangeRate: number;
  IsTransferMethodDepositEnable: boolean;
  IsTransferMethodWithdrawalEnable: boolean;
  IsTransferMethodEnable: boolean;
  IsDecimalAllow: boolean;
  IsPaymentPasswordSet: boolean;
  IsPaymentPasswordEnable: boolean;
}
export class AvailableBanksForDeposit implements IAvailableBanksForDeposit {
  BankName: string;
  BankCode: string;
  Currency: string;
  CountryCode: string;
  PpId: number;
  DepositMin: number;
  DepositMax: number;
  WithdrawalMin: number;
  WithdrawalMax: number;
  PPDepositMin: number;
  PPDepositMax: number;
  PPWithdrawalMin: number;
  PPWithdrawalMax: number;
  IsEnabled: number;
  PaymentMethod: string;
  PaymentMethodDisplayName: string;
  IsDeposit: boolean;
  IsWithdrawal: boolean;
  CryptoCurrencyNetwork: Array<string>;
  constructor(init: AvailableBanksForDeposit) {
    Object.assign(this, init);
    this.PaymentMethod = this.PaymentMethod.toLowerCase();
  }
}
export class PlayerPaymentWithdrawalBankInfo
  implements IPlayerPaymentWithdrawalBankInfo
{
  BankCode: string;
  BankName: string;
  BankAccountName: string;
  BankAccountNumber: string;
  constructor(init: PlayerPaymentWithdrawalBankInfo) {
    Object.assign(this, init);
  }
}
export class PaymentMethodsForDepositAndWithdrawal
  implements IPaymentMethodsForDepositAndWithdrawal
{
  DisplayOrder: number;
  PaymentMethodId: number;
  PaymentMethod: string;
  PaymentMethodDisplayName: string;
  get PaymentMethodIconUrl(): string {
    const paymentMethodName = _.includes(
      _.toLower(this.PaymentMethodDisplayName),
      "cryptocurrency"
    )
      ? this.PaymentMethodDisplayName.replace(/([A-Z])/g, " $1").trim()
      : this.PaymentMethodDisplayName;
    const icon = _.toLower(_.replace(paymentMethodName, " ", "-"));
    return getAssetImage(`assets/materials/paymentMethod/icon-${icon}@2x.png`);
  }
  constructor(init: PaymentMethodsForDepositAndWithdrawal) {
    Object.assign(this, init);
    this.PaymentMethod = this.PaymentMethod.toLowerCase();
  }
}
export class AvailableBanksForWithdrawal
  implements IAvailableBanksForWithdrawal
{
  BankCode: string;
  BankName: string;
  WithdrawalMin: number;
  WithdrawalMax: number;
  PaymentMethod: string;
  CryptoCurrencyNetwork: Array<string>;
  PpId: number;
  constructor(init: AvailableBanksForWithdrawal) {
    Object.assign(this, init);
    this.PaymentMethod = this.PaymentMethod.toLowerCase();
  }
}
export class GetPaymentGatewayBanksResponse
  implements IGetPaymentGatewayBanksResponse
{
  AvailableBanksForDeposit: Array<IAvailableBanksForDeposit>;
  PlayerPaymentWithdrawalBankInfo: IPlayerPaymentWithdrawalBankInfo;
  PaymentMethodsForDeposit: Array<IPaymentMethodsForDepositAndWithdrawal>;
  PaymentMethodsForWithdrawal: Array<IPaymentMethodsForDepositAndWithdrawal>;
  AvailableBanksForWithdrawal: Array<IAvailableBanksForWithdrawal>;
  WithdrawalLimit: number;
  AvailableWithdrawAmount: number;
  UsdtErcWithdrawalExchangeRate: number;
  UsdtTrc20WithdrawalExchangeRate: number;
  IsTransferMethodDepositEnable: boolean;
  IsTransferMethodWithdrawalEnable: boolean;
  IsTransferMethodEnable: boolean;
  IsDecimalAllow: boolean;
  IsPaymentPasswordSet: boolean;
  IsPaymentPasswordEnable: boolean;
  ErrorCode: number;
  ErrorMessage: string;
  IsSuccess: boolean;
  constructor(init: GetPaymentGatewayBanksResponse) {
    Object.assign(this, init);
    this.AvailableBanksForDeposit = init.AvailableBanksForDeposit.map(
      (item) => new AvailableBanksForDeposit(item)
    );
    this.PlayerPaymentWithdrawalBankInfo = new PlayerPaymentWithdrawalBankInfo(
      init.PlayerPaymentWithdrawalBankInfo
    );
    this.PaymentMethodsForDeposit = init.PaymentMethodsForDeposit.map(
      (item) => new PaymentMethodsForDepositAndWithdrawal(item)
    );
    this.PaymentMethodsForWithdrawal = init.PaymentMethodsForWithdrawal.map(
      (item) => new PaymentMethodsForDepositAndWithdrawal(item)
    );
    this.AvailableBanksForWithdrawal = init.AvailableBanksForWithdrawal.map(
      (item) => new AvailableBanksForWithdrawal(item)
    );
  }
}
