interface IInfo {
  InfoId: string;
  InfoValue: string;
}
interface IFormName {
  Username: string;
  AgentName: string;
  Domain: string;
}
interface IFormAgentCooperation extends IFormName {
  Details: IInfo[];
}
export { IInfo, IFormName, IFormAgentCooperation };
