import { store } from "@/store";
import textHelper from "@/utils/formatHelpers/textFormatHelper";
import commonHelper from "@/utils/commonHelper";
import { ISeamlessProviderList } from "~/models/bsiGameList";

const { convertString } = textHelper;
const { getAssetImage } = commonHelper;
class TabGameDropDownMenus {
  DropDownLabel: string;
  IsActive = false;
  IsNew: boolean;
  IsPromotion: boolean;
  Theme: string;
  TabType!: string;
  IsMaintain: boolean;
  displayOrder: number;
  IsHot: boolean;
  GameProviderDisplayName!: string;

  get DropDownLogoPath(): string {
    try {
      const path = `${this.Theme}/desktop/game_tab/dropdown/${convertString(
        this.DropDownLabel
      )}/normal.png`;
      return getAssetImage(`assets/theme/${path}`);
    } catch (error) {
      return "";
    }
  }

  get DropDownLogoActivePath(): string {
    try {
      const path = `${this.Theme}/desktop/game_tab/dropdown/${convertString(
        this.DropDownLabel
      )}/active.png`;
      return getAssetImage(`assets/theme/${path}`);
    } catch (error) {
      return this.DropDownLogoPath;
    }
  }

  constructor(
    init: string,
    theme: string,
    IsNew = false,
    IsPromotion = false,
    IsMaintain = false,
    displayOrder = 0,
    IsHot = false,
    GameProviderDisplayName = ""
  ) {

    this.Theme = theme;
    this.DropDownLabel = init;
    this.IsNew = IsNew;
    this.IsPromotion = IsPromotion;
    this.IsMaintain = IsMaintain;
    this.displayOrder = displayOrder;
    this.IsHot = IsHot;
    this.GameProviderDisplayName = GameProviderDisplayName;
  }
}

interface ITabGameMenu {
  Label: string;
  DropDownMenus: Array<TabGameDropDownMenus>;
}

class TabGameMenu implements ITabGameMenu {
  Label: string;
  DropDownMenus: Array<TabGameDropDownMenus> = [];
  Theme: string;
  IsActive = false;

  get LogoPath(): string {
    try {
      return getAssetImage(
        `assets/theme/common/icon/${convertString(this.Label)}.png`
      );
    } catch (error) {
      return "";
    }
  }

  get HasDropDown(): boolean {
    return this.DropDownMenus.length > 0;
  }

  constructor(init: string, theme: string) {
    this.Label = init;
    this.Theme = theme;

    if (this.Label.trim() === "Game Type") {
      const tem = store.state.gameTabProviderList.SeamlessGameGameTypeList;
      this.DropDownMenus = tem.map(
        (item: string, index: number) =>
          new TabGameDropDownMenus(
            item,
            this.Theme,
            false, // default false to hide tags
            false, // default false to hide tags
            false, // default false to hide tags
            index,
            false, // default false to hide tags
            item
          )
      );
    } else if (this.Label.trim() === "Provider") {
      const tem = store.state.gameTabProviderList.SeamlessProviderList;
      this.DropDownMenus = tem.map((item: ISeamlessProviderList) => {
        const provider = store.state.gameProviders.Games.find(
          (gameProvider) =>
            gameProvider.GameProviderName === item.ProviderName &&
            gameProvider.Category === 3 &&
            gameProvider.TabType?.toLowerCase() === "games"
        );
        if (provider) {
          const IsMaintain = provider ? provider.IsMaintain : false;
          const isNewProvider = provider ? provider.IsNewGameProvider : false;
          const isHotProvider = provider ? provider.IsHotGameProvider : false;
          const displayOrder = provider ? provider.DisplayOrder : 0;
          const { HasNewGame, HasPromoGame, ProviderName } = item;
          return new TabGameDropDownMenus(
            ProviderName,
            this.Theme,
            isNewProvider,
            HasPromoGame,
            IsMaintain,
            displayOrder,
            isHotProvider,
            provider.GameProviderDisplayName ?? "",
          );
        }
        return {} as TabGameDropDownMenus;
      }).filter((item) => Object.keys(item).length > 0);
    }
  }
}

interface IGameEntryMenu {
  Menus: Array<ITabGameMenu>;
  IsSearchBox: boolean;
  Justify: string;
  Theme: string;
}

class GameEntryMenu implements IGameEntryMenu {
  Menus: Array<TabGameMenu>;
  IsSearchBox: boolean;
  Justify: string;
  Theme: string;

  get IsVisible(): boolean {
    return this.IsSearchBox || this.Menus.length > 0;
  }

  get JustifyForCSS(): string {
    let result = "flex items-center w-full ";
    if (this.Justify.trim().toLowerCase() === "left") {
      result += "justify-start";
    } else if (this.Justify.trim().toLowerCase() === "right") {
      result += "justify-end";
    } else {
      result += "justify-center";
    }
    return result;
  }

  constructor(init: IGameEntryMenu, first = true) {
    Object.assign(this, init);
    this.Menus =
      init.Menus !== undefined
        ? init.Menus.map((item) => new TabGameMenu(item.Label, this.Theme))
        : [];
    if (this.Menus.length) {
      this.Menus[first ? 0 : 1].IsActive = true;
    }
  }
}

export { IGameEntryMenu, GameEntryMenu, TabGameDropDownMenus, TabGameMenu };
