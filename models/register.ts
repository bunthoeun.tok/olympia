import _ from "lodash";

interface ICompanyFlowSettings {
  PropertyName: string;
  PropertyValue: string;
}

export interface IRegisterRequest {
  Domain: string;
  IsTest?: boolean;
  Referral: string;
  Currency: string;
  IsWalkIn?: boolean;
  Url: string;
  CompanyFlowSettings: Array<ICompanyFlowSettings>;
  Username: string;
  Password: string;
  Ip: string;
  LoginCountry: string;
  Region: string;
  City: string;
  Platform: string;
  Browser: string;
  Fingerprint:string;
}

interface IGames {
  GameProviderName: string;
  GameProviderShortName: string;
  GameProviderId: number;
  GameId: number;
  Category: number;
  CategoryName: string;
  GameType: string;
  GameName: string;
  IsEnabled: boolean;
  IsHotGame: boolean;
  Rank: number;
  IsNewGame: boolean;
  IsJackpot: boolean;
  IsMaintainByGameProvider: boolean;
  IsMaintain: boolean;
  ForMobile: boolean;
  ForDesktop: boolean;
  IsPromotionGame: boolean;
  BsiPriority: number;
  AsiPriority: number;
  GameIconUrl: string;
  EnabledCurrency: string;
  GameCode: null;
  Device: number;
  APIGameProviderName: string;
  APIGameProviderId: number;
  DisplayOrder: number;
  IsEnabledByGameProvider: boolean;
  TabType: string;
}

interface ITabInfos {
  TabType: string;
  Order: number;
  IsFilter: boolean;
}

interface ISeamlessProviderList {
  ProviderName: string;
  HasNewGame: boolean;
  HasPromoGame: boolean;
}

export interface IRegisterResponse {
  Username: string;
  LoginName: string;
  CustomerId: number;
  ParentId: number;
  Currency: string;
  CashBalance: number;
  CreditBalance: number;
  IsCashPlayer: boolean;
  OnlineId: number;
  AccountType: number;
  Games: Array<IGames>;
  TabInfos: Array<ITabInfos>;
  SeamlessGameGameTypeList: Array<string> | null;
  SeamlessGameTabList: Array<string> | null;
  SeamlessProviderList: Array<ISeamlessProviderList> | null;
  ErrorCode: number;
  Message: string;
  IsSuccess: boolean;
}
export class GetRegisterResponse implements IRegisterResponse {
  Username: string;
  LoginName: string;
  CustomerId: number;
  ParentId: number;
  Currency: string;
  Token?: string;
  CashBalance: number;
  CreditBalance: number;
  IsCashPlayer: boolean;
  OnlineId: number;
  AccountType: number;
  Games: Array<IGames>;
  SeamlessGameGameTypeList: Array<string>;
  SeamlessGameTabList: Array<string>;
  SeamlessProviderList: Array<ISeamlessProviderList>;
  TabInfos: Array<ITabInfos>;
  ErrorCode: number;
  Message: string;
  IsSuccess: boolean;
  LandingPageRedirectUrl?: string;

  get GamesPriority(): IGames[] {
    return _.sortBy(this.Games, [
      (game) => (game.AsiPriority ? game.AsiPriority : undefined),
      "GameName",
      "DisplayOrder",
    ]);
  }

  constructor(init: GetRegisterResponse) {
    Object.assign(this, init);
  }
}
export interface IGetAccountCurrencyByDomainRequest {
  Domain: string;
}
export interface IGetAccountCurrencyByDomainResponse {
  Currency: string;
  IsSuccess: boolean;
}

export interface IGetOtpVerificaitonRequest {
  Phone: string;
  CountryCode: string;
  WebId: number;
  Domain: string;
  Language: string;
  IsSettingForApp: boolean;
}
