export interface IBet {
  RefNo: string;
  GameProviderId: number;
  GameProviderType: number;
  GameProviderName: string;
}
