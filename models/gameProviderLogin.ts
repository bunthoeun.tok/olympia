import { IBaseResponse } from "./baseResponse";

export class IRequestGameProviderLogin {
  GameId: number;
  Ip: string;
  FromUrl: string;
  IsFromMobile: boolean;
  Lang: string;
  EntranceLocation: string;
  OnlineId: number;
}

export interface IGetGameProviderLoginResponse extends IBaseResponse {
  Url: string;
}
