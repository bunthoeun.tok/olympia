export default interface IGetBetListRequest {
  GameProviderId: number;
  IsGetFromAllProviders: boolean;
  WinLoseDateStart: string;
  WinLoseDateEnd: string;
  RowCountPerPage: number;
  Page: number;
}
