import themePropertyHelper from "@/utils/themePropertyHelper";
import textHelper from "@/utils/formatHelpers/textFormatHelper";

export interface IRequestPromotion {
  Domain: string;
  Currency: string;
  CustomerId: number;
}
export interface IPromotion {
  Id: number;
  Title: string;
  Content: string | null;
  PromoCode: string;
  Remark: string | null;
  ImagePath: string;
  ProductType: number;
  GameType: string;
  PromotionTypeId: number;
  PromotionType: string;
  Rollover: number;
  Percentage: number;
  Amount: number;
  MaxAmount: number;
  Status: number;
  StartDate: string;
  EndDate: string;
  ModifiedBy: string;
  ModifiedOn: string;
  DisplayOrder: number;
  IsNew: boolean;
  AutoRebateOption: string;
  PromotionGameProviderIds: number[];
  Currency: string;
}

export class Promotion implements IPromotion {
  Id: number;
  Title: string;
  Content: string | null;
  PromoCode: string;
  Remark: string | null;
  ImagePath: string;
  ProductType: number;
  GameType: string;
  PromotionTypeId: number;
  PromotionType: string;
  Rollover: number;
  Percentage: number;
  Amount: number;
  MaxAmount: number;
  Status: number;
  StartDate: string;
  EndDate: string;
  ModifiedBy: string;
  ModifiedOn: string;
  DisplayOrder: number;
  IsNew: boolean;
  AutoRebateOption: string;
  PromotionGameProviderIds: number[];
  IsVisible = false;
  Currency: string;

  get ImagePathForDisplay(): string {
    return themePropertyHelper.getImagePathProvider(
      this.ImagePath,
      "promotion"
    );
  }
  get DateDisplay(): string {
    return textHelper.getDateFormatDisplay(this.StartDate, this.EndDate);
  }
  get DateRangeForDisplay(): string {
    return `${textHelper.getDate(this.StartDate, false)} - ${textHelper.getDate(
      this.EndDate,
      false
    )}`;
  }
  get StartDateForDisplay(): string {
    return textHelper.getDate(this.StartDate, false);
  }
  get EndDateForDisplay(): string {
    return textHelper.getDate(this.EndDate, false);
  }
  get IsAbleToApply(): boolean {
    const startDate = new Date(this.StartDate).getTime();
    const endDate = new Date(this.EndDate).getTime();
    const today = new Date().getTime();
    return this.Status === 3 && startDate <= today && endDate > today;
  }

  constructor(init: IPromotion) {
    Object.assign(this, init);
  }
}
export interface IGetPromotionResponse {
  Promotions: Array<IPromotion>;
  IsSuccess: boolean;
}

export class GetPromotionResponse implements IGetPromotionResponse {
  Promotions: Array<IPromotion>;
  IsSuccess: boolean;

  constructor(init: IGetPromotionResponse) {
    this.IsSuccess = init.IsSuccess;
    this.Promotions = init.Promotions.map((item) => new Promotion(item));
  }
}

export interface IPromotionApplyReqeust {
  PromotionId: number;
  Ip: string;
  Device: string;
  IsNew: boolean;
  FingerPrint?: string;
  OnlineId: number;
}
