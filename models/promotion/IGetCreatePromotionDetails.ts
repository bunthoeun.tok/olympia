/* eslint-disable */
interface ICreatePromotionCategory {
  CategoryId: number;
  CategoryName: string;
}

interface IPromotionType {
  Id: number;
  Name: string;
  Description: string;
  Status: number;
  ModifiedOn: string;
  ModifiedBy: string;
}

interface IGameProvider {
  ApiGameProviderId: number;
  IsEnabled: boolean;
  IsReadyToOpen: boolean;
  DisplayName: string;
  GameProviderType: string;
  IsTransferGameProvider: boolean;
  GameProviderId: number;
  GameProviderName: string;
}

interface IGetCreatePromotionDetailsResponse {
  Categories: Array<ICreatePromotionCategory>;
  PromotionTypes: Array<IPromotionType>;
  GameProviderList: Array<IGameProvider>;
}
