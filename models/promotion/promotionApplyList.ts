import EnumColor from "../enums/enumColor";
import EnumPromotionStatus from "../enums/enumPromotionStatus";
import numberFormatHelper from "@/utils/formatHelpers/numberFormatHelper";
import textHelper from "@/utils/formatHelpers/textFormatHelper";

export interface IPromotionApplyList {
  RequestId: number;
  CustomerId: number;
  Username: string;
  RequestedOn: string;
  PromotionId: number;
  PromoCode: string;
  PromotionTitle: string;
  PromotionType: string;
  PromotionTypeId: number;
  Currency: string;
  SuggestedAmount: number;
  VerifiedAmount: number;
  MinTurnover: number;
  CurrentTurnover: number;
  Status: string;
  BeforeBalance: number;
  AfterBalance: number;
  StartDate: string;
  EndDate: string;
  Remark: string;
  IsNew: boolean;
}
export interface IApplyPromotionRequest {
  PromotionId: number;
  Device: string;
  IsNew: boolean;
  Url: string;
  FingerPrint: string;
  OnlineId: number;
}
export class PromotionApplyList implements IPromotionApplyList {
  RequestId: number;
  CustomerId: number;
  Username: string;
  RequestedOn: string;
  PromotionId: number;
  PromoCode: string;
  PromotionTitle: string;
  PromotionType: string;
  PromotionTypeId: number;
  Currency: string;
  SuggestedAmount: number;
  VerifiedAmount: number;
  MinTurnover: number;
  CurrentTurnover: number;
  Status: string;
  BeforeBalance: number;
  AfterBalance: number;
  StartDate: string;
  EndDate: string;
  Remark: string;
  IsNew: boolean;
  get colorClass(): EnumColor {
    if (this.Status === EnumPromotionStatus.Waiting) {
      return EnumColor.ColorText;
    }
    if (this.Status === EnumPromotionStatus.Rejected) {
      return EnumColor.ColorFailure;
    }
    return EnumColor.ColorSuccess;
  }

  get Progress(): number {
    return this.MinTurnover === 0
      ? numberFormatHelper.floorTo2Digit(0)
      : numberFormatHelper.floorTo2Digit(
          (this.CurrentTurnover / this.MinTurnover) * 100
        );
  }

  get VerifiedAmountForDisplay(): string {
    return numberFormatHelper.addAccountingFormat(this.VerifiedAmount);
  }

  get TurnoverForDisplay(): string {
    return numberFormatHelper.addAccountingFormat(this.CurrentTurnover);
  }

  get Duration(): string {
    return `${textHelper.getDate(this.StartDate)}-${textHelper.getDate(
      this.EndDate
    )}`;
  }

  constructor(init: IPromotionApplyList) {
    Object.assign(this, init);
    this.RequestedOn = textHelper.dateStringTo24HourClock(init.RequestedOn);
    this.VerifiedAmount = numberFormatHelper.floorTo2Digit(init.VerifiedAmount);
    this.MinTurnover = numberFormatHelper.floorTo2Digit(init.MinTurnover);
    this.StartDate = textHelper.getDate(init.StartDate);
    this.EndDate = textHelper.getDate(init.EndDate);
  }
}
