import { IPromotionApplyList, PromotionApplyList } from "./promotionApplyList";

interface IGetPromotionApplyListResponse {
  PromotionApplyList: Array<IPromotionApplyList>;
  IsSuccess: boolean;
}

export class GetPromotionApplyListResponse
  implements IGetPromotionApplyListResponse
{
  PromotionApplyList: Array<IPromotionApplyList>;
  IsSuccess: boolean;
  constructor(init: IGetPromotionApplyListResponse) {
    Object.assign(this, init);
    this.PromotionApplyList = init.PromotionApplyList.map(
      (item) => new PromotionApplyList(item)
    );
  }
}

export default IGetPromotionApplyListResponse;
