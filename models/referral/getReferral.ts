import numberFormatHelper from "@/utils/formatHelpers/numberFormatHelper";
import textFormatHelper from "@/utils/formatHelpers/textFormatHelper";

interface IGetReferralLayerAmountResponse {
  ReferralChildrenCount: number;
  TotalRewardTillYesterday: number;
  TotalRebateTillYesterday: number;
  TotalReferralAmountTillYesterday: number;
  Redeemed: number;
  WaitingRedeem: number;
  Redeemable: number;
  IsSuccess: boolean;
}
class GetReferralLayerAmountResponse
  implements IGetReferralLayerAmountResponse
{
  ReferralChildrenCount: number;
  TotalRewardTillYesterday: number;
  TotalRebateTillYesterday: number;
  TotalReferralAmountTillYesterday: number;
  Redeemed: number;
  WaitingRedeem: number;
  Redeemable: number;
  IsSuccess: boolean;
  get RedeemableForDisplay(): string {
    return numberFormatHelper.addAccountingFormat(this.Redeemable);
  }
  get TotalReferralAmountTillYesterdayForDisplay(): string {
    return numberFormatHelper.addAccountingFormat(
      this.TotalReferralAmountTillYesterday
    );
  }
  get TotalRewardTillYesterdayForDisplay(): string {
    return numberFormatHelper.addAccountingFormat(
      this.TotalRewardTillYesterday
    );
  }
  get TotalRebateTillYesterdayForDisplay(): string {
    return numberFormatHelper.addAccountingFormat(
      this.TotalRebateTillYesterday
    );
  }
  get RedeemedForDisplay(): string {
    return numberFormatHelper.addAccountingFormat(this.Redeemed);
  }
  constructor(init: IGetReferralLayerAmountResponse) {
    Object.assign(this, init);
  }
}

interface INode {
  CustomerId: number;
  ParentCustomerId: number;
  Layer: number;
  Turnover: number;
  ThisMonthTurnover: number;
  LastMonthTurnover: number;
  NetTurnover: number;
  ThisMonthNetTurnover: number;
  LastMonthNetTurnover: number;
  Currency: string;
  Username: string;
}

interface ITreeNode extends INode {
  ChildCount: number;
  IsExpand: boolean;
  Children: Array<ITreeNode>;
}

class TreeNode implements ITreeNode {
  CustomerId: number;
  ParentCustomerId: number;
  Layer: number;
  Turnover: number;
  ThisMonthTurnover: number;
  LastMonthTurnover: number;
  NetTurnover: number;
  ThisMonthNetTurnover: number;
  LastMonthNetTurnover: number;
  Currency: string;
  Username: string;
  ChildCount: number;
  IsExpand: boolean;
  Children: Array<ITreeNode>;

  get ThisMonthTurnoverForDisplay(): string {
    return numberFormatHelper.addAccountingFormat(this.ThisMonthTurnover);
  }
  get LastMonthTurnoverForDisplay(): string {
    return numberFormatHelper.addAccountingFormat(this.LastMonthTurnover);
  }
  get ThisMonthNetTurnoverForDisplay(): string {
    return numberFormatHelper.addAccountingFormat(this.ThisMonthNetTurnover);
  }
  get LastMonthNetTurnoverForDisplay(): string {
    return numberFormatHelper.addAccountingFormat(this.LastMonthNetTurnover);
  }
  get TurnoverMarginPercentage(): string {
    const margin = (this.ThisMonthTurnover / this.LastMonthTurnover - 1) * 100;
    return `${margin < 0 ? "-" : "+"}${numberFormatHelper.addAccountingFormat(
      Math.round(margin),
      0
    )}`;
  }
  get TurnoverMarginColor(): string {
    if (this.ThisMonthTurnover / this.LastMonthTurnover < 1) {
      return "fc-failure";
    }
    return "fc-success";
  }
  get NetTurnoverMarginPercentage(): string {
    const margin =
      (this.ThisMonthNetTurnover / this.LastMonthNetTurnover - 1) * 100;
    return `${margin < 0 ? "-" : "+"}${numberFormatHelper.addAccountingFormat(
      Math.round(margin),
      0
    )}`;
  }
  get NetTurnoverMarginColor(): string {
    if (this.ThisMonthNetTurnover / this.LastMonthNetTurnover < 1) {
      return "fc-failure";
    }
    return "fc-success";
  }
  constructor(init: ITreeNode) {
    Object.assign(this, init);
  }
}

interface INodeResponse extends INode {
  IsSuccess: boolean;
}

interface IRedeemRequest {
  Id: number;
  RequestAmount: number;
  ApprovedAmount: number;
  Remark: string;
  StatusString: string;
  RequestDate: string;
  ModifiedDate: string;
}

class RedeemRequest implements IRedeemRequest {
  Id: number;
  RequestAmount: number;
  ApprovedAmount: number;
  Remark: string;
  StatusString: string;
  RequestDate: string;
  ModifiedDate: string;
  get RequestDateForDisplay(): string {
    return this.RequestDate.split("T")[0];
  }
  get RequestDateForDisplayMobile(): string {
    return textFormatHelper.dateStringTo12HourClock(this.RequestDate);
  }
  get ModifiedDateForDisplay(): string {
    return this.ModifiedDate.split("T")[0];
  }
  get ModifiedDateForDisplayMobile(): string {
    return textFormatHelper.dateStringTo12HourClock(this.ModifiedDate);
  }
  get RequestAmountForDisplay(): string {
    return numberFormatHelper.addAccountingFormat(this.RequestAmount);
  }
  get ApprovedAmountForDisplay(): string {
    return numberFormatHelper.addAccountingFormat(this.ApprovedAmount);
  }
  get StatusStringForDisplay(): string {
    const { $t } = useNuxtApp();
    return $t(this.StatusString.toLowerCase());
  }
  get StatusStringColorForDisplay(): string {
    if (this.StatusString.toLowerCase() === "waiting") {
      return "fc-hint";
    }
    if (this.StatusString.toLowerCase() === "approved") {
      return "fc-success";
    }
    return "fc-failure";
  }
  get StatusStringColorForDisplayMobile(): string {
    if (this.StatusString.toLowerCase() === "waiting") {
      return "text-white bg-hint";
    }
    if (this.StatusString.toLowerCase() === "approved") {
      return "text-white bg-success";
    }
    return "text-white bg-failure";
  }
  constructor(init: IRedeemRequest) {
    Object.assign(this, init);
  }
}

interface IRedeemReferralAmountRequest {
  Amount: number;
}

interface IGetRedeemReferralResponse {
  RedeemRequests: Array<IRedeemRequest>;
  IsSuccess: boolean;
}

class GetRedeemReferralResponse implements IGetRedeemReferralResponse {
  RedeemRequests: Array<RedeemRequest>;
  IsSuccess: boolean;

  constructor(init: IGetRedeemReferralResponse) {
    this.IsSuccess = init.IsSuccess;
    this.RedeemRequests = init.RedeemRequests.map(
      (item) => new RedeemRequest(item)
    );
  }
}

interface IGetReferralDiagramRequest {
  StartDate: string;
  EndDate: string;
}

interface IGetReferralDiagramResponse {
  NodesList: Array<INodeResponse>;
  IsSuccess: boolean;
}

export {
  IGetReferralLayerAmountResponse,
  GetReferralLayerAmountResponse,
  IGetRedeemReferralResponse,
  GetRedeemReferralResponse,
  IRedeemRequest,
  RedeemRequest,
  IGetReferralDiagramRequest,
  INodeResponse,
  INode,
  ITreeNode,
  IGetReferralDiagramResponse,
  IRedeemReferralAmountRequest,
  TreeNode,
};
