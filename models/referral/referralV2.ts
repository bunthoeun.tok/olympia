import _ from "lodash-es";
import { format } from "date-fns";
import numberFormatHelper from "~~/utils/formatHelpers/numberFormatHelper";

export interface IReferralMember {
    Username: string;
    Currency: string;
    RegisteredTime: string;
    BonusAmount: number;
    TotalDeposit: number;
    TotalTurnover: number;
    QualifiedDepositLimit: number;
    QualifiedTurnoverLimit: number;
    IsByDeposit: boolean;
    IsByTurnover: boolean;
    IsValidReferee: boolean;
    InValidReason: string;
}

export interface IGetPlayerRedeemRequest {
    CompanyPromotionId: number,
    ReferralCount: number,
    EffectiveReferralCount: number,
    Redeemable: number,
    ReferralMemberList: IReferralMember[],
}

export class ReferralMember implements IReferralMember {
    Username: string;
    Currency: string;
    RegisteredTime: string;
    BonusAmount: number;
    TotalDeposit: number;
    TotalTurnover: number;
    QualifiedDepositLimit: number;
    QualifiedTurnoverLimit: number;
    IsByDeposit: boolean;
    IsByTurnover: boolean;
    InValidReason: string;
    IsValidReferee: boolean;
    constructor(init: IReferralMember) {
        this.Username = init.Username;
        this.Currency = init.Currency;
        this.RegisteredTime = init.RegisteredTime;
        this.BonusAmount = init.BonusAmount;
        this.TotalDeposit = init.TotalDeposit;
        this.TotalTurnover = init.TotalTurnover;
        this.QualifiedDepositLimit = init.QualifiedDepositLimit;
        this.QualifiedTurnoverLimit = init.QualifiedTurnoverLimit;
        this.IsByDeposit = init.IsByDeposit;
        this.IsByTurnover = init.IsByTurnover;
        this.IsValidReferee = init.IsValidReferee;
        this.InValidReason = init.InValidReason;
    }

    public get ReachAmountIsNoLimit(): boolean {
        return (this.TotalDeposit === 0 && this.QualifiedDepositLimit === 0) &&
            (this.TotalTurnover === 0 && this.QualifiedTurnoverLimit === 0);
    }
    
    public get FormattedInValidReason(): string {
        if (this.InValidReason && this.InValidReason.length > 0) {
            return this.InValidReason;
        }
        if (
            this.ReachAmountIsNoLimit ||
            (
                this.TotalDeposit < this.QualifiedDepositLimit &&
                this.TotalTurnover < this.QualifiedTurnoverLimit
            ) ||
            (
                this.TotalDeposit < this.QualifiedDepositLimit &&
                this.TotalTurnover === 0 &&
                this.QualifiedTurnoverLimit === 0
            ) ||
            (
                this.TotalTurnover < this.QualifiedTurnoverLimit &&
                this.TotalDeposit === 0 &&
                this.QualifiedDepositLimit === 0
            ) || 
            (
                this.TotalDeposit >= this.QualifiedDepositLimit && this.TotalTurnover < this.QualifiedTurnoverLimit
            ) || 
            (
                this.TotalTurnover >= this.QualifiedTurnoverLimit && this.TotalDeposit < this.QualifiedDepositLimit
            )
        ) {
            return 'ineffective';
        }
        return 'effective';
    }

    public get FormattedBonusAmount(): string {
        return numberFormatHelper.addAccountingFormatWithoutRoundUp(this.BonusAmount);
    }
    public get FormattedRegisteredTime(): string {
        if (!this.RegisteredTime || this.RegisteredTime.length <= 0) {
            return "--";
        }
        return format(new Date(this.RegisteredTime), "yyyy.MM.dd");
    }
    public get FormattedTotalDeposit(): string {
        return numberFormatHelper.addAccountingFormatWithoutRoundUp(this.TotalDeposit);
    }

    public get FormattedQualifiedDepositLimit(): string {
        return numberFormatHelper.addAccountingFormatWithoutRoundUp(this.QualifiedDepositLimit);
    }

    public get FormattedTotalTurnover(): string {
        return numberFormatHelper.addAccountingFormatWithoutRoundUp(this.TotalTurnover);
    }

    public get FormattedQualifiedTurnoverLimit(): string {
        return numberFormatHelper.addAccountingFormatWithoutRoundUp(this.QualifiedTurnoverLimit);
    }
}

export class GetPlayerRedeemRequest implements IGetPlayerRedeemRequest {
  CompanyPromotionId: number;
  ReferralCount: number;
  EffectiveReferralCount: number;
  Redeemable: number;
  ReferralMemberList: ReferralMember[];
  constructor(init: IGetPlayerRedeemRequest) {
    this.CompanyPromotionId = init.CompanyPromotionId;
    this.ReferralCount = init.ReferralCount;
    this.EffectiveReferralCount = init.EffectiveReferralCount;
    this.Redeemable = init.Redeemable;
    this.ReferralMemberList = (init.ReferralMemberList ?? []).map(referralMember => new ReferralMember(referralMember));
    this.ReferralMemberList = _.orderBy(this.ReferralMemberList, 'RegisteredTime', 'desc');
  }
  public get FormattedRedeemable(): string {
    return numberFormatHelper.addAccountingFormatWithoutRoundUp(this.Redeemable);
  }
}

export interface IGetApplyRedeemRequest {
    Device: string,
    RedeemAmount: number,
    CompanyPromotionId: number,
    Url:string;
    FingerPrint:string;
    IP:string;
}

export interface IGetGeneralInformationResponse {
    StartDate: string,
    EndDate: string,
    Content: string,
    IsHaveReferralEvent: boolean,
}

export class ReferralV2GeneralInformation implements IGetGeneralInformationResponse {
    StartDate: string;
    EndDate: string;
    Content: string;
    IsHaveReferralEvent: boolean;
    constructor(init: IGetGeneralInformationResponse) {
        this.StartDate = init.StartDate;
        this.EndDate = init.EndDate;
        this.Content = init.Content;
        this.IsHaveReferralEvent = init.IsHaveReferralEvent;
    }
}

export interface IGetPlayerRedeemHistoryRequest {
    StartDate: string;
    EndDate: string;
}

enum EnumReferralStatus {
    waiting = "waiting",
    approved = "approved",
    rejected = "rejected",
    completed = "completed",
}
enum EnumReferralStatusTextColor {
    neutral = "neutral",
    waiting = "neutral",
    approved = "success",
    rejected = "failure",
    completed = "success",
}
export interface IPlayerRedeemRequest {
    RequestedDate: string,
    ApprovedDate: string,
    RedeemAmount: number,
    WithdrawalLimit: number,
    Remark: string,
    Status: EnumReferralStatus,
}

export class PlayerRedeemRequest implements IPlayerRedeemRequest {
    RequestedDate: string;
    ApprovedDate: string;
    RedeemAmount: number;
    WithdrawalLimit: number;
    Remark: string;
    Status: EnumReferralStatus;

    constructor(init: IPlayerRedeemRequest) {
        this.RequestedDate = init.RequestedDate;
        this.ApprovedDate = init.ApprovedDate;
        this.RedeemAmount = init.RedeemAmount;
        this.WithdrawalLimit = init.WithdrawalLimit;
        this.Remark = init.Remark;
        this.Status = init.Status;
    }

    public get FormattedRedeemAmount(): string {
        return numberFormatHelper.addAccountingFormatWithoutRoundUp(this.RedeemAmount);
    }
    public get FormattedWithdrawalLimit(): string {
        return numberFormatHelper.addAccountingFormatWithoutRoundUp(this.WithdrawalLimit);
    }
    public get FormattedRequestedDate(): string {
        if (!this.RequestedDate || this.RequestedDate.length <= 0) {
            return "--";
        }
        return format(new Date(this.RequestedDate), "yyyy.MM.dd");
    }
    public get FormattedApprovedDate(): string {
        if (!this.ApprovedDate || this.ApprovedDate.length <= 0 || this.Status.toLowerCase() === 'waiting') {
            return "--";
        }
        return format(new Date(this.ApprovedDate), "yyyy.MM.dd");
    }
    public get FormattedStatus(): EnumReferralStatus | "--" {
        const status = this.Status?.toLowerCase() as EnumReferralStatus;
        const isListedStatus = Object.values(EnumReferralStatus).includes(status);
        if (!this.Status || this.Status.length <=0 || !isListedStatus) {
            return "--";
        }
        return EnumReferralStatus[status];
    }
    public get FormattedStatusColorClass(): string {
        if (this.FormattedStatus === "--") return EnumReferralStatusTextColor.neutral;
        return EnumReferralStatusTextColor[this.FormattedStatus];
    }
}

export interface IGetPlayerRedeemHistoryResponse {
    RedeemRequests: IPlayerRedeemRequest[],
}

export class GetPlayerRedeemHistoryResponse implements IGetPlayerRedeemHistoryResponse {
    RedeemRequests: PlayerRedeemRequest[];
    constructor(init: IGetPlayerRedeemHistoryResponse) {
        this.RedeemRequests = (init.RedeemRequests ?? []).map(request => new PlayerRedeemRequest(request));
    }
}