import { IBaseResponse } from "./baseResponse";

export interface IPlayerCryptoCurrencyOptionsResponse {
  Id: number;
  WalletType: string;
  Priority: number;
}

export interface ICompanyCryptocurrencyOptionsResponse {
  Id: number;
  CompanyChannel: string;
  WalletAddress: string;
  Currency: string;
  Group: number;
  Status: number;
  DepositMin: number;
  DepositMax: number;
  WithdrawalMin: number;
  WithdrawalMax: number;
}

export interface IAvailablePropertyListResponse {
  PropertyStatus: string;
  PropertyName: string;
  DisplayOrder: number;
}

export interface IRequestManualCryptoCurrencyDeposit {
  WalletType: string;
  PlayerWalletAddress: string;
  WebId: number;
  Username: string;
  Ip: string;
  Currency: string;
  Language: string;
  Domain: string;
  CompanyBankId: number;
  Amount: number;
  DepositTime: Date | string;
  SlipImage: string;
  OnlineId: number;
}

export interface IGetManualCryptoCurrencyTransactionResponse
  extends IBaseResponse {
  CompanyCryptocurrencyOptions: Array<ICompanyCryptocurrencyOptionsResponse>;
  PlayerCryptocurrencyOptions: Array<IPlayerCryptoCurrencyOptionsResponse>;
  AvailablePropertyList: Array<IAvailablePropertyListResponse>;
  IsDecimalAllow: boolean;
}
