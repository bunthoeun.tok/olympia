import textHelper from "@/utils/formatHelpers/textFormatHelper";

export interface IProfileDto {
  Username: string;
  LoginName: string;
  Email: string;
  Phone: string;
  Mobile: string;
  Currency: string;
  Country: string;
  Bank: string;
  PaymentBank: string;

  LastLoginOn: string;
  LastTransactionOn: string;
  PasswordExpiryOn: string;
}

export interface ICompanyFlowSettingDto {
  PropertyName: string;
  PropertyValue: string;
}

export interface IGetProfileResponse {
  Information: IProfileDto;
  ProfileDetail: Array<ICompanyFlowSettingDto>;
  ErrorCode: number;
  ErrorMessage: string;
  IsSuccess: boolean;
}

export class ProfileDto implements IProfileDto {
  Username: string;
  LoginName: string;
  Email: string;
  Phone: string;
  Mobile: string;
  Currency: string;
  Country: string;
  Bank: string;
  PaymentBank: string;
  LastLoginOn: string;
  LastTransactionOn: string;
  PasswordExpiryOn: string;

  constructor(init: Partial<ProfileDto>) {
    const defaultValue = "--";
    this.Username = textHelper.addDefaultValue(init.Username);
    this.LoginName = textHelper.addDefaultValue(init.LoginName);
    this.Email = textHelper.addDefaultValue(init.Email);
    this.Phone = textHelper.addDefaultValue(init.Phone);
    this.Mobile = textHelper.addDefaultValue(init.Mobile);
    this.Currency = textHelper.addDefaultValue(init.Currency);
    this.Country = textHelper.addDefaultValue(init.Country);
    this.Bank = textHelper.addDefaultValue(init.Bank);
    this.PaymentBank = textHelper.addDefaultValue(init.PaymentBank);
    this.LastLoginOn = init.LastLoginOn
      ? textHelper.dateStringTo12HourClock(init.LastLoginOn)
      : defaultValue;
    this.LastTransactionOn = init.LastTransactionOn
      ? textHelper.dateStringTo12HourClock(init.LastTransactionOn)
      : defaultValue;
    this.PasswordExpiryOn = init.PasswordExpiryOn
      ? textHelper.dateStringTo12HourClock(init.PasswordExpiryOn)
      : defaultValue;
  }
}

export class CompanyFlowSettingDto implements ICompanyFlowSettingDto {
  PropertyName: string;
  PropertyValue: string;

  constructor(init: CompanyFlowSettingDto) {
    this.PropertyName = init.PropertyName;
    this.PropertyValue = textHelper.addDefaultValue(init.PropertyValue);
  }
}

export class GetProfileResponse implements IGetProfileResponse {
  Information: IProfileDto;
  ProfileDetail: ICompanyFlowSettingDto[];
  ErrorCode: number;
  ErrorMessage: string;
  IsSuccess: boolean;

  constructor(init: GetProfileResponse) {
    Object.assign(this, init);
    this.Information = new ProfileDto(init.Information);
    this.ProfileDetail = init.ProfileDetail.map(
      (d) => new CompanyFlowSettingDto(d)
    );
  }
}
