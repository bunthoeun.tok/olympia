import _ from "lodash";
import { ITabInfo, IGame } from "./IGetPlayableGamesResponse";
import commonHelper from "@/utils/commonHelper";
import textHelper from "@/utils/formatHelpers/textFormatHelper";
import EnumProductType from "./enums/enumProductType";
import { store } from "~~/store";
import useModifyGameName from "../composables/useModifyGameName";

const pathDictionary: Record<string, string> = {
  Sports: "sport/",
  "Virtual Sports": "virtualsport/",
  "Live Casino": "casino/",
  "Cock Fight": "cockfight/",
  Games: "game/",
  Provider: "provider/",
  Poker: "poker/",
  Lottery: "lottery/",
};
const { getAssetImage, handleRemoveSpaceSpecialCharFormatted } = commonHelper;

interface IRequestAsiGame {
  Provider: string;
  Category: string;
  Lang: string;
}
interface IGameBsi extends IGame {
  GameProviderShortName: string;
  IsHotGame: boolean;
  Rank: number;
  IsNewGame: boolean;
  IsJackpot: boolean;
  Device: number | null;
  TabType: string;
  IsHotGameProvider?: boolean;
  IsNewGameProvider?: boolean;
  GameIdAtGameProvider?: number;
}

class GameBsiForLFM1 implements IGameBsi {
  GameProviderShortName: string;
  IsHotGame: boolean;
  Rank: number;
  IsNewGame: boolean;
  IsJackpot: boolean;
  Device: number | null;
  TabType: string;
  GameProviderName: string;
  GameProviderId: number;
  GameId: number;
  Category: number;
  CategoryName: string;
  GameType: string;
  GameName: string;
  EnglishGameName?: string;
  IsEnabled: boolean;
  IsMaintainByGameProvider: boolean;
  IsMaintain: boolean;
  ForMobile: boolean;
  ForDesktop: boolean;
  IsPromotionGame: boolean;
  BsiPriority: number;
  AsiPriority: number;
  GameIconUrl: string;
  EnabledCurrency: string;
  GameCode: string | null;
  Platform?: number | null;
  APIGameProviderName: string;
  APIGameProviderId: number;
  DisplayOrder: number;
  IsEnabledByGameProvider: boolean;
  IsHotGameProvider?: boolean;
  IsNewGameProvider?: boolean;
  LinkOption?: string;
  GameIdAtGameProvider?: number;
  IsProvider = false;
  IsGameType = false;

  get GameIconUrlForDisplay() {
    let path = "";
    path += this.IsProvider
      ? pathDictionary.Provider
      : pathDictionary[this.TabType.trim()];
    let gameName = "";
    if (this.IsGameType) {
      gameName = this.GameType.toLowerCase();
    } else {
      gameName =
        this.EnglishGameName?.split(" ").join("").toLowerCase() ??
        this.GameName.split(" ").join("").toLowerCase();
    }
    let providerName = this.GameProviderName.split(" ")
      .join("")
      .toLowerCase();
    if (this.GameProviderId === 1052) {
      providerName = providerName.concat("new");
      gameName = gameName.concat("new");
    }
    path += `${this.CategoryName === "LIVE_CASINO" ? "casino_" : ""}${
      this.IsProvider ? providerName : gameName
    }.png`;
    if (this.CategoryName === "Favorite") path = `game/${this.CategoryName}.png`;
    // if (this.GameProviderId === 1052) path = path.concat("new");
    return getAssetImage(
      `assets/theme/light_flexible_1/mobile/images/entry/${path}`
    );
  }
  constructor(params: IGameBsi, isProvider = false, IsGameType = false) {
    Object.assign(this, params);
    this.IsProvider = isProvider;
    this.IsGameType = IsGameType;
  }
}
class GameBsi implements IGameBsi {
  GameProviderShortName: string;
  IsHotGame: boolean;
  Rank: number;
  IsNewGame: boolean;
  IsJackpot: boolean;
  Device: number | null;
  TabType: string;
  GameProviderName: string;
  GameProviderId: number;
  GameId: number;
  Category: number;
  CategoryName: string;
  GameType: string;
  GameName: string;
  EnglishGameName: string;
  IsEnabled: boolean;
  IsMaintainByGameProvider: boolean;
  IsMaintain: boolean;
  ForMobile: boolean;
  ForDesktop: boolean;
  IsPromotionGame: boolean;
  BsiPriority: number;
  AsiPriority: number;
  GameIconUrl: string;
  EnabledCurrency: string;
  GameCode: string | null;
  Platform?: number | null;
  APIGameProviderName: string;
  APIGameProviderId: number;
  DisplayOrder: number;
  IsEnabledByGameProvider: boolean;
  LinkOption?: string;
  IsHotGameProvider?: boolean;
  IsNewGameProvider?: boolean;
  GameProviderDisplayName?: string;

  get DisplayName(): string {
    if (
      ["cock fight", "poker", "lottery"].includes(this.TabType.toLowerCase())
    ) {
      return this.EnglishGameName
        ? handleRemoveSpaceSpecialCharFormatted(this.EnglishGameName)
        : "";
    }
    if ([3].includes(this.Category) && this.GameType.toLowerCase() !== "none") {
      return handleRemoveSpaceSpecialCharFormatted(this.GameType);
    }
    return this.EnglishGameName
      ? handleRemoveSpaceSpecialCharFormatted(this.EnglishGameName)
      : "";
  }
  get CategoryType(): string {
    if (this.CategoryName === "GAMES") {
      return "isGame";
    }
    if (
      [1, 4].includes(this.Category) ||
      ["poker", "cock fight"].includes(this.TabType.toLowerCase())
    ) {
      return "isSport";
    }
    return "";
  }
  get GameProviderNameForDisplay(): string {
    const isTcgLottery = this.GameProviderName.toLowerCase().includes("tcg");
    return handleRemoveSpaceSpecialCharFormatted(isTcgLottery ? this.EnglishGameName : this.GameProviderName);
  }

  get GameNameForDisplay(): string {
    return useModifyGameName(this);
  }
  getImageName(): string {
    let imageName = this.DisplayName;
    if (this.DisplayName.includes("568win")) {
      imageName = "568win";
    }
    if (this.Category === 4 && this.GameProviderNameForDisplay === "sbobet") {
      imageName = "sbo";
    }
    if ((this.Category === 2 || this.Category === 3) && this.DisplayName.includes("sbo")) {
      imageName = "sbo";
    }
    if (this.DisplayName.includes("sagaming")) {
      imageName = "sagaming";
    }
    return imageName;
  }
  get IconForDisplay(): string {
    let path = `productHover/${this.getImageName()}logo.png`;
    if (this.Category === 1) {
      path = `sportPage/${this.getImageName()}logo.png`;
    }
    return commonHelper.handleImportImage("light_flexible_1", path);
  }

  get ActiveIconForDisplay(): string {
    let path = `productHover/activeIco/${this.getImageName()}logo.png`;
    if (this.Category === 1) {
      path = `sportPage/${this.getImageName()}logoactive.png`;
    }
    return commonHelper.handleImportImage("light_flexible_1", path);
  }

  get GameImage(): string {
    const imageName = this.GameProviderNameForDisplay;
    if (imageName === "lobby") {
      return "mpokerlobby";
    }
    return imageName;
  }

  get GameLogo(): string {
    if (this.GameImage === "568wingames") {
      return "568wingames" ;
    }
    if (
      this.GameImage.includes("568win") &&
      this.GameImage !== "568wingaming"
    ) {
      return "568win";
    }
    if (this.GameImage.includes("sbo")) {
      return "sbo";
    }
    if (this.GameImage === "mpokerlobby") {
      return "mpokerlobby";
    }
    return this.GameImage;
  }

  get GameImageForDisplay(): string {
    const path = `productHover/${this.GameImage}.png`;
    return commonHelper.handleImportImage("light_flexible_1", path);
  }

  get GameLogoForDisplay(): string {
    const path = `productHover/${this.GameLogo}logo.png`;
    return commonHelper.handleImportImage("light_flexible_1", path);
  }

  get SlidesPerView(): number {
    if (["GAMES", "LIVE_CASINO", "LOTTERY"].includes(this.CategoryName)) {
      return 5;
    }
    return 3;
  }
  get IntroForDisplay(): { short: string, long: string } {
    const isSportCate = this.CategoryName === 'SPORTS';
    const providerName = this.GameProviderName.toLocaleLowerCase().replace(/\s+/g, '');
    const englistName = this.EnglishGameName.toLocaleLowerCase().replace('lobby', '').trim();
    const isSboBet = providerName === 'sbobet';
    const useSameProviderIntro = ['sexygaming', 'sagaming', 'sabasports', 'wcasino'];
    if (useSameProviderIntro.includes(providerName)) {
      return {
        short: `${providerName}_short_intro`,
        long: `${providerName}_long_intro`,
      };
    }

    if (isSboBet && isSportCate) {
      return {
        short: 'sbo_sports_short_intro',
        long: 'sbo_sports_long_intro',
      };
    }

    if (englistName.includes('568win')) {
      return {
        short: '568wingames_short_intro',
        long: '568wingames_long_intro',
      };
    }

    const formattedName = englistName.replace(/ /g, "");

    return {
      short: `${formattedName}_short_intro`,
      long: `${formattedName}_long_intro`,
    };
  }

  constructor(params: IGame | IGameBsi) {
    Object.assign(this, params);
  }
}

interface ISeamlessProviderList {
  ProviderName: string;
  HasNewGame: boolean;
  HasPromoGame: boolean;
}

interface IGetBsiGameListResponse {
  Games: IGameBsi[];
  TabInfos: ITabInfo[];
  SeamlessGameGameTypeList: string[];
  SeamlessProviderList: ISeamlessProviderList[];
  SeamlessGameTabList: string[];
  IsSuccess: boolean;
}
interface IGameTypeProviderList {
  SeamlessGameGameTypeList: string[];
  SeamlessProviderList: ISeamlessProviderList[];
  SeamlessGameTabList: string[];
  IsSuccess: boolean;
}

interface IGetMenuTabResponse {
  TabInfos: ITabInfo[];
  IsSuccess: boolean;
}

class GetBsiGameListResponse implements IGetBsiGameListResponse {
  Games: IGameBsi[];
  IsSuccess: boolean;
  SeamlessGameGameTypeList: string[];
  SeamlessGameTabList: string[];
  SeamlessProviderList: ISeamlessProviderList[];
  TabInfos: ITabInfo[];
  get SortedGame(): IGameBsi[] {
    return textHelper.sortedGames(this.Games) as IGameBsi[];
  }

  get LobbyGames(): IGameBsi[] {
    return this.SortedGame.filter(
      (game) =>
        game.Category === 3 &&
        game.ForMobile &&
        !game.IsMaintain &&
        game.IsEnabled
    );
  }

  get PopularGamesForDisplay(): IGameBsi[] {
    return this.LobbyGames.filter((game) => game.IsHotGame).slice(0, 7);
  }

  get NewGamesForDisplay(): IGameBsi[] {
    return this.LobbyGames.filter((game) => game.IsNewGame).slice(0, 7);
  }

  constructor(params: IGetBsiGameListResponse) {
    Object.assign(this, params);
  }
}

interface IGetAsiGameListResponse {
  Games: Array<IGameBsi>;
  IsSuccess: boolean;
}

class GetAsiGameListResponse implements IGetAsiGameListResponse {
  Games: Array<IGameBsi>;
  IsSuccess: boolean;
  // eslint-disable-next-line
  set gameFilterForDestop(key: number) {
    const tem = this.Games.filter(
      (item) => item.Category === key && item.ForDesktop && item.IsEnabled
    );
    this.Games = tem.filter((item) => {
      if (item.GameProviderId !== null) {
        return item.GameProviderId === 46;
      }
      return item;
    });
  }

  get SortedGame(): IGameBsi[] {
    return _.sortBy(this.Games, [
      (game) => (game.AsiPriority ? game.AsiPriority : undefined),
      "GameName",
      "DisplayOrder",
    ]);
  }

  constructor(init: IGetAsiGameListResponse) {
    Object.assign(this, init);
  }
}
class GameFilterRequest {
  Type: string;
  Value: string;

  constructor(params: GameFilterRequest) {
    Object.assign(this, params);
  }
}

const { convertString } = textHelper;

interface IGameProviders {
  ProviderName: string;
  HasNewGame: boolean;
  HasPromoGame: boolean;
  IsMaintain: boolean;
  displayOrder: number;
  isNewProvider: boolean;
  GameProviderDisplayName: string | undefined;
  isHotProvider: boolean | undefined;
  NameForDisplay: string;
}

class GameProviders implements IGameProviders {
  ProviderName: string;
  HasNewGame: boolean;
  HasPromoGame: boolean;
  IsMaintain: boolean;
  displayOrder: number;
  isNewProvider: boolean;
  GameProviderDisplayName: string | undefined;
  isHotProvider: boolean | undefined;
  NameForDisplay: string;

  get providerLogo(): string {
    try {
      const path = `common/providers/logo/${convertString(
        this.ProviderName,
        false,
        ""
      )}.png`;
      return getAssetImage(`assets/${path}`);
    } catch (error) {
      return "";
    }
  }

  get starLogo(): string {
    try {
      return getAssetImage(
        `assets/theme/common/provider_icon/games/${this.ProviderName.replace(
          /\s+/g,
          "_"
        ).toLowerCase()}/star.png`
      );
    } catch (e) {
      return "";
    }
  }

  get BackgroundGeometryImage(): string {
    return getAssetImage("assets/common/geometry/bg-games.png");
  }

  constructor(init: IGameProviders) {
    Object.assign(this, init);
  }
}

export {
  GameFilterRequest,
  IRequestAsiGame,
  GetBsiGameListResponse,
  IGetBsiGameListResponse,
  IGameBsi,
  ISeamlessProviderList,
  GetAsiGameListResponse,
  IGetAsiGameListResponse,
  GameBsi,
  GameBsiForLFM1,
  IGetMenuTabResponse,
  IGameTypeProviderList,
  IGameProviders,
  GameProviders,
};
