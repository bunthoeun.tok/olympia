import { EnumMessage, EnumMessageStatus, EnumUnreadInfoMapper } from "./enums/enumMessage";
import textHelper from "~~/utils/formatHelpers/textFormatHelper";

export interface IGetSiteMessageRequest {
  Type: EnumMessage;
  Page: number;
  Language: string;
  IsUnread: boolean;
  RowCountPerPage: number;
}

export type MemberMessageUnreadInfo = {
  [key in EnumUnreadInfoMapper]: number;
};

export interface IContents {
  Title: string;
  Language: string;
  Content: string;
}

export interface IMessages {
  Id: number;
  Type: number;
  IsUnread: boolean;
  Contents: IContents;
  CreatedOn: Date;
}

export interface IGetMessageResponse {
  UnreadInfo: MemberMessageUnreadInfo;
  Messages: Array<IMessages>;
  MaxPage: number;
  RowNumber: number;
  IsSuccess: boolean;
  ErrorCode: number;
  Message: string;
}

export interface IUpdateMessageStatus {
  Ids: Array<number>;
  Status: EnumMessageStatus;
}

export type TMemberMessageTabs = keyof typeof EnumMessage;

export class MemberMessage implements IMessages {
  Id!: number;
  Type!: number;
  IsUnread!: boolean;
  Contents!: IContents;
  CreatedOn!: Date;

  constructor(params: IMessages) {
    Object.assign(this, params);
  }

  get DateDisplay(): string {
    return textHelper.dateStringTo24HourClock(String(this.CreatedOn));
  }

  get TypeDisplay(): string {
    return EnumMessage[this.Type];
  }
}
