export type IGetLuckyWheelOptionsRequest = {
  PromotionId: number;
  WebId: number;
  Currency: string;
  SimulateId: number;
  OperatorId: number;
  OnlineId: number;
};

export type ILuckyWheelOptions = {
  Id: number;
  BonusAmount: number;
};

export type IGetLuckyWheelOptionsResponse = {
  LuckyWheelOptions: ILuckyWheelOptions[];
};

export type IGetLuckyWheelSpinResultRequest = {
  PromotionId: number;
  Device: string;
  FingerPrint: string;
  Ip: string;
  Url: string;
};

export type IResult = {
  OptionId: number;
  BonusAmount: number;
  RemainingTicketCount: number;
  IsPromotionEnded: boolean;
};

export type IGetLuckyWheelSpinResultResponse = {
  Result: IResult;
  TraceId: number;
};

export type ISpinState = {
  isDefault: boolean;
  isSpinning: boolean;
  isStoppedSpin: boolean;
  settled: boolean;
};

export type IGetAvailableLuckyWheelTickets = {
  CustomerId: number;
  OnlineId: number;
};

export type IAvailableTickets = {
  PromotionId: number;
  TicketCount: number;
  HomepageEntrance: boolean;
};

export type IGetAvailableLuckyWheelTicketsResponse = {
  AvailableTickets: IAvailableTickets[];
  ErrorCode: number;
  ErrorMessage: string;
  IsSuccess: boolean;
};
