import { IBaseResponse } from "./baseResponse";

export interface ICompanyBanksDto {
  Id: number;
  BankName: string;
  BankAccountName: string;
  BankAccountNumber: string;
  Currency: string;
  Group: number;
  Status: number;
  DepositMin: number;
  DepositMax: number;
  WithdrawalMin: number;
  WithdrawalMax: number;
  BankBalance?: number;
  IsAutoDeposit: boolean;
  Identifier: string;
  BankLogo: string;
}

export interface IPlayerBanksDto {
  Id: number;
  BankName: string;
  Priority: number;
  WithdrawMax: number;
  WithdrawMin: number;
  BankLogo: string;
}

export interface IPlayerBankListAfterFirstTimeDepositDto {
  CustomerId: number;
  BankGroup: number;
  CompanyBankName: string;
  CompanyBankAccountName: string;
  CompanyBankAccountNumber: string;
  PlayerBankId: number;
  PlayerBankName: string;
  PlayerBankAccountNumber: string;
  PlayerBankAccountName: string;
  BankLogo: string;
}
// export class PlayerBankListAfterFirstTimeDepositDto
//   implements IPlayerBankListAfterFirstTimeDepositDto
// {
//   CustomerId: number;
//   BankGroup: number;
//   CompanyBankName: string;
//   CompanyBankAccountName: string;
//   CompanyBankAccountNumber: string;
//   PlayerBankId: number;
//   PlayerBankName: string;
//   PlayerBankAccountNumber: string;
//   PlayerBankAccountName: string;
//   BankLogo: string;
//   get ImagePathForDisplay(): string {
//     return depositBankImageHelper.getImagePathBankLogo(this.BankLogo);
//   }
// }

export interface IAvailablePropertyListDto {
  PropertyStatus: string;
  PropertyName: string;
  DisplayOrder: number;
  PropertyNameForDisplay: string;
}

export interface IGetTransactionBankListResponse extends IBaseResponse {
  CompanyBanks: Array<ICompanyBanksDto>;
  PlayerBankOptions: Array<IPlayerBanksDto>;
  PlayerBankListAfterFirstTimeDeposit: Array<IPlayerBankListAfterFirstTimeDepositDto>;
  IsTransferMethodDepositEnable: boolean;
  IsTransferMethodWithdrawalEnable: boolean;
  IsTransferMethodEnable: boolean;
  IsDecimalAllow: boolean;
  AvailablePropertyList: Array<IAvailablePropertyListDto>;
}
