export interface IQrCodePayout {
    PayoutId: number;
    Token: string;
    ErrorCode: number;
    ErrorMessage: string;
    IsSuccess: boolean;
}