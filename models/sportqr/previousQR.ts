import numberFormatHelper from '~~/utils/formatHelpers/numberFormatHelper';
import textHelper from '~~/utils/formatHelpers/textFormatHelper';

export interface IPreviousSession {
  PayoutId: number;
  CustomerId: number;
  Username: string;
  Token: string;
  PayoutTime: string;
  TotalActualStake: number;
  TotalStake: number;
}

export class PreviousSession implements IPreviousSession {
  PayoutId: number;
  CustomerId: number;
  Username: string;
  Token: string;
  PayoutTime: string;
  TotalActualStake: number;
  TotalStake: number;

  get PayoutDateForDisplay(): string {
    return textHelper.getDate(this.PayoutTime, false);
  }

  get PayoutTimeForDisplay(): string {
    return textHelper.get12HourTime(this.PayoutTime);
  }

  get TotalStakeForDisplay(): string {
    return numberFormatHelper.addAccountingFormat(this.TotalStake);
  }

  constructor(init: IPreviousSession) {
    Object.assign(this, init);
  }
}

export interface IPreviousQR {
  PreviousSessions: IPreviousSession[];
  ErrorCode: number;
  ErrorMessage: string;
  IsSuccess: boolean;
}
