export interface IPayTicketsRequest {
    WebId: number;
    PayoutId: string;
    Token: string;
    Amount: number;
    SimulateId: number;
    OperatorId: number;
    OnlineId: number;
}
export interface IRedeemRequest {
    RefNo: string;
    Token: string;
    WebId: number;
    SimulateId: number;
    OperatorId: number;
    OperatorToken: string;
    OnlineId: number;
}
export interface IRedeemResponse {
    ErrorCode: number;
    ErrorMessage: string;
}