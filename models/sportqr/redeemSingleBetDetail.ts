import numberFormatHelper from '~~/utils/formatHelpers/numberFormatHelper';
import EnumSportBetStatus from '../enums/enumSportBetStatus';
import { SportBet } from "../sportbet/sportbet"
import EnumOperationStatus from '../enums/enumOperationStatus';

export interface IAllSportBetsRequest {
    PayoutId: String;
    Token: String;
}
export interface IAllSportBetsResponse {
    SportBets: SportBet[];
    ErrorCode: Number;
    ErrorMessage: String;
}
export class AllSportBetsResponse implements IAllSportBetsResponse {
    SportBets: SportBet[];
    ErrorCode: Number;
    ErrorMessage: String;

    get unPaidSportBets(): SportBet[] {
        return this.SportBets.filter((bet) => bet.OperationStatus.toLocaleLowerCase() === EnumOperationStatus.unpaid && bet.Status.toLocaleLowerCase() === EnumSportBetStatus.running);
    }

    get totalStakeUnpaid(): number {
        return this.unPaidSportBets.reduce((total, bet) => total + bet.ActualStake, 0);
    }

    get totalStakeUnpaidForDisplay(): number {
        return this.unPaidSportBets.reduce((total, bet) => total + bet.Stake, 0) ;
    }

    get PaidSportBets(): SportBet[] {
        return this.SportBets.filter((bet) => !(bet.OperationStatus.toLocaleLowerCase() === EnumOperationStatus.unpaid && bet.Status.toLocaleLowerCase() === EnumSportBetStatus.running));
    }

    get totalStakePaid(): number {
        return this.PaidSportBets.reduce((total, bet) => total + bet.ActualStake, 0);
    }

    get totalStakePaidForDisplay(): number {
        return this.PaidSportBets.reduce((total, bet) => total + bet.Stake, 0);
    }
    
    get totalPayoutForDisplay(): number {
        return this.PaidSportBets.reduce((total, bet) => total + bet.Payout, 0);
    }

    get currency(): string {
        return this.SportBets[0].Currency;
    }

    constructor(init: IAllSportBetsResponse) {
        Object.assign(this, init);
        this.SportBets = init.SportBets.map((bet) => new SportBet(bet));
    }
}