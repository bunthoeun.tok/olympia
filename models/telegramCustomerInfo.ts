export interface ICustomerInfoRequest {
  CustomerId: number,
  BotId: number,
  ChatId: number,
  TelegramUserId: number,
}