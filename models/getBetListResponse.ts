import { ISeamlessGameBet, SeamlessGameBet } from "./seamlessGameBets";

export interface IGetBetListResponse {
  SeamlessGameBets: Array<ISeamlessGameBet>;
  TotalWinLose: number;
  TotalCommission: number;
  ErrorCode: number;
  ErrorMessage: string;
  IsSuccess: boolean;
}

export class GetBetListResponse {
  SeamlessGameBets: Array<ISeamlessGameBet>;
  TotalWinLose: number;
  TotalCommission: number;
  ErrorCode: number;
  ErrorMessage: string;
  IsSuccess: boolean;
  constructor(init: IGetBetListResponse) {
    Object.assign(this, init);
    this.SeamlessGameBets = init.SeamlessGameBets.map(
      (gameBets) => new SeamlessGameBet(gameBets)
    );
  }
}
