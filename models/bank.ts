export interface IGetRegisterBankListRequest {
  Domain: string;
  Currency: string;
}

export interface IPlayerBankOption {
  Id: number;
  BankName: string;
  Priority: number;
}

export interface IGetRegisterBankListResponse {
  PlayerBankOptions: Array<IPlayerBankOption>;
  IsSuccess: boolean;
}
