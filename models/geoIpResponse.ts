export default interface IGetGeography {
  Ip: string;
  CountryCode: string;
  City: string;
  Region: string;
  IsOfficeIp: boolean;
  Currency: string;
  Continent: string;
  ContinentCode: string;
}
