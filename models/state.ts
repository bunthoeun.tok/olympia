import { ILanguage } from "./common/language";
import { EnumDisplayTimeZone } from "./enums/enumDisplayTimeOption";
import { IGameTypeProviderList } from "@/models/bsiGameList";
import {
  IGetGameProviderListResponse,
  ITabInfo,
} from "@/models/IGetPlayableGamesResponse";
import { ICompanyThemePropertiesFromApi } from "@/models/companyThemePropertiesRespone";
import IGetGeography from "@/models/geoIpResponse";
import { IAllCustomerSettingsData } from "@/models/getCustomerSettingResponse";
import { MemberMessageUnreadInfo } from "@/models/message";
import { EnumSelectedThemeMode } from "@/models/enums/enumSelectedThemeMode";

export interface IState {
  webId: number;
  auth: boolean;
  token: string;
  gameProviders: IGetGameProviderListResponse /* todo */;
  gameTabProviderList: IGameTypeProviderList;
  username: string;
  currency: string;
  customerId: number;
  onlineId: number;
  geography: IGetGeography;
  displayLocalTime: boolean;
  displayTimezone: EnumDisplayTimeZone;
  selectedThemeName: string;
  themeName: string;
  prefixSelectedTheme: string;
  isThemePropertyLoading: boolean;
  isGameUpdateLoading: boolean;
  isSuspended: boolean;
  isCashPlayer: boolean;
  isStockPlayer: boolean;
  isPasswordExpired: boolean;
  isLoginNameUpdated: boolean;
  isPaymentPasswordSet: boolean;
  isImageLess1MBEnabled: boolean;
  tabInfos: ITabInfo[];
  companyThemePropertiesFromApi: ICompanyThemePropertiesFromApi;
  skipLogin: boolean;
  notifications: [];
  showAgentCooperationDialog: boolean;
  showRegisterDialog: boolean;
  showLoginDialog: boolean;
  showDepositDialog: boolean;
  showSidebar: boolean;
  locale: string;
  imageVersion: string;
  languages: ILanguage[];
  isReferralEnabled: Boolean;
  IsShowReferralPage: Boolean;
  isPerushopEnabled: Boolean;
  umData: {
    statusCode: number;
    expireTime: number;
  };
  isV2ReferralEnabled: boolean;
  visitorId: string;
  isCockfightWhitelist: boolean;
  requestedOtpDate: string;
  allCustomerSettings: IAllCustomerSettingsData[];
  isStatementLoading: boolean;
  isLandingPage: boolean;
  gtmIdInternal: string;
  sideBarDirection: string;
  selectedThemeMode: EnumSelectedThemeMode;
  isSwitchComponentCallBefore: boolean;
}
