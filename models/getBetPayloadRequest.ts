export default interface IGetBetPayloadRequest {
  RefNo: string;
  Language: string;
  GameProviderId: number;
  GameProviderType: number;
}
