import _ from "lodash";
import EnumTransactionStatus from "./enums/enumTransactionStatusForLogic";
import EnumTransactionStatusDisplay from "./enums/enumTransactionStatusForDisplay";
import EnumColor from "./enums/enumColor";
import textHelper from "@/utils/formatHelpers/textFormatHelper";
import numberFormatHelper from "@/utils/formatHelpers/numberFormatHelper";

import commonHelper from "@/utils/commonHelper";

const statusColorMapping: Record<string, EnumColor> = {
  [EnumTransactionStatusDisplay.Completed]: EnumColor.ColorSuccess,
  [EnumTransactionStatusDisplay.Succeed]: EnumColor.ColorSuccess,
  [EnumTransactionStatusDisplay.Approved]: EnumColor.ColorSuccess,

  [EnumTransactionStatusDisplay.Rejected]: EnumColor.ColorFailure,
  [EnumTransactionStatusDisplay.Canceled]: EnumColor.ColorFailure,
  [EnumTransactionStatusDisplay.Error]: EnumColor.ColorFailure,

  [EnumTransactionStatusDisplay.Waiting]: EnumColor.ColorText,
  [EnumTransactionStatusDisplay.Verified]: EnumColor.ColorText,
  [EnumTransactionStatusDisplay.Processing]: EnumColor.ColorText,
  [EnumTransactionStatusDisplay.Pending]: EnumColor.ColorText,
};

const statusForDisplayMapping: Record<string, EnumTransactionStatusDisplay> = {
  [EnumTransactionStatus.Waiting]: EnumTransactionStatusDisplay.Waiting,
  [EnumTransactionStatus.Verified]: EnumTransactionStatusDisplay.Verified,
  [EnumTransactionStatus.Approved]: EnumTransactionStatusDisplay.Approved,

  [EnumTransactionStatus.Rejected]: EnumTransactionStatusDisplay.Rejected,
  [EnumTransactionStatus.PaymentAdminRejected]:
    EnumTransactionStatusDisplay.Rejected,
  [EnumTransactionStatus.PaymentRejected]:
    EnumTransactionStatusDisplay.Rejected,
  [EnumTransactionStatus.PaymentRejectedByCompany]:
    EnumTransactionStatusDisplay.Rejected,
  [EnumTransactionStatus.PaymentSuccessRollbackCreditToPlayer]:
    EnumTransactionStatusDisplay.Rejected,

  [EnumTransactionStatus.Canceled]: EnumTransactionStatusDisplay.Canceled,
  [EnumTransactionStatus.PaymentCanceled]:
    EnumTransactionStatusDisplay.Canceled,

  [EnumTransactionStatus.PaymentStarted]:
    EnumTransactionStatusDisplay.Processing,
  [EnumTransactionStatus.PaymentTransferSuccess]:
    EnumTransactionStatusDisplay.Succeed,
  [EnumTransactionStatus.PaymentProcessing]:
    EnumTransactionStatusDisplay.Processing,
  [EnumTransactionStatus.PaymentSuccessHoldOnCreditFromPlayer]:
    EnumTransactionStatusDisplay.Processing,
  [EnumTransactionStatus.PaymentAdminWaiting]:
    EnumTransactionStatusDisplay.Processing,
  [EnumTransactionStatus.PaymentAdminVerified]:
    EnumTransactionStatusDisplay.Processing,
  [EnumTransactionStatus.PaymentAdminApproved]:
    EnumTransactionStatusDisplay.Processing,

  [EnumTransactionStatus.PaymentPending]: EnumTransactionStatusDisplay.Pending,
  [EnumTransactionStatus.PaymentSuccessDepositToPlayer]:
    EnumTransactionStatusDisplay.Succeed,
  [EnumTransactionStatus.PaymentPaid]: EnumTransactionStatusDisplay.Succeed,

  [EnumTransactionStatus.Completed]: EnumTransactionStatusDisplay.Completed,
  [EnumTransactionStatus.Manual]: EnumTransactionStatusDisplay.Completed,
};

const { Pending, Succeed, Waiting, Verified, Approved, Completed, Processing } =
  EnumTransactionStatusDisplay;

const statusProcessing = [Verified, Waiting, Pending, Processing];
const statusSuccess = [Succeed, Approved, Completed];

export interface ITransaction {
  TransactionNo: string;
  RequestedTime: string;
  PlayerBankName: string;
  BranchName: string;
  PlayerBankAccountNumber: string;
  PlayerBankAccountName: string;
  BeforeBalance: number;
  AfterBalance: number;
  Currency: string;
  RequestedAmount: number;
  VerifiedAmount: number;
  CompanyBankName: string;
  CompanyBankAccountName: string;
  CompanyBankId?: string;
  CompanyBankAccountNumber: string;
  Email: string;
  Type: string;
  Status: string;
  PlayerRemark: string;
  Remark: string;
  IsPayment: boolean;
  ReferenceNumber: string;
  ModifiedBy: string;
  IsUsingBank: boolean;
  SlipImage: string;
  DepositTime: string;
  MaxPage: number;
  RowNumber: number;
  DisplayTransactionNo: string;
  IsSuccess: boolean;
}
export class TransactionDto implements ITransaction {
  TransactionNo: string;
  RequestedTime: string;
  PlayerBankName: string;
  BranchName: string;
  PlayerBankAccountNumber: string;
  PlayerBankAccountName: string;
  BeforeBalance: number;
  AfterBalance: number;
  Currency: string;
  RequestedAmount: number;
  VerifiedAmount: number;
  CompanyBankName: string;
  CompanyBankAccountName: string;
  CompanyBankAccountNumber: string;
  CompanyBankId?: string;
  Email: string;
  Type: string;
  Status: string;
  PlayerRemark: string;
  Remark: string;
  IsPayment: boolean;
  ReferenceNumber: string;
  ModifiedBy: string;
  IsUsingBank: boolean;
  SlipImage: string;
  DepositTime: string;
  MaxPage: number;
  RowNumber: number;
  DisplayTransactionNo: string;
  IsSuccess: boolean;
  get IsCancelable(): boolean {
    return _.toLower(this.Status) === "waiting";
  }
  get RequestedAmountForDisplay(): string {
    return numberFormatHelper.addAccountingFormat(
      this.RequestedAmount,
      2,
      false
    );
  }
  get RequestedTimeForDisplay(): string {
    return textHelper.dateStringTo12HourClock(this.RequestedTime);
  }
  get StatusForDisplay(): EnumTransactionStatusDisplay {
    const status = _.toLower(this.Status);
    return (
      statusForDisplayMapping[status] ?? EnumTransactionStatusDisplay.Error
    );
  }
  get ColorClass(): EnumColor {
    return statusColorMapping[this.StatusForDisplay] ?? EnumColor.ColorText;
  }
  get ApprovedAmountForDisplay(): string {
    if (statusProcessing.includes(this.StatusForDisplay)) {
      return "--";
    }
    if (statusSuccess.includes(this.StatusForDisplay)) {
      return numberFormatHelper.addAccountingFormat(
        this.VerifiedAmount,
        2,
        false
      );
    }
    return numberFormatHelper.addAccountingFormat(0, 2, false);
  }

  constructor(init: ITransaction) {
    Object.assign(this, init);
    this.DepositTime = init.DepositTime
      ? textHelper.dateStringTo12HourClock(init.DepositTime)
      : "--";
  }
}

interface IGetTransactionHistoryRequest {
  TransType: string;
  Status: string;
  RowCountPerPage: number;
  Page: number;
  StartDate: string;
  EndDate: string;
  IsGetAll: boolean;
}

export class GetTransactionHistoryRequest
  implements IGetTransactionHistoryRequest
{
  TransType: string;
  Status = "all";
  RowCountPerPage: number;
  Page: number;
  StartDate: string;
  EndDate: string;
  IsGetAll: boolean;

  constructor() {
    Object.assign(this);
    const today = commonHelper.getDateRange("today");
    this.StartDate = textHelper.getGmtMinusFourDate(String(today.startDate));
    this.EndDate = textHelper.getGmtMinusFourDate(String(today.endDate));
  }
}

export class GetTransactionHistoryResponse {
  Transactions: Array<ITransaction>;
  IsSuccess: boolean;
  constructor(init: GetTransactionHistoryResponse) {
    Object.assign(this, init);
    this.Transactions = init.Transactions.map(
      (item) => new TransactionDto(item)
    );
  }
}

export interface IProceedTransactionRequest {
  IsPayment: boolean;
  Amount: number;
  FromStatus: string;
  ToStatus: string;
  Remark: string;
  CompanyBankId: number;
  TransType: string;
  TransactionNo: string;
  BankCode: string;
  PaymentProviderId: number;
  PaymentProviderName: string;
  AddWithdrawalLimitAmount: number;
  Currency: string;
}
