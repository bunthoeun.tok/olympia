export interface IRequestWithdrawalBankTransfer {
  PlayerBankName: string;
  PlayerBankAccountName: string;
  PlayerBankAccountNumber: string;
  BranchName: string;
  CustomizeBankName: string;
  CPFNumber: string;
  BSB: string;
  PayId: string;
  Amount: number;
  PaymentPassword: string;
  Url: string;
  FingerPrint: string;
  OnlineId: number;
  Domain?: string;
  Language?: string;
}

interface IExtraParameter {
  pixnumber?: string,
  pixtype?: string,
  name?: string,
  cpfnumber?: string,
}
export interface IRequestWithdrawalInternetBank {
  Amount: number;
  PaymentPassword: string;
  BankCode: string;
  PlayerBankId: number;
  PlayerBankAccountName: string;
  PlayerBankAccountNumber: string;
  PaymentMethod: number;
  ReturnUrl: string;
  FailedUrl: string;
  BankName: string;
  PpId: number;
  WalletAddress: string;
  CryptoCurrencyNetwork: string;
  OnlineId: number;
  ExtraParameter?: IExtraParameter;
}

export interface IGetWithdrawalInternetBankReponse {
  Currency: string;
  RequestAmount: number;
  TransactionStatus: number;
  TransactionStatusMessage: string;
  ExRefNo: string;
  RefNo: string;
  UserBankAccount: string;
  BankCode: string;
  BankName: string;
  DeclineReason: string;
}

export interface IRequestSetPaymentPassword {
  PaymentPassword: string;
  LoginPassword: string;
}
