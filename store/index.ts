import { InjectionKey } from "vue";
import { createStore, useStore as baseUseStore, Store } from "vuex";
import createPersistedState from "vuex-persistedstate";
import { ICompanyThemePropertiesFromApi } from "@/models/companyThemePropertiesRespone";
import {
  IGetGameProviderListResponse,
  ITabInfo,
} from "@/models/IGetPlayableGamesResponse";
import { IGameBsi, IGameTypeProviderList } from "@/models/bsiGameList";
import { IState } from "@/models/state";
import IGetGeography from "@/models/geoIpResponse";
import { ILanguage } from "@/models/common/language";
import { IAllCustomerSettingsData } from "@/models/getCustomerSettingResponse";
import EnumMutation from "~~/models/enums/enumMutation";
import { MemberMessageUnreadInfo } from "~~/models/message";
import { EnumDisplayTimeZone } from "~~/models/enums/enumDisplayTimeOption";
import { EnumSelectedThemeMode } from "~~/models/enums/enumSelectedThemeMode";

const plugins = [];
if (process.client) {
  plugins.push(createPersistedState());
}
export const key: InjectionKey<Store<IState>> = Symbol("Copy From Vuex Doc");

export const store = createStore<IState>({
  state: {
    webId: 0,
    auth: false,
    token: "",
    gameProviders: {} as IGetGameProviderListResponse /* todo */,
    gameTabProviderList: {} as IGameTypeProviderList,
    currency: "",
    username: "",
    customerId: 0,
    onlineId: 0,
    geography: {} as IGetGeography,
    selectedThemeName: "",
    themeName: "",
    prefixSelectedTheme: "",
    displayLocalTime: true,
    displayTimezone: EnumDisplayTimeZone.MyComputerTime,
    isSuspended: false,
    isCashPlayer: false,
    isStockPlayer: false,
    isPasswordExpired: false,
    isLoginNameUpdated: false,
    isPaymentPasswordSet: false,
    isThemePropertyLoading: true,
    isGameUpdateLoading: true,
    isImageLess1MBEnabled: false,
    tabInfos: [] as ITabInfo[],
    companyThemePropertiesFromApi: {} as ICompanyThemePropertiesFromApi,
    skipLogin: false,
    notifications: [],
    showAgentCooperationDialog: false,
    showRegisterDialog: false,
    showLoginDialog: false,
    showDepositDialog: false,
    showSidebar: false,
    locale: "",
    imageVersion: "",
    languages: [] as ILanguage[],
    isReferralEnabled: false,
    IsShowReferralPage: false,
    isPerushopEnabled: false,
    isCockfightWhitelist: false,
    umData: {
      statusCode: 0,
      expireTime: 0,
    },
    isV2ReferralEnabled: false,
    visitorId: "",
    requestedOtpDate: "",
    allCustomerSettings: [] as IAllCustomerSettingsData[],
    isStatementLoading: false,
    isLandingPage: false,
    gtmIdInternal: "",
    sideBarDirection: "ltr",
    selectedThemeMode: EnumSelectedThemeMode.Light,
    isSwitchComponentCallBefore: false,
  },
  mutations: {
    [EnumMutation.updateWebId](state, webId) {
      state.webId = webId;
    },
    [EnumMutation.updateAuth](state, auth) {
      state.auth = auth;
    },
    [EnumMutation.updateToken](state, token) {
      state.token = token;
    },
    [EnumMutation.updateGames](state, games: IGameBsi[]) {
      state.games = games;
    },
    [EnumMutation.updateCurrency](state, currency) {
      state.currency = currency;
    },
    [EnumMutation.updateGeography](state, geography) {
      state.geography = geography;
    },
    [EnumMutation.updateIsThemePropertyLoading](state, loading) {
      state.isThemePropertyLoading = loading;
    },
    [EnumMutation.updateIsGameUpdateLoading](state, loading) {
      state.isGameUpdateLoading = loading;
    },
    [EnumMutation.updateSelectedThemeName](state, theme: string) {
      state.selectedThemeName = theme;
    },
    [EnumMutation.updateOriginalThemeName](state, themeName: string) {
      state.themeName = themeName;
    },
    [EnumMutation.updatePrefixSelectedTheme](state, prefix: string) {
      state.prefixSelectedTheme = prefix;
    },
    [EnumMutation.updateDisplayLocalTime](state, local: boolean) {
      state.displayLocalTime = local;
    },
    [EnumMutation.updateDisplayTimeZone](state, timeZone: EnumDisplayTimeZone) {
      state.displayTimezone = timeZone;
    },
    [EnumMutation.updateIsSuspended](state, suspend: boolean) {
      state.isSuspended = suspend;
    },
    [EnumMutation.updateIsCashPlayer](state, cash: boolean) {
      state.isCashPlayer = cash;
    },
    [EnumMutation.updateIsStockPlayer](state, stock: boolean) {
      state.isStockPlayer = stock;
    },
    [EnumMutation.updateIsPasswordExpired](state, password: boolean) {
      state.isPasswordExpired = password;
    },
    [EnumMutation.updateIsLoginNameUpdated](state, name: boolean) {
      state.isLoginNameUpdated = name;
    },
    [EnumMutation.updateIsPaymentPasswordSet](state, payment: boolean) {
      state.isPaymentPasswordSet = payment;
    },
    [EnumMutation.updateTabInfos](state, tabs: ITabInfo[]) {
      state.tabInfos = tabs;
    },
    [EnumMutation.updateCompanyThemePropertiesFromApi](
      state,
      companyThemeProperties: ICompanyThemePropertiesFromApi
    ) {
      state.companyThemePropertiesFromApi = companyThemeProperties;
    },
    [EnumMutation.updateUsername](state, name: string) {
      state.username = name;
    },
    [EnumMutation.updateCustomerId](state, id: number) {
      state.customerId = id;
    },
    [EnumMutation.updateOnlineId](state, onlineId: number) {
      state.onlineId = onlineId;
    },
    [EnumMutation.updateIsImageLess1MBEnabled](state, enabled: boolean) {
      state.isImageLess1MBEnabled = enabled;
    },
    [EnumMutation.updateGameProviders](
      state,
      provider: IGetGameProviderListResponse
    ) {
      state.gameProviders = provider;
    },
    [EnumMutation.updateGameTabProviderList](
      state,
      provider: IGameTypeProviderList
    ) {
      state.gameTabProviderList = provider;
    },
    [EnumMutation.updateSkipLogin](state, status: boolean) {
      state.skipLogin = status;
    },
    [EnumMutation.updateNotification](state, status: []) {
      state.notifications = status;
    },
    [EnumMutation.updateAgentCooperationDialog](state, status: boolean) {
      state.showAgentCooperationDialog = status;
    },
    [EnumMutation.updateRegisterDialog](state, status: boolean) {
      state.showRegisterDialog = status;
    },
    [EnumMutation.updateLoginDialog](state, status: boolean) {
      state.showLoginDialog = status;
    },
    [EnumMutation.updateDepositDialog](state, status: boolean) {
      state.showDepositDialog = status;
    },
    [EnumMutation.updateSidebar](state, status: boolean) {
      state.showSidebar = status;
    },
    [EnumMutation.updateUm](state, status) {
      state.umData.statusCode = status.statusCode;
      state.umData.expireTime = status.expireTime;
    },
    [EnumMutation.updateLocale](state, locale: string) {
      state.locale = locale;
    },
    [EnumMutation.updateLanguage](state, lang: ILanguage[]) {
      state.languages = lang;
    },
    [EnumMutation.updateImageVersion](state, version: string) {
      state.imageVersion = version;
    },
    [EnumMutation.updateIsReferralEnabled](state, referral: boolean) {
      state.isReferralEnabled = referral;
    },
    [EnumMutation.updateIsPerushopEnabled](state, isPerushopEnabled: boolean) {
      state.isPerushopEnabled = isPerushopEnabled;
    },
    [EnumMutation.resetDefaultInformation](state) {
      state.auth = false;
      state.currency = "";
      state.token = "";
      state.username = "";
      state.onlineId = 0;
      state.isCashPlayer = false;
      state.isImageLess1MBEnabled = false;
      state.isLoginNameUpdated = false;
      state.isPasswordExpired = false;
      state.isPaymentPasswordSet = false;
    },
    [EnumMutation.updateIsV2ReferralEnabled](
      state,
      isV2ReferralEnabled: boolean
    ) {
      state.isV2ReferralEnabled = isV2ReferralEnabled;
    },
    [EnumMutation.updateIsShowReferralPage](
      state,
      IsShowReferralPage: boolean
    ) {
      state.IsShowReferralPage = IsShowReferralPage;
    },
    [EnumMutation.updateVisitorId](state, visitorId: string) {
      state.visitorId = visitorId;
    },
    [EnumMutation.updateIsCockfightWhitelist](
      state,
      isCockfightWhitelist: boolean
    ) {
      state.isCockfightWhitelist = isCockfightWhitelist;
    },
    [EnumMutation.updateRequestedOtpDate](state, date: string) {
      state.requestedOtpDate = date;
    },
    [EnumMutation.updateAllCustomerSettings](
      state,
      settings: IAllCustomerSettingsData[]
    ) {
      state.allCustomerSettings = settings;
    },
    [EnumMutation.updateIsStatementLoading](state, status: boolean) {
      state.isStatementLoading = status;
    },
    [EnumMutation.updateIsLandingPage](state, status: boolean) {
      state.isLandingPage = status;
    },
    [EnumMutation.updateGTMID](state, id: string) {
      state.gtmIdInternal = id;
    },
    [EnumMutation.updateSideBarDirection](state, direction: string) {
      state.sideBarDirection = direction;
    },
    [EnumMutation.updateSelectedThemeMode](state, selectedThemeMode: EnumSelectedThemeMode) {
      state.selectedThemeMode = selectedThemeMode;
    },
    [EnumMutation.updateIsSwitchComponentCallBefore](state, isSwitchComponentCallBefore: boolean) {
      state.isSwitchComponentCallBefore = isSwitchComponentCallBefore;
    },
  },
  actions: {},
  modules: {},
  plugins,
});
export function useStore(): Store<IState> {
  return baseUseStore(key);
}
