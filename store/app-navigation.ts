import { createStore } from "vuex";
import EnumMutation from "~~/models/enums/enumMutation";

export const appNavigationStore = createStore({
  state: {
    backToRoute: "/",
  },
  mutations: {
    [EnumMutation.updateAppNavigationBackToRoute]: (state, url: string) => {
      state.backToRoute = url;
    },
  },
});
