import { createStore, useStore as baseUseStore, Store } from "vuex";
import { IGameBsi } from "@/models/bsiGameList";

interface IGameStoreState {
  games: Array<IGameBsi>;
}

export const gameStore = createStore<IGameStoreState>({
  state: {
    games: [],
  },
  mutations: {
    updateGames(state, games: IGameBsi[]) {
      state.games = games;
    },
  },
});

export const key: InjectionKey<Store<IGameStoreState>> =
  Symbol("IGameStoreState");
export function useGameStore(): Store<IGameStoreState> {
  return baseUseStore(key);
}
