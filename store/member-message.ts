import { createStore } from "vuex";
import EnumMutation from "~~/models/enums/enumMutation";
import {
  MemberMessage,
  MemberMessageUnreadInfo,
  TMemberMessageTabs,
} from "~~/models/message";
import { Pagination } from "~~/models/common/pagination";

interface IMemberMessageState {
  messages: MemberMessage[];
  pagination: Pagination;
  memberMessageInfo: MemberMessageUnreadInfo;
  activeTab: TMemberMessageTabs;
}

export const memberMessageStore = createStore<IMemberMessageState>({
  state: {
    messages: [],
    pagination: new Pagination({
      MaxPage: 1,
      RowNumber: 10,
    }),
    memberMessageInfo: {
      TotalCount: 0,
      AnnouncementCount: 0,
      NotificationCount: 0,
      PromotionCount: 0,
      PersonalMessageCount: 0,
    },
    activeTab: "Unread",
  },
  mutations: {
    [EnumMutation.updateMemberMessages](state, messages: MemberMessage[]) {
      state.messages = messages;
    },
    [EnumMutation.updateMemberMessageInfo](
      state,
      memberMessageInfo: MemberMessageUnreadInfo
    ) {
      state.memberMessageInfo = memberMessageInfo;
    },
    [EnumMutation.updateMemberMessagePagination](
      state,
      pagination: Pagination
    ) {
      state.pagination = pagination;
    },
    [EnumMutation.updateActiveTab](state, activeTab: TMemberMessageTabs) {
      state.activeTab = activeTab;
    },
  },
  getters: {
    isHasUnreadMessage(state) {
      return state.memberMessageInfo && state.memberMessageInfo.TotalCount > 0;
    },
    isLastpage(state) {
      return state.pagination.CurrentPage === state.pagination.MaxPage;
    },
  },
});
