import { createStore, useStore as baseUseStore, Store } from "vuex";
import { ICurrencyWhiteList } from "@/models/currencyWhiteList";
import { ICurrencies } from "~~/models/login";

interface IAppState {
  IsIpWhitelistEnabled: boolean;
  IsWLAppEnabled: boolean;
  CurrencyWhiteList: ICurrencyWhiteList[];
  PlayerCurrencyList: ICurrencies[];
  IsReferralV2Enabled: boolean;
}

export const appStore = createStore<IAppState>({
  state: {
    IsIpWhitelistEnabled: false,
    IsWLAppEnabled: false,
    CurrencyWhiteList: [],
    PlayerCurrencyList: [],
    IsReferralV2Enabled: false,
  },
  mutations: {
    updateIsIpWhitelistEnabled(state, IsWhitelisted: boolean) {
      state.IsIpWhitelistEnabled = IsWhitelisted;
    },
    updateIsWLAppEnabled(state, IsEnabled: boolean) {
      state.IsWLAppEnabled = IsEnabled;
    },
    updateCurrencyWhiteList(state, CurrencyWhiteList: ICurrencyWhiteList[]) {
      state.CurrencyWhiteList = CurrencyWhiteList;
    },
    updatePlayerCurrencyList(state, PlayerCurrencyList: ICurrencies[]) {
      state.PlayerCurrencyList = PlayerCurrencyList;
    },
    updateIsV2ReferralEnabled(state, IsReferralV2Enabled: boolean) {
      state.IsReferralV2Enabled = IsReferralV2Enabled;
    },
  },
});

export const key: InjectionKey<Store<IAppState>> = Symbol("IAppState");
export function useAppStore(): Store<IAppState> {
  return baseUseStore(key);
}
