import _ from "lodash-es";
import { computed, ref, watch, reactive } from "vue";
import { STORE_MUTATIONS } from "../utils/constants";
import apis from "@/utils/apis";
import { store } from "@/store";
import { IGame, TabInfo } from "@/models/IGetPlayableGamesResponse";
import { IGameBsi, GameFilterRequest, GameBsi, GameProviders, IGameProviders } from "@/models/bsiGameList";
import themePropertyHelper from "@/utils/themePropertyHelper";
import textHelper from "@/utils/formatHelpers/textFormatHelper";
import { IThemePropertiesFromApi } from "@/models/companyThemePropertiesRespone";
import commonHelper from "@/utils/commonHelper";
import { ILanguage } from "@/models/common/language";
import EnumEntranceLocation from "@/models/enums/enumEntranceLocation";
import featuresToggleHelper from "@/utils/featuresToggleHelper";
import EnumThemePropertyKey from "@/models/enums/enumThemePropertyKey";
import { gameStore } from "~~/store/game";
import { appStore } from "~~/store/app";
import {
  EPlayerSearchGameDevice,
  IPlayerSearchGameRequest,
} from "~~/models/playerSearchGame";
import useModifyGameName from "./useModifyGameName";
import EnumMutation from "~~/models/enums/enumMutation";
import { EnumDisplayTimeZone } from "~~/models/enums/enumDisplayTimeOption";
import { EnumSelectedThemeMode } from "~~/models/enums/enumSelectedThemeMode";

const { getAssetImage } = commonHelper;
const handleReloadRoute = (route: any) => {
  window.location = route;
};
const handleGameEntryOpenWindow = async (
  game: IGame,
  width: number,
  height: number,
  entranceLocation = EnumEntranceLocation.Other,
  viewAsDesktop = false
) => {

  if(await isEnableLaunchTheGameWithIframe()){
    considerLaunchTheGameWithIframeOrNewTeb(game, entranceLocation, viewAsDesktop);
  }
  else{
    launchTheGameWithNormalPopup(width, height, game, entranceLocation, viewAsDesktop);
  }

};

const launchTheGameWithNormalPopup = (
  width: number,
  height: number,
  game: IGame,
  entranceLocation: EnumEntranceLocation,
  viewAsDesktop: boolean) => {
  const position = reactive({
    x: screen.width / 2 - width / 2,
    y: screen.height / 2 - height / 2,
  });
  window.open(
    `/game/${game.GameId}?location=${entranceLocation}&pop${viewAsDesktop ? "&DesktopView" : "" }`,
    "_blank",
    width === -1 && height === -1
      ? "fullscreen=yes"
      : `width=${width},height=${height},left=${position.x},top=${position.y}`
  );
};
const considerLaunchTheGameWithIframeOrNewTeb = (
  game: IGame,
  entranceLocation: EnumEntranceLocation,
  viewAsDesktop: boolean) => {

  if (game.IsSupportIframe) {
    launchTheGameWithIframe(game, entranceLocation, viewAsDesktop)
  } else {
    launchTheGameWithNewTeb(game, entranceLocation, viewAsDesktop)
  }

};
const launchTheGameWithIframe = (
  game: IGame,
  entranceLocation: EnumEntranceLocation,
  viewAsDesktop: boolean) =>{

  const router = useRouter();
  router.push(`/game/${game.GameId}?location=${entranceLocation}&iframe${viewAsDesktop ? "&DesktopView" : ""}`)

};
const launchTheGameWithNewTeb = (
  game: IGame,
  entranceLocation: EnumEntranceLocation,
  viewAsDesktop: boolean) =>{

  const router = useRouter();
  window.open(`/game/${game.GameId}?location=${entranceLocation}&pop${viewAsDesktop ? "&DesktopView" : ""}`);

};
const isEnableLaunchTheGameWithIframe = async () => {
  const response = await commonHelper.getCustomerSettingById("IsEnableLaunchTheGameWithIframe")
  return response?.Value.toUpperCase() === "Y"
};
const auth = store.state.auth;
const handlePopupNewWindow = (
  segment: string,
  width: number,
  height: number
) => {
  const position = reactive({
    x: screen.width / 2 - width / 2,
    y: screen.height / 2 - height / 2,
  });
  const url = `${window.location.origin}/${segment}`;
  window.open(
    url,
    "NewWindowInitial",
    `width=${width},height=${height},left=${position.x},top=${position.y}`
  );
};
const themeProperties = computed(
  () => store.state.companyThemePropertiesFromApi.ThemeProperties
);
const isCashPlayer = computed(() => store.state.isCashPlayer);
const menuTabHeaders = ref([] as TabInfo[]);

const useHeaderComponent = () => {
  menuTabHeaders.value = [];
  const route = useRoute();
  const referralEnabled = ref(false);
  const isLoading = ref(true);

  const tabSelected = computed(() => {
    if (_.includes(_.toLower(route.path), "/games/")) {
      return "/games";
    }
    if (route.query.tab !== undefined) {
      return `/${route.query.tab?.toString()}`;
    }
    return _.toLower(route.path);
  });
  const tabInfoMenuHeader = (tabInfos: Array<TabInfo>) => {
    _.orderBy(tabInfos, "Order", "asc").forEach((tab: TabInfo) => {
      const __tab = _.toLower(tab.TabType);
      const tabLang = `${_.replace(__tab, " ", "_")}`;
      const tabURL =
        _.replace(__tab, " ", "") === "cockfight"
          ? "/cockfight"
          : `/${_.replace(__tab, " ", "-")}`;
      tab.TabLanguage = tabLang;
      tab.TabURL = tabURL;
      menuTabHeaders.value.push(tab);
    });
    isLoading.value = false;
  };
  tabInfoMenuHeader(store.state.tabInfos);
  watch(
    () => store.state.tabInfos,
    () => {
      menuTabHeaders.value = [];
      tabInfoMenuHeader(store.state.tabInfos);
    }
  );
  const providerFeature = computed(() => store.state.gameProviders);
  const isCockfightWhitelist = computed(() => store.state.isCockfightWhitelist);
  const productListFeature = computed(() => {
    let games = providerFeature.value.Games;
    if (!isCockfightWhitelist.value)
      games = games.filter((item) => item.GameProviderId !== 46);
    return games;
  });
  const isFeatureContentEnabled = ref(false);
  const filterProductFeature = ref<Array<IGame>>();
  const productListFeatureProperty = computed(
    (): IThemePropertiesFromApi =>
      themePropertyHelper.getSingleThemeProperty(
        themeProperties.value,
        "product_list_feature"
      )
  );
  const checkImageURL = (item: IGame) => {
    const named = _.toLower(
      _.replace(item.GameProviderName, new RegExp(/\W/g), "")
    );
    const tabTypeVirtualSport =
      _.toLower(_.replace(item.TabType ?? "", " ", "")) === "virtualsports";
    let iconURL = item.GameIconUrl;
    switch (_.toUpper(item.TabType)) {
      case "LIVE CASINO":
        iconURL = themePropertyHelper.getImagePathProvider(
          `casino_${named}.png`,
          "casino"
        );
        break;
      case "GAMES":
        iconURL = themePropertyHelper.getImagePathProvider(
          `${named}.png`,
          "game"
        );
        break;
      default:
        iconURL = item.GameIconUrl;
        break;
    }
    if (
      _.includes(_.replace(named, "-", ""), "sbobet") &&
      !tabTypeVirtualSport
    ) {
      iconURL = getAssetImage("assets/theme/black/customize/sbobet.png");
    }
    if (
      _.includes(_.replace(named, "-", ""), "sbobet") &&
      tabTypeVirtualSport
    ) {
      iconURL = getAssetImage(
        "assets/theme/black/customize/sbovirtualsport.png"
      );
    }
    if (_.includes(_.replace(named, "-", ""), "sbosport")) {
      iconURL = getAssetImage("assets/theme/black/customize/sbosport.png");
    }
    if (_.includes(_.replace(named, "-", ""), "sabasport")) {
      iconURL = getAssetImage("assets/theme/black/customize/sabasport.png");
    }
    if (_.includes(_.replace(named, "-", ""), "sbolive")) {
      iconURL = getAssetImage("assets/theme/black/customize/sbolive.png");
    }
    if (_.includes(_.replace(named, "-", ""), "sbovirtualsport")) {
      iconURL = getAssetImage(
        "assets/theme/black/customize/sbovirtualsport.png"
      );
    }
    if (_.includes(_.replace(named, "-", ""), "lobby")) {
      iconURL = getAssetImage("assets/theme/black/customize/lobby.png");
    }
    if (_.includes(_.replace(named, "-", ""), "mpoker")) {
      iconURL = getAssetImage("assets/theme/black/customize/mpoker.png");
    }
    if (_.includes(_.replace(named, "-", ""), "sv388")) {
      iconURL = getAssetImage("assets/theme/black/customize/sv388cockfighting.png");
    }
    if (_.includes(_.replace(named, "-", ""), "ws168")) {
      iconURL = getAssetImage("assets/theme/black/customize/ws168cockfighting.png");
    }
    return iconURL;
  };
  const checkURL = (item: IGame, link: string) => {
    const tabArray = ["LIVE CASINO", "KENO", "LOTTERY"];
    const baseURL = `${window.location.origin}`;
    const selected = _.toUpper(item.TabType);
    let url = "";
    switch (true) {
      case tabArray.includes(selected):
        url = auth ? "" : `${baseURL}/login`;
        break;
      case selected === "GAMES":
        url = `${baseURL}${link}/${item.GameProviderName}`;
        break;
      default:
        url = auth
          ? `${baseURL}/game/${item.GameId}?tab=${_.replace(
              link,
              "/",
              ""
            )}&provider=${item.GameProviderShortName}`
          : `${baseURL}/login`;
        break;
    }
    return url;
  };
  const filterer = (type: string, name: string): boolean => {
    const typed = _.toLower(_.replace(type, / /g, ""));
    const array: any = {
      sports: ["SBO", "SABA", "BTI", "eSport", "Sport", "AFB"],
      livecasino: [
        "Sexy Gaming",
        "Evolution Gaming",
        "All Bet",
        "Bg Live Casino",
        "Pragmatic Play Casino",
        "Green Dragon",
        "Sa Gaming",
        "Wm",
        "SBO Live Casino",
        "568Win Live Casino",
        "IONLC",
      ],
      virtualsports: ["sbovirtualsport", "virtualbasketball"],
      keno: ["Atom", "Keno"],
      poker: ["MPoker", "Kaiyuan Gaming"],
      lottery: ["TC Gaming", "BBIN", "XBB"],
    };
    if (
      array[typed] !== undefined &&
      store.state.selectedThemeName === "black" &&
      typed !== "livecasino"
    ) {
      return _.some(array[typed], (value) =>
        _.includes(
          _.toLower(_.replace(name, / /g, "")),
          _.toLower(_.replace(value, / /g, ""))
        )
      );
    }
    return true;
  };
  const componentKey = ref(0);
  const productListFeatureHover = (type: string, link: string) => {
    componentKey.value += 1;
    filterProductFeature.value = _.orderBy(
      _.uniqBy(
        productListFeature.value?.filter((item) => {
          item.GameIconUrl = checkImageURL(item);
          item.LinkOption = checkURL(item, link);
          return (
            _.toUpper(item.TabType) === _.toUpper(type) &&
            filterer(type, item.GameName) &&
            _.toUpper(item.TabType) !== "KENO"
          );
        }),
        "GameProviderName"
      ),
      "DisplayOrder"
    );
    if (filterProductFeature.value?.length === 0) {
      isFeatureContentEnabled.value = false;
    } else {
      isFeatureContentEnabled.value =
        !productListFeatureProperty.value.IsHidden;
    }
  };
  const isShowDepositWithdrawal = computed(() => {
    const depositWithdraw = featuresToggleHelper.getThemePropertyFromApi(
      EnumThemePropertyKey.HtmlId,
      "deposit_withdraw"
    );
    let isShow = true;
    if (depositWithdraw.HtmlId) {
      isShow = !depositWithdraw.IsHidden;
    }
    return isShow;
  });
  const getCheckHaveReferralPromotion = async () => {
    await apis.getCheckHaveReferralPromotion("").then((response) => {
      referralEnabled.value = response.isSuccessful && isCashPlayer.value;
    });
  };
  const isReferralV2Enabled = computed(
    () => isCashPlayer.value && appStore.state.IsReferralV2Enabled
  );
  const promotionTab = computed(
    (): IThemePropertiesFromApi =>
      themePropertyHelper.getSingleThemeProperty(
        themeProperties.value,
        "promotion_tab"
      )
  );
  const isAllowPromotionTab = computed(() =>
    auth
      ? !promotionTab.value.IsHidden && isCashPlayer.value
      : !promotionTab.value.IsHidden
  );
  const register = computed(
    (): IThemePropertiesFromApi =>
      themePropertyHelper.getSingleThemeProperty(
        themeProperties.value,
        "register"
      )
  );
  const homeTab = computed(
    (): IThemePropertiesFromApi =>
      themePropertyHelper.getSingleThemeProperty(
        themeProperties.value,
        "home_tab"
      )
  );
  const agentCooperation = computed(
    (): IThemePropertiesFromApi =>
      themePropertyHelper.getSingleThemeProperty(
        themeProperties.value,
        "agent_cooperation"
      )
  );
  const appDownload = computed(
    (): IThemePropertiesFromApi =>
      themePropertyHelper.getSingleThemeProperty(
        themeProperties.value,
        "wl_app_download"
      )
  );
  const isAppDownloadEnabled = ref(
    !appDownload.value.IsHidden && appStore.state.IsWLAppEnabled
  );

  const appUrl = `${commonHelper.getAppDownloadCdn() ?? ""}/Bong/${
    store.state.webId
  }/app-stable-v${store.state.webId}.apk`;

  const username = store.state.username;
  const isSuspendedAccount = computed(() => store.state.isSuspended);
  const isSportLiveEnabled = computed(
    () => _.find(gameStore.state.games, { GameId: 73 }) !== undefined
  );
  const logout = async () => {
    isLoading.value = true;
    localStorage.setItem(
      "PeruShopEnabled",
      store.state.isPerushopEnabled ? "true" : "false"
    );
    localStorage.setItem(
      "ThemeMode",
      store.state.selectedThemeMode ===  EnumSelectedThemeMode.Light ? "Light" : "Dark",
    );
    await new Promise((resolve) => resolve(apis.getLogout())).finally(() => {
      isLoading.value = false;
    });
  };
  const appDownloadURL = ref("");
  const initAppDownloadURL = async () => {
    const response = await commonHelper.getCustomerSettingById(
      "AppDownloadUrl"
    );
    appDownloadURL.value = (response && response.Value) ?? "";
  };
  initAppDownloadURL();

  const getAppDownloadUrl = computed(() => {
    if (appDownloadURL.value !== "") {
      return appDownloadURL.value.includes("://")
        ? appDownloadURL.value
        : `//${appDownloadURL.value}`;
    }
    return appUrl;
  });
  return {
    menuTabHeaders,
    tabSelected,
    isLoading,
    filterProductFeature,
    productListFeatureHover,
    isFeatureContentEnabled,
    isAllowPromotionTab,
    referralEnabled,
    agentCooperation,
    register,
    auth,
    username,
    logout,
    isSuspendedAccount,
    productListFeature,
    productListFeatureProperty,
    isAppDownloadEnabled,
    isSportLiveEnabled,
    appDownload,
    appUrl,
    isShowDepositWithdrawal,
    isCashPlayer,
    componentKey,
    isReferralV2Enabled,
    appDownloadURL,
    homeTab,
    getAppDownloadUrl,
  };
};
const activeTab = ref("");
const favouriteGamesForDisplay = ref<Record<number, Array<number>>>({});
const currentIndex = ref(0);
const customerId = computed(() => store.state.customerId);
const initialedGame = ref<IGameBsi[]>([]);

const initialGame = computed(() => store.state.gameTabProviderList);
const useGameEntryComponent = () => {
  const route = useRoute();
  const filtered = ref(route.params.filter ?? "");
  const isLoading = ref(store.state.isGameUpdateLoading);
  const headerTab = computed(() => initialGame.value.SeamlessGameTabList);
  const isFilterAllGames = ref(false);
  const gameTypes = computed(() => initialGame.value.SeamlessGameGameTypeList);
  const isPlayful = computed(() => store.state.selectedThemeName === "playful");
  const isMobile = !useIsDesktop();
  const urlParams = new URLSearchParams(window.location.search);
  const gameSearchQuery = ref({
    Type: urlParams.get("Type") ?? "",
    Value: urlParams.get("Value") ?? "",
  });
  const providers = computed(() =>
    _.orderBy(
      _.orderBy(
        initialGame.value.SeamlessProviderList.filter((provider) => {
          if (provider.ProviderName === "Sv388Cockfighting") return false;
          if (isMobile && provider.ProviderName === "KingMaker") return false;
          return true;
        }).map((item) => {
          const provider = store.state.gameProviders.Games.find(
            (gameProvider) =>
              gameProvider.GameProviderName === item.ProviderName
          );
          const IsMaintain = (provider && provider.IsMaintain) ?? false;
          const displayOrder = (provider && provider.DisplayOrder) ?? 0;
          const isNewProvider =
            (provider && provider.IsNewGameProvider) ?? false;
          const isHotProvider = provider ? provider.IsHotGameProvider : false;
          const obj: IGameProviders = {
            ...item,
            IsMaintain,
            displayOrder,
            isNewProvider,
            GameProviderDisplayName: provider?.GameProviderDisplayName,
            isHotProvider,
            NameForDisplay: useModifyGameName(provider, true),
          };
          return new GameProviders(obj);
        }),
        "ProviderName"
      ),
      "displayOrder"
    )
  );
  const authenticated = ref(store.state.auth);
  const isGameProvider = computed(
    () => filtered.value !== "" && !filtered.value.includes("__n__")
  );
  const gameCollection = ref<IGameBsi[]>([]);
  const shouldUpdateGameCollection = ref<boolean>(false);
  const searchedGame = ref("");
  const tabInitial = ref(
    route.query.f === undefined && headerTab.value !== undefined
      ? headerTab.value[0]
      : String(route.query.f)
  );
  const subTabInitial = ref(
    route.query.f === undefined ? "" : String(route.query.f)
  );

  switch (_.toLower(_.replace(_.first(headerTab.value) ?? "", " ", ""))) {
    case "gametype" || "gametypes":
      subTabInitial.value = String(_.first(gameTypes.value) ?? "");
      break;
    case "provider" || "providers":
      subTabInitial.value = String(_.first(providers.value)?.ProviderName);
      break;
    default:
      subTabInitial.value = String(_.first(headerTab.value) ?? "");
      break;
  }
  if (isGameProvider.value) {
    tabInitial.value = "Provider";
    subTabInitial.value = String(filtered.value);
  }
  const initialQuery = ref<Array<IGameBsi>>();
  const getJackpotGameListInitial = async () => {
    // initialedGame.value = [];
    const response = await apis.getJackpotGameList();
    if (response.isSuccessful) {
      initialedGame.value = response.Data.Games.map(
        (item) => new GameBsi(item)
      );
    } else {
      initialedGame.value = [];
    }
  };
  const getHotGameListInitial = async () => {
    // initialedGame.value = [];
    const response = await apis.getHotGameList();
    if (response.isSuccessful) {
      initialedGame.value = response.Data.Games.map(
        (item) => new GameBsi(item)
      );
    } else {
      initialedGame.value = [];
    }
  };
  const getNewGameListInitial = async () => {
    // initialedGame.value = [];
    const response = await apis.getNewGameList();
    if (response.isSuccessful) {
      initialedGame.value = response.Data.Games.map(
        (item) => new GameBsi(item)
      );
    } else {
      initialedGame.value = [];
    }
  };
  const getGameListByGameTypeInitial = async (gameType: string) => {
    // initialedGame.value = [];
    const response = await apis.getGameListByGameType(gameType);
    if (response.isSuccessful) {
      initialedGame.value = response.Data.Games.map(
        (item) => new GameBsi(item)
      );
    } else {
      initialedGame.value = [];
    }
  };
  const getGameListByProviderInitial = async (provider: string) => {
    // initialedGame.value = [];
    const response = await apis.getGameListByProvider(provider);
    if (response.isSuccessful) {
      initialedGame.value = response.Data.Games.map(
        (item) => new GameBsi(item)
      );
    } else {
      initialedGame.value = [];
    }
  };
  const playableGame = computed(() => {
    if (isPlayful.value) {
      return gameStore.state.games.filter(
        (game) =>
          game.Category === 3 &&
          (!game.TabType || game.TabType.toLowerCase() === "games")
      );
    }
    return gameStore.state.games.filter((game) => game.Category === 3);
  });

  favouriteGamesForDisplay.value = JSON.parse(
    cookieHelper.getCookie("favouriteGames") || "{}"
  );

  const getGameEntryFiltered = async (request: GameFilterRequest) => {
    activeTab.value = _.toLower(_.replace(request.Type, " ", ""));
    isLoading.value = true;
    shouldUpdateGameCollection.value = true;
    try {
      isFilterAllGames.value = false;
      switch (_.toLower(_.replace(request.Type, " ", ""))) {
        case "specialselection":
          await getJackpotGameListInitial();
          break;
        case "newgames":
          await getNewGameListInitial();
          break;

        case "gametype":
          await getGameListByGameTypeInitial(request.Value);
          break;

        case "provider":
          await getGameListByProviderInitial(request.Value);
          break;

        case "favorite":
          initialedGame.value = [];
          favouriteGamesForDisplay.value = JSON.parse(
            cookieHelper.getCookie("favouriteGames") || "{}"
          );
          playableGame.value.forEach((gameItem) => {
            if (
              favouriteGamesForDisplay.value[customerId.value] &&
              favouriteGamesForDisplay.value[customerId.value].includes(
                gameItem.GameId
              )
            )
              initialedGame.value.push(gameItem);
          });
          break;
        case "allgames":
          if (isPlayful.value) {
            initialedGame.value = gameStore.state.games.filter(
              (game) =>
                game.Category === 3 &&
                (!game.TabType || game.TabType.toLowerCase() === "games")
            );
          } else {
            initialedGame.value = playableGame.value;
          }
          isFilterAllGames.value = true;
          break;

        default:
          await getHotGameListInitial();
      }
    } catch (error) {
      initialedGame.value = [];
      isLoading.value = false;
    }
    isLoading.value = false;
  };

  const request: GameFilterRequest = {
    Type: tabInitial.value,
    Value: subTabInitial.value,
  };
  const checkIsGameType = (params: string) => gameTypes.value?.includes(params);
  const isGameType = checkIsGameType(route.fullPath.slice(7));
  if (isGameType) {
    request.Type = "GameType";
    getGameEntryFiltered(request);
  } else {
    if (String(route.params.filter).includes("Favorite"))
      request.Type = "Favorite";
    if (String(route.params.filter).includes("HotGames"))
      request.Type = "HotGames";

    const isChina = store.state.selectedThemeName === "china";
    const isDesktop = useIsDesktop();
    if (isChina && isDesktop) {
      if (!route.params.filter) {
        if (authenticated.value) {
          request.Type = "hotgames";
        } else {
          request.Type = "GameType";
          request.Value = gameTypes.value[0] ?? "slots";
        }
      } else {
        const game = String(route.params.filter).slice(0, -7);
        const isFilterAGameType = String(route.params.filter).includes(
          "__n__gt"
        );
        const isFilterAGameProvider = String(route.params.filter).includes(
          "__n__gp"
        );
        const authTypes = ["hot games", "new games"];
        const isHotOrNewGame = authTypes.includes(game);
        if (isFilterAGameType) {
          if (isHotOrNewGame) {
            request.Type = game;
          } else {
            request.Type = "GameType";
          }
          request.Value = game;
        } else if (isFilterAGameProvider) {
          request.Type = "provider";
          request.Value = game;
        }
      }
    }
    if (gameSearchQuery.value.Type !== "") {
      request.Type = gameSearchQuery.value.Type;
      request.Value = gameSearchQuery.value.Value;
    }
    getGameEntryFiltered(request);
  }
  watch(subTabInitial, () => {
    const params: GameFilterRequest = {
      Type: tabInitial.value,
      Value: subTabInitial.value,
    };
    getGameEntryFiltered(params);
  });
  const handleSelectedTabOption = (segment: string, selected?: string) => {
    tabInitial.value = segment;
    subTabInitial.value = selected ?? segment;
    searchedGame.value = "";
    filtered.value = "";
  };
  const cloneGame = ref<IGameBsi[]>([]);
  const handleSearchGame = (needFilterCrossCategory = true) => {
    if (!searchedGame.value) {
      initialQuery.value = _.cloneDeep(cloneGame.value);
    } else {
      initialQuery.value = (
        needFilterCrossCategory
          ? playableGame.value
          : _.cloneDeep(cloneGame.value)
      ).filter((game) => {
        const gameName = game.GameName
          ? game.GameName.toLowerCase().replace(/\s/g, " ")
          : "";
        const searchedTerm = searchedGame.value.toLowerCase();
        if (gameName.includes(searchedTerm)) {
          return true;
        }
        return false;
      });
    }
  };
  // eslint-disable-next-line vue/return-in-computed-property
  const initialGameEntry = computed(() => {
    const isActiveHot = activeTab.value === "hotgames";
    const isSpecialSelection = activeTab.value === "specialselection";
    const isNotSortGameOrderByRank = isActiveHot || isSpecialSelection;
    if (searchedGame.value === "")
      return (
        !isNotSortGameOrderByRank
          ? _.sortBy(initialedGame.value, ["Rank"])
          : initialedGame.value
      ).filter((g) => {
        const isDesktop = useIsDesktop();
        if (isDesktop) return g.ForDesktop;
        return g.ForMobile;
      });
    else if (initialQuery.value)
      return (
        !isActiveHot
          ? _.sortBy(initialQuery.value, ["Rank"])
          : initialQuery.value
      ).filter((g) => {
        const isDesktop = useIsDesktop();
        if (isDesktop) return g.ForDesktop;
        return g.ForMobile;
      });
  });

  const callSearchRecordApi = () => {
    const searchTerm = searchedGame.value;
    if (searchTerm && searchTerm.length > 0) {
      const isDesktop = useIsDesktop();
      const deviceType = isDesktop
        ? EPlayerSearchGameDevice.Desktop
        : EPlayerSearchGameDevice.Mobile;
      const payload: IPlayerSearchGameRequest = {
        WebId: store.state.webId ?? 0,
        Username: store.state.username ?? "",
        SearchContent: searchTerm,
        SearchPage: window.location.toString(),
        DeviceType: deviceType,
        OnlineId: Number(localStorage.getItem("onlineId") ?? 0),
      };
      apis.getPlayerSearchGame(payload);
    }
  };

  watch(
    () => initialedGame.value,
    () => {
      if (shouldUpdateGameCollection.value) {
        gameCollection.value = initialGameEntry.value ?? [];
        cloneGame.value = _.cloneDeep(initialedGame.value);
        shouldUpdateGameCollection.value = false;
      }
    }
  );
  const router = useRouter();
  const updateSearchResult = () => {
    gameCollection.value = initialGameEntry.value ?? [];
    callSearchRecordApi();
    isFilterAllGames.value = searchedGame.value === "";
    if (!route.fullPath.includes("/games"))
      router.push(`/games?search=${searchedGame.value}`);
  };
  const isProviderGames1 = ref(false);
  themeProperties.value.forEach((property) => {
    if (property.Text === "Provider_type_games_1") {
      isProviderGames1.value = true;
    }
  });

  return {
    isLoading,
    headerTab,
    gameTypes,
    providers,
    initialGameEntry,
    searchedGame,
    tabInitial,
    subTabInitial,
    authenticated,
    handleSearchGame,
    handleSelectedTabOption,
    getGameEntryFiltered,
    filtered,
    menuTabHeaders,
    favouriteGamesForDisplay,
    currentIndex,
    customerId,
    activeTab,
    gameCollection,
    updateSearchResult,
    isFilterAllGames,
    isProviderGames1,
    getHotGameListInitial,
    getNewGameListInitial,
    initialedGame,
  };
};
const useLanguageComponent = () => {
  const selectedLang = ref<string>();
  const nuxtApp = useNuxtApp();
  const isLanguageLoading = computed(() => store.state.isThemePropertyLoading);
  const languageSetting = computed(
    (): Array<ILanguage> => store.state.languages
  );
  const hasBeenSelected = useCookie<boolean>("local_been_selected");
  const switchLanguage = (locale: string, shouldReload = true) => {
    const local = useCookie<string>(KEYS.LANGUAGE_COOKIE_KEY);
    hasBeenSelected.value = true;
    local.value = locale;
    selectedLang.value = locale;
    nuxtApp.$i18n.setLocale(String(locale));
    if (shouldReload) {
      window.location.reload();
    }
  };
  const isAllowTheme = ["peru", "global"].includes(
    store.state.selectedThemeName
  );
  const isPerushopEnabled = computed(() => store.state.isPerushopEnabled);
  // Get the user's IP address
  const userIpAddress = computed(() =>
    (store.state.geography?.ContinentCode ?? "").toUpperCase()
  );
  let defaultLanguage = selectedLang.value
    ? selectedLang
    : nuxtApp.$i18n.getLocaleCookie();
  selectedLang.value = defaultLanguage;
  const telegramLang = computed({
    get: () => {
      if (selectedLang.value) {
        return selectedLang.value;
      }
      return nuxtApp.$i18n.getLocaleCookie();
    },
    set: (value) => {
      selectedLang.value = value;
    },
  });
  if (isAllowTheme && !hasBeenSelected.value && isPerushopEnabled.value) {
    if (userIpAddress.value === "SA") {
      defaultLanguage = "es_ES";
      store.commit(STORE_MUTATIONS.LOCALE, defaultLanguage);
    } else {
      defaultLanguage = "en";
      store.commit(STORE_MUTATIONS.LOCALE, defaultLanguage);
    }
    nuxtApp.$i18n.setLocale(defaultLanguage);
    telegramLang.value = defaultLanguage;
  }
  const init = () => {
    const nuxtApp = useNuxtApp();
    nuxtApp.$i18n.setLocale(String(defaultLanguage));
  };
  const defaultLanguageForDisplay = computed(
    () =>
      _.find(languageSetting.value, (lang) => lang.ISO === defaultLanguage)
        ?.LanguageLocalName
  );
  const languages = computed(
    (): Array<ILanguage> =>
      _.filter(languageSetting.value, (lang) => lang.ISO !== defaultLanguage)
  );
  const languageShortCodeForDisplay = computed(
    () =>
      _.find(languageSetting.value, (lang) => lang.ISO === defaultLanguage)
      ?.ShortCountryCode
  );
  const reactiveLanguageShortCodeForDisplay = computed(
    () =>
      _.find(languageSetting.value, (lang) => lang.ISO === selectedLang.value)
        ?.ShortCountryCode
  );
  return {
    init,
    languages,
    telegramLang,
    defaultLanguage,
    switchLanguage,
    isLanguageLoading,
    defaultLanguageForDisplay,
    languageSetting,
    languageShortCodeForDisplay,
    selectedLang,
    reactiveLanguageShortCodeForDisplay,
  };
};
const useGameEntryBaseOnTabSelected = () => {
  const route = useRoute();
  const isMobile = !useIsDesktop();
  const template = isMobile ? "mobile" : "desktop";
  const isCockfightWhitelist = computed(() => store.state.isCockfightWhitelist);
  const orderByGameIDDesktop = [61, 1 , 59];
  const orderByGameIDMobile = [61, 1 , 64, 65, 59];
  const webId = store.state.webId;
  const { isIndependentModule } = useIndependentModule();
  const gameEntrySelected = computed(() => {
    let { games } = gameStore.state;
    if (!isCockfightWhitelist.value)
      games = games.filter((item) => item.GameProviderId !== 46);
    const filterGame = ref<Array<IGameBsi>>([]);
    let routeName = String(route.name);
    if (isIndependentModule.value && routeName === "index") {
      const firstTabRoute = useRouter().resolve(
        menuTabHeaders.value[0]?.TabURL ?? "index"
      );
      routeName = String(firstTabRoute.name ?? "index");
    }
    switch (routeName) {
      case "index":
      case "sports": {
        const sport = _.filter(
          games,
          (item) =>
            item.Category === 1 &&
            (template === "desktop" ? item.ForDesktop : item.ForMobile)
        );
        const gameId61 = _.find(sport, { GameId: 61 });
        const webId3 = computed(() => webId === 3);
        const otherGames = _.reject(sport, { GameId: 61 });
        let sortedSport =
          gameId61 && webId3.value ? [gameId61, ...otherGames] : sport;
        const sboLive = _.filter(
          games,
          (item) =>
            item.GameId === 27 &&
            (template === "desktop" ? item.ForDesktop : item.ForMobile)
        );
        if (sboLive.length) {
          sortedSport.push(sboLive[0]);
        }
        if (store.state.geography.CountryCode !== "PH") {
          sortedSport = sortedSport.filter(
            (item: IGameBsi) =>
              item.EnglishGameName?.toLocaleLowerCase() !==
              "568win sports classic"
          );
        }
        filterGame.value = sortedSport;
        break;
      }
      case "live-casino":
        filterGame.value = _.orderBy(
          _.filter(
            games,
            (item) =>
              item.Category === 2 &&
              (template === "desktop" ? item.ForDesktop : item.ForMobile)
          ),
          "DisplayOrder"
        );
        filterGame.value = _.sortBy(filterGame.value, (provider) => {
          if (
            provider.EnglishGameName?.toLocaleLowerCase() === "568Win Casino"
          ) {
            return 0; // '568win' comes first
          }
          return 1; // All other elements come last
        });
        break;
      case "keno":
        filterGame.value = _.filter(
          games,
          (item) =>
            item.GameProviderName === "FunkyGames" &&
            _.includes(
              ["SBO ATOM", "SBO KENO", "RNG WAR", "NUMBER GAME"],
              item.GameName
            )
        );
        break;
      case "cockfight":
        filterGame.value = _.filter(
          games,
          (item) => _.toLower(_.replace(item.TabType, " ", "")) === "cockfight" && item.GameProviderId !== -1
        );
        break;
      case "lottery":
        filterGame.value = _.filter(
          games,
          (item) => item.TabType === "Lottery"
        );
        break;
      case "poker":
        filterGame.value = _.filter(
          games,
          (items) =>
            items.Category === 3 &&
            items.GameProviderId === 1009 &&
            items.ForDesktop &&
            items.IsEnabled
        );
        break;
      case "sbo-live":
        filterGame.value = _.filter(
          games,
          (item) => item.TabType === "SBO-Live"
        );
        break;
      default:
        filterGame.value = _.filter(
          games,
          (item) =>
            item.Category === 4 &&
            (template === "desktop" ? item.ForDesktop : item.ForMobile)
        );
        break;
    }
    const mappedGames = filterGame.value.map((item) => new GameBsi(item));
    if(route.name !== 'live-casino' && route.name === 'sports') {
      return _.sortBy(mappedGames, (game) => {
        if ((template === "desktop" ? orderByGameIDDesktop : orderByGameIDMobile).includes(game.GameId)) {
          return template === "desktop" ? orderByGameIDDesktop.indexOf(game.GameId) : orderByGameIDMobile.indexOf(game.GameId);;
        }
        return game.DisplayOrder + 999; // All other elements come last
      });
    } else if (route.name !== 'live-casino' && route.name !== 'sports') {
      return _.orderBy(mappedGames, "DisplayOrder")
    } else {
      return mappedGames;
    }
  });
  const customizeObjectSports = ref<Array<IGameBsi>>([]);
  const customizeObjectCasinos = ref<Array<IGameBsi>>([]);
  const customizeObjectVirtuals = ref<Array<IGameBsi>>([]);
  const customizeObjectKeno = ref<Array<IGameBsi>>([]);
  const customizeObjectPoker = ref<Array<IGameBsi>>([]);
  const customizeObjectLottery = ref<Array<IGameBsi>>([]);
  const customizeSport: Array<string> = ["SBO", "SABA"];
  const customizeCasino: Array<string> = [
    "Sexy Gaming",
    "Evolution Gaming",
    "All Bet",
    "Bg Live Casino",
    "Pragmatic Play Casino",
    "Green Dragon",
    "Sa Gaming",
    "Wm",
    "SBO Live Casino",
    "IONLC",
  ];
  const customizeVirtual: Array<string> = [
    "sbovirtualsport",
    "virtualbasketball",
    "virtualfootball",
  ];
  const customizeKeno: Array<string> = ["Atom", "Keno"];
  const customizePoker: Array<string> = ["MPoker", "Kaiyuan Gaming"];
  const customizeLottery: Array<string> = ["TC Gaming", "BBIN", "XBB"];
  const objectConverter = (
    fakeObjects: Array<string>,
    convertObject: Array<IGameBsi>,
    path: string,
    extension: string
  ) => {
    fakeObjects.forEach((object) => {
      let name = _.toLower(_.replace(object, / /g, ""));
      const selected = gameEntrySelected.value.find((game) =>
        _.includes(_.toLower(_.replace(game.EnglishGameName, / /g, "")), name)
      );
      if (selected !== undefined) {
        name =
          path === "virtuals" && name === "sbovirtualsport"
            ? "virtualfootball"
            : name;
        selected.GameIconUrl = getAssetImage(
          `assets/theme/black/${path}/${name}.${extension}`
        );
        convertObject.push(selected);
      }
    });
  };
  if (route.name === "sports" && store.state.selectedThemeName === "black") {
    objectConverter(
      customizeSport,
      customizeObjectSports.value,
      "customize",
      "png"
    );
  }
  if (
    route.name === "live-casino" &&
    store.state.selectedThemeName === "black"
  ) {
    objectConverter(
      customizeCasino,
      customizeObjectCasinos.value,
      "casinos",
      "jpg"
    );
  }
  if (
    route.name === "virtual-sports" &&
    store.state.selectedThemeName === "black"
  ) {
    objectConverter(
      customizeVirtual,
      customizeObjectVirtuals.value,
      "virtuals",
      "png"
    );
  }
  if (route.name === "keno" && store.state.selectedThemeName === "black") {
    objectConverter(customizeKeno, customizeObjectKeno.value, "kenos", "png");
  }
  if (route.name === "poker" && store.state.selectedThemeName === "black") {
    objectConverter(
      customizePoker,
      customizeObjectPoker.value,
      "pokers",
      "png"
    );
  }
  if (route.name === "lottery" && store.state.selectedThemeName === "black") {
    objectConverter(
      customizeLottery,
      customizeObjectLottery.value,
      "lotteries",
      "png"
    );
  }
  return {
    gameEntrySelected,
    customizeObjectCasinos,
    customizeObjectVirtuals,
    customizeObjectKeno,
    customizeObjectPoker,
    customizeObjectLottery,
  };
};
const useDisplayLocalTime = () => {
  const datetimeDisplay = ref();
  const getPlayerPreference = async () => {
    const response = await apis.getPlayerPreferenceForDisplay();
    const themeMode = store.state.selectedThemeMode;
    if (response.isSuccessful) {
      store.commit(
        "updateSelectedThemeMode",
        themeMode,
      );
      store.commit(
        "updateDisplayLocalTime",
        response.Data.TimeZone.IsUsingLocalTime
      );
      store.commit(
        EnumMutation.updateDisplayTimeZone,
        response.Data.TimeZone.RegionTimeZone
      );
    }
  };
  getPlayerPreference();
  const updateCurrentTime = () => {
    const timeZoneNumber = new Date().getTimezoneOffset() / 60;
    const currentTime = new Date();
    let gmtToDisplay = timeZoneNumber;

    if (store.state.displayTimezone === EnumDisplayTimeZone.HongKongTime) {
      gmtToDisplay = -8;
    } else if (
      store.state.displayTimezone === EnumDisplayTimeZone.PacificEfateTime
    ) {
      gmtToDisplay = -11;
    }

    datetimeDisplay.value = textHelper.getDateTimeByGMT(
      currentTime.toString(),
      gmtToDisplay
    );
  };
  setInterval(updateCurrentTime, 1000);
  return {
    datetimeDisplay,
  };
};
const useAddToFavorite = (concatClass = "") => {
  const setAndUnsetGameToFavourite = (
    game: IGameBsi,
    set = true,
    currIndex: number
  ) => {
    const activeIconEl = document.querySelector(
      `.scale-animation${concatClass}-${game.GameId}`
    );
    const activeContainerEl = document.querySelector(
      `.colorFade${concatClass}-${game.GameId}`
    );
    currentIndex.value = currIndex;
    if (activeIconEl && activeContainerEl) {
      activeIconEl.classList.add(`scale-animation${concatClass}`);
      activeContainerEl.classList.add(`colorFade${concatClass}`);
    }
    const retrievedData = cookieHelper.getCookie("favouriteGames");
    let favouriteGames: Record<number, Array<number>> = {};
    if (retrievedData) {
      favouriteGames = JSON.parse(retrievedData) || {};
    }
    if (!favouriteGames[customerId.value])
      favouriteGames[customerId.value] = [];
    if (!set) {
      activeIconEl?.classList.add("favorite-inactive-delay");
      setTimeout(() => {
        activeIconEl?.classList.remove("favorite-inactive-delay");
      }, 1000);
      favouriteGames[customerId.value] = favouriteGames[
        customerId.value
      ].filter((item: number) => item !== game.GameId);
    }
    else {
      favouriteGames[customerId.value].push(game.GameId);
    }
    // localStorage.setItem('favouriteGames', JSON.stringify(favouriteGames));
    const favouriteGamesJSON = JSON.stringify(favouriteGames);
    document.cookie = `favouriteGames=${encodeURIComponent(
      favouriteGamesJSON
    )}; path=/`;
    favouriteGamesForDisplay.value = JSON.parse(
      cookieHelper.getCookie("favouriteGames") || "{}"
    );
    if (activeTab.value === "favorite") {
      // playableGame.value.forEach((gameItem) => {
      //   if (favouriteGamesForDisplay.value[customerId.value] && favouriteGamesForDisplay.value[customerId.value].includes(gameItem.GameId)) initialedGame.value.push(gameItem);
      // });
      initialedGame.value = initialedGame.value.filter(
        (gameItem) => gameItem.GameId !== game.GameId
      );
    }
    setTimeout(function () {
      if (activeIconEl && activeContainerEl) {
        activeIconEl.classList.remove("scale-animation");
        activeContainerEl.classList.remove(`colorFade${concatClass}`);
      }
    }, 3500);
  };
  const checkIsFavourite = (gameId: number) => {
    return (
      favouriteGamesForDisplay.value[customerId.value] &&
      favouriteGamesForDisplay.value[customerId.value].includes(gameId)
    );
  };
  const router = useRouter();
  const redirectToLogin = () => {
    router.push("/login");
  };
  return {
    setAndUnsetGameToFavourite,
    checkIsFavourite,
    redirectToLogin,
  };
};

export {
  handleReloadRoute,
  useHeaderComponent,
  useLanguageComponent,
  useGameEntryComponent,
  handlePopupNewWindow,
  handleGameEntryOpenWindow,
  useGameEntryBaseOnTabSelected,
  useDisplayLocalTime,
  menuTabHeaders,
  useAddToFavorite,
};
