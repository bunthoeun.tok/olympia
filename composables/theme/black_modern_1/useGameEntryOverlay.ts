import { ref } from "vue";
import { IGameBsi } from "@/models/bsiGameList";

const drawer = ref(false);
export const useGameEntryOverlay = () => {
  const game = ref<IGameBsi>({} as IGameBsi);
  const handleClickGame = (gameItem: IGameBsi) => {
    drawer.value = false;
    setTimeout(() => {
      game.value = gameItem;
      drawer.value = true;
    }, 500);
  };
  return {
    game,
    drawer,
    handleClickGame,
  };
};
