import { ref, Ref } from "vue";
import {
  ILoginByMatchIdRequest,
  RecommendMatch,
} from "@/models/theme/popular_match/popularMatch";
import { store } from "@/store";
import { useLanguageComponent } from "@/composables/useTheme";
import apis from "@/utils/apis";
import EnumApiErrorCode from "@/models/enums/enumApiErrorCode";

interface IUsePopularMatch {
  isDisabled: Ref<boolean>;
  onBetNowClick: (match: RecommendMatch) => Promise<void>;
}

export const usePopularMatchCard = (): IUsePopularMatch => {
  const router = useRouter();
  const isDisabled = ref<boolean>(false);
  const { defaultLanguage } = useLanguageComponent();

  const onBetNowClick = async (match: RecommendMatch): Promise<void> => {
    const isNotLoggedIn = !store.state.auth;
    if (isNotLoggedIn) {
      if (useIsDesktop()) {
        router.push("/login");
      } else {
        store.commit("updateLoginDialog", !store.state.showLoginDialog);
      }
      return;
    }

    isDisabled.value = true;
    const request: ILoginByMatchIdRequest = {
      MatchId: match.MatchId,
      IsFromMobile: false,
      FromUrl: window.location.href,
      Lang: defaultLanguage,
      OnlineId: Number(localStorage.getItem("onlineId") ?? 0),
    };
    const response = await apis.loginByMatchId(request);
    const isRequestFails = response.ErrorCode !== EnumApiErrorCode.success;
    if (isRequestFails) return;
    router.push({
      path: "/bet-now",
      query: { tab: "Sports", url: response.Data.Url },
    });
  };

  return {
    isDisabled,
    onBetNowClick,
  };
};
