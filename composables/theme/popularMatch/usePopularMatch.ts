import { onBeforeUnmount, ref } from "vue";
import { IRecommendMatch } from "@/models/theme/popular_match/popularMatch";
import apis from "@/utils/apis";

export const usePopularMatch = () => {
  const recommendMatches = ref<IRecommendMatch[]>([]);
  const getRecommendMatches = async () => {
    const response = await apis.recommendMatches();
    if (response.isSuccessful) {
      if (response.Data.RecommendMatches.length > 3) {
        recommendMatches.value = response.Data.RecommendMatches.slice(0, 3);
      } else {
        recommendMatches.value = response.Data.RecommendMatches;
      }
    }
  };
  getRecommendMatches();
  const repeatCallRecommendMatches = setInterval(getRecommendMatches, 30000);
  onBeforeUnmount(() => {
    clearInterval(repeatCallRecommendMatches);
  });
  return {
    recommendMatches,
  };
};
