import { computed } from "vue";

import { store } from "@/store";

interface IUseLiteTheme {
  isLiteTheme: () => boolean;
  redirectLite: () => void;
  checkHasAccess: () => void;
}

export const useLiteTheme = (): IUseLiteTheme => {
  const router = useRouter();
  const isLoggedIn = () => store.state.auth;
  const selectedTheme = computed(() => store.state.selectedThemeName);
  const isPureLite = computed(() => selectedTheme.value === "pure");
  const isSportLite = computed(() => selectedTheme.value === "sport");
  const PATHS = {
    HOME: "/",
    PASSWORD_CHANGE: ROUTES.ACCOUNT.PASSWORD,
    LOGIN: "/login",
    GAMES: "/games",
    SPORTS: "/sports",
  };

  const isLiteTheme = () =>
    selectedTheme.value === "pure" || selectedTheme.value === "sport";

  const redirectLite = () => {
    if (!isLoggedIn()) {
      router.push({
        path: PATHS.LOGIN,
      });
      return;
    }

    if (isPureLite.value) {
      router.push({
        path: PATHS.GAMES,
      });
      return;
    }

    if (isSportLite.value) {
      router.push({
        path: PATHS.SPORTS,
      });
      return;
    }

    router.push({
      path: store.state.isPasswordExpired ? PATHS.PASSWORD_CHANGE : PATHS.HOME,
    });
  };

  /**
   * checks if user can access the page.
   * the rules:
   * - pure lite can only visit games page.
   * - sport lite can only visit sports page.
   * @returns void
   */
  const checkHasAccess = () => {
    const currentRoute = router.currentRoute.value.fullPath.toLowerCase();
    const isSportPath = currentRoute.startsWith(PATHS.SPORTS.toLowerCase());
    const isGamesPath = currentRoute.startsWith(PATHS.GAMES.toLowerCase());

    if (isPureLite.value && isSportPath) {
      router.push({
        path: PATHS.GAMES,
      });
      return;
    }

    if (isSportLite.value && isGamesPath) {
      router.push({
        path: PATHS.SPORTS,
      });
    }
  };

  return {
    isLiteTheme,
    redirectLite,
    checkHasAccess,
  };
};
