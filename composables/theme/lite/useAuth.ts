import { ref, watch } from "vue";
import { store } from "@/store";

export const useAuth = () => {
  const router = useRouter();
  const isLoggedIn = ref<boolean>(store.state.auth);
  const authCheck = () => {
    if (!isLoggedIn.value) {
      router.push({
        path: "/login",
      });
    }
  };
  watch(
    () => isLoggedIn.value,
    () => {
      authCheck();
    }
  );
  authCheck();
  return {
    isLoggedIn,
  };
};
