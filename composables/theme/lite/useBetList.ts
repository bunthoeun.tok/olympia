import { ref } from "vue";
import apis from "@/utils/apis";
import {
  IRequestBetList,
  IResponseSportBets,
} from "@/models/theme/lite/PureLite";

export const useBetList = () => {
  const betLists = ref<Array<IResponseSportBets>>();
  const request = ref<IRequestBetList>({} as IRequestBetList);
  const getBetList = async () => {
    request.value = {
      StartDate: new Date(),
      EndDate: new Date(),
    };
    const response = await apis.getBetList(request.value);

    if (response.isSuccessful) {
      betLists.value = response.Data.SportBets;
    }
  };
  getBetList();
  return {
    betLists,
    request,
  };
};
