import apis from "@/utils/apis";
import EnumApiErrorCode from "~~/models/enums/enumApiErrorCode";
import {
  IGetAvailableLuckyWheelTicketsResponse,
  IGetLuckyWheelOptionsResponse,
  IResult,
  ISpinState,
  IGetLuckyWheelSpinResultRequest,
} from "~~/models/lucky-wheel/luckyWheel";
import { IPromotion } from "~~/models/promotion/promotion";
import { store } from "~~/store";
import textHelper from "~~/utils/formatHelpers/textFormatHelper";

export default function useLuckyWheel() {
  const { promotions } = usePromotion();
  const dialogVisible = ref(false);
  const isClickPlay = ref(false);
  const spinState = ref<ISpinState>({
    isDefault: true,
    isSpinning: false,
    isStoppedSpin: false,
    settled: false,
  });
  const isPromotionEnded = ref(true);
  const isSomethingWentWrong = ref(false);
  const hasValidLuckyWheel = ref(false);
  const resultErrorMessage = ref("");
  const getResultRequest = ref<IGetLuckyWheelSpinResultRequest>({
    PromotionId: 0,
    Device: commonHelper.getUserAgent(),
    FingerPrint: store.state.visitorId,
    Ip: store.state.geography?.Ip ?? "",
    Url: String(window.location),
  });
  const currentPromotionId = ref(0);
  const traceId = ref(0);

  const router = useRouter();
  const currency = computed(() =>
    store.state.currency ? store.state.currency : store.state.geography.Currency
  );

  const result = ref<IResult>({} as IResult);
  const availableTickets = ref<IGetAvailableLuckyWheelTicketsResponse>(
    {} as IGetAvailableLuckyWheelTicketsResponse
  );

  const luckyWheelOptions = ref<IGetLuckyWheelOptionsResponse>(
    {} as IGetLuckyWheelOptionsResponse
  );

  const getLuckyWheelOptions = async (PromotionId: number) => {
    const response = await apis.getLuckyWheelOptions({
      PromotionId,
      Currency: currency.value,
      OnlineId: store.state.onlineId,
      OperatorId: store.state.customerId,
      SimulateId: 0,
      WebId: store.state.webId,
    });
    if (response.ErrorCode === EnumApiErrorCode.success) {
      luckyWheelOptions.value = response.Data;
    }
  };

  const findValidPromotion = () => {
    if (
      Object.keys(availableTickets.value).length === 0 ||
      availableTickets.value.AvailableTickets.length === 0
    ) {
      const newDate = new Date();
      const formattedDate = Date.parse(
        textHelper.getGmtMinusFourDate(String(newDate), false, false)
      );
      const validDate = (promotion: IPromotion) =>
        Date.parse(promotion.StartDate) <= formattedDate &&
        formattedDate <= Date.parse(promotion.EndDate);
      const firstLuckyWheel = promotions.value.find(
        (item) =>
          item.PromotionTypeId === 10 &&
          validDate(item) &&
          item.Currency === currency.value
      );

      if (firstLuckyWheel) {
        hasValidLuckyWheel.value = true;
        currentPromotionId.value = firstLuckyWheel.Id;
        getLuckyWheelOptions(firstLuckyWheel.Id);
      } else {
        hasValidLuckyWheel.value = false;
      }
    }
  };

  const getLuckyWheelResult = async (PromotionId: number) => {
    try {
      getResultRequest.value.PromotionId = PromotionId;
      const response = await apis.getLuckyWheelResult(getResultRequest.value);
      if (response.ErrorCode === EnumApiErrorCode.success) {
        result.value = response.Data.Result;
      } else {
        traceId.value = response.Data.TraceId;
        isSomethingWentWrong.value = true;
        resultErrorMessage.value = response.Message;
        spinState.value = {
          isDefault: true,
          isSpinning: false,
          isStoppedSpin: false,
          settled: false,
        };
      }
    } catch (err) {
      isSomethingWentWrong.value = true;
    }
  };

  const getAvailableLuckyWheelTickets = async () => {
    try {
      const response = await apis.getAvailableLuckyWheelTicket();
      if (response.ErrorCode === EnumApiErrorCode.success) {
        availableTickets.value = response.Data;
        if (availableTickets.value.AvailableTickets.length) {
          hasValidLuckyWheel.value = true;
          isPromotionEnded.value = false;
          currentPromotionId.value = availableTickets.value.AvailableTickets[0].PromotionId;
        }
      }
    } catch (err) {
      console.log(err);
    }
  };

  const initData = async () => {
    await getAvailableLuckyWheelTickets();
    if (
      availableTickets.value.AvailableTickets &&
      availableTickets.value.AvailableTickets.length
    ) {
      getLuckyWheelOptions(
        availableTickets.value.AvailableTickets[0].PromotionId
      );
      result.value.RemainingTicketCount = availableTickets.value.AvailableTickets[0].TicketCount;
    } else findValidPromotion();
  };

  onMounted(() => {
    if (store.state.auth) {
      initData();
    }
  });

  const onClickSpin = async () => {
    isClickPlay.value = true;
    if (
      availableTickets.value.AvailableTickets &&
      availableTickets.value.AvailableTickets.length
    ) {
      await getLuckyWheelResult(
        availableTickets.value.AvailableTickets[0].PromotionId
      );
    }
    if (!isSomethingWentWrong.value) {
      spinState.value = {
        isDefault: false,
        isSpinning: true,
        isStoppedSpin: false,
        settled: false,
      };

      const clickPlayTimeout = setTimeout(() => {
        isClickPlay.value = false;
        clearTimeout(clickPlayTimeout);
      }, 1000);
      const wheel = document.querySelectorAll(".wheel") as any;
      const numbers = Array.from(document.querySelectorAll(".number"));
      const resultElement = document.getElementById(
        result.value.OptionId.toString()
      );

      if (resultElement && wheel) {
        wheel.forEach((item: HTMLElement) => {
          item.style.animation = "none";
          const resultIndex = numbers.findIndex((number) =>
            number.contains(resultElement)
          );
          const degrees = (10 - resultIndex) * 36;

          setTimeout(() => {
            item.style.transform = `rotate(${degrees + 360 * 4}deg)`;
            const stoppedSpinTimeout = setTimeout(() => {
              spinState.value = {
                isDefault: false,
                isSpinning: false,
                isStoppedSpin: true,
                settled: false,
              };
              clearTimeout(stoppedSpinTimeout);
            }, 5000);
            const settledTimeout = setTimeout(() => {
              spinState.value = {
                isDefault: false,
                isSpinning: false,
                isStoppedSpin: true,
                settled: true,
              };
              clearTimeout(settledTimeout);
            }, 6500);
          }, 300);
        });
      }
    }
  };

  const isMobile = !useIsDesktop();

  const onClickHowToGet = () => {
    dialogVisible.value = false;
    const isChinaMobile = store.state.selectedThemeName === "china" && isMobile;
    if (isChinaMobile) {
      router.push({
        name: "promotion",
        query: { id: currentPromotionId.value },
      });
    } else {
      router.push(`/promotion/${currentPromotionId.value}`);
    }
  };

  const onClickCloseDialog = () => {
    dialogVisible.value = false;
    router.replace({ query: undefined });
  };

  watch(
    () => spinState.value.isDefault,
    (newVal, oldVal) => {
      if (newVal && !oldVal) {
        const wheel = document.querySelectorAll(".wheel") as any;
        if (wheel) {
          wheel.forEach((item: HTMLElement) => {
            item.style.animation = "";
            item.style.transform = "";
            item.style.transition = "none";

            setTimeout(() => {
              item.style.transition = "transform 5s ease-in-out";
              item.style.animation = "spin 8.5s linear infinite";
            }, 300);
          });
        }
      }
    }
  );

  watch(
    () => dialogVisible.value,
    () => {
      if (!dialogVisible.value) {
        if (store.state.auth) initData();
        else findValidPromotion();
      }
    }
  );

  watch(promotions, () => {
    findValidPromotion();
  });

  const route = useRoute();
  watch(
    () => route.query,
    () => {
      if (route.name === "index" && route.query.luckyWheel) {
        dialogVisible.value = Boolean(route.query.luckyWheel);
      }
    },
    { deep: true }
  );

  return {
    dialogVisible,
    luckyWheelOptions,
    spinState,
    result,
    isClickPlay,
    isPromotionEnded,
    availableTickets,
    isSomethingWentWrong,
    resultErrorMessage,
    hasValidLuckyWheel,
    currency,
    traceId,

    onClickSpin,
    initData,
    onClickHowToGet,
    onClickCloseDialog,
  };
}
