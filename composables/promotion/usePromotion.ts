import { computed, ref } from "vue";
import _ from "lodash";
import apis from "@/utils/apis";
import { store } from "@/store";
import { IPromotion, IRequestPromotion } from "@/models/promotion/promotion";
import notificationHelper from "@/utils/elementUiHelpers/notificationHelper";
import EnumMessageType from "@/models/enums/enumMessageType";
import commonHelper from "@/utils/commonHelper";
import textHelper from '~~/utils/formatHelpers/textFormatHelper';
import { IApplyPromotionRequest } from "~~/models/promotion/promotionApplyList";

const dialogInfo = ref<IPromotion>();
const today = new Date().toJSON().split("T")[0];
const visible = ref(false);
export const usePromotion = () => {
  const { $t } = useNuxtApp();
  const isProcessing = ref(false);
  const authenticated = computed(() => store.state.auth);
  const promotions = ref<Array<IPromotion>>([]);
  const promotionModel = ref<IPromotion>();
  const isDialogVisible = ref(false);
  const tabMenu = ref("All");
  const promotionCategories = ref<Array<ICreatePromotionCategory>>([]);
  const promotionType = ref<Array<IPromotionType>>([]);
  const gameProviderList = ref<Array<IGameProvider>>([]);
  const isLoading = ref(false);
  const applyRequest = ref<IApplyPromotionRequest>({
    PromotionId: 0,
    Device: commonHelper.getUserAgent(),
    IsNew: false,
    Url: String(window.location),
    FingerPrint: store.state.visitorId,
    OnlineId: Number(localStorage.getItem("onlineId")),
  });
  const promotionGroup = [
    {
      Type: "All Offer",
      label: "all offer",
      class: "all",
    },
    {
      Type: "First Deposit",
      label: "First Deposit",
      class: "first_deposit",
    },
    {
      Type: "Common",
      label: "Common",
      class: "common",
    },
    {
      Type: "Rebate Offer",
      label: "Rebate On",
      class: "rebate",
    },
  ];
  const selectedType = ref("all offer");
  const getCreatePromotionDetail = async () => {
    isLoading.value = true;
    const response = await apis.getCreatePromotionDetails();
    if (response.isSuccessful) {
      promotionCategories.value = response.Data.Categories;
      gameProviderList.value = response.Data.GameProviderList;
      promotionType.value = response.Data.PromotionTypes;
    }
    isLoading.value = false;
  };
  const promotionFiltered = (promos: Array<IPromotion>) => {
    const filtered = _.filter(
      promos,
      (promotion) =>
        promotion.PromotionTypeId !== 12 && _.includes([0, 3], promotion.Status)
    );
    return _.sortBy(filtered, (promo) => promo.DisplayOrder);
  };
  const getPromotions = async () => {
    // Prevent fetching landing page
    if (useState("isLandingPage").value) return;
    let currency = store.state.auth
      ? store.state.currency
      : store.state.geography.Currency;
    currency = !store.state.auth && currency === "KHR" ? "USD" : currency;
    const params: IRequestPromotion = {
      Domain: commonHelper.getDomain(),
      Currency: currency,
      CustomerId: Number(store.state.customerId) ?? 0,
    };
    const response = await apis.getPromotions(params);
    if (response.isSuccessful) {
      if (response.Data.Promotions.length !== 0) {
        promotions.value = promotionFiltered(response.Data.Promotions);
      }
    }
  };
  const onMoreInformation = (arg: IPromotion) => {
    if (!useIsDesktop() && promotionModel.value?.Id === arg.Id) {
      useRouter().push(`/promotion/${arg?.Id}`);
      return;
    }
    promotionModel.value = arg;
    isDialogVisible.value = true;
  };

  const onApplyPromotion = async (promotion: IPromotion) => {
    isProcessing.value = true;
    applyRequest.value.IsNew = promotion.IsNew;
    applyRequest.value.PromotionId = promotion.Id;
    applyRequest.value.FingerPrint = store.state.visitorId;
    applyRequest.value.OnlineId = Number(localStorage.getItem("onlineId") ?? 0);
    const response = await apis
      .applyPromotion(applyRequest.value)
      .finally(() => {
        applyRequest.value.PromotionId = 0;
      });
    if (response.isSuccessful) {
      notificationHelper.notification(
        response.ErrorMessageForDisplay,
        EnumMessageType.Success
      );
    } else {
      const notificationMessage = $t(response.ErrorMessageForDisplay) + (response.Data?.TraceId ? ` (${response.Data.TraceId})` : ' (0)');
      notificationHelper.notification(notificationMessage, EnumMessageType.Error);
    }
    isProcessing.value = false;
  };
  getPromotions();
  const onDialog = (promotion: IPromotion) => {
    visible.value = true;
    dialogInfo.value = promotion;
  };
  const buttonApplyPromotionEnabled = (promotion: IPromotion) => {
    const newDate = new Date();
    const formattedDate = Date.parse(textHelper.getGmtMinusFourDate(String(newDate), false, false));
    const validateDate =
    Date.parse(promotion.StartDate) < formattedDate &&
      formattedDate < Date.parse(promotion.EndDate);
    return authenticated.value && promotion.Status === 3 && validateDate && promotion.AutoRebateOption !== '1';
  };
  const validateLuckyWheelbutton = (promotion: IPromotion) => {
    const newDate = new Date();
    const formattedDate = Date.parse(textHelper.getGmtMinusFourDate(String(newDate), false, false));
    const validateDate =
      Date.parse(promotion.StartDate) <= formattedDate &&
      formattedDate <= Date.parse(promotion.EndDate);
    return promotion.PromotionTypeId === 10 && validateDate;
  };
  const checkPromotionGroup = (promoType: string) => {
    if (promoType.toLowerCase().includes("rebate on")) {
      return $t("rebate");
    }
    if (promoType.toLowerCase().includes("first deposit")) {
      return $t("first_deposit");
    }
    return $t("common");
  };
  const promotionListForDisplay = computed(() => {
    if (promotions.value.length > 0) {
      if (
        selectedType.value.toLowerCase() === "rebate on" ||
        selectedType.value.toLowerCase() === "first deposit"
      ) {
        return promotions.value.filter((promotion) =>
          promotion.PromotionType.toLowerCase().includes(selectedType.value.toLowerCase())
        );
      }
      if (selectedType.value === "Common") {
        return promotions.value.filter(
          (promotion) =>
            !promotion.PromotionType.toLowerCase().includes("rebate on") &&
            !promotion.PromotionType.toLowerCase().includes("first deposit")
        );
      }
      return promotions.value;
    }
    return promotions.value;
  });
  const onClickPromotion = (promotion: string) => {
    selectedType.value = promotion;
  };
  const themeProperties = store.state.companyThemePropertiesFromApi.ThemeProperties;
  const isProviderPromotion1 = ref(false);
  themeProperties.forEach(property => {
      if (property.Text === 'Provider_type_promotion_1') {
          isProviderPromotion1.value = true;
      }
  });
  const randomSixCharacter = (Math.random() + 1).toString(36).substring(2, 8);
  const assignHashText = computed(
    () =>
      `#${randomSixCharacter}_${
        store.state.auth
          ? store.state.currency
          : store.state.geography.CountryCode
      }`
  );
  const router = useRouter();

  const loginDialogTheme = ["main", "black", "sbo", "global", "playful"];
  const isMobile = !useDevice().isDesktop;

  const onClickSpinNow = () => {
    if (store.state.auth)
      router.push({ name: "index", query: { luckyWheel: "true" } });
    else if (
      !store.state.auth &&
      loginDialogTheme.includes(store.state.selectedThemeName) &&
      isMobile
    ) {
      commonHelper.toggleLoginRegisterDialog();
    } else {
      router.push("/login");
    }
  };

  const { allowCompany, isMain } = commonHelper;

  return {
    promotions,
    isDialogVisible,
    onMoreInformation,
    authenticated,
    promotionModel,
    isProcessing,
    onApplyPromotion,
    onDialog,
    visible,
    dialogInfo,
    today,
    tabMenu,
    promotionCategories,
    promotionType,
    isLoading,
    getCreatePromotionDetail,
    gameProviderList,
    buttonApplyPromotionEnabled,
    checkPromotionGroup,
    promotionGroup,
    selectedType,
    promotionListForDisplay,
    onClickPromotion,
    assignHashText,
    applyRequest,
    isProviderPromotion1,
    allowCompany,
    isMain,
    onClickSpinNow,
    validateLuckyWheelbutton,
  };
};
