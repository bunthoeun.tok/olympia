import { ref } from "vue";
import apis from "@/utils/apis";
import EnumPromotionStatus from "@/models/enums/enumPromotionStatus";
import IGetPromotionApplyListResponse from "@/models/promotion/getPromotionApplyListResponse";

export const useMyPromotion = () => {
  const promotion = ref<IGetPromotionApplyListResponse>(
    {} as IGetPromotionApplyListResponse
  );
  const isLoading = ref(true);
  const approved = ref(EnumPromotionStatus.Approved);
  const getPromotion = async () => {
    isLoading.value = true;
    const res = await apis.getPromotionApplyList({
      Status: "All",
    });
    if (res.isSuccessful) {
      promotion.value = res.Data;
      isLoading.value = false;
    }
  };
  getPromotion();
  return { promotion, approved, isLoading };
};
