import { Ref, ref, reactive } from "vue";
import { format } from "date-fns";
import { IStatementRequest, StatementListResponse } from "@/models/statement";
import textHelper from "@/utils/formatHelpers/textFormatHelper";
import apis from "@/utils/apis";
import notificationHelper from "@/utils/elementUiHelpers/notificationHelper";
import EnumMessageType from "@/models/enums/enumMessageType";

interface IStatementList {
  isDataLoading: Ref<boolean>;
  statementList: Ref<Array<StatementListResponse>>;
  monthFilterRange: any;
  getStatmentByMonth: (statement: any) => Promise<void>;
  selectedMonth: Ref<string>;
}

export const useStatement = (): IStatementList => {
  const isDataLoading = ref(false);
  const statementList = ref<Array<StatementListResponse>>([]);
  const date = new Date();
  const dateMonthRange = {
    firstDayOfLastTwoMonth: new Date(
      date.getFullYear(),
      date.getMonth() - 2,
      1
    ).toString(),
    lastDayOfLastTwoMonth: new Date(
      date.getFullYear(),
      date.getMonth() - 1,
      0
    ).toString(),
    firstDayOfLastMonth: new Date(
      date.getFullYear(),
      date.getMonth() - 1,
      1
    ).toString(),
    lastDayOfLastMonth: new Date(
      date.getFullYear(),
      date.getMonth(),
      0
    ).toString(),
    firstDayOfThisMont: new Date(
      date.getFullYear(),
      date.getMonth(),
      1
    ).toString(),
    // todayDateOfThisMonth: format(new Date(), 'MM/dd'),
    todayDateOfThisMonth: new Date().toString(),
  };
  const selectedMonth = ref(
    `${textHelper.getMonthRange(
      dateMonthRange.firstDayOfThisMont.toString()
    )} ~ ${dateMonthRange.todayDateOfThisMonth}`
  );
  const monthFilterRange = reactive([
    {
      MonthRange: `${textHelper.getMonthRange(
        dateMonthRange.firstDayOfLastTwoMonth
      )} ~ ${textHelper.getMonthRange(dateMonthRange.lastDayOfLastTwoMonth)}`,
      StartDate: textHelper.getGmtMinusFourDate(
        dateMonthRange.firstDayOfLastTwoMonth,
        true
      ),
      EndDate: textHelper.getGmtMinusFourDate(
        dateMonthRange.lastDayOfLastTwoMonth,
        true
      ),
    },
    {
      MonthRange: `${textHelper.getMonthRange(
        dateMonthRange.firstDayOfLastMonth
      )} ~ ${textHelper.getMonthRange(dateMonthRange.lastDayOfLastMonth)}`,
      StartDate: textHelper.getGmtMinusFourDate(
        dateMonthRange.firstDayOfLastMonth,
        true
      ),
      EndDate: textHelper.getGmtMinusFourDate(
        dateMonthRange.lastDayOfLastMonth,
        true
      ),
    },
    {
      MonthRange: `${textHelper.getMonthRange(
        dateMonthRange.firstDayOfThisMont
      )} ~ ${textHelper.getMonthRange(dateMonthRange.todayDateOfThisMonth)}`,
      StartDate: textHelper.getGmtMinusFourDate(
        dateMonthRange.firstDayOfThisMont,
        true
      ),
      EndDate: format(new Date(), "yyyy-MM-dd HH:mm:ss"),
    },
  ]);
  selectedMonth.value =
    monthFilterRange[monthFilterRange.length - 1].MonthRange;
  const getStatmentByMonth = async (statement: any) => {
    isDataLoading.value = true;
    const request: IStatementRequest = {
      StartTime: textHelper.getGmtMinusFourDate(
        dateMonthRange.firstDayOfThisMont,
        true
      ),
      EndTime: format(new Date(), "yyyy-MM-dd HH:mm:ss"),
    };
    if (statement !== "") {
      request.StartTime = statement.StartDate;
      request.EndTime = statement.EndDate;
      selectedMonth.value = statement.MonthRange;
    }
    const response = await apis.getStatement(request);
    if (response.isSuccessful) {
      statementList.value = response.Data.StatementList.map(
        (statementResponse) => new StatementListResponse(statementResponse)
      );
    } else {
      notificationHelper.notification(response.Message, EnumMessageType.Error);
    }
    isDataLoading.value = false;
  };
  getStatmentByMonth("");
  return {
    isDataLoading,
    statementList,
    monthFilterRange,
    getStatmentByMonth,
    selectedMonth,
  };
};
