import { computed, Ref, ref, watch } from "vue";
import _ from "lodash";
import apis from "@/utils/apis";
import notificationHelper from "@/utils/elementUiHelpers/notificationHelper";
import IGetBetListRequest from "@/models/getBetListRequest";
import { ISeamlessGameBet } from "@/models/seamlessGameBets";
import { IGameProvider, IGameProviders } from "@/models/gameProvider";
import commonHelper from "@/utils/commonHelper";
import ApiResponse from "@/models/apiResponse";
import { IGetBetListResponse } from "@/models/getBetListResponse";
import IGetBetPayloadRequest from "@/models/getBetPayloadRequest";
import IGetSportBetRequest from "@/models/sportbet/getSportBetRequest";
import { ISportBet, SportBet } from "@/models/sportbet/sportbet";
import EnumMessageType from "@/models/enums/enumMessageType";
import textHelper from "@/utils/formatHelpers/textFormatHelper";
import { IBet } from "@/models/bet";
import { store } from "@/store";
import { TabInfo } from "~~/models/IGetPlayableGamesResponse";

interface IBetHistory {
  gameBetList: Ref<Array<ISeamlessGameBet>>;
  sportBetList: Ref<Array<ISportBet>>;
  gameProviders: Ref<Array<IGameProvider>>;
  selectedProvider: Ref<number>;
  activeTab: Ref<string>;
  onSearch: () => void;
  disableSelect: Ref<boolean>;
  currentPage: Ref<number>;
  totalBetList: Ref<number>;
  totalBetPerPage: Ref<number>;
  isLoading: Ref<boolean>;
  selectedStartDate: Ref<string>;
  selectedEndDate: Ref<string>;
  selectedSportStatus: Ref<string>;
  allSportStatus: Ref<Array<string>>;
  isSettled: Ref<boolean>;
  onDisplayBetDetail: (bet: IBet) => void;
  isSportType: Ref<boolean>;
  gameRequest: Ref<IGetBetListRequest>;
  getSportBetList: () => Promise<void>;
  allTabs: ComputedRef<Array<TabInfo>>;
}
export const useBetHistory = (): IBetHistory => {
  const route = useRoute();
  const router = useRouter();
  const isUCPopup = computed(() => route.fullPath.includes("uc=/bet-history"));
  const isTelegramRoute = computed(() => route.fullPath.includes("telegram/bet-history"));
  const currentTab = computed(() => {
    const ucUrlParts = String(route.query.uc).split("/").filter(part => part);
    if (ucUrlParts[1]) {
      return ucUrlParts[1];
    }
    return route.params.tab;
  });
  const isLoading = ref(false);
  const { startDate, endDate } = commonHelper.getDateRange(
    "today",
    "yyyy/MM/dd HH:mm:ss"
  );
  const gameBetList = ref<Array<ISeamlessGameBet>>([]);
  const sportBetList = ref<Array<ISportBet>>([]);
  const selectedStartDate = ref(
    textHelper.getGmtMinusFourDate(String(startDate))
  );
  const selectedEndDate = ref(textHelper.getGmtMinusFourDate(String(endDate)));
  const isSettled = ref(false);
  const gameProviders = ref<Array<IGameProvider>>([]);
  const selectedSportStatus = ref("Running");
  const allSportStatus = ref(["Running", "Settled"]);
  const selectedProvider = ref(-5);
  const disableSelect = ref<boolean>(false);
  const activeTab = ref<string>(
    String(route.params.tab !== "" ? route.params.tab : "sport")
  );
  const betType = computed(() => activeTab.value);
  const isSportType = computed(
    () =>
      betType.value === BET_HISTORY_TAB.SPORT ||
      betType.value === BET_HISTORY_TAB.VIRTUAL_SPORT
  );
  const currentPage = ref(1);
  const totalBetList = ref<number>(1);
  const totalBetPerPage = ref<number>(15);
  const gameRequest = ref<IGetBetListRequest>({
    GameProviderId: -5,
    IsGetFromAllProviders: true,
    WinLoseDateStart: selectedStartDate.value,
    WinLoseDateEnd: selectedEndDate.value,
    RowCountPerPage: totalBetPerPage.value,
    Page: currentPage.value,
  });
  const isLoadMore = computed(() => {
    return !useIsDesktop() && currentPage.value > 1;
  });
  const sportRequest = ref<IGetSportBetRequest>({
    IsRunningBet: true,
    Language: store.state.locale,
    GameProviderId: -5,
    IsGetFromAllProviders: true,
    WinLoseDateStart: selectedStartDate.value,
    WinLoseDateEnd: selectedEndDate.value,
    RowCountPerPage: totalBetPerPage.value,
    Page: currentPage.value,
  });

  const allTabs = computed(()=>tabInfoMenuHeader(store.state.tabInfos));
  const getGameProvider = async () => {
    selectedProvider.value = -5;
    disableSelect.value = false;
    gameProviders.value = [{ GameProviderName: "All", GameProviderId: -5 }];
    let response: ApiResponse<IGameProviders>;
    switch (betType.value) {
      case BET_HISTORY_TAB.GAME_BET:
        response = await apis.getGamesProvider();
        break;
      case BET_HISTORY_TAB.CASINO:
        response = await apis.getCasinoProvider();
        break;
      case BET_HISTORY_TAB.COCKFIGHT:
        response = await apis.getCockfightProvider();
        break;
      case BET_HISTORY_TAB.POKER:
        response = await apis.getPokerProvider();
        break;
      case BET_HISTORY_TAB.LOTTERY:
        response = await apis.getLotteryProvider();
        break;
      default:
        response = await apis.getSportProvider();
    }
    if (response.isSuccessful) {
      response.Data.GameProviders.forEach((value) => {
        gameProviders.value.push(value);
      });
    } else {
      notificationHelper.notification(response.Message, EnumMessageType.Error);
    }
    gameProviders.value = _.uniqBy(gameProviders.value, "GameProviderName");
  };
  const getSportBetList = async () => {
    const isBetRunning = !isSettled.value;
    isLoading.value = true;
    sportRequest.value = {
      IsRunningBet: isBetRunning,
      Language: store.state.locale,
      GameProviderId: selectedProvider.value,
      IsGetFromAllProviders: selectedProvider.value === -5,
      WinLoseDateStart: selectedStartDate.value,
      WinLoseDateEnd: selectedEndDate.value,
      RowCountPerPage: totalBetPerPage.value,
      Page: currentPage.value,
    };
    const response =
      betType.value === BET_HISTORY_TAB.VIRTUAL_SPORT
        ? await apis.getVirtualSportsbetList(sportRequest.value)
        : await apis.getSportsbetList(sportRequest.value);
    if (response.isSuccessful) {
      const _betList = response.Data.SportBets.map((value) => {
        const temporaryObject = new SportBet(value);
        totalBetList.value = value.MaxPage * totalBetPerPage.value;
        return temporaryObject;
      });
      if (isLoadMore.value) {
        sportBetList.value = [...sportBetList.value, ..._betList];
      } else {
        sportBetList.value = _betList;
      }
      isLoading.value = false;
    } else {
      isLoading.value = false;
      notificationHelper.notification(response.Message, EnumMessageType.Error);
    }
    if (response.Data.SportBets.length === 0) {
      totalBetList.value = 0;
    }
  };
  const getGameBetList = async () => {
    isLoading.value = true;
    gameRequest.value.Page = currentPage.value;
    let response: ApiResponse<IGetBetListResponse>;
    gameRequest.value.GameProviderId = selectedProvider.value;
    gameRequest.value.IsGetFromAllProviders = selectedProvider.value === -5;
    gameRequest.value.WinLoseDateStart = selectedStartDate.value;
    gameRequest.value.WinLoseDateEnd = selectedEndDate.value;
    if (betType.value === BET_HISTORY_TAB.CASINO) {
      response = await apis.getCasinoBetList(gameRequest.value);
    } else if (betType.value === BET_HISTORY_TAB.COCKFIGHT) {
      response = await apis.getCockfightingBetList(gameRequest.value);
    } else if (betType.value === BET_HISTORY_TAB.POKER) {
      response = await apis.getPokerBetList(gameRequest.value);
    } else if (betType.value === BET_HISTORY_TAB.LOTTERY) {
      response = await apis.getLotteryBetList(gameRequest.value);
    } else {
      response = await apis.getGamesBetList(gameRequest.value);
    }
    if (response.isSuccessful) {
      const _gameBetList = response.Data.SeamlessGameBets.map((gamebetlist) => {
        totalBetList.value = gamebetlist.MaxPage * totalBetPerPage.value;
        return gamebetlist;
      });
      if (isLoadMore.value) {
        gameBetList.value = [...gameBetList.value, ..._gameBetList];
      } else {
        gameBetList.value = _gameBetList;
      }
      isLoading.value = false;
    } else {
      isLoading.value = false;
      notificationHelper.notification(response.Message, EnumMessageType.Error);
    }
    if (response.Data.SeamlessGameBets.length === 0) {
      totalBetList.value = 0;
    }
  };
  if (betType.value === BET_HISTORY_TAB.VIRTUAL_SPORT) {
    gameProviders.value = [{ GameProviderName: "SBO", GameProviderId: -1 }];
    selectedProvider.value = -1;
    disableSelect.value = true;
  } else {
    getGameProvider();
  }
  if (isSportType.value) {
    getSportBetList();
  } else {
    getGameBetList();
  }
  const onSearch = () => {
    currentPage.value = 1;
    gameRequest.value.Page = 1;
    if (isSportType.value) {
      isSettled.value = selectedSportStatus.value === "Settled";
      getSportBetList();
    } else {
      getGameBetList();
    }
  };
  watch(betType, () => {
    selectedStartDate.value = textHelper.getGmtMinusFourDate(String(startDate));
    selectedEndDate.value = textHelper.getGmtMinusFourDate(String(endDate));
    if (isUCPopup.value) {
      router.push({
        path: route.path,
        query: {
          uc: `${BET_HISTORY_ROUTES.INDEX}/${betType.value}`,
        }
      });
    }
    else if (isTelegramRoute.value){
      router.push({
        path: `/telegram${BET_HISTORY_ROUTES.INDEX}/${betType.value}`
      })
    } 
    else {
      router.push({
        path: `${BET_HISTORY_ROUTES.INDEX}/${betType.value}`,
      });
    }
    selectedProvider.value = -5;
    currentPage.value = 1;
    isSettled.value = false;
    selectedSportStatus.value = "Running";
    if (betType.value !== BET_HISTORY_TAB.VIRTUAL_SPORT) {
      getGameProvider();
    }
    switch (betType.value) {
      case BET_HISTORY_TAB.SPORT:
        disableSelect.value = false;
        getSportBetList();
        break;
      case BET_HISTORY_TAB.VIRTUAL_SPORT:
        disableSelect.value = true;
        gameProviders.value = [{ GameProviderName: "SBO", GameProviderId: -1 }];
        selectedProvider.value = -1;
        getSportBetList();
        break;
      default:
        disableSelect.value = false;
        getGameBetList();
    }
  });
  watch(currentPage, () => {
    if (isSportType.value) {
      getSportBetList();
    } else {
      getGameBetList();
    }
  });
  const onDisplayBetDetail = async (bet: IBet) => {
    const params: IGetBetPayloadRequest = {
      Language: useCookie(KEYS.LANGUAGE_COOKIE_KEY).value ?? "en",
      GameProviderType: bet.GameProviderType,
      GameProviderId: bet.GameProviderId,
      RefNo: bet.RefNo,
    };
    const response = await apis.getBetPayload(params);
    if (response.isSuccessful) {
      if (response.Data.Url) {
        window.open(response.Data.Url.includes('http') ? response.Data.Url : `https://${response.Data.Url}`, "popUpWindow", "height=auto");
      }
    }
  };
  
  function tabInfoMenuHeader (tabInfos: Array<TabInfo>) {
    return tabInfos.map((tab: TabInfo) => {
      const __tab = _.toLower(tab.TabType);
      const tabLang = `${_.replace(__tab, " ", "_")}`;
      const tabURL =
        _.replace(__tab, " ", "") === "cockfight"
          ? "/cockfight"
          : `/${_.replace(__tab, " ", "-")}`;
      tab.TabLanguage = tabLang;
      tab.TabURL = tabURL;
      return tab;
    });
  };

  const tabsInfoToBetHistoryTab = {
    sports: "sport",
    virtual_sports: "vSport",
    live_casino: "casino",
    games: "gameBet",
    cock_fight: "cockfight",
    poker: "poker",
    lottery: "lottery",
  }
  type TabInfoKeys = keyof typeof tabsInfoToBetHistoryTab;

  const setActiveTab = () => {
    const firstTab = allTabs.value[0].TabLanguage as TabInfoKeys;
    const defaultFromTabs = tabsInfoToBetHistoryTab[firstTab];
    const defaultTab = allTabs.value.length > 0 ? defaultFromTabs : BET_HISTORY_TAB.SPORT;
    if (!currentTab.value) {
      let navigateOption = {
          params: {
            tab: defaultTab,
          },
        }
      if (isUCPopup.value) {
        navigateOption = {
          // @ts-ignore
          path: route.path,
          query: {
            uc: `/bet-history/${defaultTab}`,
          }
        }
      } else if (isTelegramRoute.value){
        navigateOption = {
          // @ts-ignore
          path: '/telegram'+ route.path,
        }
      } 

      const betHistoryTabs = Object.values(BET_HISTORY_TAB);
      const currentTabRoute = String(currentTab.value ?? defaultTab);
      if (betHistoryTabs.includes(currentTabRoute)) {
        activeTab.value = currentTabRoute;
      }
      navigateTo(navigateOption);
      return;
    }
    const betHistoryTabs = Object.values(BET_HISTORY_TAB);
    const currentTabRoute = String(currentTab.value ?? defaultTab);
    
    if (betHistoryTabs.includes(currentTabRoute)) {
      activeTab.value = currentTabRoute;
    }
  };


  watch(currentTab, () => {
    if (!currentTab.value) return;
    setActiveTab();
  });
  onMounted(() => {
    setActiveTab();
  });
  return {
    gameBetList,
    sportBetList,
    gameProviders,
    selectedProvider,
    activeTab,
    onSearch,
    disableSelect,
    currentPage,
    totalBetList,
    totalBetPerPage,
    isLoading,
    selectedStartDate,
    selectedEndDate,
    selectedSportStatus,
    allSportStatus,
    isSettled,
    onDisplayBetDetail,
    isSportType,
    gameRequest,
    getSportBetList,
    allTabs,
  };
};
