import { ref, reactive, computed, Ref, ComputedRef } from "vue";
import _ from "lodash";
import type { FormInstance } from "element-plus";
import useCurrencyWhitelist from "./useCurrencyWhitelist";
import apis from "@/utils/apis";
import EnumMessageType from "@/models/enums/enumMessageType";
import notificationHelper from "@/utils/elementUiHelpers/notificationHelper";
import formHelper from "@/utils/elementUiHelpers/formHelper";
import commonHelper from "@/utils/commonHelper";
import { IRequestLogin, IRequestPlayerIsOnline } from "@/models/login";
import { store } from "@/store";
import { useCaptcha } from "@/composables/captcha/useCaptcha";
import themePropertyHelper from "@/utils/themePropertyHelper";
import { IThemePropertiesFromApi } from "@/models/companyThemePropertiesRespone";
import { gameStore } from "~~/store/game";
import featuresToggleHelper from "@/utils/featuresToggleHelper";
import EnumThemePropertyKey from "~~/models/enums/enumThemePropertyKey";
import { ICustomerInfoRequest } from "~~/models/telegramCustomerInfo";
import formRulesHelper from "~~/utils/elementUiHelpers/formRulesHelper";
import { ITelegramUser } from "~~/models/telegram";
import { EnumSelectedThemeMode } from "~~/models/enums/enumSelectedThemeMode";

interface IUseLogin {
  loginFormRef: Ref<FormInstance | undefined>;
  loginFormData: IRequestLogin;
  loginRules: any;
  submitLogin: (formEl: FormInstance) => void;
  proceedLogin: (isFromLandingPage: boolean) => void;
  isLoginProccessing: Ref<boolean>;
  getCaptchaErrorMsg: (valid: any) => void;
  captchaErrorMsg: Ref<string>;
  register: ComputedRef<IThemePropertiesFromApi>;
  clientCaptcha: Ref<IThemePropertiesFromApi>;
  isSubmitSuccess: Ref<boolean>;
  isTelegramBotSetup: Ref<boolean>;
  telegramBotUsername: Ref<string>;
  checkLoginTelegramSetup: () => void;
  telegramCallbackFunction: (user: ITelegramUser) => void;
}
enum PasswordLength {
  minLength = 6,
  maxLength = 20,
}
const isLoginProccessing = ref(false);
const isRestricted = ref(false);
const isSubmitSuccess = ref<boolean>(false);
const telegramBotUsername = ref("");
const handleMultiplePlayerLogin = async () => {
  const request: IRequestPlayerIsOnline = {
    OnlineId: Number(localStorage.getItem("onlineId") ?? 0),
  };
  const response = await apis.getPlayerIsOnline(request);
  if (response.ErrorCode === 204 || response.ErrorCode === 205) {
    notificationHelper.notification(
      response.ErrorMessageForDisplay,
      EnumMessageType.Error
    );
    setTimeout(() => apis.getLogout(), 2000);
  }
};
const updateCustomerInfoTelegram = async (customerId: number) => {
  const route = useRoute();
  const request: ICustomerInfoRequest = {
    CustomerId: customerId ?? 0,
    BotId: Number(route.query.botId ?? 0),
    ChatId: Number(route.query.chatId ?? 0),
    TelegramUserId: Number(route.query.tguid ?? 0),
  };
  try {
    const response = await apis.updateCustomerInformation(request);
    if (response.isSuccessful) {
      isSubmitSuccess.value = true;
    } else {
      notificationHelper.notification(
        response.ErrorMessageForDisplay,
        EnumMessageType.Error
      );
    }
  } catch (error) {
    notificationHelper.notification("Failed Update", EnumMessageType.Error);
  }
};
export default function useLogin(): IUseLogin {
  const { $t } = useNuxtApp();
  const isMobile = !useIsDesktop();
  const template = isMobile ? "mobile" : "desktop";
  const redirected = window.location.origin;
  const { isLiteTheme, redirectLite } = useLiteTheme();
  const loginFormRef = ref<FormInstance>();
  const loginFormData: IRequestLogin = reactive({
    Username: "",
    Password: "",
    OnlineId: 0,
    Fingerprint: store.state.visitorId,
    // WebId: 1,
    WebId: store.state.webId,
    Ip: store.state.geography?.Ip ?? "",
    Country: store.state.geography?.CountryCode ?? "",
    Url: String(window.location),
    Region: store.state.geography?.Region ?? "",
    City: store.state.geography?.City ?? "",
    Platform: template,
    Browser: commonHelper.getUserAgent(),
    Lang: store.state.locale,
    Captcha: "",
  });
  const { captchaErrorMsg, getCaptchaErrorMsg } = useCaptcha(loginFormData);
  const { whiteListCurrencies, isIpWhitelistEnabled } = useCurrencyWhitelist();
  const manualHandleRestrictErrorMessage = (currency: string) => {
    const websiteCurrencyWhiteList = _.find(
      whiteListCurrencies.value,
      (item) => item.Currency === _.toUpper(currency)
    );
    if (websiteCurrencyWhiteList === null && !isIpWhitelistEnabled.value) {
      const message = `Our system detected you are using { ${currency} } but currency in your location { ${store.state.geography?.CountryCode} } is not available`;
      notificationHelper.notification(message, EnumMessageType.Error);
      isLoginProccessing.value = false;
      isRestricted.value = true;
    }
  };
  const passwordFormat = () =>
    formRulesHelper.passwordFormat(loginFormData.Password, true);
  const rules = {
    Username: { required: true },
    Password: { customRule: passwordFormat, required: true },
    Captcha: { customRule: () => captchaErrorMsg.value, required: true },
  };
  const loginRules = formHelper.getRules(rules, loginFormData);
  const themeColor = computed(() =>
    themePropertyHelper.getSingleThemeProperty(
      store.state.companyThemePropertiesFromApi.ThemeProperties,
      "theme_color"
    )
  );
  const register = computed(() =>
    themePropertyHelper.getSingleThemeProperty(
      store.state.companyThemePropertiesFromApi.ThemeProperties,
      "register"
    )
  );
  const isLightBlue = themeColor.value.Text === "main_lightblue";
  const IsRedirectToSportsPageLightBlueSky = ref(false);
  const getCustomerSetting = async () => {
    const response = await commonHelper.getCustomerSettingById(
      "IsRedirectToSportsPage-LightBlueSky"
    );
    IsRedirectToSportsPageLightBlueSky.value = response && response.Value === "Y";
  };
  const shouldRedirectToSport = async () => {
    await getCustomerSetting();
    if (IsRedirectToSportsPageLightBlueSky.value) {
      window.location.href = "/sports";
    } else {
      window.location.href = "/";
    }
  };
  const route = useRoute();
  const isFromTelegramBot = computed(() =>
    route.path.includes("telegram-login")
  );
  const proceedLogin = async (isFromLandingPage = false) => {
    isLoginProccessing.value = true;
    loginFormData.Fingerprint = store.state.visitorId;
    const response = await apis.getLogin(loginFormData);
    if (response.isSuccessful) {
      if (isFromTelegramBot.value) {
        updateCustomerInfoTelegram(response.Data.CustomerId);
        notificationHelper.notification(
          response.ErrorMessageForDisplay,
          EnumMessageType.Success
        );
      } else {
        const result = response.Data;
        localStorage.setItem("onlineId", String(result.OnlineId));
        localStorage.setItem("betlisttokenid", String(result.OnlineId));
        if (_.toUpper(result.Currency) !== "TMP") {
          manualHandleRestrictErrorMessage(result.Currency);
          if (isRestricted.value) return;
        }
        if (!isFromLandingPage) {
          notificationHelper.notification(
            response.ErrorMessageForDisplay,
            EnumMessageType.Success
          );
        }
        store.commit(STORE_MUTATIONS.AUTH, true);
        store.commit(STORE_MUTATIONS.TOKEN, result.Token);
        // store.commit(STORE_MUTATIONS.GAMES, result.GamesPriority);
        // gameStore.commit(STORE_MUTATIONS.GAMES, result.GamesPriority);
        store.commit(STORE_MUTATIONS.ONLINE_ID, result.OnlineId);
        store.commit(STORE_MUTATIONS.IS_STOCK_PLAYER, result.IsStockPlayer);
        store.commit(
          STORE_MUTATIONS.IS_PASSWORD_EXPIRED,
          result.IsPasswordExpired
        );
        store.commit(
          STORE_MUTATIONS.IS_LOGIN_NAME_UPDATED,
          result.IsLoginNameUpdated
        );
        store.commit(
          STORE_MUTATIONS.IS_PAYMENT_PASSWORD_SET,
          result.IsPaymentPasswordEnable
        );
        store.commit(STORE_MUTATIONS.LOGIN_DIALOG, false);
        const routed = result.IsPasswordExpired ? "account/password" : "";
        localStorage.setItem("DialogAds", "show");
        localStorage.setItem(
          "PeruShopEnabled",
          store.state.isPerushopEnabled ? "true" : "false"
        );
        localStorage.setItem(
          "ThemeMode",
          store.state.selectedThemeMode ===  EnumSelectedThemeMode.Light ? "Light" : "Dark",
        );
    
        if (isLiteTheme()) {
          redirectLite();
        } else if (isLightBlue) {
          shouldRedirectToSport();
        } else {
          window.location.href = `${redirected}/${routed}`;
        }
        if (result.Notifications.length > 0) {
          localStorage.setItem("DialogNotification", "show");
        }
      }
    } else {
      notificationHelper.notification(
        response.ErrorMessageForDisplay,
        EnumMessageType.Error
      );
    }
    isLoginProccessing.value = false;
  };
  const submitLogin = formHelper.getSubmitFunction(proceedLogin);
  const clientCaptcha = computed(() =>
    featuresToggleHelper.getThemePropertyFromApi(
      EnumThemePropertyKey.HtmlId,
      "login_captcha_two"
    )
  );

  const isTelegramBotSetup = ref(false);
  const checkLoginTelegramSetup = async () => {
    const tokenResponse = await commonHelper.getCustomerSettingById(
      "TelegramBetBotToken"
    );
    const domainResponse = await commonHelper.getCustomerSettingById(
      "TelegramBetBotDomain"
    );
    if (domainResponse && domainResponse.Value === commonHelper.getDomain()) {
      if (tokenResponse && tokenResponse.Value != null) {
        const telegramUrl =
          "https://api.telegram.org/bot" + tokenResponse.Value + "/getMe";
        const bot: any = await apis.getTelegramBotInformation(telegramUrl);
        if (bot.ok) {
          telegramBotUsername.value = bot.result.username;
          isTelegramBotSetup.value = true;
        }
      }
    }
  };

  const telegramCallbackFunction = (user: ITelegramUser) => {
    isLoginProccessing.value = true;
    const { username, id } = user;
    loginFormData.Username = username;
    loginFormData.Password = `${id}qwer`;
    nextTick(() => {
      document.getElementById("loginSubmitBtn")?.click();
    });
    isLoginProccessing.value = false;
  };

  return {
    loginFormRef,
    loginFormData,
    loginRules,
    submitLogin,
    proceedLogin,
    isLoginProccessing,
    getCaptchaErrorMsg,
    captchaErrorMsg,
    register,
    clientCaptcha,
    isSubmitSuccess,
    isTelegramBotSetup,
    telegramBotUsername,
    checkLoginTelegramSetup,
    telegramCallbackFunction,
  };
}

export { handleMultiplePlayerLogin, updateCustomerInfoTelegram };
