import { Ref, ref } from "vue";
import apis from "@/utils/apis";
import textHelper from "@/utils/formatHelpers/textFormatHelper";
import {
  ICompanyFlowSettingDto,
  ProfileDto,
} from "@/models/getProfileResponse";

interface useProfile {
  profile: Ref<ProfileDto>;
  profileDetail: Ref<Array<ICompanyFlowSettingDto>>;
}

export default function useProfile(): useProfile {
  const profile = ref<ProfileDto>({} as ProfileDto);
  const profileDetail = ref<Array<ICompanyFlowSettingDto>>(
    [] as Array<ICompanyFlowSettingDto>
  );

  const getProfile = async () => {
    const response = await apis.getProfileInformation();
    if (response.isSuccessful && response.Data) {
      profile.value = response.Data.Information;

      response.Data.ProfileDetail.forEach(
        (property: ICompanyFlowSettingDto) => {
          property.PropertyName = textHelper.removeAllSpace(
            property.PropertyName
          );
          property.PropertyName = textHelper.convertCamelCaseToSnakeCase(
            property.PropertyName
          );
        }
      );

      profileDetail.value = response.Data.ProfileDetail;
    } else {
      // TODO: Error Handle
    }
  };

  getProfile();

  return {
    profile,
    profileDetail,
  };
}
