export default function useCloseTelegramWebApp(){
  const isSubmitSuccess = ref(false)
  let telegramClose: () => void;
    const importTelegram = async () =>  {
      try {
        const {useWebApp} = await import('vue-tg');
        const {close} = useWebApp();
        telegramClose = close;
      } catch (error) {
        console.log(error);
      }
    }
    importTelegram();

    const onClose = () => {
      if (typeof telegramClose === 'function') {
        telegramClose();
      }
    }

    watch(() => isSubmitSuccess.value, () => {
      setTimeout(function() {
        onClose();
      }, 2000);
    });
    return {
      isSubmitSuccess,
      onClose,
    }
}
