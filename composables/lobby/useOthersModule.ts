import { ThemePropertiesFromApiDto } from "~~/models/companyThemePropertiesRespone";
import { store } from "~~/store";

type propsType = {
  data: ThemePropertiesFromApiDto;
};

export interface ISubPath {
  ImgPath: string;
  IsDefault: boolean;
  IsUploadedImage: boolean;
}

type ItemType = {
  Path: ISubPath;
  HyperLink: string;
  ImagePath: string;
};

const useOthersModule = (props: propsType) => {
  const property = toRef(props, "data");
  const childProperties = ref<ItemType[]>([]);

  const getImagePath = (
    group: ThemePropertiesFromApiDto,
    webId: Number = 0,
    path: ISubPath
  ): string => {
    const cdn = commonHelper.getThemePropertiesCdn();
    const version = store.state.imageVersion;
    const isDefaultFolderName = path.IsDefault
      ? "default"
      : `customize/WebId-${webId}`;
    return `${cdn}/storage/${isDefaultFolderName}/${group.ThemeName?.toLowerCase()}/${encodeURIComponent(
      path.ImgPath
    )}?v=${version}`;
  };

  const initSubProperties = () => {
    if (typeof property.value?.Text === "object") {
      const paths = JSON.parse(property.value?.Path) as ISubPath[];
      const hyperLinks = JSON.parse(property.value?.HyperLink) as string[];
      const children: ItemType[] = [];
      paths.forEach((path, index) => {
        const subItem: ItemType = {
          Path: path,
          HyperLink: hyperLinks[index],
          ImagePath: getImagePath(property.value, store.state.webId, path),
        };
        children.push(subItem);
      });
      childProperties.value = children;
    }
  };

  return {
    property,
    initSubProperties,
    childProperties,
  };
};

export default useOthersModule;
