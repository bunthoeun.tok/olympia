import { orderBy } from "lodash-es";
import { gameStore } from "~~/store/game";
import SportDisplayModel from "~~/components/sport/sportDisplayModel";
import { ILobbySettingsText } from "~~/models/customizeTheme";
import { store } from "@/store";

export default function useLiveCasinoModule(
  lobbySettingData: ILobbySettingsText
) {
  const router = useRouter();
  const authenticated = computed(() => store.state.auth);
  const isLiveCasino = computed(() =>
    lobbySettingData.SelectedModule.toLowerCase().includes("live casino")
  );
  const playableGame = computed(() => {
    return orderBy(gameStore.state.games, "Rank")
      .filter(
        (game) =>
          game.Category === (isLiveCasino.value ? 2 : 1) && game.ForDesktop
      )
      .slice(0, 4)
      .map((item) => new SportDisplayModel(item));
  });
  const { toggleLoginRegisterDialog } = commonHelper;
  const redirectPage = (redirectURL: string) => {
    if (window.innerWidth < 768) {
      if (authenticated.value) {
        router.push(redirectURL);
      } else {
        toggleLoginRegisterDialog();
      }
    }
  };
  const redirectToLiveCasino = (redirectURL: string) => {
      if (authenticated.value) {
        router.push(redirectURL) ;
      } else {
        toggleLoginRegisterDialog();
    }
  };
  return {
    playableGame,
    redirectPage,
    authenticated,
    redirectToLiveCasino,
  };
}
