import { ref, computed } from "vue";
import _ from "lodash-es";
import { isString } from "lodash";
import { store } from "@/store";
import { GameBsi, IGameBsi } from "@/models/bsiGameList";
import apis from "@/utils/apis";
import {
  GameEntryMenu,
  TabGameMenu,
  IGameEntryMenu,
} from "@/models/gameEntryFilter";

export const useGameModule = () => {
  const route = useRoute();
  const router = useRouter();
  const { headerTab } = useGameEntryComponent();
  const popularGames = ref<Array<IGameBsi>>([]);
  const newGames = ref<Array<IGameBsi>>([]);
  const hotGames = ref<Array<IGameBsi>>([]);
  const moduleFiveDisplayTabs = ref<TabGameMenu[]>();
  const specialselection = ref<IGameBsi[]>([]);
  const gameType = ref<IGameBsi[]>([]);
  const selectedGameType = ref("Slots");
  const gameEntryTabMenus = computed(() => {
    const result: IGameEntryMenu[] = [
      {
        Menus: headerTab.value.map((item) => new TabGameMenu(item, "main")),
        IsSearchBox: false,
        Justify: "left",
        Theme: "common",
      },
      {
        Menus: [],
        IsSearchBox: true,
        Justify: "left",
        Theme: "main",
      },
    ];
    return result;
  });
  const gameMenuForDisplay: Ref<GameEntryMenu[]> = ref<GameEntryMenu[]>(
    gameEntryTabMenus.value.map(
      (item) => new GameEntryMenu(item, route.query.f === undefined)
    )
  );
  const allGameTypeMenus = gameMenuForDisplay.value[0].Menus.find(
    (item) => item.Label === "Game Type"
  )?.DropDownMenus.map((item) => item.DropDownLabel);
  const isMobile = !useIsDesktop();
  const indexToSlice = isMobile ? 3 : 5;
  const authenticated = computed(() => store.state.auth);
  const getTopGameListInitial = async () => {
    try {
      const response = await apis.getHotGameList();
      if (response.isSuccessful) {
        popularGames.value = response.Data.Games.slice(0, 7).map(
          (item) => new GameBsi(item)
        );
      }
    } catch (error) {
      popularGames.value = [];
    }
  };
  const getNewGameListInitial = async () => {
    try {
      const response = await apis.getNewGameList();
      if (response.isSuccessful) {
        newGames.value = _.sortBy(response.Data.Games, "Rank")
          .slice(0, indexToSlice)
          .map((item) => new GameBsi(item));
      }
    } catch (error) {
      newGames.value = [];
    }
  };
  const getHotGameListInitial = async () => {
    // initialedGame.value = [];
    const response = await apis.getHotGameList();
    if (response.isSuccessful) {
      hotGames.value = response.Data.Games.map((item) => new GameBsi(item))
        .filter((item) => (isMobile ? item.ForMobile : item.ForDesktop))
        .slice(0, indexToSlice);
    } else {
      hotGames.value = [];
    }
  };
  const getSpecialSelectionGameListInitial = async () => {
    try {
      const response = await apis.getJackpotGameList();
      if (response.isSuccessful) {
        specialselection.value = response.Data.Games.filter((item) =>
          isMobile ? item.ForMobile : item.ForDesktop
        )
          .slice(0, indexToSlice)
          .map((item) => new GameBsi(item));
      }
    } catch (error) {
      specialselection.value = [];
    }
  };
  const getGameListByGameTypeInitial = async () => {
    const response = await apis.getGameListByGameType(selectedGameType.value);
    if (response.isSuccessful) {
      gameType.value = _.sortBy(response.Data.Games, ["Rank"])
        .filter((item) => (isMobile ? item.ForMobile : item.ForDesktop))
        .slice(0, indexToSlice)
        .map((item) => new GameBsi(item));
    } else {
      gameType.value = [];
    }
  };
  const lobbyGames = computed(() => ({
    PopularGamesForDisplay: popularGames.value,
    NewGamesForDisplay: newGames.value,
    SpecialSelectionForDisplay: specialselection.value,
    GameTypeForDisplay: gameType.value,
    HotGameForDisplay: hotGames.value,
    GameByType: gameType.value,
  }));
  const getmoduleFiveDisplayTabs = () => {
    moduleFiveDisplayTabs.value = gameMenuForDisplay.value[0].Menus.filter(
      (item) => ["New Games", "Hot Games"].includes(item.Label)
    );
    moduleFiveDisplayTabs.value.sort((a, b) => {
      if (a.Label === "Hot Games") return -1; // Move 'grape' to the first position
      if (b.Label === "Hot Games") return 1;
      return 0;
    });
    const allGameTypeDisplay = ref<TabGameMenu[]>(
      gameMenuForDisplay.value[0].Menus.find(
        (item) => item.Label === "Game Type"
      )
        ?.DropDownMenus.filter(
          (filteredItems) => filteredItems.DropDownLabel !== "OthersGames"
        )
        .map((tabItem) => new TabGameMenu(tabItem.DropDownLabel, "main"))
    );
    moduleFiveDisplayTabs.value = [
      ...moduleFiveDisplayTabs.value,
      ...allGameTypeDisplay.value,
    ];
  };
  const onGoGameEntryFilter = (searchGameRequest: TabGameMenu) => {
    if (!useIsDesktop()) {
      if (
        searchGameRequest.Label &&
        ["New Games", "Hot Games", "Special Selection"].includes(
          searchGameRequest.Label
        )
      )
        router.push(`/games?f=${searchGameRequest.Label.replaceAll("_", " ")}`);
      else router.push(`/games/${searchGameRequest.Label}`);
      return;
    }
    router.push(
      `/games?Type=${
        allGameTypeMenus.includes(searchGameRequest.Label)
          ? "Game Type"
          : searchGameRequest.Label
      }&Value=${
        allGameTypeMenus.includes(searchGameRequest.Label)
          ? searchGameRequest.Label
          : ""
      }`
    );
  };
  const onClickViewMore = (filterParam?: string) => {
    if (
      filterParam &&
      ["New Games", "Hot Games", "Special Selection"].includes(filterParam)
    )
      router.push(`/games?f=${filterParam.replaceAll("_", " ")}`);
    else router.push(`/games/${selectedGameType.value}`);
  };
  const checkStatus = (game: IGameBsi): string => {
    let status = "disabled";
    if (status) {
      if (game.IsNewGame) status = "new";
      if (game.IsHotGame && !game.IsNewGame) status = "hot";
    }
    return status;
  };
  const { t } = useI18n();
  const getDisplayName = (name: string) => {
    const specialName = ["Special Selection", "Hot Games", "New Games"];
    if (!isString(name) || ['Special Selection', ''].includes(name)) {
      return t('special_selection')
    } else if(specialName.includes(name)) {
      return t(name.replace(" ", "_").toLowerCase());
    } else {
      return t(name.replace(/\s/g, ""));
    }
  };
  return {
    popularGames,
    getTopGameListInitial,
    lobbyGames,
    getNewGameListInitial,
    authenticated,
    moduleFiveDisplayTabs,
    getmoduleFiveDisplayTabs,
    onGoGameEntryFilter,
    getSpecialSelectionGameListInitial,
    isMobile,
    getGameListByGameTypeInitial,
    selectedGameType,
    onClickViewMore,
    checkStatus,
    getHotGameListInitial,
    getDisplayName,
  };
};
