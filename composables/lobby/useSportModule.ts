import { computed } from "vue";
import { orderBy } from "lodash-es";
import { ILobbySettingsText } from "~~/models/customizeTheme";
import { gameStore } from "~~/store/game";
import SportDisplayModel from "~~/components/sport/sportDisplayModel";
import { store } from "@/store";
import { IGameBsi } from "~~/models/bsiGameList";

export default function useSportModule(lobbySettingData: ILobbySettingsText) {
  const dataForDisplay = computed(() => lobbySettingData);
  const isLiveCasino = computed(() =>
    dataForDisplay.value.SelectedModule.toLowerCase().includes("live casino")
  );
  const authenticated = computed(() => store.state.auth);
  const gameByRanks = computed(() =>
    orderBy(gameStore.state.games, "Rank").filter(
      (game: IGameBsi) =>
        game.Category === (isLiveCasino.value ? 2 : 1) && game.ForDesktop
    )
  );
  const playableGame = computed(() =>
    gameByRanks.value.map((item) => new SportDisplayModel(item))
  );
  const playableGameForModule5 = computed(() => {
    let orderGame = gameByRanks.value;
    if (orderGame.length > 0) {
      if (orderGame.length >= 5) {
        orderGame = orderGame.slice(0, 5);
        orderGame = orderGame.concat([
          ...orderGame,
          ...orderGame,
          ...orderGame,
        ]);
      } else {
        // current item need currentLength to reach 15
        const currentLength = 15 - orderGame.length;
        for (let i = 0; i < currentLength; i++) {
          orderGame.push(orderGame[i % orderGame.length]);
        }
      }
    }
    return orderGame.map((game: IGameBsi) => new SportDisplayModel(game));
  });
  const displayName = computed(() =>
    dataForDisplay.value.SelectedModule.replace(" Module", "")
      .replace(/\s/g, "-")
      .replace("ing", "")
  );
  return {
    authenticated,
    playableGame,
    displayName,
    playableGameForModule5,
  };
}
