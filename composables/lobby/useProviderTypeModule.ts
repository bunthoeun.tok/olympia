import { ILobbySettingsText } from "~~/models/customizeTheme";

type propsType = {
  data: ILobbySettingsText;
};

export default function useProvidertypeModule(props: propsType) {
  const router = useRouter();

  const isHover = ref<Record<string, boolean>>({});

  const data = toRef(props, "data");

  const checkColor = (position: string) => {
    if (position === "Sports") return "--dmc-color: #0D69F2;";
    if (position === "Virtual Sports") return "--dmc-color: #F2B20D;";
    if (position === "Live Casino") return "--dmc-color: #F20D0D;";
    if (position === "Games") return "--dmc-color: #1BBF00;";
    if (position === "Cockfighting") return "--dmc-color: #445B68;";
    if (position === "Lottery") return "--dmc-color: #DF6624;";
    if (position === "Poker") return "--dmc-color: #5D070A;";
    return "";
  };
  const checkBgColorModuleFive = (position: string) => {
    if (position === "Sports") return "--bgl-1: #000840;--bgl-2: #001C96";
    if (position === "Virtual Sports")
      return "--bgl-1: #FF7A00;--bgl-2: #FFB800";
    if (position === "Live Casino") return "--bgl-1: #CF000C;--bgl-2: #CA3C66";
    if (position === "Games") return "--bgl-1: #006849;--bgl-2: #009A59";
    if (position === "Cockfighting") return "--bgl-1: #1C2E37;--bgl-2: #5B6B72";
    if (position === "Lottery") return "--bgl-1: #DF6624;--bgl-2: #F08D58";
    if (position === "Poker") return "--bgl-1: #380002;--bgl-2: #851418";
    return "";
  };

  data.value.Position.forEach((item, index) => {
    if (!isHover.value[item]) {
      if (index === 0) isHover.value[item] = true;
      else isHover.value[item] = false;
    }
  });
  const onMouseEnter = (position: string) => {
    Object.keys(isHover.value).forEach((key) => {
      isHover.value[key] = false;
    });
    isHover.value[position] = true;
  };
  const onMouseLeave = (position: string) => {
    const mouseLeaveTimeout = setTimeout(() => {
      isHover.value[position] = false;
      if (Object.values(isHover.value).every((val) => !val)) {
        Object.keys(isHover.value).forEach((key, index) => {
          if (index === 0) isHover.value[key] = true;
        });
      }
      clearTimeout(mouseLeaveTimeout);
    }, 200);
  };

  const handleRedirect = (position: string) => {
    router.push(position.replace(/\s/g, "-").replace("ing", ""));
  };
  const moduleThreeDescriptions: Record<string, stirng> = {
    Sports: "Easy to bet and win! Great matches from big leagues available.",
    "Virtual Sports":
      "Easy to bet and Win! Enjoy all the benefits of live sports betting 24 hours a day, with matches taking place around the clock.",
    "Live Casino":
      "Try your luck! Multiple types of live dealers to give you the most authentic player experience.",
    Games:
      "The best slots machine game available from the palm of your hand! Let’s get in the excitement today!",
    Cockfighting:
      "15 different sports bet options give you the best betting experience. Come and bet to support your favorite sports!",
    Lottery:
      "Easy to bet and Win! Enjoy all the benefits of live sports betting 24 hours a day, with matches taking place around the clock.",
    Poker:
      "Try your luck! Multiple types of live dealers to give you the most authentic player experience.",
  };
  const moduleOneDescriptions: Record<string, stirng> = {
    Sports: "Many ways to play, more chances to win!",
    "Virtual Sports":
      "Apart from popular sports, we offer pioneering In-Play solutions.",
    "Live Casino": "Real Casino experience",
    Games: "Multiple themes for Asian Market",
    Cockfighting:
      "Apart from popular sports, we offer pioneering In-Play solutions.",
    Lottery: "Real Casino experience",
    Poker: "Multiple themes for Asian Market",
  };
  return {
    isHover,
    moduleThreeDescriptions,
    moduleOneDescriptions,

    checkColor,
    checkBgColorModuleFive,
    onMouseEnter,
    onMouseLeave,
    handleRedirect,
  };
}
