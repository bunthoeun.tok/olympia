import { computed, Ref, ComputedRef } from "vue";
import useProfile from "../useProfile";
import featuresToggleHelper from "@/utils/featuresToggleHelper";
import EnumThemePropertyKey from "@/models/enums/enumThemePropertyKey";
import { store } from "@/store";
import {
  ICompanyFlowSettingDto,
  ProfileDto,
} from "@/models/getProfileResponse";

interface IUseAccountInformation {
  profile: Ref<ProfileDto>;
  profileDetail: Ref<ICompanyFlowSettingDto[]>;
  timeZoneIsHidden: ComputedRef<boolean>;
  isCashPlayer: ComputedRef<boolean>;
}
export const useAccountInformation = (): IUseAccountInformation => {
  const { profile, profileDetail } = useProfile();
  const timeZoneIsHidden = computed(
    () =>
      featuresToggleHelper.getThemePropertyFromApi(
        EnumThemePropertyKey.HtmlId,
        "time_zone"
      ).IsHidden
  );
  const isCashPlayer = computed(() => store.state.isCashPlayer);
  return {
    profile,
    profileDetail,
    timeZoneIsHidden,
    isCashPlayer,
  };
};
