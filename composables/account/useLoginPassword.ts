import { ref, reactive, Ref } from "vue";
import type { FormInstance } from "element-plus";
import apis from "@/utils/apis";
import EnumMessageType from "@/models/enums/enumMessageType";
import notificationHelper from "@/utils/elementUiHelpers/notificationHelper";
import formHelper from "@/utils/elementUiHelpers/formHelper";
import cookieHelper from "@/utils/cookieHelper";
import formRules from "@/utils/elementUiHelpers/formRulesHelper";
import { store } from "@/store";

interface IPasswordFormData {
  currentPassword: string;
  newPassword: string;
  confirmPassword: string;
}
interface IUseLoginPassword {
  ruleLoginPasswordFormRef: Ref<FormInstance | undefined>;
  loginPasswordFormData: IPasswordFormData;
  changeLoginPassword: (formEl: FormInstance) => void;
  loginPasswordRules: any;
  isLoginPassProccessing: Ref<boolean>;
  isExpire: string | string[];
}
export const useLoginPassword = (): IUseLoginPassword => {
  const { $t } = useNuxtApp();
  const ruleLoginPasswordFormRef = ref<FormInstance>();
  const loginPasswordFormData: IPasswordFormData = reactive({
    currentPassword: "",
    newPassword: "",
    confirmPassword: "",
  });
  const checkNewLoginPassword = () => {
    if (
      loginPasswordFormData.currentPassword ===
      loginPasswordFormData.newPassword
    ) {
      return $t("new_password_must_not_be_the_same_as_old_password");
    }
    return formRules.passwordFormat(loginPasswordFormData.newPassword);
  };
  const checkConfirmLoginPassword = () => {
    if (
      loginPasswordFormData.confirmPassword !==
      loginPasswordFormData.newPassword
    ) {
      return $t("password_does_not_match");
    }
    return "";
  };
  const rules = {
    currentPassword: { required: true },
    newPassword: { customRule: checkNewLoginPassword, required: true },
    confirmPassword: { customRule: checkConfirmLoginPassword, required: true },
  };
  const loginPasswordRules = formHelper.getRules(rules, loginPasswordFormData);
  const isLoginPassProccessing = ref(false);
  const route = useRoute();
  const isExpire = route.params.expire;
  const getOnlineID = Number(localStorage.getItem("onlineId"));
  const proceedChangeLoginPassword = async () => {
    isLoginPassProccessing.value = true;
    const response = await apis.changePassword(
      loginPasswordFormData.currentPassword,
      loginPasswordFormData.newPassword,
      getOnlineID
    );
    if (response.isSuccessful) {
      notificationHelper.notification(
        response.Message,
        EnumMessageType.Success
      );
      cookieHelper.setCookie("isPasswordExpired", "false", 60);
      store.commit("updateIsPasswordExpired", false);
      setTimeout(() => {
        window.location.href = `${window.location.origin}/`;
      }, 1500);
    } else {
      notificationHelper.notification(response.Message, EnumMessageType.Error);
    }
    isLoginPassProccessing.value = false;
  };
  const changeLoginPassword = formHelper.getSubmitFunction(
    proceedChangeLoginPassword
  );
  return {
    ruleLoginPasswordFormRef,
    loginPasswordFormData,
    loginPasswordRules,
    changeLoginPassword,
    isLoginPassProccessing,
    isExpire,
  };
};
