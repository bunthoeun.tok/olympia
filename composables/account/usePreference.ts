import { computed, onMounted, ref, Ref } from "vue";
import EnumDisplayTimeOption, {
  EnumDisplayTimeZone,
} from "@/models/enums/enumDisplayTimeOption";
import apis from "@/utils/apis";
import notificationHelper from "@/utils/elementUiHelpers/notificationHelper";
import EnumMessageType from "@/models/enums/enumMessageType";
import { store } from "~~/store";
import EnumMutation from "~~/models/enums/enumMutation";
import { display } from "html2canvas/dist/types/css/property-descriptors/display";
import { IUpdatePlayerPreferenceRequest } from "~~/models/account/preference";
import { EnumSelectedThemeMode } from "@/models/enums/enumSelectedThemeMode";

interface ITimezoneOption {
  value: number;
  label: string;
}
interface IPreference {
  selected: Ref<EnumDisplayTimeOption>;
  isInProgress: Ref<boolean>;
  updatePreference: () => void;
  timeZoneOptions: Ref<Array<ITimezoneOption>>;
  playerPreferenceRequest: IUpdatePlayerPreferenceRequest;
}
export const usePreference = (): IPreference => {
  const { $t } = useNuxtApp();

  const timezoneMapper: Record<number, keyof typeof EnumDisplayTimeZone> = {
    0: "HongKongTime",
    1: "MyComputerTime",
    2: "PacificEfateTime",
  };

  const selected = ref(EnumDisplayTimeOption.MyComputerTime);
  const playerPreferenceRequest = reactive<IUpdatePlayerPreferenceRequest>({
    DisplayTime: selected.value,
    ThemeMode: 1,
  });
  const isInProgress = ref(false);
  const optionAvailables: Ref<Array<ITimezoneOption>> = ref([
    {
      value: EnumDisplayTimeOption.MyComputerTime,
      label: $t("my_computer_time"),
    },
    {
      value: EnumDisplayTimeOption.HongKongTime,
      label: $t("hong_kong_time_gmt_8"),
    },
    {
      value: EnumDisplayTimeOption.PacificEfateTime,
      label: $t("pacific_efate_time_gmt_11"),
    },
  ]);
  const timeZoneOptions = computed(
    (): Array<ITimezoneOption> => optionAvailables.value
  );

  const updatePreference = async (isShowNotification = true) => {
    isInProgress.value = true;
    playerPreferenceRequest.ThemeMode = store.state.selectedThemeMode;
    playerPreferenceRequest.DisplayTime = selected.value;
    const response = await apis.updatePlayerPreference(playerPreferenceRequest);
    if (response.isSuccessful) {
      if (isShowNotification) {
        notificationHelper.notification(
          response.Message,
          EnumMessageType.Success
        );
      }
      store.commit(
        EnumMutation.updateDisplayTimeZone,
        EnumDisplayTimeZone[timezoneMapper[selected.value]]
      );
    } else {
      if (isShowNotification) {
        notificationHelper.notification(response.Message, EnumMessageType.Error);
      }
    }
    isInProgress.value = false;
  };
  onMounted(async () => {
    try {
      const response = await apis.getPlayerPreferenceForDisplay();
      if (response.isSuccessful) {
        const { RegionTimeZone } = response.Data.TimeZone;
        selected.value = EnumDisplayTimeOption[RegionTimeZone];
      } else {
        notificationHelper.notification(
          response.Message,
          EnumMessageType.Error
        );
      }
    } catch (e) {
      notificationHelper.notification(
        "can not get player preference setting, please contact our support !",
        EnumMessageType.Error
      );
    }
  });
  return {
    selected,
    isInProgress,
    updatePreference,
    timeZoneOptions,
    playerPreferenceRequest,
  };
};
