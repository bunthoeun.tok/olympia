import { computed, ref, reactive, Ref, ComputedRef } from "vue";
import type { FormInstance } from "element-plus";
import { store } from "@/store";
import apis from "@/utils/apis";
import EnumMessageType from "@/models/enums/enumMessageType";
import notificationHelper from "@/utils/elementUiHelpers/notificationHelper";
import formHelper from "@/utils/elementUiHelpers/formHelper";

interface IPasswordFormData {
  currentPassword: string;
  newPassword: string;
  confirmPassword: string;
}
interface IUsePaymentPassword {
  rulePaymentPasswordFormRef: Ref<FormInstance | undefined>;
  paymentPasswordFormData: IPasswordFormData;
  changePaymentPassword: (formEl: FormInstance) => void;
  isPaymentPasswordSet: Ref<boolean>;
  paymentPasswordRules: any;
  isPaymentPassProccessing: Ref<boolean>;
  isCashPlayer: ComputedRef<boolean>;
}
export const usePaymentPassword = (): IUsePaymentPassword => {
  const { $t } = useNuxtApp();
  const isPaymentPasswordSet = computed(() => store.state.isPaymentPasswordSet);
  const rulePaymentPasswordFormRef = ref<FormInstance>();
  const paymentPasswordFormData = reactive({
    currentPassword: "",
    newPassword: "",
    confirmPassword: "",
  });
  const checkNewPaymentPassword = (): string => {
    if (
      paymentPasswordFormData.newPassword ===
      paymentPasswordFormData.currentPassword
    ) {
      return $t("new_password_must_not_be_the_same_as_old_password");
    }
    if (
      paymentPasswordFormData.confirmPassword &&
      paymentPasswordFormData.newPassword !==
        paymentPasswordFormData.confirmPassword
    ) {
      return $t("password_does_not_match");
    }
    return "";
  };
  const checkConfirmPaymentPassword = () => {
    if (
      paymentPasswordFormData.confirmPassword !==
      paymentPasswordFormData.newPassword
    ) {
      return $t("password_does_not_match");
    }
    return "";
  };
  const rules = {
    currentPassword: { required: true },
    newPassword: { customRule: checkNewPaymentPassword, required: true },
    confirmPassword: {
      customRule: checkConfirmPaymentPassword,
      required: true,
    },
  };
  const paymentPasswordRules = formHelper.getRules(
    rules,
    paymentPasswordFormData
  );
  const isPaymentPassProccessing = ref(false);
  const getOnlineID = Number(localStorage.getItem("onlineId"));
  const proceedChangePaymentPassword = async () => {
    isPaymentPassProccessing.value = true;
    const response = await apis.changePaymentPassword(
      paymentPasswordFormData.currentPassword,
      paymentPasswordFormData.newPassword,
      getOnlineID
    );
    if (response.isSuccessful) {
      notificationHelper.notification(
        response.Message,
        EnumMessageType.Success
      );
    } else {
      notificationHelper.notification(
        response.ErrorMessageForDisplay,
        EnumMessageType.Error
      );
    }
    isPaymentPassProccessing.value = false;
  };
  const changePaymentPassword = formHelper.getSubmitFunction(
    proceedChangePaymentPassword
  );
  const isCashPlayer = computed(() => store.state.isCashPlayer);
  return {
    rulePaymentPasswordFormRef,
    paymentPasswordFormData,
    paymentPasswordRules,
    changePaymentPassword,
    isPaymentPasswordSet,
    isPaymentPassProccessing,
    isCashPlayer,
  };
};
