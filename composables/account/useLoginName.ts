import { ref, reactive, Ref, computed } from "vue";
import type { FormInstance } from "element-plus";
import apis from "@/utils/apis";
import { store } from "@/store";
import notificationHelper from "@/utils/elementUiHelpers/notificationHelper";
import featuresToggleHelper from "@/utils/featuresToggleHelper";
import EnumThemePropertyKey from "@/models/enums/enumThemePropertyKey";
import EnumMessageType from "@/models/enums/enumMessageType";
import formHelper from "@/utils/elementUiHelpers/formHelper";
import formRules from "@/utils/elementUiHelpers/formRulesHelper";

interface ILoginNameFormData {
  newLoginName: string;
}
interface IUseLoginName {
  ruleLoginNameFormRef: Ref<FormInstance | undefined>;
  loginNameFormData: ILoginNameFormData;
  changeLoginName: (formEl: FormInstance) => void;
  loginNameRules: any;
  isLoginNameProcessing: Ref<boolean>;
  isLoginNameEnabled: Ref<boolean>;
}
export const useLoginName = (): IUseLoginName => {
  const isLoginNameEnabled = computed(
    () =>
      !store.state.isLoginNameUpdated &&
      !featuresToggleHelper.getThemePropertyFromApi(
        EnumThemePropertyKey.HtmlId,
        "update_login_name"
      ).IsHidden
  );
  const ruleLoginNameFormRef = ref<FormInstance>();
  const loginNameFormData: ILoginNameFormData = reactive({
    newLoginName: "",
  });
  const checkLoginName = () =>
    formRules.userNameFormat(loginNameFormData.newLoginName);
  const rules = {
    newLoginName: { customRule: checkLoginName, required: true },
  };
  const loginNameRules = formHelper.getRules(rules, loginNameFormData);
  const isLoginNameProcessing = ref(false);
  const proceedChangeLoginName = async () => {
    isLoginNameProcessing.value = true;
    const response = await apis.changeLoginName(loginNameFormData.newLoginName);
    if (response.isSuccessful) {
      notificationHelper.notification(
        response.ErrorMessageForDisplay,
        EnumMessageType.Success
      );
      store.commit("updateIsLoginNameUpdated", true);
      setTimeout(() => {
        window.location.href = `${window.location.origin}/`;
      }, 1500);
    } else {
      notificationHelper.notification(
        response.ErrorMessageForDisplay,
        EnumMessageType.Error
      );
    }
    isLoginNameProcessing.value = false;
  };
  const changeLoginName = formHelper.getSubmitFunction(proceedChangeLoginName);
  return {
    ruleLoginNameFormRef,
    loginNameFormData,
    loginNameRules,
    changeLoginName,
    isLoginNameProcessing,
    isLoginNameEnabled,
  };
};
