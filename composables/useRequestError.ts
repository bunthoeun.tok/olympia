import axios from "axios";
import notificationHelper from "~~/utils/elementUiHelpers/notificationHelper";

export const useRequestError = (error: unknown) => {
  const { t } = useI18n();
  let message: string = t("general_error");
  if (axios.isAxiosError(error)) {
    message = error.message;
  }
  notificationHelper.error(message);
};
