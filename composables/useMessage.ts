import {
  IGetSiteMessageRequest,
  MemberMessage,
  TMemberMessageTabs,
} from "@/models/message";
import { Pagination } from "~~/models/common/pagination";
import { EnumMessage, EnumMessageStatus } from "~~/models/enums/enumMessage";
import EnumMutation from "~~/models/enums/enumMutation";
import { store } from "~~/store";
import { memberMessageStore } from "~~/store/member-message";
import notificationHelper from "~~/utils/elementUiHelpers/notificationHelper";
type ActionType = "MarkAllRead" | "DeleteAll" | "SingleDelete";

export const useMessage = () => {
  const i18n = useI18n();
  const isShowPreviewMessageDialog = ref<boolean>(false);
  const isLoadingMore = ref<boolean>(false);
  const isUpdatingStatus = ref<ActionType>();
  const isLoading = ref<boolean>(false);
  const isShowDeleteDialog = ref<boolean>(false);
  const isLoadMore = ref<boolean>(false);
  const isLoggedIn = () => store.state.auth;
  let interval: NodeJS.Timer | undefined;

  const messageDetail = ref<MemberMessage>();
  const activeTab = computed<TMemberMessageTabs>({
    get: () => memberMessageStore.state.activeTab,
    set: (value) =>
      memberMessageStore.commit(EnumMutation.updateActiveTab, value),
  });
  const tabs: TMemberMessageTabs[] = [
    "Unread",
    "Announcement",
    "Notification",
    "Promotion",
    "PersonalMessage",
  ];
  const activeTabIndex = computed(() => tabs.indexOf(activeTab.value));
  const messages = computed({
    get: () => memberMessageStore.state.messages,
    set: (value) =>
      memberMessageStore.commit(EnumMutation.updateMemberMessages, value),
  });
  const pagination = computed({
    get: () => {
      if (useIsDesktop()) {
        return memberMessageStore.state.pagination;
      }
      return {
        ...memberMessageStore.state.pagination,
        RowNumber: 65000,
      };
    },
    set: (value) =>
      memberMessageStore.commit(
        EnumMutation.updateMemberMessagePagination,
        value
      ),
  });
  const messageFilterModel = ref<IGetSiteMessageRequest>({
    Type: EnumMessage[activeTab.value],
    IsUnread: true,
    Language: String(i18n.locale.value),
    Page: 1,
    RowCountPerPage: 10,
  });

  const unreadInfo = computed(() => memberMessageStore.state.memberMessageInfo);

  const setFilterCriteria = () => {
    messageFilterModel.value.RowCountPerPage = useIsDesktop() ? 10 : 65000;
    messageFilterModel.value.Page = pagination.value.CurrentPage;
    messageFilterModel.value.Type =
      EnumMessage[memberMessageStore.state.activeTab];
    messageFilterModel.value.IsUnread = activeTab.value === "Unread";
  };

  const clearOldData = () => {
    messages.value = [];
    pagination.value = new Pagination({
      ...pagination.value,
      MaxPage: 1,
      RowNumber: 10,
    });
  };

  const setLoadMorePagination = () => {
    const { CurrentPage, MaxPage } = pagination.value;
    if (CurrentPage < MaxPage) {
      pagination.value.CurrentPage += 1;
    }
  };

  const fetchMessages = async (isTriggerLoading = true) => {
    if (!isLoggedIn()) return;
    try {
      if (isTriggerLoading) {
        if (isLoadMore.value) {
          isLoadingMore.value = true;
          setLoadMorePagination();
        } else {
          clearOldData();
        }
        isLoading.value = true;
      }
      setFilterCriteria();
      const response = await apis.getMemberMessage(messageFilterModel.value);
      if (response.isSuccessful) {
        const newData = response.Data.Messages.map((v) => new MemberMessage(v));
        if (isLoadMore.value) {
          messages.value = [...messages.value, ...newData];
        } else {
          messages.value = newData;
        }
        pagination.value = new Pagination({
          ...pagination.value,
          MaxPage: response.Data.MaxPage,
          RowNumber: !response.Data.RowNumber ? 10 : response.Data.RowNumber,
        });
        memberMessageStore.commit(
          EnumMutation.updateMemberMessageInfo,
          response.Data.UnreadInfo ?? {}
        );
      }
    } catch (error) {
      useRequestError(error);
    } finally {
      isLoading.value = false;
      isLoadingMore.value = false;
    }
  };

  const updateMessageStatus = async (
    Ids: number[],
    Status: EnumMessageStatus,
    options: {
      showMessage?: boolean;
      actionType?: ActionType;
    } = {}
  ) => {
    const { actionType, showMessage } = options;
    isUpdatingStatus.value = actionType;
    try {
      const response = await apis.updateMemberMessage({
        Ids,
        Status,
      });
      if (response.isSuccessful) {
        if (showMessage) {
          notificationHelper.success(response.Message);
          isShowDeleteDialog.value = false;
          // Use when click delete from detail view.
          if (Status === EnumMessageStatus.Delete) {
            isShowPreviewMessageDialog.value = false;
          }
        }
        if (
          (activeTab.value === "Unread" && actionType === "MarkAllRead") ||
          actionType === "DeleteAll" ||
          (messages.value.length === 1 &&
            (actionType === "SingleDelete" || activeTab.value === "Unread"))
        ) {
          pagination.value.CurrentPage = 1;
        }
        await fetchMessages(false);
      }
    } catch (error) {
      useRequestError(error);
    } finally {
      isUpdatingStatus.value = undefined;
    }
  };

  const onMessageClicked = async (message: MemberMessage) => {
    messageDetail.value = message;
    isShowPreviewMessageDialog.value = true;
    if (message.IsUnread) {
      await updateMessageStatus([message.Id], EnumMessageStatus.Read);
    }
  };

  const onOpenDeleteConfirmation = (
    messageOrIsDeleteAll?: MemberMessage | boolean
  ) => {
    isShowDeleteDialog.value = true;
    if (messageOrIsDeleteAll && messageOrIsDeleteAll instanceof MemberMessage) {
      messageDetail.value = messageOrIsDeleteAll;
    }
  };

  const onConfirmDelete = async (isDeleteAll = false) => {
    if (isDeleteAll) {
      await deleteAllMessage();
      return;
    }
    const id = messageDetail.value?.Id;
    if (id) {
      messageDetail.value = undefined;
      await updateMessageStatus([id], EnumMessageStatus.Delete, {
        showMessage: true,
        actionType: "SingleDelete",
      });
    }
  };

  const onMarkAllRead = async () => {
    const IDs = messages.value.filter((v) => v.IsUnread).map((v) => v.Id);
    if (!IDs.length) return;
    await updateMessageStatus(IDs, EnumMessageStatus.Read, {
      actionType: "MarkAllRead",
      showMessage: true,
    });
  };

  const deleteAllMessage = async () => {
    const IDs = messages.value.map((v) => v.Id);
    if (!IDs.length) return;
    await updateMessageStatus(IDs, EnumMessageStatus.Delete, {
      actionType: "DeleteAll",
      showMessage: true,
    });
  };
  let loadMoreTimeout: string | number | NodeJS.Timeout | undefined;

  const onLoadMoreMessages = () => {
    clearTimeout(loadMoreTimeout);
    const { CurrentPage, MaxPage } = pagination.value;
    if (CurrentPage >= MaxPage || isLoadingMore.value) return;
    isLoadMore.value = true;
    loadMoreTimeout = setTimeout(fetchMessages, 100);
  };

  const startMessageInterval = () => {
    if (interval) {
      clearInterval(interval);
    }
    interval = setInterval(() => fetchMessages(false), 30000);
  };

  return {
    isLoadMore,
    activeTabIndex,
    isLoadingMore,
    isUpdatingStatus,
    isShowDeleteDialog,
    isShowPreviewMessageDialog,
    isLoading,
    messageFilterModel,
    messageDetail,
    pagination,
    activeTab,
    tabs,
    messages,
    unreadInfo,
    fetchMessages,
    onMessageClicked,
    onOpenDeleteConfirmation,
    onConfirmDelete,
    onMarkAllRead,
    deleteAllMessage,
    onLoadMoreMessages,
    startMessageInterval,
  };
};
