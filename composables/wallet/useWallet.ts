import { computed, ref } from "vue";

import apis from "@/utils/apis";
import { GetBalanceResponse } from "@/models/getBalanceResponse";
import notificationHelper from "@/utils/elementUiHelpers/notificationHelper";
import EnumMessageType from "@/models/enums/enumMessageType";
import { store } from "@/store";
import { IThemePropertiesFromApi } from "@/models/companyThemePropertiesRespone";
import themePropertiesHelper from "@/utils/themePropertyHelper";

export const useWallet = () => {
  const balance = ref({} as GetBalanceResponse);
  const getBalance = async () => {
    const response = await apis.getAllWalletsBalance();
    if (response.isSuccessful && response.Data) {
      balance.value = response.Data;
    }
  };
  // if (store.state.auth) {
  //   getBalance();
  // }
  const router = useRouter();
  const route = useRoute();
  const goToDeposit = () => {
    router.push("/deposit");
  };
  const isProcessing = ref(false);
  const withdrawalFromGameProvider = async () => {
    isProcessing.value = true;
    const response = await apis.getWithdrawalFromGameProvider();
    if (response.isSuccessful) {
      notificationHelper.notification(
        response.Message,
        EnumMessageType.Success
      );
    } else {
      notificationHelper.notification(response.Message, EnumMessageType.Error);
    }
    isProcessing.value = false;
  };
  const { companyThemePropertiesFromApi, isCashPlayer } = store.state;
  const themeProperties = computed(
    () => companyThemePropertiesFromApi.ThemeProperties
  );
  const depositWithdrawalProperty = computed(
    (): IThemePropertiesFromApi =>
      themePropertiesHelper.getSingleThemeProperty(
        themeProperties.value,
        "deposit_withdraw"
      )
  );
  return {
    balance,
    getBalance,
    goToDeposit,
    withdrawalFromGameProvider,
    isProcessing,
    depositWithdrawalProperty,
    isCashPlayer,
  };
};
