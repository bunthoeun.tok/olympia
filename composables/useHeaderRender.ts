import { computed } from "vue";
import { store } from "@/store";
import themePropertyHelper from "@/utils/themePropertyHelper";
import { IThemePropertiesFromApi } from "@/models/companyThemePropertiesRespone";
import { TabInfo } from "~~/models/IGetPlayableGamesResponse";

export default function useHeaderRender() {
  const themeProperties =
    store.state.companyThemePropertiesFromApi.ThemeProperties;
  const headerStyle = themePropertyHelper.getSingleThemeProperty(
    themeProperties,
    "header_style"
  );
  const isStyleTwo = headerStyle.Text === "header_2";
  const isStyleThree = headerStyle.Text === "header_3";

  const shownRoute = [
    "home",
    "sports",
    "virtual-sports",
    "live-casino",
    "games",
    "game-entry",
    "keno",
    "poker",
    "cockfight",
    "lottery",
    "promotion",
    "contact-us",
    "index",
  ];

  const route = useRoute();
  const { name } = toRefs(route);
  const isHeaderShow = computed(() =>
    shownRoute.some(
      (item) =>
        String(name.value).includes(item) &&
        !String(name.value).includes("my-promotion") &&
        !commonHelper.isIndependentGameFilterPage()
    )
  );
  const logo = computed(
    (): IThemePropertiesFromApi =>
      themePropertyHelper.getSingleThemeProperty(themeProperties, "logo")
  );

  const activetab = computed(() =>
    route.path === "/" ? route.path : route.path.slice(1) || "sports"
  );
  const checkActiveTab = (active: string, menu: TabInfo) => {
    if (route.matched[0].name === "GameInitial") {
      return active
        .replace(" ", "")
        .replace("-", "")
        .toLowerCase()
        .includes(menu?.TabType.replace(" ", "").toLowerCase());
    }
    if (route.matched[0].name !== "GameInitial") {
      return (
        active.replace(" ", "").replace("-", "").toLowerCase() ===
        menu?.TabType.replace(" ", "").toLowerCase()
      );
    }
    return false;
  };

  return {
    isStyleTwo,
    isStyleThree,
    isHeaderShow,
    logo,
    activetab,
    checkActiveTab,
  };
}
