import { store } from "~~/store";
import commonHelper from "../utils/commonHelper";
import { IAllCustomerSettingsData } from "~~/models/getCustomerSettingResponse";
import { EnumSelectedThemeMode } from "~~/models/enums/enumSelectedThemeMode";

const allowPages = ['/live', '/bet-history', '/favorite'];

export default function useWlSportConfig(isGlobalTheme: boolean) {
    if (!isGlobalTheme) {
      return {
        sportUrl: '',
        isWlSportLoading: false
      };
    }

    const allCustomerSettings = useState<IAllCustomerSettingsData[]>(
      KEYS.ALL_CUSTOMER_SETTINGS
    );

    const getBaseURL = () => {
      if (process.dev) {
        return useRuntimeConfig().public.SPORT_URL;
      } else {
        return String(useState(KEYS.WL_SPORT).value);
      }
    }
    const domian = `${window.location.protocol}//${commonHelper.getDomain()}${window.location.port ? ':': ''}${window.location.port}`;
    const getSportUrl = () => {
      const token = cookieHelper.getCookie('auth_token');
      const route = useRoute();
      
      let sportUrl = getBaseURL();
      const page = route.query.page ? String(route.query.page) : '';
      if (page && (allowPages.includes(page) || page.includes('sport-type'))) {
        sportUrl += page.startsWith('/') ? `${page}` : `/${page}`;
      }
      sportUrl += `?domain=${domian}&forceDesktop=${useIsDesktop()}&token=${token ?? ''}&lang=${useNuxtApp().$i18n.getLocaleCookie()?.toUpperCase()}&v=${new Date().getTime()}`;
      return sportUrl;
    }
    const sportUrl = getSportUrl();
    const config = {
        themeColor: `${domian}${useState<string>('color-config').value}`,
        template: useIsDesktop() ? 'desktop' : 'mobile',
        theme: 'globle'
    }
    const banners = themePropertyHelper.getMultipleThemesProperties(store.state.companyThemePropertiesFromApi.ThemeProperties, 'sport_banner_');
    const announcement = themePropertyHelper.getMultipleThemesProperties(store.state.companyThemePropertiesFromApi.ThemeProperties, 'announcement');
    const sportLayout = themePropertyHelper.getMultipleThemesProperties(store.state.companyThemePropertiesFromApi.ThemeProperties, 'sports_wl_layout');
    const oddStyle = themePropertyHelper.getMultipleThemesProperties(store.state.companyThemePropertiesFromApi.ThemeProperties, 'Sport_Odd_Style');
    
    const initdata = {
        config,
        username: store.state.username,
        domian: window.location.origin,
        isForceDesktop: useIsDesktop(),
        assetPath: commonHelper.getAssetPath(),
        customerSettings: allCustomerSettings.value.length
          ? JSON.parse(JSON.stringify(allCustomerSettings.value))
          : [],
        vmServer: commonHelper.getCurrentServer(),
        themeProperties: [
          ...banners.filter((banner) => !banner.HtmlId.includes("virtual_")),
          ...announcement,
          ...sportLayout,
          ...oddStyle,
        ]
    }
    const sendMessageToIframe = (data: any) => {
      const iframe = document.querySelector("#wl-sport") as HTMLIFrameElement;
      iframe.contentWindow?.postMessage(data, '*');
    };
    const isWlSportLoading = ref(true);
    const onIframeLoad = () => {
      sendMessageToIframe(initdata);
    };
    const router = useRouter();
  const receiveMessage = (event: MessageEvent) => {
    if (event.data === 'redirectToLogin') {
      router.push('/login')
    }
  };
  const themeProperties = computed(
    () => store.state.companyThemePropertiesFromApi.ThemeProperties
  );
  const isDarkMode = computed(
    () => store.state.selectedThemeMode === EnumSelectedThemeMode.Dark
  );
  watch(isDarkMode, () => {
    const globalThemeColorId = isDarkMode.value
      ? "theme_color_dark"
      : "theme_color_light";
    const colorId = themePropertyHelper.getSingleThemeProperty(
      themeProperties.value,
      globalThemeColorId
    );
    initdata.config.themeColor = `${domian}/colors/${colorId.Text}.css`;
    sendMessageToIframe(initdata);
  });
    onMounted(() => {
      const iframe = document.querySelector("#wl-sport") as HTMLIFrameElement;
      iframe.addEventListener('load', onIframeLoad, false);
      window.addEventListener('message', receiveMessage);
      setTimeout(() => {
        isWlSportLoading.value = false;
      }, 3000);
    });
    return {
      sportUrl,
      isWlSportLoading
    }
}