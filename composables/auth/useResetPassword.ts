import { reactive, Ref, ref } from "vue";
import { FormInstance } from "element-plus";
import formHelper from "@/utils/elementUiHelpers/formHelper";
import { IRequestResetPassword } from "@/models/auth/resetPassword";
import notificationHelper from "@/utils/elementUiHelpers/notificationHelper";
import apis from "@/utils/apis";
import EnumMessageType from "@/models/enums/enumMessageType";

interface IUseResetPassword {
  isProcessing: Ref<boolean>;
  forgotPasswordFormRef: Ref<FormInstance | undefined>;
  forgotPasswordFormData: {
    Email: string;
  };
  forgotPasswordFormRules: unknown;
  submitForm: (formEl: FormInstance | undefined) => void;
}
interface IUseResetPasswordCallbacks {
  onSuccess?: () => void;
  onFailure?: () => void;
}

export const useResetPassword = (
  params: IUseResetPasswordCallbacks
): IUseResetPassword => {
  const { $t } = useNuxtApp();
  const isProcessing = ref<boolean>(false);
  const forgotPasswordFormRef = ref<FormInstance | undefined>(undefined);
  const forgotPasswordFormData = reactive<{ Email: string }>({
    Email: "",
  });
  const validateEmail = () => {
    const validEmailRegex = /^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/g;
    if (!forgotPasswordFormData.Email) {
      return $t("required");
    }
    if (!validEmailRegex.test(forgotPasswordFormData.Email)) {
      return $t("form_validation_not_a_valid_email");
    }
    return "";
  };
  const forgotPasswordRules = {
    Email: {
      required: true,
      customRule: validateEmail,
    },
  };
  const forgotPasswordFormRules = formHelper.getRules(
    forgotPasswordRules,
    forgotPasswordFormData
  );
  const proceedResetRequest = async () => {
    try {
      isProcessing.value = true;
      const requestData: IRequestResetPassword = {
        Domain: commonHelper.getDomain(),
        Email: forgotPasswordFormData.Email.trim(),
      };

      const response = await apis.getResetPassword(requestData);
      const success = response.isSuccessful;
      if (success) {
        notificationHelper.notification(
          $t("reset_password_success_message"),
          EnumMessageType.Success,
          "",
          "reset-password-notification",
          ".forgot-form",
          false
        );
        if (params.onSuccess) {
          params.onSuccess();
        }
        return;
      }
      notificationHelper.notification(
        response.Message,
        EnumMessageType.Error,
        "",
        "reset-password-notification",
        ".forgot-form",
        false
      );
      if (params.onFailure) {
        params.onFailure();
      }
    } catch (error) {
      notificationHelper.notification(
        $t("error"),
        EnumMessageType.Error,
        "",
        "reset-password-notification",
        ".forgot-form",
        false
      );
      if (params.onFailure) {
        params.onFailure();
      }
    } finally {
      isProcessing.value = false;
    }
  };
  const submitForm = formHelper.getSubmitFunction(proceedResetRequest);

  return {
    isProcessing,
    forgotPasswordFormRef,
    forgotPasswordFormData,
    forgotPasswordFormRules,
    submitForm,
  };
};
