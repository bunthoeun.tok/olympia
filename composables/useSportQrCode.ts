import apis from "@/utils/apis";
import EnumMessageType from '~~/models/enums/enumMessageType';
import notificationHelper from '~~/utils/elementUiHelpers/notificationHelper';
import { store } from "@/store";
import { IAllSportBetsRequest } from '~~/models/sportqr/redeemSingleBetDetail';
import { SportBet } from '~~/models/sportbet/sportbet';
import { IRedeemRequest } from '~~/models/sportqr/redeemTicket';
import { IPreviousSession, PreviousSession } from '~~/models/sportqr/previousQR';
import { IPreviousQRRequest } from '~~/models/sportqr/previousQRReq';
import EnumApiErrorCode from '~~/models/enums/enumApiErrorCode';
import _ from 'lodash';
import arrayHelper from '~~/utils/arrayHelper';

const isShowQrCodePopup = ref(false);

export default function useSportQrCode() {
    const PayoutInfo = ref({
        PayoutId: 0,
        Token: '',
    });
    const isFirstLoad = ref<boolean>(true);
    const showInstruction = ref<boolean>(false);
    const isLoading = ref<boolean>(false);
    const isPayTicketLoading = ref<boolean>(false);
    const isRedeemedTicketLoading = ref<boolean>(false);
    const oid = useRoute().query.oid;
    const pid = useRoute().query.pid;
    const token = useRoute().query.token;
    const encodeToken = decodeURI(token as string);
    const operatorId = useRoute().query.i ?? 0;
    const OperatorToken = generateCryptoToken(
        useRoute().query.i?.toString() ?? ""
    );
    const getAllSportBetsRequest: IAllSportBetsRequest = {
        PayoutId: String(pid),
        Token: String(encodeToken),
    }
    const allSportBets = ref<SportBet[]>([]);
    const unPaidSportBets = ref<SportBet[]>([]);
    const PaidSportBets = ref<SportBet[]>([]);
    const totalStakeUnpaid = ref(0);
    const totalStakeUnpaidForDisplay = ref(0);
    const totalStakePaid = ref(0);
    const totalStakePaidForDisplay = ref(0);
    const totalPayoutForDisplay = ref(0);
    const currency = ref("");

    const previousBets = ref<IPreviousSession[]>([]);
    const isPreviousBetsLoading = ref<boolean>(false);
    const getPreviousBetsRequest = ref<IPreviousQRRequest>({
        WebId: Number(useRoute().query.webId) ?? 0,
        OperatorId: 0,
    })

    // call api to generate payout qr code
    const generatePayoutQrCode = async () => {
        const response = await apis.getPayoutQrCode();
        if (response.isSuccessful && response.Data) {
            PayoutInfo.value.PayoutId = response.Data.PayoutId;
            PayoutInfo.value.Token = response.Data.Token;
            isShowQrCodePopup.value = true;
        } else if (response.ErrorCode === 16000) {
            isShowQrCodePopup.value = false;
            notificationHelper.notification(response.Message.replace("Peru", ""), EnumMessageType.Error);
        }
    }
    const showQrCodePopup = () => {
        generatePayoutQrCode();
    }
    
    // call api to get all sport bets transaction
    const getAllSportBets = async () => {
        if (isFirstLoad) {
            isLoading.value = true;
        }
        const response = await apis.getAllSportBets(getAllSportBetsRequest);
        totalStakeUnpaid.value = 0;
        totalStakeUnpaidForDisplay.value = 0;
        totalStakePaid.value = 0;
        totalStakePaidForDisplay.value = 0;
        totalPayoutForDisplay.value = 0;
        if (response.ErrorCode === 0) {
            response.Data.SportBets.forEach((item) => {
                arrayHelper.upsert(allSportBets.value, item, "RefNo");
            });
            allSportBets.value = response.Data.SportBets;
            unPaidSportBets.value = response.Data.unPaidSportBets;
            PaidSportBets.value = response.Data.PaidSportBets;
            totalStakeUnpaid.value = response.Data.totalStakeUnpaid;
            totalStakeUnpaidForDisplay.value = response.Data.totalStakeUnpaidForDisplay;
            totalStakePaid.value = response.Data.totalStakePaid;
            totalStakePaidForDisplay.value = response.Data.totalStakePaidForDisplay;
            totalPayoutForDisplay.value = response.Data.totalPayoutForDisplay;
            currency.value = response.Data.currency;
            if (isFirstLoad) {
                isLoading.value = false;
                isFirstLoad.value = false;
            }
        } else if (response.ErrorCode === 205) {
            notificationHelper.notification("Invalid QR Code", EnumMessageType.Error);
            showInstruction.value = true;
        } else {
            isLoading.value = false;
            notificationHelper.notification(response.Message, EnumMessageType.Error);
        } 
    }

    // function for operator to collect money when the bet running and unpaid
    const onPayTicket = async () => {
        isPayTicketLoading.value = true;
        await getAllSportBets();
        const payTicketRequest = {
            WebId: store.state.webId,
            PayoutId: String(pid),
            Token: String(token),
            Amount: totalStakeUnpaid.value,
            OperatorId: Number(operatorId),
            SimulateId: 0,
            OnlineId: Number(oid),
        }
        const payTicketResponse = await apis.payTickets(payTicketRequest);
        if (payTicketResponse.ErrorCode === 0) {
            notificationHelper.notification(
                payTicketResponse.Message,
                EnumMessageType.Success
            );
            getAllSportBets();
            isPayTicketLoading.value = false;
        } else {
            notificationHelper.notification(
                payTicketResponse.Message,
                EnumMessageType.Error
            );
            isPayTicketLoading.value = false;
        }
    }

    // function for operator to redeem ticket when the bet is finished
    const onRedeemTicket = async (ref: string) => {
        isRedeemedTicketLoading.value = true;
        const refToken = generateCryptoToken(ref);
        const redeemTicketRequest: IRedeemRequest = {
            RefNo: ref,
            Token: String(refToken),
            WebId: store.state.webId,
            OperatorToken: String(OperatorToken),
            SimulateId: 0,
            OperatorId: Number(operatorId),
            OnlineId: Number(oid),
        };
        const redeemTicketResponse = await apis.redeemTicket(redeemTicketRequest);
        if (redeemTicketResponse.isSuccessful) {
            notificationHelper.notification(
                redeemTicketResponse.Message,
                EnumMessageType.Success
            );
            getAllSportBets();
            isRedeemedTicketLoading.value = false;
        } else {
            notificationHelper.notification(
                redeemTicketResponse.Message,
                EnumMessageType.Error
            );
            isRedeemedTicketLoading.value = false;
        }
    };

    const getPreviousBets = async () => {
        isPreviousBetsLoading.value = true;
        getPreviousBetsRequest.value.OperatorId = Number(operatorId);
        const response = await apis.getPreviousQR(getPreviousBetsRequest.value);
        if (response.ErrorCode === EnumApiErrorCode.success) {
            previousBets.value = _.orderBy(
                response.Data.PreviousSessions,
                "PayoutTime",
                "desc"
            );
            previousBets.value = previousBets.value.map((bet) => new PreviousSession(bet));
            isPreviousBetsLoading.value = false;
        } else {
            notificationHelper.notification(
                response.ErrorMessageForDisplay,
                EnumMessageType.Error
            );
            isPreviousBetsLoading.value = false;
        }
    }

    return {
        isShowQrCodePopup,
        showQrCodePopup,
        showInstruction,
        PayoutInfo,
        getPreviousBets,
        previousBets,
        isPreviousBetsLoading,
        getAllSportBets,
        allSportBets,
        unPaidSportBets,
        PaidSportBets,
        onRedeemTicket,
        onPayTicket,
        totalStakeUnpaidForDisplay,
        totalStakePaidForDisplay,
        isLoading,
        isPayTicketLoading,
        isRedeemedTicketLoading,
        totalPayoutForDisplay,
        currency,
    }
}