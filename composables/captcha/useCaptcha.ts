import { computed, Ref, ref } from "vue";
import { FormInstance } from "element-plus";
import featuresToggleHelper from "@/utils/featuresToggleHelper";
import EnumThemePropertyKey from "@/models/enums/enumThemePropertyKey";
import { ThemePropertiesFromApiDto } from "@/models/companyThemePropertiesRespone";

interface IUseCaptcha {
  captchaErrorMsg: Ref<string>;
  getCaptchaErrorMsg: (valid: FormInstance) => void;
}
interface ICaptcha {
  Captcha: string;
}

const clientCaptcha = computed(
  (): ThemePropertiesFromApiDto =>
    featuresToggleHelper.getThemePropertyFromApi(
      EnumThemePropertyKey.HtmlId,
      "login_captcha_two"
    )
);
const registerCaptcha = computed(
  (): ThemePropertiesFromApiDto =>
    featuresToggleHelper.getThemePropertyFromApi(
      EnumThemePropertyKey.HtmlId,
      "register_captcha_two"
    )
);
const useCaptcha = (model: ICaptcha): IUseCaptcha => {
  const { $t } = useNuxtApp();
  const captchaErrorMsg = ref("");
  const getCaptchaErrorMsg = (valid: FormInstance) => {
    if (!valid && model.Captcha.length > 0) {
      captchaErrorMsg.value = $t("please_enter_the_correct_validation_code");
    } else {
      captchaErrorMsg.value = "";
    }
  };
  return {
    captchaErrorMsg,
    getCaptchaErrorMsg,
  };
};

export { clientCaptcha, registerCaptcha, useCaptcha };
