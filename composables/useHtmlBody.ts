export const useAddBodyClasses = (classList: string | string[]) => {
  nextTick(() => {
    document.body.classList.add(
      Array.isArray(classList) ? classList.join(" ") : classList
    );
  });
};

export const useAddBodyAttributes = (attrName: string, attrValue: string) => {
  nextTick(() => {
    document.body.setAttribute(attrName, attrValue);
  });
};

export const useRemoveBodyAttributes = (attrName: string) => {
  nextTick(() => {
    document.body.removeAttribute(attrName);
  });
};

export const useSetV2Layout = () => {
  useAddBodyAttributes("layout-version", "2");
};

export const useToggleBodyOverflow = (isAllowScroll: boolean) => {
  if (isAllowScroll) {
    useRemoveBodyAttributes("body-noscroll");
  } else {
    useAddBodyAttributes("body-noscroll", "true");
  }
};
