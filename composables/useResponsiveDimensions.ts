interface IUseResponsiveDimensions {
    sizes: {
        width: number,
        height: number,
    }
}

export const useResponsiveDimensions = (): IUseResponsiveDimensions => {
    const sizes = reactive({
        width: window.innerWidth,
        height: window.innerHeight,
    })
    const onResize = () => {
        sizes.width = window.innerWidth;    
        sizes.height = window.innerHeight;    
    }
    onMounted(()=> {
        nextTick(()=>{
            window.addEventListener('resize', onResize);
        });
    });
    onBeforeUnmount(()=>{
        window.removeEventListener('resize', onResize);
    });
    return { sizes };
}