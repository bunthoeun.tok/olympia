import { IUpdateTelegramSsoTokenRequest } from '@/models/UpdateTelegramSsoToken';
import apis from '@/utils/apis';
import useCloseTelegramWebApp from "@/composables/useCloseTelegramWebApp";


export default function useTelegramWebApp() {
  const isShowPaymentPage = ref(true);
  const { isSubmitSuccess } = useCloseTelegramWebApp();

  const updateTelegramSsoToken = async () => {
    try {
      const ssoKey = localStorage.getItem('ssoKey');
      const onlineId = localStorage.getItem('onlineId');

      const request: IUpdateTelegramSsoTokenRequest = {
        SsoKey: ssoKey,
        OnlineId: onlineId
      }
      const response = await apis.updateTelegramSsoToken(request);
      if(response.isSuccessful){
        if(response.Data.IsConsumed){
          isShowPaymentPage.value = false;
        }
      }
    } catch (e) {
      isShowPaymentPage.value = false;
    }
  };

  const onCloseTelegramWebApp = ()=>{
    isSubmitSuccess.value = true;
  };

  return {
    updateTelegramSsoToken,
    isShowPaymentPage,
    onCloseTelegramWebApp
  }
}
