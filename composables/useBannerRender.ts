import { replace } from "lodash";
import { store } from "@/store";

export default function useBannerRender() {
  const route = useRoute();
  const tabSelected = computed(() =>
    replace(replace(route.path.split("/")[1], "-", ""), "/", "")
  );
  const actived = computed(() =>
    tabSelected.value !== "" ? tabSelected.value : "home"
  );
  const themeProperties = computed(
    () => store.state.companyThemePropertiesFromApi.ThemeProperties
  );
  const keyForMap: Record<string, string> = {
    home: "home_page_banner_style",
    sports: "sports_page_banner_style",
    livecasino: "live_casino_page_banner_style",
    games: "games_page_banner_style",
    virtualsports: "virtual_sports_page_banner_style",
    cockfight: "cockfight_page_banner_style",
    promotion: "promotion_page_banner_style",
    lottery: "lottery_page_banner_style",
    poker: "poker_page_banner_style",
  };
  const allBannerPageStyle = themePropertyHelper.getAllMultipleThemesProperties(
    themeProperties.value,
    "_page_banner_style"
  );
  const pageStyleSelected = computed(() =>
    allBannerPageStyle.find((item) => item.HtmlId === keyForMap[actived.value])
  );
  return {
    pageStyleSelected,
  };
}
