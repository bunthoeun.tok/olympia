import { ref, Ref, computed, ComputedRef, reactive, watch } from "vue";
import _ from "lodash";
import { FormInstance } from "element-plus";
import html2canvas from "html2canvas";
import featuresToggleHelper from "@/utils/featuresToggleHelper";
import apis from "@/utils/apis";
import EnumThemePropertyKey from "@/models/enums/enumThemePropertyKey";
import commonHelper from "@/utils/commonHelper";
import {
  IRedeemReferralAmountRequest,
  RedeemRequest,
  IGetReferralDiagramRequest,
  ITreeNode,
  TreeNode,
} from "@/models/referral/getReferral";
import { store } from "@/store";
import formHelper from "@/utils/elementUiHelpers/formHelper";
import notificationHelper from "@/utils/elementUiHelpers/notificationHelper";
import EnumMessageType from "@/models/enums/enumMessageType";
import textHelper from "@/utils/formatHelpers/textFormatHelper";
import { ThemePropertiesFromApiDto } from "@/models/companyThemePropertiesRespone";
import useOnBack from "@/composables/useOnBack";
import numberFormatHelper from "~~/utils/formatHelpers/numberFormatHelper";

interface IReferralDto {
  referralLink: ComputedRef<string>;
  onCopyTextToClipBoard: () => void;
  onCopyQRCode: () => void;
  programRule: ComputedRef<ThemePropertiesFromApiDto>;
}
interface IReferralTabDto {
  activeName: Ref<string | undefined>;
  handleClick: () => void;
}
const dialogVisible = ref(false);
const isLoading = ref(false);
const redeemableModel = ref(0);
const isProcessing = ref(false);
const { currentRoute } = useOnBack();

const useReferral = (): IReferralDto => {
  const { copyTextToClipBoard } = commonHelper;
  const onCopyQRCode = () => {
    const img = document.getElementById("qrCode") as HTMLCanvasElement;
    const htmlString = `
        <div style="width:750px; height:1335px; display:flex; align-items:center; background:center/contain no-repeat">
            <img src='${img.toDataURL()}' style="width:350px; height:350px; padding:20px; margin:auto; background:#fff">
        </div>
      `;
    const html = document.createElement("div");
    html.setAttribute("style", "width:750px; height:1335px");
    html.innerHTML = htmlString;
    document.body.appendChild(html);
    html2canvas(html).then((canvas) => {
      const anchor = document.createElement("a");
      anchor.href = canvas.toDataURL("image/png");
      anchor.download = "IMAGE.PNG";
      anchor.click();
    });
    document.body.removeChild(html);
  };


  const referralLink = computed((): string => `${document.location.protocol}//${document.location.hostname}/register?ref=${store.state.username}`);
  const programRule = computed(
    (): ThemePropertiesFromApiDto =>
      featuresToggleHelper.getThemePropertyFromApi(
        EnumThemePropertyKey.HtmlId,
        "program_rule"
      )
  );
  const onCopyTextToClipBoard = (): void => {
    copyTextToClipBoard(referralLink.value);
  };
  return {
    referralLink,
    onCopyTextToClipBoard,
    onCopyQRCode,
    programRule,
  };
};

const useReferralTab = (): IReferralTabDto => {
  const isMobile = !useIsDesktop();
  const { push } = useRouter();
  const { name, matched } = useRoute();
  // TODO: change this after the old version is phased out.
  const activeName: Ref<string | undefined> = ref(
    isMobile ? name?.toString().replaceAll("Feature", "") : name?.toString()
  );
  const handleClick = () => {
    push({ name: activeName.value });
  };

  watch(
    () => currentRoute.value,
    (currentVal) => {
      const currentTab = currentVal.split("/")[2];
      const tabs = ["Referral", "History", "Diagram"];
      if (tabs.includes(currentTab)) {
        activeName.value = currentTab;
      }
    }
  );
  return { activeName, handleClick };
};

const useReferralModel = () => {
  const { $t } = useNuxtApp();
  const isMobile = !useIsDesktop();
  const labelPosition = computed(() => (isMobile ? "top" : "left"));
  const formRuleRef = ref<FormInstance>();
  const apiRequest: IRedeemReferralAmountRequest = reactive({
    Amount: 0,
  });
  const onCloseDialog = () => {
    dialogVisible.value = false;
  };
  const validateAmount = (): string => {
    if (apiRequest.Amount > redeemableModel.value) {
      return $t("insufficient_balance");
    }
    if (apiRequest.Amount <= 0) {
      return $t("amount_should_be_bigger_than_0");
    }
    return "";
  };
  const rules = {
    Amount: { customRule: validateAmount, required: true },
  };
  const formRule = formHelper.get2Rules(rules);
  const submitRequest = () => {
    isProcessing.value = true;
    apis
      .getRedeemReferralAmountCallRedeemReferralAmount(apiRequest)
      .then((response) => {
        if (response.isSuccessful) {
          notificationHelper.notification(
            response.Message,
            EnumMessageType.Success
          );
          onCloseDialog();
        } else {
          notificationHelper.notification(
            response.Message,
            EnumMessageType.Error
          );
        }
      });
    isProcessing.value = false;
  };
  const redeemableModelForDisplay = computed(() => numberFormatHelper.addAccountingFormat(redeemableModel.value));
  const onSubmit = formHelper.getSubmitFunction(submitRequest);
  return {
    dialogVisible,
    labelPosition,
    onCloseDialog,
    submitRequest,
    apiRequest,
    formRule,
    redeemableModel,
    redeemableModelForDisplay,
    formRuleRef,
    isProcessing,
    onSubmit,
  };
};

const useReferralComponent = () => {
  const referralForDisplay = ref("0");
  const totalForDisplay = ref("0");
  const redeemable = ref(0);
  const redeemableForDisplay = ref("0");
  const redeemedForDisplay = ref("0");
  const referralCount = ref(0);
  const totalRewardForDisplay = ref("0");
  const rebet = ref("0");
  const getReferralAmount = async () => {
    isLoading.value = true;
    const response = await apis.getReferralLayerAmount();
    if (response.isSuccessful) {
      const {
        TotalReferralAmountTillYesterdayForDisplay,
        RedeemedForDisplay,
        Redeemable,
        RedeemableForDisplay,
        ReferralChildrenCount,
        TotalRebateTillYesterdayForDisplay,
        TotalRewardTillYesterdayForDisplay,
      } = response.Data;
      referralForDisplay.value = TotalReferralAmountTillYesterdayForDisplay;
      totalForDisplay.value = TotalReferralAmountTillYesterdayForDisplay;
      redeemable.value = Redeemable;
      totalRewardForDisplay.value = TotalRewardTillYesterdayForDisplay;
      referralCount.value = ReferralChildrenCount;
      rebet.value = TotalRebateTillYesterdayForDisplay;
      redeemedForDisplay.value = RedeemedForDisplay;
      redeemableForDisplay.value = RedeemableForDisplay;
    }
    isLoading.value = false;
  };
  const onRedeemNow = () => {
    dialogVisible.value = true;
    redeemableModel.value = redeemable.value;
  };
  watch(dialogVisible, (newVal) => {
    if (!newVal) {
      getReferralAmount();
    }
  });
  getReferralAmount();
  return {
    referralForDisplay,
    isLoading,
    onRedeemNow,
    totalForDisplay,
    redeemedForDisplay,
    redeemableForDisplay,
    totalRewardForDisplay,
    dialogVisible,
    rebet,
    referralCount,
  };
};

const useReferralHistory = () => {
  const tableData = ref<Array<RedeemRequest>>([]);
  const getRedeemReferralRequest = async () => {
    isLoading.value = true;
    const response = await apis.getRedeemReferralRequest();
    if (response.isSuccessful) {
      tableData.value = response.Data.RedeemRequests;
    }
    isLoading.value = false;
  };
  getRedeemReferralRequest();
  return {
    tableData,
    isLoading,
  };
};

const useReferralDiagram = () => {
  const treeData = ref<Array<ITreeNode>>([]);
  const createTree = (
    nodes: Array<ITreeNode>,
    parentId = 0
  ): Array<ITreeNode> => {
    nodes.sort((item1, item2) => item1.Layer - item2.Layer);
    const layer = nodes[0].Layer;
    let tree = nodes.filter((item) => item.Layer === layer);
    if (parentId > 0) {
      tree = tree.filter((item) => item.ParentCustomerId === parentId);
    }
    const childNode = nodes.filter((item) => item.Layer !== layer);
    if (childNode.length > 0) {
      tree.forEach((node) => {
        const child = createTree(childNode, node.CustomerId);
        node.Children = child;
        node.ChildCount = child.length;
      });
    }
    return tree;
  };
  const getNodeData = async () => {
    isLoading.value = true;
    const date = new Date();
    const request: IGetReferralDiagramRequest = {
      StartDate: textHelper.dateStringTo24HourClock(String(date)),
      EndDate: textHelper.dateStringTo24HourClock(String(date)),
    };
    const response = await apis.getReferralDiagram(request);
    if (response.isSuccessful) {
      response.Data.NodesList.forEach((item) => {
        const {
          CustomerId,
          ParentCustomerId,
          Layer,
          Turnover,
          ThisMonthTurnover,
          LastMonthTurnover,
          NetTurnover,
          ThisMonthNetTurnover,
          LastMonthNetTurnover,
          Currency,
          Username,
        } = item;
        const res: ITreeNode = {
          CustomerId,
          ParentCustomerId,
          Layer,
          Turnover,
          ThisMonthTurnover,
          LastMonthTurnover,
          NetTurnover,
          ThisMonthNetTurnover,
          LastMonthNetTurnover,
          Currency,
          Username,
          Children: [],
          ChildCount: 0,
          IsExpand: false,
        };
        treeData.value.push(new TreeNode(res));
      });
    }
    treeData.value = createTree(treeData.value);
    isLoading.value = false;
  };
  getNodeData();
  return { treeData, isLoading };
};

export {
  useReferral,
  useReferralTab,
  useReferralModel,
  useReferralComponent,
  useReferralHistory,
  useReferralDiagram,
};
