import { computed } from "vue";
import featuresToggleHelper from "@/utils/featuresToggleHelper";
import EnumThemePropertyKey from "@/models/enums/enumThemePropertyKey";
import { useHeaderComponent } from "@/composables/useTheme";
import { store } from '@/store';

const useMenu = () => {
  const { auth, isShowDepositWithdrawal, isCashPlayer, register } = useHeaderComponent();
  const promotionTab = computed(() => featuresToggleHelper.getThemePropertyFromApi(EnumThemePropertyKey.HtmlId, "promotion_tab"));
  const isShowPromotion = computed(() => promotionTab.value.HtmlId ? (!promotionTab.value.IsHidden && isCashPlayer.value) : isCashPlayer.value);
  const isShowReferral = computed(() => (store.state.isReferralEnabled && isCashPlayer.value) && store.state.IsShowReferralPage);
  const isDev = useRuntimeConfig().public.NODE_ENV === "development";
  const checkPlayer = computed(() => {
    let isShow = true;
    if (!auth) {
      isShow = isShowDepositWithdrawal.value;
    } else {
      isShow = isCashPlayer.value && isShowDepositWithdrawal.value;
    }
    return isShow;
  });
  return {
    isDev,
    isCashPlayer,
    isShowDepositWithdrawal,
    isShowPromotion,
    isShowReferral,
    checkPlayer,
    register,
  };
};

export default useMenu;
