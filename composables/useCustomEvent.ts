const CUSTOM_EVENT = {
  GAME_SEARCH: "game-search",
};

export const useCustomEvents = () => {
  const dispatchGameSearchEvent = (searchTerm: string) => {
    const event = new CustomEvent(CUSTOM_EVENT.GAME_SEARCH, {
      bubbles: true,
      detail: { term: searchTerm },
    });
    document.dispatchEvent(event);
  };

  return { dispatchGameSearchEvent };
};
