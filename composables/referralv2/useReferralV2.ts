import { addHours, format } from 'date-fns';
import _ from "lodash-es";
import { store } from "~~/store";
import commonHelper from "~~/utils/commonHelper";
import EnumThemePropertyKey from "~~/models/enums/enumThemePropertyKey";
import html2canvas from "html2canvas";
import { GetPlayerRedeemRequest, IGetPlayerRedeemHistoryRequest, PlayerRedeemRequest, ReferralMember } from "~~/models/referral/referralV2";
import { reactive, ref, ComputedRef, } from 'vue';
import EnumMessageType from '~~/models/enums/enumMessageType';
import notificationHelper from '~~/utils/elementUiHelpers/notificationHelper';
import textHelper from '~~/utils/formatHelpers/textFormatHelper';

interface IReferralTab { 
    link: string, 
    label: string,
}
interface IUseReferralV2 {
    referralTabs: IReferralTab[],
    activeName: Ref<string>,
    setDisplayLocalTimeState: () => Promise<void>,
}

type TTimeLeft = {
    days: number,
    hours: number,
    minutes: number,
}

type TReferralInformationPageData = {
    referralLink: string,
    referralCode: string,
    isGettingPageData: boolean,
    isGeneratingQRCode: boolean,
    doesCompanyHaveReferralEvent: boolean,
    criteria: string,
    timeLeft: TTimeLeft,
}

interface IUseReferralV2Information {
    copyTextToClipBoard: (text: string, showNotification?: boolean) => void,
    onDownloadQRCode: (canvasId: string) => void,
    getPageData: () => Promise<void>,
    pageData: TReferralInformationPageData,
}

interface IReferralRedeemPageData {
    isSubmitingRedeem: boolean,
    isGettingPageData: boolean,
    referralData: GetPlayerRedeemRequest,
}

interface IUseReferralV2Redeem {
    pageData: IReferralRedeemPageData,
    getPlayerRedeemRequest: () => Promise<void>,
    applyRedeem: () => Promise<void>,
}

interface IReferralV2HistoryPageData {
    isGettingPageData: boolean,
    StartDate: Date,
    EndDate: Date,
    requestHistory: PlayerRedeemRequest[],
}

interface IUseReferralV2History {
    pageData: IReferralV2HistoryPageData,
    getPageData: () => Promise<void>,
    disabledDatesForStartDate: (time: Date) => boolean,
    disabledDatesForEndDate: (time: Date) => boolean,
    isSubmitable: ComputedRef<boolean>,
}

export const useReferralV2 = (): IUseReferralV2 => {
    const route = useRoute();
    const activeName = ref(route.path);

    const isUCPopup = computed(() => route.fullPath.includes("uc=/"));
    const updateActiveName = () => {
        let activeTabName = route.path;
        if (isUCPopup.value) {
            activeTabName = getLink(String(route.query.uc));
        }
        activeName.value = activeTabName;
    }
    watch(() => route.fullPath, updateActiveName);
    watch(() => activeName.value, () => {
        navigateTo(activeName.value);
    })
    const getLink = (link: string) => {
        if (isUCPopup.value) return `${route.path}?uc=${link}`;
        return link;
    }
    onMounted(() => {
        if (route.fullPath === getLink("/referral-v2") || route.fullPath === getLink("/referral-v2/")) {
            activeName.value = getLink("/referral-v2/information");
            return;
        }
        updateActiveName();
    })
    const referralTabs = [
        {
            link: getLink("/referral-v2/information"),
            label: "general_information",
        },
        {
            link: getLink("/referral-v2/redeem"),
            label: "redeem",
        },
        {
            link: getLink("/referral-v2/history"),
            label: "history",
        },
    ];

    const setDisplayLocalTimeState = async () => {
        const response = await apis.getPlayerPreferenceForDisplay();
        if (response.isSuccessful) {
            store.commit(
                "updateDisplayLocalTime",
                response.Data.TimeZone.IsUsingLocalTime
            );
        }
    };

    return {
        referralTabs,
        activeName,
        setDisplayLocalTimeState,
    }
}

export const useReferralV2Information = (): IUseReferralV2Information => {
    const { copyTextToClipBoard } = commonHelper;

    const pageData = reactive({
        referralLink: `${document.location.protocol}//${document.location.hostname}/register?ref=${store.state.username}`,
        referralCode: store.state.username ?? '',
        isGettingPageData: false,
        doesCompanyHaveReferralEvent: false,
        isGeneratingQRCode: false,
        criteria: '',
        timeLeft: {
            days: 0,
            hours: 0,
            minutes: 0,
        }
    });

    const getTimeApart = (fromDate: string, toDate: string) => {
        // Convert the dates to UTC timestamps
        const from = Date.parse(fromDate);
        const to = Date.parse(toDate);

        // Calculate the difference in milliseconds
        const diff = to - from;

        // Calculate the difference in days, hours, minutes, and seconds
        const days = Math.floor(diff / (1000 * 60 * 60 * 24));
        const hours = Math.floor((diff / (1000 * 60 * 60)) % 24);
        const minutes = Math.floor((diff / (1000 * 60)) % 60);
        const seconds = Math.floor((diff / 1000) % 60);

        // Return the time difference as an object
        return {
            days,
            hours,
            minutes,
            seconds
        };
    }

    const getComputersTimeOffset = () => ((new Date()).getTimezoneOffset() / 60) * -1;
    const convertToGMTOffset = (date: Date, dateOffset: number, targetOffset: number): Date => {
        const timeOffset = (targetOffset) - (dateOffset);
        return addHours(date, timeOffset);
    }

    const getPageData = async () => {
        pageData.isGettingPageData = true;
        await apis.getReferralV2GetGeneralInformation().then((response) => {
            pageData.doesCompanyHaveReferralEvent = response.Data.IsHaveReferralEvent;
            if (!pageData.doesCompanyHaveReferralEvent) {
                pageData.isGettingPageData = false;
                return;
            }
            pageData.criteria = response.Data.Content;
            const expiryDate = new Date(response.Data.EndDate)
            // get current time the computer
            let now = new Date();
            // convert it to GMT-4
            const isUsingLocalTime = store.state.displayLocalTime;
            const usersTimeOffset = isUsingLocalTime ? getComputersTimeOffset() : 8;
            if (!isUsingLocalTime) {
                // convert it to gmt+8 first
                now = convertToGMTOffset(now, getComputersTimeOffset(), 8)
            }
            const nowMinusFour = convertToGMTOffset(now, usersTimeOffset, -4);
            const timeLeft = getTimeApart(nowMinusFour.toString(), expiryDate.toString());
            // if the event has expired (somehow it was sent from api), make it looks like company doesn't have event.
            if (timeLeft.days < 0) {
                pageData.doesCompanyHaveReferralEvent = false;
            }
            pageData.timeLeft.days = timeLeft.days;
            pageData.timeLeft.hours = timeLeft.hours;
            pageData.timeLeft.minutes = timeLeft.minutes;
        }).finally(()=> {
            pageData.isGettingPageData = false;
        });
    }


    const onDownloadQRCode = (canvasId: string) => {
        pageData.isGeneratingQRCode = true;
        const img = document.getElementById(canvasId) as HTMLCanvasElement;
        if (!img) {
            pageData.isGeneratingQRCode = false;
            return;
        }

        const htmlString = `
        <div style="width:750px; height:1335px; display:flex; align-items:center; background:center/contain no-repeat">
            <img src='${img.toDataURL()}' style="width:350px; height:350px; padding:20px; margin:auto; background:#fff">
        </div>
      `;
        const html = document.createElement("div");
        html.setAttribute("style", "width:750px; height:1335px");
        html.innerHTML = htmlString;
        document.body.appendChild(html);
        html2canvas(html).then((canvas) => {
            const anchor = document.createElement("a");
            anchor.href = canvas.toDataURL("image/png");
            anchor.download = "IMAGE.PNG";
            anchor.click();
            document.body.removeChild(html);
        }).finally(() => {
            pageData.isGeneratingQRCode = false;
        });
    };
    return {
        pageData,
        getPageData,
        onDownloadQRCode,
        copyTextToClipBoard,
    }

}

export const useReferralV2Redeem = (isGettingPageDataDefault = true): IUseReferralV2Redeem => {
    const pageData = reactive({
        isSubmitingRedeem: false,
        isGettingPageData: isGettingPageDataDefault,
        referralData: new GetPlayerRedeemRequest({
            CompanyPromotionId: 0,
            ReferralCount: 0,
            EffectiveReferralCount: 0,
            Redeemable: 0,
            ReferralMemberList: [],
        }),
    });

    const getPlayerRedeemRequest = async () => {
        if (!pageData.isGettingPageData) {
            pageData.isGettingPageData = true;
        }
        await apis.getReferralV2PlayerRedeemRequest().then(res => {
            pageData.referralData = res.Data;
        }).finally(() => {
            pageData.isGettingPageData = false;
        });
    }
    const applyRedeem = async () => {
        pageData.isSubmitingRedeem = true;
        await apis.getReferralV2GetApplyRedeem({
            CompanyPromotionId: pageData.referralData.CompanyPromotionId,
            RedeemAmount: pageData.referralData.Redeemable,
            Device: commonHelper.getUserAgent(),
            Url:String(window.location),
            FingerPrint:store.state.visitorId,
            IP:store.state.geography?.Ip,
        }).then(response => {
            if (response.isSuccessful) {
                notificationHelper.notification(response.ErrorMessageForDisplay, EnumMessageType.Success);
                getPlayerRedeemRequest();
            } else {
                notificationHelper.notification(response.ErrorMessageForDisplay, EnumMessageType.Error);
            }
        }).finally(()=>{
            pageData.isSubmitingRedeem = false;
        });        
    }
    return {
        pageData,
        getPlayerRedeemRequest,
        applyRedeem,
    }
}
export const useReferralV2History = (): IUseReferralV2History => {
    const initStartDate = new Date(textHelper.getGmtMinusFourDate(String(new Date())));
    const initEndDate = new Date(textHelper.getGmtMinusFourDate(String(new Date())));
    initEndDate.setHours(23, 59, 59, 999);

    const pageData = reactive<IReferralV2HistoryPageData>({
        isGettingPageData: false,
        StartDate: initStartDate,
        EndDate: initEndDate,
        requestHistory: [],
    });
    const ONE_DAY_IN_MILLISECONDS = 86_400_000;
    // use with el-date-picker's disabled-date attribute.
    const disabledDatesForEndDate = (time: Date) => {
        if (time.getTime() === (pageData.EndDate.getTime())) return false;
        return time.getTime() < (pageData.StartDate.getTime());
    }
    const disabledDatesForStartDate = (time: Date) => {
        if (time.getTime() === (pageData.EndDate.getTime())) return false;
        return time.getTime() > (pageData.EndDate.getTime());
    }
    // check if data is correct for making a request
    const isSubmitable = computed(() => {
        if (pageData.isGettingPageData) return false;
        if (!pageData.StartDate || !pageData.EndDate) return false;
        return pageData.StartDate <= pageData.EndDate;
    });
    const getPageData = async () => {
        if (!isSubmitable.value) return;
        pageData.isGettingPageData = true;
        const requestParams: IGetPlayerRedeemHistoryRequest = {
            StartDate: format(pageData.StartDate, "yyyy/MM/dd 00:00:00"),
            EndDate: format(pageData.EndDate, "yyyy/MM/dd 23:59:59"),
        }
        await apis.getReferralV2GetPlayerRedeemHistory(requestParams).then(response => {
            pageData.requestHistory = response.Data.RedeemRequests;
            pageData.requestHistory = _.orderBy(pageData.requestHistory, 'RequestedDate', 'desc');
        }).finally(() => {
            pageData.isGettingPageData = false;
        });
      }
    return {
        pageData,
        getPageData,
        isSubmitable,
        disabledDatesForStartDate,
        disabledDatesForEndDate,
    }
}