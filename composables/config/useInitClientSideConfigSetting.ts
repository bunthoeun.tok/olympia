import { useVisitorData } from "@fingerprintjs/fingerprintjs-pro-vue-v3";
import { GetBsiGameListResponse } from "~~/models/bsiGameList";
import { store } from "~~/store";
import { appStore } from "~~/store/app";
import { gameStore } from "~~/store/game";
import { STORE_MUTATIONS } from "~~/utils/constants";
import useLogin from "../useLogin";
import cookieHelper from "../../utils/cookieHelper";

export default function useInitClientSideConfigSetting(globleStates: any) {
  const nuxtApp = useNuxtApp();
  const route = useRoute();
  const checkLayout = () => {
    const pagesWithNoLayout = ["um-page", "gamepopup", "gamepopup-id"];
    if (route.name === "game-id" && Object.keys(route.query).includes("pop")) {
      globleStates.layout = "full-screen";
      return;
    }
    const shouldUseLayout = !pagesWithNoLayout.includes(String(route.name));
    if (shouldUseLayout) return;
    globleStates.layout = "full-screen";
  };
  watch(() => route.name, checkLayout);
  onMounted(() => {
    checkLayout();
    const useUCPopup =
      globleStates.themeColor.Text === "bk8_green" ||
      globleStates.themeColor.Text.includes("global") ||
      globleStates.themeColor.Text.includes("playful");
    if (
      route.fullPath.includes("uc=/") &&
      !(useUCPopup && (globleStates.isDesktop || (globleStates.isPerushopEnabled && globleStates.themeColor.Text.includes("global"))))
    ) {
      navigateTo(String(route.query.uc));
    }
  });

  const fpLibrary = globleStates.fingerPrintLibrary ?? '';

  const today = new Date();
  const sevenDaysLater = new Date(today.getTime() + 7 * 24 * 60 * 60 * 1000);
  if (fpLibrary == 'Distill') {
    store.commit(STORE_MUTATIONS.VISITOR_ID, '');
    const reese84 = useCookie("reese84");
    const localFP = useCookie('distillFP', {
      expires: sevenDaysLater,
    });
    let intervalCount = 0;
    const handleStoreDistillToken = async (token: string) => {
      if (!localFP.value) {
        if (token) {
          const response: any = await apiCalling.callGetDistillFP(token);
          if (response.ErrorCode === 0) {
            store.commit(STORE_MUTATIONS.VISITOR_ID, response.Data.Result.fingerprint);
            localFP.value = response.Data.Result.fingerprint;
            await nextTick();
            const scriptElement = document.querySelector('script[src="/52k3j5h2mnasdi"]');
            if (scriptElement) {
              scriptElement.remove();
            }
            localStorage.removeItem('reese84')
            cookieHelper.removeCookie('reese84');
          }
        }
      } else {
        store.commit(STORE_MUTATIONS.VISITOR_ID, localFP.value);
      }
    };
    if (!reese84.value && !localFP.value) {
      useHead({script: [
        {
          src: '/52k3j5h2mnasdi',
          tagPosition: 'head',
          defer: '',
          type: 'text/javascript',
          onload: () => {
            const intervalToGetToken = setInterval(() => {
              intervalCount += 1;
              reese84.value = useCookie("reese84").value ?? '';
              if (reese84.value) handleStoreDistillToken(reese84.value ?? '');
              if(reese84.value !== '' || intervalCount === 6) clearInterval(intervalToGetToken);
            }, 1300);
          }
        },
      ]});
    } else {
      handleStoreDistillToken(reese84.value ?? '');
    }
  }

  if (fpLibrary === 'FingerPrintJS') {
    store.commit(STORE_MUTATIONS.VISITOR_ID, '');
    const localFP = useCookie(STORE_MUTATIONS.VISITOR_ID, {
      expires: sevenDaysLater,
    });
    if (!localFP.value) {
      const { data } = useVisitorData(
        { extendedResult: true, ignoreCache: false },
        { immediate: true }
      );
  
      watch(data, (currentData) => {
        if (currentData) {
          store.commit(STORE_MUTATIONS.VISITOR_ID, currentData.visitorId);
          localFP.value = currentData.visitorId;
        }
      });
    } else {
      store.commit(STORE_MUTATIONS.VISITOR_ID, localFP.value);
    }
  }

  const getCurrencyWhiteList = async () => {
    const response = await apis.getCurrencyWhiteList();
    if (response.isSuccessful) {
      appStore.commit(
        "updateCurrencyWhiteList",
        response.Data.CurrencyWhiteList
      );
    } else {
      appStore.commit("updateCurrencyWhiteList", []);
    }
  };

  const getGames = async () => {
    const response = await apis.getPlayableGame(
      globleStates.currency ?? "",
      globleStates.locale
    );
    if (response.isSuccessful) {
      const result = new GetBsiGameListResponse(response.Data);
      gameStore.commit(STORE_MUTATIONS.GAMES, result.SortedGame);
    } else {
      gameStore.commit(STORE_MUTATIONS.GAMES, []);
    }
  };

  const storeCashAgentValue = () => {
    const tenMins = 600;
    const requestUrl = window.location.search;
    const cashAgentParams = new URLSearchParams(requestUrl).get("ca");
    if (cashAgentParams) {
      cookieHelper.setCookie("vue_agent_name", cashAgentParams, tenMins);
    }
  };
  let isShowReferralPage = false;
  const getCheckHaveReferral = async () => {
    if (globleStates.isLandingPage) return;
    const response = await apis.getReferralV2GetGeneralInformation();
    if (response.isSuccessful) {
      isShowReferralPage = response.Data.IsHaveReferralEvent;
    }
    return isShowReferralPage;
  };
  const storeReferralValue = () => {
    const requestUrl = window.location.search;
    const tenMins = 600;
    const referralParams =
      new URLSearchParams(requestUrl).get("referral") ??
      new URLSearchParams(requestUrl).get("ref");
    if (referralParams) {
      cookieHelper.setCookie("vue_referral", referralParams, tenMins);
    }
  };
  const initClientSideConfig = async () => {
    const localeCookie = nuxtApp.$i18n.getLocaleCookie();
    nuxtApp.$i18n.setLocale(String(localeCookie));
    store.commit(STORE_MUTATIONS.WEB_ID, globleStates.stateWebIdByDomian);
    store.commit(STORE_MUTATIONS.GEOGRAPHY, globleStates.stateGeography);
    store.commit(
      STORE_MUTATIONS.COMPANY_THEME_PROPERTIES_FROM_API,
      globleStates.stateDataThemeProperties
    );
    store.commit(STORE_MUTATIONS.IMAGE_VERSION, globleStates.version);
    store.commit(
      STORE_MUTATIONS.SELECTED_THEME_NAME,
      globleStates.stateThemeName
    );
    store.commit(
      STORE_MUTATIONS.ORIGINAL_THEME_NAME,
      globleStates.themeName,
    );
    store.commit(STORE_MUTATIONS.LANGUAGE, globleStates.stateLanguages);
    store.commit(STORE_MUTATIONS.LOCALE, globleStates.locale);
    store.commit(STORE_MUTATIONS.TAB_INFOS, globleStates.stateTabMenu);
    store.commit(STORE_MUTATIONS.GAME_PROVIDERS, globleStates.gameProviders);
    store.commit(
      STORE_MUTATIONS.GAME_TAB_PROVIDER_LIST,
      globleStates.gameTabProviders
    );
    store.commit(
      STORE_MUTATIONS.IS_PERO_SHOP_ENABLED,
      globleStates.isPeruShopEnabled
    );
    appStore.commit(
      "updateIsIpWhitelistEnabled",
      globleStates.isIpWhitelistEnabled
    );
    appStore.commit("updateIsWLAppEnabled", globleStates.isWLAppEnabled);
    appStore.commit(
      "updateIsV2ReferralEnabled",
      globleStates.isV2ReferralEnabled
    );
    appStore.commit("updatePlayerCurrencyList", globleStates.allCurrencies);
    let isShowReferral = await getCheckHaveReferral();
    store.commit("updateIsShowReferralPage", isShowReferral);
    const flp = useRoute().query.flp;
    if (flp) {
      const [username, password] = atob(flp)?.split(":");

      if (username && password) {
        const { loginFormData, proceedLogin } = useLogin();
        loginFormData.Username = username;
        loginFormData.Password = password;
        await proceedLogin(true);
        return;
      }
    }
    if (globleStates.isAuth && !globleStates.isLandingPage) {
      isShowReferral = await getCheckHaveReferral();
      store.commit(STORE_MUTATIONS.IS_SHOW_REFERRAL, isShowReferral);
      store.commit(
        STORE_MUTATIONS.IS_CASH_PLAYER,
        themePropertyHelper
          .getDecodeKeyJWT(globleStates.token, "icp")
          .toLowerCase() === "true"
      );
      store.commit(
        STORE_MUTATIONS.USERNAME,
        themePropertyHelper.getDecodeKeyJWT(globleStates.token, "n")
      );
      store.commit(
        STORE_MUTATIONS.CUSTOMER_ID,
        Number(themePropertyHelper.getDecodeKeyJWT(globleStates.token, "c"))
      );
      store.commit(
        STORE_MUTATIONS.CURRENCY,
        themePropertyHelper.getDecodeKeyJWT(globleStates.token, "cu")
      );
      store.commit(
        STORE_MUTATIONS.IS_IMAGE_LESS_1MB_ENABLED,
        globleStates.isUpdateIsImageLess1MBEnabled
      );
      if (globleStates.isCockfightWhitelist === "")
        store.commit("updateIsCockfightWhitelist", true);
      else
        store.commit(
          "updateIsCockfightWhitelist",
          globleStates.isCockfightWhitelist
            .toLocaleLowerCase()
            .split(",")
            .includes(
              themePropertyHelper
                .getDecodeKeyJWT(globleStates.token, "n")
                .toLocaleLowerCase()
            )
        );
      appHelper.initTokenRefresh();
    } else {
      store.commit("updateIsCockfightWhitelist", true);
      store.commit(STORE_MUTATIONS.USERNAME, "");
      store.commit(STORE_MUTATIONS.CUSTOMER_ID, "");
      store.commit(STORE_MUTATIONS.CURRENCY, "");
      store.commit(STORE_MUTATIONS.AUTH, false);
      store.commit(STORE_MUTATIONS.TOKEN, "");
    }
    store.commit(
      STORE_MUTATIONS.ALL_CUSTOMER_SETTINGS,
      globleStates.allCustomerSettings
    );
    if (!globleStates.isLandingPage) {
      await getCurrencyWhiteList();
      await getGames();
    }
    appHelper.renewResourceExpiryTime();
    // Becasue it can cause error when trying to reorder to before renewResourceExpiryTime;
    // That's why we put it in to two difference if (!globleStates.isLandingPage)
    //
    // }
    if (!globleStates.isLandingPage) {
      storeCashAgentValue();
      storeReferralValue();
      useLiveChat();
    }
  };
  return {
    initClientSideConfig,
  };
}
