import _ from "lodash";
import { ILanguage } from "~~/models/common/language";
import {
  GetThemePropertiesRequestDto,
  IThemePropertiesFromApi,
  ICompanyThemeOption,
} from "~~/models/companyThemePropertiesRespone";
import EnumApiErrorCode from "~~/models/enums/enumApiErrorCode";
import EnumCountryCodeLocale, { EnumIsoToShortCountryCode } from "~~/models/enums/enumCountryCodeLocale";
import apis from "~~/utils/apis";
import commonHelper from "../../utils/commonHelper";
import { ILobbySettingsText } from "~~/models/customizeTheme";

export default function useInitServerSideConfigSetting(globleStates: any) {
  let isPerushopEnabled: boolean = false;
  let isDesktopVersion: boolean = false;

  const getSelectedTheme = async (): Promise<ICompanyThemeOption | undefined> => {
    let selectedTheme: ICompanyThemeOption;
    try {
      const response = await apis.getCompanyThemeOptions(globleStates.stateWebIdByDomian);
      if (response.ErrorCode === EnumApiErrorCode.success) {
        const hostname = commonHelper.getDomain();
        const isOptionSelected = (option: ICompanyThemeOption, themeName: string, isMobile: boolean): boolean =>
          option.ThemeName === themeName &&
          option.IsSelected &&
          option.IsMobileTheme === isMobile &&
          _.last(option.Domain?.toLowerCase()?.split("://")) === hostname;

          globleStates.isLandingPage = response.Data.ThemeOptions.find((option) => {
            return isOptionSelected(option, "Landing_Desktop", false) || isOptionSelected(option, "Landing_Mobile", true);
          })?.IsSelected ?? false;
          useState("isLandingPage").value = globleStates.isLandingPage;

        // Set for landing page only, it will be removed in future
        if (globleStates.isLandingPage) {
          globleStates.locale = "vi_VN";
        }
        
        const isSelectedGlobalDesktop = response.Data.ThemeOptions.find(
          (option) => isOptionSelected(option, "Global_Desktop", false)
        );
        const isSelectedGlobalMobile = response.Data.ThemeOptions.find(
          (option) => isOptionSelected(option, "Global_Mobile", true)
        );
        return isSelectedGlobalMobile && isSelectedGlobalDesktop;
      }
      return selectedTheme;
    } catch (error) {
      console.log(error);
    }
  }
  const getThemeProperties = async () => {
    const selectedTheme = await getSelectedTheme();
    
    isDesktopVersion = globleStates.isDesktop || (isPerushopEnabled && (selectedTheme?.IsSelected ?? false));
    const requestParams: GetThemePropertiesRequestDto = {
      IsMobileTheme: !isDesktopVersion,
      Domain: globleStates.domain,
      IsAppTheme: false,
      Language: globleStates.locale,
    };
    const response = await apis.getThemeProperties(
      requestParams,
      globleStates.stateWebIdByDomian,
      globleStates.themePropertiesCdn
    );
    if (response.ErrorCode === EnumApiErrorCode.success) {
      const themeName: string | undefined = _.toLower(
        _.first(response.Data.ThemeProperties)?.ThemeName
      );
      globleStates.themeName = themeName;
      const named: string = _.first(_.split(themeName, "_", 1)) ?? "main";
      const themeDirection: string = _.includes(
        [
          "kr",
          !isDesktopVersion ? "bk8" : "",
          !isDesktopVersion ? "cool88" : "",
        ],
        named
      )
        ? "main"
        : named;
      if (themeName?.includes("global")) {
        globleStates.stateThemeName = "global";
      } else {
        globleStates.stateThemeName = themeDirection;
      }
      globleStates.stateDataThemeProperties = response.Data;
      globleStates.homepage =
        globleStates.stateDataThemeProperties?.ThemeProperties?.find(
          (p: IThemePropertiesFromApi) => p.HtmlId === "homepage"
        );
      const componentTypeProperties =
        globleStates.stateDataThemeProperties?.ThemeProperties?.filter(
          (item: IThemePropertiesFromApi) =>
            item.HtmlId.includes("component_type")
        );
      const isIndependentModule = componentTypeProperties.some(
        (item: IThemePropertiesFromApi) => {
          const text = JSON.parse(item.Text) as ILobbySettingsText;
          return text.SelectedStyle.includes("Independent_provider_type");
        }
      );
      const isUseFlexibleModule = globleStates.allCustomerSettings.some(
        (v: any) =>
          v.Id === "isUseFlexibleModule" && v.Value.toLowerCase() === "y"
      );
      globleStates.isIndependentModule =
        isIndependentModule && isUseFlexibleModule;
    }
  };
  const getWebIdByDomain = async () => {
    const response = await apis.getWebIdByDomainName(globleStates.domain);
    if (response.isSuccessful) {
      globleStates.stateWebIdByDomian = response.Data.WebId;
      globleStates.gtmIdInternal = response.Data.GTMId;
    }
  };
  const getIp = () => {
    const ips = globleStates.ips;
    if (!ips) return "127.0.0.1";
    return String(_.first(ips.split(",")))
      .replace(/(::ffff:)/g, "")
      .trim();
  };
  const getGeography = async () => {
    const ip = getIp();
    const response = await apis.getGeographyByIp(ip).then(async (data) => {
      if (data.isSuccessful) {
        return data;
      } else {
        const newReq = await apis.getGeography();
        return newReq;
      }
    });
    globleStates.stateGeography = response.Data;
  };
  const currentLocale = (): string => {
    const selected = _.find(globleStates.stateLanguages, {
      // @ts-ignore
      ISO: EnumCountryCodeLocale[
        _.toUpper(globleStates.stateGeography?.CountryCode ?? "")
      ],
    });
    const firstLangauge = _.first(globleStates.stateLanguages) as
      | ILanguage
      | undefined;
    return selected ? selected.ISO : firstLangauge ? firstLangauge.ISO : "";
  };
  const getLanguage = async () => {
    const response = await apis.getLanguageSettingBsi(globleStates.domain);
    if (response.ErrorCode === EnumApiErrorCode.success) {
      globleStates.stateLanguages = _.filter(
        response.Data.Language,
        (lang) => lang.Status
      ).map((lang) => {
        return {
          ...lang,
          ShortCountryCode: EnumIsoToShortCountryCode[lang.ISO]
        }
      });
      if (globleStates.locale === undefined) {
        globleStates.locale = currentLocale();
      }
    }
  };
  const getMenuTabInitial = async () => {
    const response = await apis.getMenuTab(
      globleStates.domain,
      globleStates.locale,
      globleStates.isAuth,
      globleStates.isAuth ? globleStates.token : ""
    );
    if (response.isSuccessful) {
      globleStates.stateTabMenu = response.Data.TabInfos;
    } else {
      globleStates.stateTabMenu = [];
    }
  };
  const getGameProviderInitial = async () => {
    const response = await apis.getGameProviderList(
      globleStates.domain,
      globleStates.currency ?? "",
      globleStates.locale,
      globleStates.currency ? globleStates.isAuth === false : true
    );
    if (response.isSuccessful) {
      if (!isDesktopVersion) {
        response.Data.Games = response.Data.Games.filter(
          (g) => g.GameProviderName !== "KingMaker"
        );
      }
      globleStates.gameProviders = response.Data;
    }
  };
  const themeNeedTabOrder = ['playful', 'bk8', 'sbo', 'main', 'cool88', 'global'];
  const tabName = ['Special Selection', 'Hot Games', 'New Games', 'Favorite', 'Game Type', 'Provider'];
  const getGameTabProviderListInitial = async () => {
    const isPlayful = globleStates.stateThemeName === "playful";
    const response = await apis.getGameTabProviderList(
      globleStates.domain,
      globleStates.currency ?? "",
      globleStates.locale,
      !isDesktopVersion
    );
    if (response.isSuccessful) {
      if (!isDesktopVersion) {
        response.Data.SeamlessProviderList =
          response.Data.SeamlessProviderList.filter(
            (p) => p.ProviderName !== "KingMaker"
          );
      }
      if(themeNeedTabOrder.includes(globleStates.stateThemeName)) {
        if (!isPlayful && !globleStates.isIndependentModule) {
          response.Data.SeamlessGameTabList =
            response.Data.SeamlessGameTabList.filter(
              (tab: string) => tab !== "All Games"
            );
        }
      } else {
        const filteredTabName = tabName.filter(item => response.Data.SeamlessGameTabList.includes(item));
        response.Data.SeamlessGameTabList = filteredTabName;
      }
      globleStates.gameTabProviders = response.Data;
    }
  };
  const checkIfUserIpIsWhiteListIp = async () => {
    const response = await apis.getWhiteListedIpEnabled(
      getIp(),
      globleStates.domain
    );
    if (response.isSuccessful) {
      globleStates.isIpWhitelistEnabled = true;
    } else {
      globleStates.isIpWhitelistEnabled = false;
    }
  };
  const getAllCurrencies = async () => {
    const response = await apis.getCurrencies(globleStates.domain);
    if (response.isSuccessful) {
      return response.Data.Currencies;
    } else return [];
  };
  const getAllCustomerSettings = async () => {
    const response = await apis.getAllCustomerSettings(globleStates.domain);
    if (response.isSuccessful) {
      globleStates.allCustomerSettings = response.Data.Results;
      isPerushopEnabled = featuresToggleHelper.isCustomerSettingEnabled(
        "IsPeruShopEnabled",
        globleStates.allCustomerSettings
      );
      useState(KEYS.IS_PERU_SHOP_ENABLED).value = isPerushopEnabled;
    }
  };
  const fetchWithRetry = async (callback: Function, isResetCount = true) => {
    try {
      if (isResetCount) {
        globleStates.errorRetryCount = 0;
      }
      await callback();
    } catch (error) {
      globleStates.errorRetryCount += 1;
      if (globleStates.errorRetryCount < 3) {
        await fetchWithRetry(callback, false);
      } else {
        // useSlackAlert(error.toString());
        throw createError({
          statusCode: 500,
          data: {
            isNeedReload: true,
          },
        });
      }
    }
  };
  const getVersion = async () => {
    await apis.getVersion().then((data) => {
      if (data.isSuccessful) {
        globleStates.version = data.Data.Version;
      }
    });
  };
  const fetchAllCurrency = async () => {
    globleStates.allCurrencies = await getAllCurrencies();
  };
  const initServerSideConfig = async () => {
    await fetchWithRetry(getVersion);
    await fetchWithRetry(getWebIdByDomain);
    await fetchWithRetry(getGeography);
    await fetchWithRetry(getLanguage);
    await fetchWithRetry(getAllCustomerSettings);
    await fetchWithRetry(getThemeProperties);
    if (!globleStates.isLandingPage) {
      await fetchWithRetry(getMenuTabInitial);
      await fetchWithRetry(getGameProviderInitial);
      await fetchWithRetry(getGameTabProviderListInitial);
    }
    await fetchWithRetry(checkIfUserIpIsWhiteListIp);
    if (!globleStates.isLandingPage) {
      await fetchWithRetry(fetchAllCurrency);
      globleStates.isWLAppEnabled = featuresToggleHelper.isCustomerSettingEnabled(
        "isWLAppEnabled",
        globleStates.allCustomerSettings
      );
      globleStates.isV2ReferralEnabled =
        featuresToggleHelper.isCustomerSettingEnabled(
          "IsV2ReferralEnabled",
          globleStates.allCustomerSettings
        );
      globleStates.isCockfightWhitelist = globleStates.allCustomerSettings.find(
        (setting: any) => setting.Id === "Sv388WhiteListUserName"
      )?.Value;
      globleStates.fingerPrintLibrary = globleStates.allCustomerSettings.find(
        (setting: any) => setting.Id === "FingerPrintLibrary"
      )?.Value;
      globleStates.isUpdateIsImageLess1MBEnabled =
        featuresToggleHelper.isCustomerSettingEnabled(
          "IsEnableSlipCompressToLess1Mb",
          globleStates.allCustomerSettings
        );
      if (globleStates.stateThemeName.includes("peru") || globleStates.stateThemeName.includes("global")) {
        globleStates.isPeruShopEnabled =
          featuresToggleHelper.isCustomerSettingEnabled(
            "IsPeruShopEnabled",
            globleStates.allCustomerSettings
          );
      }
    }
    const themeProp = globleStates.stateDataThemeProperties.ThemeProperties;
    const _color = themePropertyHelper.getSingleThemeProperty(
      themeProp,
      "theme_color"
    );
    if (
      globleStates.isPeruShopEnabled &&
      globleStates.stateThemeName.includes("global")
    ) {
      globleStates.lightColorConfig = `/colors/peru_green.css${
        globleStates.version ? "?v=" + globleStates.version : ""
      }`;
      globleStates.darkColorConfig = globleStates.lightColorConfig;
      globleStates.colorConfig = globleStates.darkColorConfig;
    } else if (globleStates.stateThemeName.includes("global")) {
      const themeColorSet = {
        lightThemeColor: themePropertyHelper.getSingleThemeProperty(
          themeProp,
          "theme_color_light"
        ),
        darkThemeColor: themePropertyHelper.getSingleThemeProperty(
          themeProp,
          "theme_color_dark"
        ),
      };
      globleStates.lightColorConfig = `/colors/${themeColorSet.lightThemeColor.Text}.css${globleStates.version ? "?v=" + globleStates.version : ""}`;
      globleStates.darkColorConfig = `/colors/${themeColorSet.darkThemeColor.Text}.css${globleStates.version ? "?v=" + globleStates.version : ""}`;
      globleStates.colorConfig =
        globleStates.selectedColorConfig === "Dark"
          ? globleStates.darkColorConfig
          : globleStates.lightColorConfig;
    } else {
      globleStates.colorConfig = `/colors/${_color.Text}.css${globleStates.version ? "?v=" + globleStates.version : ""
        }`;
    }

    if (_color.Text === "bk8_green" && isDesktopVersion) {
      globleStates.layout = `${isDesktopVersion ? "desktop" : "mobile"}-${
        _color.Text
      }`.replace(/_/g, "-");
    } else {
      globleStates.layout = `${isDesktopVersion ? "desktop" : "mobile"}-${
        globleStates.stateThemeName
      }`;
    }
    globleStates.themeDirectory = `${
      isDesktopVersion ? "desktop" : "mobile"
    }/${globleStates.stateThemeName}`;
  };

  return {
    initServerSideConfig,
  };
}
