import { computed } from "vue";
import _ from "lodash";
import { store } from "@/store";
import themePropertyHelper from "@/utils/themePropertyHelper";
declare global {
  // eslint-disable-next-line no-var
  var __lc: {
    license: string;
  };
}
export const useLiveChat = () => {
  const themeProperties = computed(
    () => store.state.companyThemePropertiesFromApi.ThemeProperties
  );
  const tawkId = themePropertyHelper.getSingleThemeProperty(
    themeProperties.value,
    "tawkid"
  );
  const whatsAppNumber = themePropertyHelper.getSingleThemeProperty(
    themeProperties.value,
    "whatsAppNumber"
  );
  const respondId = themePropertyHelper.getSingleThemeProperty(
    themeProperties.value,
    "respondid"
  );
  const livechatId = themePropertyHelper.getSingleThemeProperty(
    themeProperties.value,
    "livechatid"
  );
  const loadTawk = () => {
    const s1 = document.createElement("script");
    const s0 = document.getElementsByTagName("script")[0];
    s1.async = true;
    s1.src = `https://embed.tawk.to/${tawkId.Text}`;
    s1.charset = "UTF-8";
    s1.setAttribute("crossorigin", "*");
    s0.parentNode?.insertBefore(s1, s0);
  };
  const loadWhatApp = () => {
    const options = {
      whatsapp: whatsAppNumber.Text,
      call_to_action: "Chat VIA WHATSAPP!",
      position: "left",
    };
    const proto = document.location.protocol;
    const host = "getbutton.io";
    const url = `${proto}//static.${host}`;
    const s = document.createElement("script");
    s.type = "text/javascript";
    s.async = true;
    s.src = `${url}/widget-send-button/js/init.js`;
    s.onload = function () {
      // @ts-ignore
      WhWidgetSendButton.init(host, proto, options);
    };
    const x = document.getElementsByTagName("script")[0];
    x.parentNode?.insertBefore(s, x);
  };
  const loadRespondId = () => {
    const s1 = document.createElement("script");
    const s0 = document.getElementsByTagName("script")[0];
    const type = _.includes(respondId.Text, "-") ? "wId" : "cId";
    const prefix = type === "cId" ? "webchat/widget" : "widget";
    if (type === "cId") {
      s1.setAttribute("id", "respondio__widget");
    } else {
      s1.setAttribute("id", "respondio__growth_tool");
    }
    s1.async = true;
    s1.src = `https://cdn.respond.io/${prefix}/widget.js?${type}=${respondId.Text}`;
    s1.charset = "UTF-8";
    s1.setAttribute("crossorigin", "*");
    s0.parentNode?.insertBefore(s1, s0);
  };
  const loadLiveChatId = () => {
    const scriptEl = document.createElement("script");
    const body = document.querySelector("body");
    window.__lc = window.__lc || {};
    window.__lc.license = livechatId.Text;
    scriptEl.innerHTML = `!function(t,n,c){function e(t){return i._h?i._h.apply(null,t):i._q.push(t)}var i={_q:[],_h:null,_v:"2.0",on:function(){e(["on",c.call(arguments)])},once:function(){e(["once",c.call(arguments)])},off:function(){e(["off",c.call(arguments)])},get:function(){if(!i._h)throw Error("[LiveChatWidget] You can't use getters before load.");return e(["get",c.call(arguments)])},call:function(){e(["call",c.call(arguments)])},init:function(){var t=n.createElement("script");t.async=!0,t.type="text/javascript",t.src="https://cdn.livechatinc.com/tracking.js",n.head.appendChild(t)}};t.__lc.asyncInit||i.init(),t.LiveChatWidget=t.LiveChatWidget||i}(window,document,[].slice);`; // eslint-disable-line
    body?.appendChild(scriptEl);
  };

  if (
    tawkId &&
    !tawkId.IsHidden &&
    tawkId.Text !== "" &&
    tawkId.Text !== undefined
  ) {
    loadTawk();
  }
  if (
    whatsAppNumber &&
    !whatsAppNumber.IsHidden &&
    whatsAppNumber.Text !== ""
  ) {
    loadWhatApp();
  }

  if (
    respondId.Text !== "" &&
    !respondId.IsHidden &&
    respondId.Text !== undefined
  ) {
    loadRespondId();
  }
  if (
    !livechatId.IsHidden &&
    livechatId.Text !== "" &&
    livechatId.Text !== undefined
  ) {
    loadLiveChatId();
  }
};
