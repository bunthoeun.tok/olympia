import { store } from "@/store";

export const useIsDesktop = () => {
  try {
    if (store.state.isPerushopEnabled && store.state.selectedThemeName.includes('global')) {
      return true;
    }
    return useDevice().isDesktop;
  } catch (error) {
    return useDevice().isDesktop;
  }
}