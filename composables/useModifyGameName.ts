import { computed } from "vue";
import { IGame } from "../models/IGetPlayableGamesResponse";
import EnumProductType from "../models/enums/enumProductType";
import { store } from "../store";

export default function useModifyGameName(game: IGame, isGameCategory = false) {
  const { $t } = useNuxtApp();
  const isChina = computed(() => store.state.selectedThemeName === "china");
  const isShouldDisplayGameName =
    [
      EnumProductType.LiveCasino,
      EnumProductType.Sports,
      EnumProductType.Games,
      EnumProductType.VirtualSports,
    ].includes(game.Category) && game.GameProviderId === -1;
  if (isShouldDisplayGameName || (isChina.value && !isGameCategory)) {
    return $t(game.GameName);
  }
  return game.GameProviderDisplayName;
}
