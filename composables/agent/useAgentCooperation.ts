import { ref, reactive, Ref, computed } from "vue";
import type { FormInstance } from "element-plus";
import useCurrencyWhitelist from "../useCurrencyWhitelist";
import { store } from "@/store";
import apis from "@/utils/apis";
import EnumMessageType from "@/models/enums/enumMessageType";
import notificationHelper from "@/utils/elementUiHelpers/notificationHelper";
import formHelper from "@/utils/elementUiHelpers/formHelper";
import { ICurrencies } from "@/models/login";
import { IFormAgentCooperation, IInfo } from "@/models/agentCooperation";

const router = useRouter();
interface IAgentCooperation {
  ruleFormRef: Ref<FormInstance | undefined>;
  agentRules: any;
  submitForm: (formEl: FormInstance) => void;
  toggleAgentCooperation: (formEl: FormInstance) => void;
  showAgentCooperation: Ref<boolean>;
  instanceMessages: any;
  whiteListCurrencies: Ref<Array<ICurrencies>>;
  formAgentCooperation: any;
  createAgentCooperation: () => void;
  isLoading: Ref<boolean>;
}

export function useAgentCooperation(): IAgentCooperation {
  const { $t } = useNuxtApp();
  const ruleFormRef = ref<FormInstance>();
  const { whiteListCurrencies } = useCurrencyWhitelist();
  const isLoading = ref(false);
  const parameterObject = {
    Username: "",
    AgentName: "",
    FirstName: "",
    LastName: "",
    Email: "",
    Currency: "",
    SocialType: "",
    SocialUsername: "",
    Phone: "",
    Mobile: "",
    Password: "",
    PasswordConfirmation: "",
  };
  const formAgentCooperation = reactive({ ...parameterObject });
  const toggleAgentCooperation = () => {
    Object.assign(formAgentCooperation, parameterObject);
    store.commit(
      "updateAgentCooperationDialog",
      !store.state.showAgentCooperationDialog
    );
  };
  const showAgentCooperation = computed(
    () => store.state.showAgentCooperationDialog
  );
  const instanceMessages = ["Telegram", "WeChat", "Line", "Skype", "WhatsApp"];
  const customUsername = () => {
    if (!/[A-Za-z]/.test(formAgentCooperation.Username)) {
      return $t("username_must_be_contain_letter");
    }
    if (!/^(?=.{6,20}$)\D*\d/.test(formAgentCooperation.Username)) {
      return $t(
        "must_be_in_6_20_characters_Use_only_letters_a_z_and_numbers_0_9__username_is_not_case_sensitive"
      );
    }
    if (!/^\S*$/.test(formAgentCooperation.Username)) {
      return $t("username_must_not_contain_with_space_or_symbol");
    }
    return "";
  };
  const customPassword = () => {
    if (
      formAgentCooperation.Password.length < 6 ||
      formAgentCooperation.Password.length > 20
    ) {
      return $t("passwordlenght");
    }
    if (!/^[A-Za-z0-9]+$/.test(formAgentCooperation.Password)) {
      return $t("password_only_letter_number");
    }
    if (!/[A-Za-z]/.test(formAgentCooperation.Password)) {
      return $t("passwordlater");
    }
    if (!/\d/.test(formAgentCooperation.Password)) {
      return $t("passwordnumber");
    }
    return "";
  };
  const customConfirmPassword = () => {
    if (
      formAgentCooperation.PasswordConfirmation !==
      formAgentCooperation.Password
    ) {
      return $t("password_does_not_match");
    }
    return "";
  };
  const customEmail = () => {
    if (!formAgentCooperation.Email) {
      return "Valid email address is required for deposit and withdrawal verification purposes.";
    }
    if (!/^\S+@\S+\.\S+$/.test(formAgentCooperation.Email)) {
      return $t("must_be_valid_email");
    }
    return "";
  };
  const customCurrency = () => {
    if (!formAgentCooperation.Currency) {
      return $t("please_select_currency");
    }
    return "";
  };
  const customPhone = () => {
    if (!/\d{8}/.test(formAgentCooperation.Phone)) {
      return $t(
        "valid_mobile_number_is_required_for_deposit_and_withdrawal_verification_purposes"
      );
    }
    return "";
  };
  const rules = {
    Username: { customRule: customUsername, required: true },
    AgentName: { required: false },
    FirstName: { required: true },
    LastName: { required: true },
    Email: { customRule: customEmail, required: true },
    Currency: {
      customRule: customCurrency,
      required: true,
      isTriggerByChange: true,
    },
    SocialType: { required: true, isTriggerByChange: true },
    SocialUsername: { required: true },
    Phone: { customRule: customPhone, required: true },
    Mobile: { required: false },
    Password: { customRule: customPassword, required: true },
    PasswordConfirmation: { customRule: customConfirmPassword, required: true },
  };
  const agentRules = formHelper.getRules(rules, formAgentCooperation);
  const createAgentCooperation = async () => {
    isLoading.value = true;
    const detailsAgent = ref<IInfo[]>([]);
    Object.keys(formAgentCooperation).forEach((item) => {
      if (item !== "Username" && item !== "AgentName") {
        detailsAgent.value.push({
          InfoId: item,
          InfoValue:
            formAgentCooperation[item as keyof typeof formAgentCooperation],
        });
      }
    });
    const request: IFormAgentCooperation = {
      Username: formAgentCooperation.Username,
      AgentName: formAgentCooperation.AgentName,
      Details: detailsAgent.value,
      Domain: commonHelper.getDomain(),
    };
    const response = await apis.createAgentCooperation(request);
    if (response.isSuccessful) {
      notificationHelper.notification(
        response.Message,
        EnumMessageType.Success
      );
      router.push("/");
      store.commit("updateSidebar", false);
      setTimeout(() => toggleAgentCooperation(), 800);
    } else {
      notificationHelper.notification(
        response.ErrorMessageForDisplay,
        EnumMessageType.Error
      );
    }
    detailsAgent.value = [];
    isLoading.value = false;
  };
  const submitForm = formHelper.getSubmitFunction(createAgentCooperation);
  return {
    ruleFormRef,
    submitForm,
    instanceMessages,
    whiteListCurrencies,
    formAgentCooperation,
    agentRules,
    createAgentCooperation,
    isLoading,
    toggleAgentCooperation,
    showAgentCooperation,
  };
}
