const useTest = () => {
  const testingFunction = (value: string) => `Hello ${value}`
  return { testingFunction };
};
export default useTest