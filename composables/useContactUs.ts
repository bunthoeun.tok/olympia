import { ThemePropertiesFromApiDto } from "~~/models/companyThemePropertiesRespone";
import { store } from "~~/store";
import commonHelper from "../utils/commonHelper";

export const useContactUs = () => {
  const defaultContact = [
    "contact_us_facebook",
    "contact_us_instagram",
    "contact_us_livechat",
    "contact_us_phone",
    "contact_us_skype",
    "contact_us_whatapp",
    "contact_us_whatapp",
    "contact_us_youtube",
  ];

  const { allowCompany } = commonHelper;

  const themeProperties =
    store.state.companyThemePropertiesFromApi.ThemeProperties;
  const SPLITTER = "[;]";

  const contactUsAvailable = themePropertyHelper
    .getMultipleThemesProperties(themeProperties, "contact_us_")
    .filter((item) => !item.HtmlId.includes("_qr_code"));

  const contactUsAvailableWithQr =
    themePropertyHelper.getAllMultipleThemesProperties(
      themeProperties,
      "contact_us_"
    );

  const contactQrCodePaths = computed(() => {
    const otherContact = themePropertyHelper.getSingleThemeProperty(
      themeProperties,
      `contact_us_other`
    );
    const otherContactQr = themePropertyHelper.getSingleThemeProperty(
      themeProperties,
      `contact_us_other_qr_code`
    );

    const { ResourceKey } = otherContact;
    const QrCodes = otherContactQr.Path.split(SPLITTER);
    const newArr = [];

    ResourceKey?.split(SPLITTER).forEach((key, index) => {
      if (defaultContact.includes(key)) {
        const contact = contactUsAvailableWithQr.find((c) => c.HtmlId === key);
        if (!contact?.IsHidden) {
          const qr = contactUsAvailableWithQr.find((c) => c.HtmlId === `${key}_qr_code`);
          if (qr) newArr.push(getImagePath(qr, store.state.webId, qr?.Path));
        }
      } else {
        newArr.push(getImagePath(otherContactQr, store.state.webId, QrCodes[index]));
      }
    });

    return newArr;
  });

  const getContactList = computed(() => {
    const mainContact = contactUsAvailable.find(
      (v) => v.HtmlId === "contact_us_other"
    );
    let mainContactText: string[] = [];
    let mainContactHyperlink: string[] = [];
    let mainContactPath: string[] = [];
    let mainContactResouseKey: string[] = [];
    if (mainContact) {
      mainContactText = mainContact.Text.split(SPLITTER);
      mainContactHyperlink = mainContact.HyperLink.split(SPLITTER);
      mainContactPath = mainContact.Path.split(SPLITTER);
      mainContactResouseKey = mainContact.ResourceKey.split(SPLITTER);
    }
    const contactByMain = mainContactText
      .filter((Text) => !!Text)
      .reduce<ThemePropertiesFromApiDto[]>((acc, Text, index) => {
        if (defaultContact.includes(Text)) {
          const contact = contactUsAvailable.find((c) => c.HtmlId === Text);
          if (contact) {
            acc.push(
              new ThemePropertiesFromApiDto({
                ...contact,
                ResourceKey: `${index + 1}`,
                ContactOrder: index + 1,
              })
            );
          }
        } else {
          acc.push(
            new ThemePropertiesFromApiDto({
              ...mainContact!,
              Text,
              IsHidden: mainContactResouseKey[index] === "0",
              Path: mainContactPath[index] ?? "other.png",
              HyperLink: mainContactHyperlink[index] ?? "",
              ContactOrder: index + 1,
            })
          );
        }
        return acc;
      }, [])
      .filter((item) => !item.IsHidden);
    if (!allowCompany.value) {
      return contactUsAvailable.filter(
        (item) => !item.HtmlId.includes("other")
      );
    }
    return contactByMain;
  });

  const getImagePath = (
    group: ThemePropertiesFromApiDto,
    webId: Number = 0,
    path = ""
  ): string => {
    const cdn = commonHelper.getThemePropertiesCdn();
    const version = store.state.imageVersion;
    const isDefaultFolderName = group.IsDefault
      ? "default"
      : `customize/WebId-${webId}`;
    return `${cdn}/storage/${isDefaultFolderName}/${group.ThemeName?.toLowerCase()}/${path}?v=${version}`;
  };

  const gettingQrCodeImagePath = (key: string, index: number) => {
    if (key === "contact_us_other") {
      const removePrefix = themePropertyHelper.getSingleThemeProperty(
        themeProperties,
        `${key}_qr_code`
      );
      const paths = removePrefix.Path.split(SPLITTER);
      const splitted = paths[index];
      if (splitted.length >= 2) {
        return splitted[0] + splitted[1];
      }
      return getImagePath(removePrefix, store.state.webId, splitted);
    }
    const removePrefix = themePropertyHelper
      .getSingleThemeProperty(themeProperties, `${key}_qr_code`)
      .ImagePath.split("v2_")
      .map((item) => item);
    return removePrefix[0] + removePrefix[1];
  };

  const hasOtherContactus = computed(() =>
    contactUsAvailable.some((item) => item.HtmlId.includes("_other"))
  );

  const isHidden = (key: string) =>
    themePropertyHelper.getSingleThemeProperty(
      themeProperties,
      `${key}_qr_code`
    ).IsHidden;

  return {
    getContactList,
    gettingQrCodeImagePath,
    isHidden,
    hasOtherContactus,
    allowCompany,
    contactQrCodePaths,
  };
};
