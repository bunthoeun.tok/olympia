import { Ref, ComputedRef, ref, computed, watch } from "vue";
import { RouteRecordName } from "vue-router";
import _ from "lodash";
import {
  ITransaction,
  IProceedTransactionRequest,
  GetTransactionHistoryRequest,
} from "@/models/getTransactionHistoryResponse";
import TransactionType from "@/models/enums/enumTransactionType";
import apis from "@/utils/apis";
import notificationHelper from "@/utils/elementUiHelpers/notificationHelper";
import EnumMessageType from "@/models/enums/enumMessageType";
import cookieHelper from "~~/utils/cookieHelper";
import EnumMutation from "~~/models/enums/enumMutation";
import { store } from "~~/store";

interface ITransactionHistoryMainContent {
  transactions: Ref<ITransaction[]>;
  transactionType: ComputedRef<RouteRecordName>;
  isDataLoading: Ref<boolean>;
  cancelTransaction: (transaction: ITransaction) => Promise<void>;
  getTransactionHistory: () => Promise<void>;
  isProcessing: Ref<boolean>;
}
export const useTransactionHistoryMainContent =
  (): ITransactionHistoryMainContent => {
    const { EnumKwaiMeta, kwaiMetaTrigger } = featuresToggleHelper;

    const route = useRoute();
    const transactions = ref<ITransaction[]>([]);
    const isDataLoading = ref(true);
    const isUCPopup = computed(() => route.fullPath.includes("uc=/"));
    const paymentAmount = computed(() => Number(route.query.rfpa));
    
    const transactionType = computed(() => {
      if (isUCPopup.value) return String(route.query.uc);
      return route.name ?? "transaction-history";
    });

    const getTransactionHistory = async () => {
      isDataLoading.value = true;
      const params = new GetTransactionHistoryRequest();
      params.TransType = TransactionType.Withdrawal;
      params.IsGetAll = true;
      store.commit(EnumMutation.updateIsStatementLoading, true);

      params.Page = 1;
      params.RowCountPerPage = 50;
      if (transactionType.value.toString().endsWith("deposit")) {
        params.TransType = TransactionType.Deposit;
        const response = await apis.getTransactionHistory(params);
        transactions.value = _.filter(response.Data.Transactions, {
          IsPayment: false,
        });
      } else {
        const response = await apis.getTransactionHistory(params);
        transactions.value = response.Data.Transactions;
      }
      const response = await apis.getTransactionHistory(params);
      transactions.value = _.orderBy(
        response.Data.Transactions,
        (data) => new Date(data.RequestedTime),
        ["desc"]
      );
      isDataLoading.value = false;
      store.commit(EnumMutation.updateIsStatementLoading, false);
    };
    getTransactionHistory();
    // watch(transactionType.va, async (transaction) => {
    //   if (transaction !== "transaction-history-winlose") {
    //     await getTransactionHistory();
    //   }
    // });
    const isProcessing = ref(false);
    const cancelTransaction = async (transaction: ITransaction) => {
      isProcessing.value = true;
      const params = {
        IsPayment: transaction.IsPayment,
        ModifiedBy: transaction.ModifiedBy,
        Amount: transaction.RequestedAmount,
        FromStatus: "waiting",
        ToStatus: "cancelled",
        Remark: "",
        CompanyBankId: 0,
        TransType: transaction.Type,
        TransactionNo: transaction.TransactionNo,
        BankCode: "",
        PaymentProviderId: 0,
        PaymentProviderName: "",
        AddWithdrawalLimitAmount: 0,
        Currency: transaction.Currency,
      } as IProceedTransactionRequest;
      const response = await apis.proceedTransaction(params);
      if (response.isSuccessful) {
        notificationHelper.notification(
          response.Message,
          EnumMessageType.Success
        );
        await getTransactionHistory();
      } else {
        notificationHelper.notification(
          response.Message,
          EnumMessageType.Error
        );
      }
      isProcessing.value = false;
    };

    const triggerKwaiManage = (amount: number) => {
      const currency = cookieHelper.getCookie("currency");
      kwaiMetaTrigger(EnumKwaiMeta.DEPOSIT, amount, currency);
    };

    onBeforeMount(() => {
      if (paymentAmount.value > 0 && String(route.name) === 'transaction-history-deposit') {
        triggerKwaiManage(paymentAmount.value);
        useRouter().replace(route.fullPath.replace(`rfpa=${paymentAmount.value}`, ''));
      }
    })

    return {
      transactions,
      transactionType,
      isDataLoading,
      cancelTransaction,
      getTransactionHistory,
      isProcessing,
    };
  };
