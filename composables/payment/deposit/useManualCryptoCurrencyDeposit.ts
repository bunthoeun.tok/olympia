/* eslint-disable */
import { ComputedRef, Ref, computed, ref, reactive } from "vue";
import _ from "lodash";
import { FormInstance } from "element-plus";
import {
  ICompanyCryptocurrencyOptionsResponse,
  IPlayerCryptoCurrencyOptionsResponse,
  IRequestManualCryptoCurrencyDeposit,
  IGetManualCryptoCurrencyTransactionResponse,
} from "@/models/getManualCryptoCurrencyTransactionResponse";
import { ProfileDto } from "@/models/getProfileResponse";
import commonHelper from "@/utils/commonHelper";
import formHelper from "@/utils/elementUiHelpers/formHelper";
import textHelper from "@/utils/formatHelpers/textFormatHelper";
import apis from "@/utils/apis";
import notificationHelper from "@/utils/elementUiHelpers/notificationHelper";
import EnumMessageType from "@/models/enums/enumMessageType";
import EnumApiErrorCode from "@/models/enums/enumApiErrorCode";
import numberFormatHelper from "@/utils/formatHelpers/numberFormatHelper";
import featuresToggleHelper from "@/utils/featuresToggleHelper";

interface IManualCryptoCurrencyDeposit {
  checkedCurrency: ComputedRef<boolean>;
  playerCryptocurrencyOptions: Array<IPlayerCryptoCurrencyOptionsResponse>;
  companyCryptocurrencyOptions: Array<ICompanyCryptocurrencyOptionsResponse>;
  manualCryptoDepositMin: ComputedRef<number>;
  manualCryptoDepositMax: ComputedRef<number>;
  manualCryptoCurrencyDepositFormRef: Ref<any>;
  manualCryptoCurrencyDepositFormData: any;
  manualCryptoCurrencyDepositRules: any;
  submitManualCryptoCurrencyDeposit: (formEl: FormInstance) => void;
  isProccesing: Ref<boolean>;
  isUploadImage: Ref<boolean>;
  companyFlowSettings: any;
  amountPlaceholder: ComputedRef<string>;
}
export const useManualCryptoCurrencyDeposit = (
  manualCryptoCurrency: Ref<IGetManualCryptoCurrencyTransactionResponse>,
  profile: Ref<ProfileDto>
): IManualCryptoCurrencyDeposit => {
  const { $t } = useNuxtApp();
  const router = useRouter();
  const {
    CompanyCryptocurrencyOptions,
    PlayerCryptocurrencyOptions,
    AvailablePropertyList,
    IsDecimalAllow,
  } = manualCryptoCurrency.value;
  const { slipUpload, isdisabled } = commonHelper;
  const companyFlowSettings = {
    slipUploadSetting: _.find(AvailablePropertyList, {
      PropertyName: "SlipUpload",
    }),
    depositTimeSetting: _.find(AvailablePropertyList, {
      PropertyName: "DepositTime",
    }),
  };
  const userCurrency = computed(() => _.lowerCase(profile.value.Currency));
  const walletType = ref(PlayerCryptocurrencyOptions[0].WalletType);
  const amount = ref();

  const companyBankId = ref(CompanyCryptocurrencyOptions[0].Id);
  const companyBankSelected = computed(() =>
    CompanyCryptocurrencyOptions.find(
      (company) => company.Id === companyBankId.value
    )
  );
  const cryptoDepositAddress = computed(
    () => companyBankSelected.value?.WalletAddress
  );
  const checkedCurrency = computed(
    () => userCurrency.value === "idr" || userCurrency.value === "vnd"
  );
  const isSlipUploadRequired = commonHelper.isRequiredField(
    companyFlowSettings,
    "slipUploadSetting"
  );
  const isDepositTimeRequired = commonHelper.isRequiredField(
    companyFlowSettings,
    "depositTimeSetting"
  );
  const validateAmount = computed(() => amount.value);
  const manualCryptoDepositMin = computed(() =>
    companyBankSelected.value !== undefined
      ? companyBankSelected.value.DepositMin
      : 0
  );
  const manualCryptoDepositMax = computed(() =>
    companyBankSelected.value !== undefined
      ? companyBankSelected.value.DepositMax
      : 0
  );
  const manualCryptoCurrencyDepositFormRef = ref<FormInstance>();
  const manualCryptoCurrencyDepositFormData = reactive({
    walletType,
    playerWalletAddress: "",
    companyBankId,
    amount,
    depositTime: "",
    slipUpload: computed(() => slipUpload.value[0]?.name ?? ""),
    cryptoDepositAddress,
  });
  const checkAmount = () => {
    if (
      validateAmount.value < manualCryptoDepositMin.value ||
      validateAmount.value > manualCryptoDepositMax.value
    ) {
      return `${$t("min")}: ${numberFormatHelper.addAccountingFormat(Number(manualCryptoDepositMin.value), 0, false)} ${$t("max")}: ${
        numberFormatHelper.addAccountingFormat(Number(manualCryptoDepositMax.value), 0, false)
      }`;
    }
    if (
      !IsDecimalAllow &&
      /\./.test(`${manualCryptoCurrencyDepositFormData.amount}`)
    ) {
      return $t("decimal_point_not_allow");
    }
    return "";
  };
  const cryptoCurrencyRules = {
    walletType: { required: true },
    playerWalletAddress: { required: true },
    amount: { customRule: checkAmount, required: true },
    depositTime: { required: isDepositTimeRequired, isTriggerByChange: true },
    slipUpload: { required: isSlipUploadRequired },
  };
  const route = useRoute();
  const manualCryptoCurrencyDepositRules = formHelper.getRules(
    cryptoCurrencyRules,
    manualCryptoCurrencyDepositFormData
  );
  const isProccesing = ref(false);
  const isUploadImage = ref(isdisabled);
  const amountPlaceholder = computed(
    () =>
      `${$t("min")}: ${numberFormatHelper.addAccountingFormat(Number(manualCryptoDepositMin.value), 0, false)}  ${$t("max")}: ${
        manualCryptoDepositMax.value === 0 ? "--" : numberFormatHelper.addAccountingFormat(Number(manualCryptoDepositMax.value), 0, false)
      }`
  );
  const { kwaiMetaTrigger, EnumKwaiMeta } = featuresToggleHelper;
  const proceedManualCryptoCurrencyDeposit = async () => {
    isProccesing.value = true;
    const requestParameters = {
      WalletType: manualCryptoCurrencyDepositFormData.walletType,
      PlayerWalletAddress:
        manualCryptoCurrencyDepositFormData.playerWalletAddress,
      CompanyBankId: companyBankId.value,
      Amount: manualCryptoCurrencyDepositFormData.amount,
      DepositTime: textHelper.dateStringTo12HourClock(
        manualCryptoCurrencyDepositFormData.depositTime
      ),
      SlipImage: manualCryptoCurrencyDepositFormData.slipUpload,
      Domain: commonHelper.getDomain(),
      OnlineId: Number(localStorage.getItem("onlineId") ?? 0),
    } as IRequestManualCryptoCurrencyDeposit;
    if (slipUpload.value.length > 0) {
      const uploaded = await apis.uploadFile(
        slipUpload.value,
        "theme/SlipImages"
      );
      if (uploaded.ErrorCode !== EnumApiErrorCode.success) {
        notificationHelper.notification(
          "Failed to Upload Slip Image",
          EnumMessageType.Error
        );
      }
    }
    const response = await apis.proceedManualCryptoCurrencyDeposit(
      requestParameters
    );
    if (response.isSuccessful) {
      notificationHelper.notification(
        response.ErrorMessageForDisplay,
        EnumMessageType.Success
      );
      // Get the player currency
      const currency = cookieHelper.getCookie("currency");
      kwaiMetaTrigger(EnumKwaiMeta.DEPOSIT, requestParameters.Amount, currency);
      const { isSubmitSuccess } = useCloseTelegramWebApp();
      isSubmitSuccess.value = true;
      setTimeout(() => {
        if (route.matched[0].name === "deposit") {
          router.push({
            path: "/transaction-history/deposit",
          });
        }
      }, 1000);
    } else {
      notificationHelper.notification(
        response.ErrorMessageForDisplay,
        EnumMessageType.Error
      );
    }
    isProccesing.value = false;
  };
  const submitManualCryptoCurrencyDeposit = formHelper.getSubmitFunction(
    proceedManualCryptoCurrencyDeposit
  );
  return {
    checkedCurrency,
    playerCryptocurrencyOptions: PlayerCryptocurrencyOptions,
    companyCryptocurrencyOptions: CompanyCryptocurrencyOptions,
    manualCryptoDepositMin,
    manualCryptoDepositMax,
    manualCryptoCurrencyDepositFormRef,
    manualCryptoCurrencyDepositFormData,
    manualCryptoCurrencyDepositRules,
    submitManualCryptoCurrencyDeposit,
    isProccesing,
    isUploadImage,
    companyFlowSettings,
    amountPlaceholder,
  };
};
