import { computed, ref, Ref } from "vue";
import _ from "lodash";
import { FormInstance } from "element-plus";
import { ProfileDto } from "@/models/getProfileResponse";
import { IGetTransactionBankListResponse } from "@/models/getTransactionBankListResponse";
import textHelper from "@/utils/formatHelpers/textFormatHelper";
import commonHelper from "@/utils/commonHelper";
import notificationHelper from "@/utils/elementUiHelpers/notificationHelper";
import EnumMessageType from "@/models/enums/enumMessageType";
import { IRequestAutoDepositBankTransfer } from "@/models/requestDepositBanks";
import formHelper, { IRule } from "@/utils/elementUiHelpers/formHelper";
import apis from "@/utils/apis";
import EnumApiErrorCode from "@/models/enums/enumApiErrorCode";
import {
  GetTransactionHistoryRequest,
  IProceedTransactionRequest,
  ITransaction,
} from "@/models/getTransactionHistoryResponse";
import numberFormatHelper from "@/utils/formatHelpers/numberFormatHelper";

export const useAutoDeposit = (
  autoDepositBanks: Ref<IGetTransactionBankListResponse>,
  profile: Ref<ProfileDto>
) => {
  const { $t } = useNuxtApp();
  const isloading = ref(false);
  const beforeSubmit = ref(true);
  const {
    CompanyBanks,
    PlayerBankOptions,
    IsDecimalAllow,
    PlayerBankListAfterFirstTimeDeposit,
  } = autoDepositBanks.value;
  const bankName = ref("");
  const bankAccountName = ref("");
  const accountNumber = ref("");
  const currency = ref(_.upperCase(profile.value.Currency));
  const transactionNo = ref("");
  const balance = ref("");
  const targetTime = ref(new Date());
  const remainingTime = ref(0);
  const transactionModel = ref<ITransaction>();
  const remainingDate = ref("");
  const autoDepositBank = computed(() =>
    CompanyBanks.filter((item) => item.IsAutoDeposit)
  );
  const playerBankDeposit = ref(PlayerBankOptions);
  const isFirstTimeDeposit = ref(
    PlayerBankListAfterFirstTimeDeposit.length === 0
  );
  const requestModel = ref({
    PlayerBankName: playerBankDeposit.value[0].Id,
    PlayerBankAccountName: isFirstTimeDeposit.value
      ? ""
      : PlayerBankListAfterFirstTimeDeposit[0].PlayerBankAccountName,
    PlayerBankAccountNumber: isFirstTimeDeposit.value
      ? ""
      : PlayerBankListAfterFirstTimeDeposit[0].PlayerBankAccountNumber,
    BankReferenceCode: "",
    BranchName: "",
    CustomizeBankName: "",
    CompanyBankId: autoDepositBank.value[0].Id,
    Amount: "",
    DepositTime: "",
    Domain: "",
    IsAutoDeposit: true,
  });
  const getHistoryDeposit = async () => {
    const date = new Date();
    const request: GetTransactionHistoryRequest = {
      TransType: "100",
      Status: "Waiting",
      RowCountPerPage: 100,
      Page: 1,
      StartDate: textHelper.dateStringTo24HourClock(String(date)),
      EndDate: textHelper.dateStringTo24HourClock(String(date)),
      IsGetAll: true,
    };
    const response = await apis.getTransactionHistory(request);
    if (response.isSuccessful) {
      if (response.Data.Transactions.length > 0) {
        remainingDate.value = response.Data.Transactions[0].RequestedTime;
        const [transaction] = response.Data.Transactions;
        transactionModel.value = transaction;
        beforeSubmit.value = false;
        bankName.value = transactionModel.value.CompanyBankName ?? "";
        bankAccountName.value = transactionModel.value.CompanyBankAccountName;
        accountNumber.value = transactionModel.value.CompanyBankAccountNumber;
        balance.value = numberFormatHelper.addAccountingFormat(
          _.includes(["IDR", "VND"], currency.value)
            ? transactionModel.value.RequestedAmount * 1000
            : transactionModel.value.RequestedAmount,
          3
        );
        transactionNo.value = transactionModel.value.TransactionNo;
        targetTime.value = new Date(
          textHelper.getDateTimeFromGMTMinusFour(remainingDate.value)
        );
      } else {
        beforeSubmit.value = true;
        remainingDate.value = "";
        requestModel.value.PlayerBankName = playerBankDeposit.value[0].Id;
        requestModel.value.Amount = "";
        requestModel.value.CompanyBankId = autoDepositBank.value[0].Id;
      }
    }
    isloading.value = false;
  };
  getHistoryDeposit();
  const calculateRemainingTime = () => {
    const now = new Date();
    remainingTime.value = targetTime.value.getTime() + 595000 - now.getTime();
  };
  setInterval(() => calculateRemainingTime(), 1000);
  setInterval(
    () => (remainingDate.value !== "" ? getHistoryDeposit() : ""),
    15000
  );

  const selectedCompanyBank = computed(() =>
    autoDepositBank.value.find(
      (item) => item.Id === requestModel.value.CompanyBankId
    )
  );
  const isNeedToMultiplyByThousand = computed(
    () =>
      profile.value.Currency.toLowerCase() === "idr" ||
      profile.value.Currency.toLowerCase() === "vnd"
  );
  const validateDepositMin = computed(
    () => selectedCompanyBank.value?.DepositMin ?? 0
  );
  const validateDepositMax = computed(
    () => selectedCompanyBank.value?.DepositMax ?? 0
  );
  const validateAmount = computed(() => Number(requestModel.value.Amount));
  const amountPlaceholder = computed(
    () =>
      `${$t("min")}: ${validateDepositMin.value} ${$t("max")}: ${
        validateDepositMax.value
      }`
  );
  const remainingTimeForDisplay = computed(() =>
    textHelper.convertMiliSecToTime(remainingTime.value)
  );
  const copyAccountNumber = () => {
    commonHelper.copyTextToClipBoard(accountNumber.value, false);
    notificationHelper.notification($t("copied!"), EnumMessageType.Info);
  };
  const copyExactBalance = () => {
    commonHelper.copyTextToClipBoard(balance.value, false);
    notificationHelper.notification(
      `${$t("exact")}: ${currency.value} ${balance.value}`,
      EnumMessageType.Info
    );
  };
  const checkAmount = () => {
    if (
      validateAmount.value < validateDepositMin.value ||
      validateAmount.value > validateDepositMax.value
    ) {
      return `${$t("min")}: ${validateDepositMin.value} ${$t("max")}: ${
        validateDepositMax.value
      }`;
    }
    if (!IsDecimalAllow && /\./.test(`${Number(requestModel.value.Amount)}`)) {
      return $t("decimal_point_not_allow");
    }
    return "";
  };
  const rule: Record<string, IRule> = {
    PlayerBankAccountName: { required: true },
    PlayerBankAccountNumber: { required: true },
    Amount: { required: true, customRule: checkAmount },
  };
  const formRule = formHelper.get2Rules(rule);
  const ruleFormRef = ref<FormInstance>();
  const performBankDeposit = async () => {
    isloading.value = true;
    const request: IRequestAutoDepositBankTransfer = {
      PlayerBankName: selectedCompanyBank.value?.BankName as string,
      PlayerBankAccountName: requestModel.value.PlayerBankAccountName,
      PlayerBankAccountNumber: requestModel.value.PlayerBankAccountNumber,
      BankReferenceCode: requestModel.value.BankReferenceCode,
      BranchName: requestModel.value.BranchName,
      CustomizeBankName: requestModel.value.CustomizeBankName,
      CompanyBankId: requestModel.value.CompanyBankId,
      Amount: requestModel.value.Amount,
      DepositTime: requestModel.value.DepositTime,
      Domain: commonHelper.getDomain(),
      IsAutoDeposit: true,
      OnlineId: Number(localStorage.getItem("onlineId") ?? 0),
    };
    const response = await apis.proceedAutoDeposit(request);
    if (response.isSuccessful) {
      beforeSubmit.value = false;
      getHistoryDeposit();
      const {isSubmitSuccess} = useCloseTelegramWebApp();
      isSubmitSuccess.value = true;
    } else {
      isloading.value = false;
    }
  };
  const onSubmitDeposit = formHelper.getSubmitFunction(performBankDeposit);
  const cancelTransaction = async () => {
    isloading.value = true;
    const params = {
      IsPayment: false,
      ModifiedBy: "",
      Amount: transactionModel.value?.BeforeBalance,
      FromStatus: "Waiting",
      ToStatus: "Cancelled",
      Remark: "",
      CompanyBankId: 0,
      TransType: "2100",
      TransactionNo: transactionNo.value,
      BankCode: "",
      PaymentProviderId: 0,
      PaymentProviderName: "",
      AddWithdrawalLimitAmount: 0,
      Currency: transactionModel.value?.Currency,
    } as IProceedTransactionRequest;
    const response = await apis.proceedTransaction(params);
    if (
      response.isSuccessful ||
      response.ErrorCode === EnumApiErrorCode.no_transaction_found
    ) {
      notificationHelper.notification(
        response.Message,
        EnumMessageType.Success
      );
      beforeSubmit.value = true;
    } else {
      notificationHelper.notification(response.Message, EnumMessageType.Error);
    }
    isloading.value = false;
  };
  return {
    bankName,
    bankAccountName,
    currency,
    balance,
    remainingTimeForDisplay,
    copyAccountNumber,
    copyExactBalance,
    beforeSubmit,
    requestModel,
    autoDepositBank,
    isNeedToMultiplyByThousand,
    formRule,
    ruleFormRef,
    onSubmitDeposit,
    amountPlaceholder,
    isloading,
    playerBankDeposit,
    cancelTransaction,
    isFirstTimeDeposit,
  };
};
export default useAutoDeposit;
