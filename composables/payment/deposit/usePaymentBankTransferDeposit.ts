/* eslint-disable */
import _ from "lodash";
import { ComputedRef, Ref, computed, reactive, ref, watch } from "vue";

import { FormInstance } from "element-plus";
import { IAvailableBanksForDeposit, IGetPaymentGatewayBanksResponse } from "@/models/getPaymentGatewayBanksRespone";
import { ProfileDto } from "@/models/getProfileResponse";
import formHelper from "@/utils/elementUiHelpers/formHelper";
import notificationHelper from "@/utils/elementUiHelpers/notificationHelper";
import EnumMessageType from "@/models/enums/enumMessageType";
import { IRequestDepositInternetBank } from "@/models/requestDepositBanks";
import { ThemePropertiesFromApiDto } from "~~/models/companyThemePropertiesRespone";
import apis from "@/utils/apis";
import commonHelper from "@/utils/commonHelper";
import numberFormatHelper from "@/utils/formatHelpers/numberFormatHelper";
import EnumThemePropertyKey from "~~/models/enums/enumThemePropertyKey";
import featuresToggleHelper from "@/utils/featuresToggleHelper";
import { store } from "@/store";

const { getAssetImage, getMultiplesOf100InRange } = commonHelper;
interface IPaymentBankTransferDeposit {
  bankRadios: any;
  selectedBank: Ref<string>;
  channelRadios: any;
  selectedChannel: any;
  isNotcryptoGroup: ComputedRef<boolean>;
  validateDepositMin: ComputedRef<number>;
  validateDepositMax: ComputedRef<number>;
  isNeedToMultiplyByThousand: ComputedRef<boolean>;
  ruleFormRef: Ref<any>;
  formData: any;
  getRule: any;
  cryptoCurrencyDepositFormData: any;
  cryptoCurrencyDepositFormRef: Ref<any>;
  cryptoCurrencyDepositRule: any;
  isProccesing: Ref<boolean>;
  submitPaymentDeposit: (formEl: FormInstance) => void;
  amountPlaceholder: ComputedRef<string>;
  cryptocurrencyNetwork: Ref<string>;
  cryptocurrencyNetworks: ComputedRef<Array<string> | undefined>;
  triggerImageError: (e: any) => void;
  paymentDepositTextInfo: ComputedRef<ThemePropertiesFromApiDto>;
  isPaymentGewayMethodShowBankAcc: Ref<boolean>;
  internetBankingDepositTextInfo: ComputedRef<ThemePropertiesFromApiDto>;
  isNeedSelectAmount: ComputedRef<boolean>;
  amountOptions: ComputedRef<number[]>;
  amountNoLimitOptions: Record<string, number[]>;
  isMomoPayGobalPay: ComputedRef<boolean>;
}
export const usePaymentBankTransferDeposit = (
  paymentBanks: Ref<IGetPaymentGatewayBanksResponse>,
  profile: Ref<ProfileDto>
): IPaymentBankTransferDeposit => {
  const { $t } = useNuxtApp();
  const route = useRoute();
  const router = useRouter();
  const { AvailableBanksForDeposit, IsDecimalAllow } = paymentBanks.value;
  const cryptocurrencyNetwork = ref();
  const isPaymentGewayMethodShowBankAcc = ref(false);
  const isNotcryptoGroup = computed(() => {
    const crytoGroup = ["VTPay", "MomoPay", "CryptoCurrency"];
    const isExist = crytoGroup.some(
      (item) => item.toLowerCase() === route.params.method || route.query?.uc?.includes(item.toLowerCase())
    );
    return !isExist;
  });
  const banksForCurrentPaymentMethod = computed(() =>
    AvailableBanksForDeposit.filter(
      (bank) => bank.PaymentMethod === route.params.method || route.query?.uc?.includes(bank.PaymentMethod)
    )
  );
  const uniqueBanksByMethod = computed(() =>
    _.uniqBy(banksForCurrentPaymentMethod.value, (bank) => bank.BankName)
  );
  const banks = computed(() => {
    const result = uniqueBanksByMethod.value.map((item) => {
      let imgSrc = "";
      try {
        imgSrc = getAssetImage(
          `assets/banklogos/${item.Currency.toUpperCase()}_${item.BankCode.toUpperCase()}.png`
        );
      } catch (_) { }
      return {
        text: item.BankName,
        value: item.BankName,
        imgSrc,
      };
    });
    return result;
  });
  const bankRadios = reactive({
    name: "banks",
    radios: banks,
  });
  const selectedBank = ref(
    banks.value.length === 0 ? "" : banks.value[0].value
  );
  const channels = computed(() => {
    const getBanksGroup = banksForCurrentPaymentMethod.value.filter(
      (bank) => bank.BankName === selectedBank.value
    );
    const result = _.sortBy(getBanksGroup, [(bank) => bank.PpId]).map(
      (bank) => ({
        text: `${$t("channel")} ${bank.PpId}`,
        value: bank.PpId,
      })
    );
    return result;
  });
  const amount = ref();
  const bankAccountName = ref();
  const bankAccountNumber = ref();
  const selectedChannel = ref(
    channels.value.length === 0 ? "" : channels.value[0].value
  );
  const getSelectedBank = computed(() => {
    const getBanksGroup = AvailableBanksForDeposit.filter(
      (bank) => bank.BankName === selectedBank.value && route.fullPath.includes(bank.PaymentMethod)
    );
    const getBank = _.find(
      getBanksGroup,
      (bank) => bank.PpId === parseInt(`${selectedChannel.value}`, 10)
    );
    return getBank;
  });
  const cryptocurrencyNetworks = computed(
    () => getSelectedBank.value?.CryptoCurrencyNetwork
  );
  cryptocurrencyNetwork.value = cryptocurrencyNetworks.value
    ? cryptocurrencyNetworks.value[0]
    : "";
  watch(cryptocurrencyNetworks, (value) => {
    cryptocurrencyNetwork.value = _.first(value);
  });
  const depositMax = computed(() => {
    const getBank = getSelectedBank.value;
    return getBank ? getBank.DepositMax : 0;
  });
  const depositMin = computed(() => {
    const getBank = getSelectedBank.value;
    return getBank ? getBank.DepositMin : 0;
  });
  const cryptoAmount = ref();
  const channelRadios = reactive({
    name: "channels",
    radios: channels,
  });
  watch(banks, () => {
    selectedBank.value = banks.value[0] ? banks.value[0].value : "";
    amount.value = null;
    cryptoAmount.value = null;
  });
  watch(channels, () => {
    selectedChannel.value = channels.value[0] ? channels.value[0].value : 0;
    amount.value = null;
    cryptoAmount.value = null;
  });
  const userCurrency = _.lowerCase(profile.value.Currency);
  const isNeedToMultiplyByThousand = computed(
    () => userCurrency === "idr" || userCurrency === "vnd"
  );
  const validateDepositMin = computed(() => depositMin.value);
  const validateDepositMax = computed(() => depositMax.value);
  const cryptoCurrencyDepositFormRef = ref<FormInstance>();
  const ruleFormRef = ref<FormInstance>();
  const formData = reactive({
    amount,
    bankAccountName,
    bankAccountNumber,
  });
  const cryptoCurrencyDepositFormData = reactive({
    cryptoAmount,
  });
  const validateAmount = computed(() => amount.value);
  const validateCryptoAmount = computed(() => cryptoAmount.value);
  const checkAmount = () => {
    const isInValid =
      validateDepositMax.value !== 0 &&
      (Number.isNaN(validateAmount.value) ||
        validateAmount.value < validateDepositMin.value ||
        validateAmount.value > validateDepositMax.value);
    if (isInValid) {
      return `${$t("min")}: ${numberFormatHelper.addAccountingFormat(Number(validateDepositMin.value), 0, false)} ${$t("max")}: ${numberFormatHelper.addAccountingFormat(Number(validateDepositMax.value), 0, false)
        }`;
    }
    if (!IsDecimalAllow && /\./.test(`${formData.amount}`)) {
      return $t("decimal_point_not_allow");
    }
    return "";
  };
  const isBankAccountNumberRequired = ref(false);
  const isBankAccountNameRequired = ref(false);
  // Get the last part of the URL
  watch(route, () => {
    const segments = route.query.uc ? String(route.query.uc).split('/') : window.location.href.split('/');
    const lastSegment = segments[segments.length - 1];
    const isOfflineKRW = lastSegment === 'offline' && store.state.currency === 'KRW' && selectedChannel.value === 9;
    const isPromptpayTHB = lastSegment === 'promptpay' && store.state.currency === 'THB' && selectedChannel.value === 14;
    const isQrpayTHB = (lastSegment === 'qrpay' || lastSegment === 'offline') && store.state.currency === 'THB' && selectedChannel.value === 9;
    const isGlobalPayPaymentProvider =  selectedChannel.value === 32; // GlobalPayProviderId
    isPaymentGewayMethodShowBankAcc.value = isOfflineKRW || isPromptpayTHB || isQrpayTHB || isGlobalPayPaymentProvider;
    isBankAccountNumberRequired.value = (isOfflineKRW || isPromptpayTHB || isQrpayTHB) && !isGlobalPayPaymentProvider;
    isBankAccountNameRequired.value = isQrpayTHB || isGlobalPayPaymentProvider;
  }, { immediate: true });
  const checkBankAccountNumberMinLength = () => {
    if (isBankAccountNumberRequired.value) {
      if (formData.bankAccountNumber.length < 10) {
        return $t('Bank account number must be at least 10 digits long');
      }
      return '';
    }
    return '';
  };
  const rules = {
    amount: { customRule: checkAmount, required: true },
    bankAccountName: { required: isBankAccountNameRequired.value },
    bankAccountNumber: { customRule: checkBankAccountNumberMinLength, required: isBankAccountNumberRequired.value },
  };
  const checkCryptoAmount = () => {
    if (
      validateDepositMax.value !== 0 &&
      (validateCryptoAmount.value < validateDepositMin.value ||
        validateCryptoAmount.value > validateDepositMax.value)
    ) {
      return `${$t("min")}: ${numberFormatHelper.addAccountingFormat(Number(validateDepositMin.value), 0, false)} ${$t("max")}: ${numberFormatHelper.addAccountingFormat(Number(validateDepositMax.value), 0, false)
        }`;
    }
    if (
      validateDepositMax.value === 0 &&
      validateCryptoAmount.value < validateDepositMin.value
    ) {
      return `${$t("min")}: ${numberFormatHelper.addAccountingFormat(Number(validateDepositMin.value), 0, false)}`;
    }
    if (!IsDecimalAllow && /\./.test(`${validateCryptoAmount.value}`)) {
      return $t("decimal_point_not_allow");
    }
    return "";
  };
  const cryptoCurrencyRule = {
    cryptoAmount: { customRule: checkCryptoAmount, required: true },
  };
  const getRule = formHelper.getRules(rules, formData);
  const cryptoCurrencyDepositRule = formHelper.getRules(
    cryptoCurrencyRule,
    cryptoCurrencyDepositFormData
  );
  const amountPlaceholder = computed(
    () =>
      `${$t("min")}: ${numberFormatHelper.addAccountingFormat(Number(validateDepositMin.value), 0, false)} ${$t("max")}: ${validateDepositMax.value === 0 ? "--" : numberFormatHelper.addAccountingFormat(Number(validateDepositMax.value), 0, false)
      }`
  );
  const isProccesing = ref(false);
  const isUCPopup = computed(() => route.fullPath.includes('uc='));
  const { getThemePropertyFromApi, kwaiMetaTrigger, EnumKwaiMeta } = featuresToggleHelper;
  const proceedDepositInternetBank = async () => {
    let paymentMethodFromRoute = route.params.method;
    if (isUCPopup.value && route.query.uc) {
      const ucUrlParts = String(route.query.uc).split("/").filter(part => part);
      if (ucUrlParts[2]) {
        paymentMethodFromRoute = ucUrlParts[2];
      }
    }
    isProccesing.value = true;
    const isFromTelegram = computed(() => route.fullPath.includes("telegram/deposit"));
    const Amount = (isNotcryptoGroup.value || isMomoPayGobalPay.value)
      ? formData.amount
      : cryptoCurrencyDepositFormData.cryptoAmount;
    const rfpa = isNotcryptoGroup.value ? `?rfpa=${Amount}` : '';
    const requestParams = {
      PaymentMethod: paymentMethodFromRoute,
      BankCode: getSelectedBank.value?.BankCode,
      PpId: parseInt(`${selectedChannel.value}`, 10),
      BankAccName: formData.bankAccountName,
      BankAccNumber: formData.bankAccountNumber,
      Amount,
      ReturnUrl: `${window.location.origin}${isUCPopup.value ? '/?uc=' : ''}/transaction-history/deposit${rfpa}`,
      FailedUrl: window.location.href,
      HomeUrl: `${window.location.origin}${isUCPopup.value ? '/?uc=' : ''}/deposit`,
      CryptoCurrencyNetwork: cryptocurrencyNetwork.value ?? "",
      OnlineId: Number(localStorage.getItem("onlineId") ?? 0),
    } as IRequestDepositInternetBank;
    const response = await apis.proceedDepositInternetBank(requestParams);
    if (response.isSuccessful) {
      kwaiMetaTrigger(EnumKwaiMeta.ADD_TO_CARD);
      if (response.Data.RedirectUrl) {
        if (_.includes(response.Data.RedirectUrl, "/")) {
          window.location.href = response.Data.RedirectUrl;
        } else {
          router.push({
            path: isFromTelegram.value ? "/telegram/deposit/maintain" : "/deposit/maintain",
            query: {
              content: btoa(
                `${response.Data.RedirectUrl}&&prevURL${route.path}`
              ),
            },
          });
        }
      } else if (response.Data.PaymentInfo) {
        router.push({
          path: isFromTelegram.value ? "/telegram/crypto-payment" : "/crypto-payment",
          query: {
            informations: btoa(JSON.stringify(response.Data)),
          },
        });
      } else {
        const view = decodeURIComponent(
          response.Data.RedirectView.replace(/[\u003D]/g, "%").replace(
            /[\u002B]/g,
            " "
          )
        );
        router.push({
          path: isFromTelegram.value ? "/telegram/internet-payment-view" : "/internet-payment-view",
          query: {
            view,
          },
        });
      }
    } else {
      notificationHelper.notification(response.ErrorMessageForDisplay, EnumMessageType.Error);
    }
    isProccesing.value = false;
  };
  const submitPaymentDeposit = formHelper.getSubmitFunction(
    proceedDepositInternetBank
  );
  const triggerImageError = (e: any) => {
    const { alt } = e.target;
    e.target.replaceWith(`${alt}`);
  };
  const paymentDepositTextInfo = computed(() =>
    getThemePropertyFromApi(
      EnumThemePropertyKey.HtmlId,
      "payment_deposit_text_info"
    )
  );
  const internetBankingDepositTextInfo = computed(() =>
    getThemePropertyFromApi(
      EnumThemePropertyKey.HtmlId,
      "internet_banking_deposit_text_info"
    )
  );

  const methodNeedSelectAmount = ['tmalldirect', 'newjdwechatjdecard'];
  const amountNoLimitOptions: Record<string, number[]> = {
    newjdwechatjdecard: [100, 200, 300, 500, 600, 800, 1000],
    tmalldirect: [100, 200, 300, 400, 500],
  };
  const amountOptions = computed(() => {
    if (!methodNeedSelectAmount.includes(getSelectedBank.value?.PaymentMethod.toLowerCase() ?? "")) return [];
    if (depositMax.value > 0) {
      if (depositMin.value > 0) return amountNoLimitOptions[getSelectedBank.value?.PaymentMethod.toLowerCase() ?? ""].filter((item) => item >= depositMin.value && item <= depositMax.value);
      return amountNoLimitOptions[getSelectedBank.value?.PaymentMethod.toLowerCase() ?? ""].filter((item) => item <= depositMax.value);
    }
    return amountNoLimitOptions[getSelectedBank.value?.PaymentMethod.toLowerCase() ?? ""];
  });
  const isNeedSelectAmount = computed(() =>  methodNeedSelectAmount.includes(getSelectedBank.value?.PaymentMethod.toLowerCase() ?? ""));
  watch(() => [getSelectedBank.value, formData.amount], () => {
    if (amountOptions.value && amountOptions.value.length) {
      if (isNeedSelectAmount.value) {
        if (formData.amount === undefined || formData.amount === null || formData.amount === "") formData.amount = amountOptions.value[0];
      }
      else formData.amount = "";
    }
  }, { deep: true, immediate: true });

  const isMomoPayGobalPay = computed(() => {
    const isMomoPay = ["MomoPay"];
    const isExistMomoPay = isMomoPay.some(
      (item) => item.toLowerCase() === route.params.method || route.query?.uc?.includes(item.toLowerCase())
    );
    return isExistMomoPay && selectedChannel.value === 32;
  });

  return {
    bankRadios,
    selectedBank,
    channelRadios,
    selectedChannel,
    isNotcryptoGroup,
    validateDepositMin,
    validateDepositMax,
    isNeedToMultiplyByThousand,
    ruleFormRef,
    formData,
    getRule,
    cryptoCurrencyDepositFormData,
    cryptoCurrencyDepositFormRef,
    cryptoCurrencyDepositRule,
    isProccesing,
    submitPaymentDeposit,
    amountPlaceholder,
    cryptocurrencyNetwork,
    cryptocurrencyNetworks,
    triggerImageError,
    paymentDepositTextInfo,
    isPaymentGewayMethodShowBankAcc,
    internetBankingDepositTextInfo,
    isNeedSelectAmount,
    amountOptions,
    amountNoLimitOptions,
    isMomoPayGobalPay,
  };
};
