import { Ref } from "vue";
import {
  IGetDepositInternetBankReponse,
  IPaymentInfo,
} from "@/models/requestDepositBanks";

interface CrytoPaymentViewDeposit {
  paymentInfo: IPaymentInfo;
  afterAmount: number;
  transactionFee: number;
}
export const useCrytoPaymentViewDeposit = (
  informations: Ref<string>,
  isFromTelegram = false
): CrytoPaymentViewDeposit => {
  if (informations.value === undefined) {
    if (isFromTelegram) {
      window.location.href = "/telegram/deposit/payment-bank/cryptocurrency";
    } else {
      window.location.href = "/deposit/payment-bank/cryptocurrency";
    }
  }
  const walletInfo = JSON.parse(
    informations.value
  ) as IGetDepositInternetBankReponse;
  const paymentInfo = walletInfo.PaymentInfo as IPaymentInfo;
  const afterAmount = walletInfo.AfterAmount;
  const transactionFee = walletInfo.TransactionFee;
  return {
    paymentInfo,
    afterAmount,
    transactionFee,
  };
};
