interface IInternetPaymentViewDeposit {
  execJavaScript: () => void;
}
export const useInternetPaymentViewDeposit = (
  view: string,
  isFromTelegram = false
): IInternetPaymentViewDeposit => {
  if (view === undefined) {
    if (isFromTelegram) {
      window.location.href = "/telegram/deposit/payment-bank/internet-banking";
    } else {
      window.location.href = "/deposit/payment-bank/internet-banking";
    }
  }
  const execJavaScript = () => {
    const javaScripts = Array.from(view.matchAll(/<script>(.*?)<\/script>/g));
    javaScripts.forEach((javaScript) => {
      // eslint-disable-next-line no-eval
      eval(javaScript[1]);
    });
  };
  return {
    execJavaScript,
  };
};
