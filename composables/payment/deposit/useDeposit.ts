import { ref, Ref, computed, ComputedRef } from "vue";
import _ from "lodash";
import { IGetPaymentGatewayBanksResponse, IPaymentMethodsForDepositAndWithdrawal } from "@/models/getPaymentGatewayBanksRespone";
import { IGetTransactionBankListResponse } from "@/models/getTransactionBankListResponse";
import { IGetManualCryptoCurrencyTransactionResponse } from "@/models/getManualCryptoCurrencyTransactionResponse";
import featuresToggleHelper from "@/utils/featuresToggleHelper";
import apis from "@/utils/apis";
import notificationHelper from "@/utils/elementUiHelpers/notificationHelper";
import EnumMessageType from "@/models/enums/enumMessageType";
import commonHelper from "@/utils/commonHelper";
import { store } from "@/store";

interface IDeposit {
  paymentBanks: Ref<IGetPaymentGatewayBanksResponse>;
  normalBanks: Ref<IGetTransactionBankListResponse>;
  manualCryptoCurrency: Ref<IGetManualCryptoCurrencyTransactionResponse>;
  isNormalBankMethodEnabled: Ref<boolean>;
  isPaymentBankMethodEnabled: Ref<boolean>;
  isManualCryptoCurrencyEnabled: Ref<boolean>;
  isLoading: ComputedRef<boolean>;
  IsAutoDepositEnabled: ComputedRef;
  isBankMethodAvailabled: ComputedRef;
  isUfun: ComputedRef<boolean>;
}
export const useDeposit = (): IDeposit => {
  const router = useRouter();
  const route = useRoute();
  const paymentBanks = ref<IGetPaymentGatewayBanksResponse>(
    {} as IGetPaymentGatewayBanksResponse
  );
  const normalBanks = ref<IGetTransactionBankListResponse>(
    {} as IGetTransactionBankListResponse
  );
  const manualCryptoCurrency = ref<IGetManualCryptoCurrencyTransactionResponse>(
    {} as IGetManualCryptoCurrencyTransactionResponse
  );
  const isPaymentBanksLoading = ref(true);
  const isNormalBanksLoading = ref(true);
  const isLoading = computed(
    () => isPaymentBanksLoading.value || isNormalBanksLoading.value
  );
  const cryptoCurrencyEnabled = ref(false);
  const isManualCryptoCurrency = ref(false);
  const usingPaymentGateway = ref(false);
  const isAutoDepositEnabledSetting = ref(false);
  const { isUsingPaymentGateway } = featuresToggleHelper;

  const getPaymentGatewayBanks = async () => {
    const response = await apis.getPaymentGatewayBanks();
    const responsePaymentGateway = await isUsingPaymentGateway();
    usingPaymentGateway.value = responsePaymentGateway;

    if (response.isSuccessful) {
      paymentBanks.value = response.Data;
      //check if the PaymentMethodsForDeposit Display Order are  all 0 so will order by alphabet
      if (_.every(paymentBanks.value.PaymentMethodsForDeposit, { DisplayOrder: 0 })) {
        const allZero = _.orderBy(paymentBanks.value.PaymentMethodsForDeposit, (payment: IPaymentMethodsForDepositAndWithdrawal) => payment.PaymentMethod.toLowerCase(), 'asc');
        paymentBanks.value.PaymentMethodsForDeposit = allZero;
      } else {
        //check if the PaymentMethodsForDeposit Display Order not all 0 will order by DisplayOrder
        const sortedByASC = _.orderBy(paymentBanks.value.PaymentMethodsForDeposit, 'DisplayOrder', ['asc', 'desc']);
        const duplicatesOfZero = _.filter(sortedByASC, { DisplayOrder: 0 });
        // if include any duplicate 0 will order by alphabet
        if (duplicatesOfZero) {
          const sortedDuplicatesOfZero = _.orderBy(duplicatesOfZero, 'PaymentMethod', 'asc');
          const filteredSortedByASC = _.reject(sortedByASC, { DisplayOrder: 0 });
          const finalSorted = [...sortedDuplicatesOfZero, ...filteredSortedByASC];
          paymentBanks.value.PaymentMethodsForDeposit = finalSorted;
        } else {
          // if no duplicate 0 will return the defualt sorted by DisplayOrder
          paymentBanks.value.PaymentMethodsForDeposit = sortedByASC;
        }
      paymentBanks.value.PaymentMethodsForDeposit = _.orderBy(response.Data.PaymentMethodsForDeposit, 'DisplayOrder', ['asc', 'desc'])
      }
      isPaymentBanksLoading.value = false;
      const isUCPopup = route.fullPath.includes('uc=/deposit');
      if (route.name === "deposit" || route.query.uc === '/deposit' || route.query.uc === '/deposit/') {
        const selectedPaymentMethod = computed(() =>
          _.first(paymentBanks.value.AvailableBanksForDeposit)
        );
        const isTransferMethodDepositEnabled = computed(
          () => paymentBanks.value.IsTransferMethodDepositEnable
        );
        const depositPath = isTransferMethodDepositEnabled.value &&
        selectedPaymentMethod.value &&
        responsePaymentGateway && !isUfun.value
        ? `/deposit/payment-bank/${selectedPaymentMethod.value.PaymentMethod}`
        : "/deposit/bank-transfer";
        if (isUCPopup) {
          router.push({
            path: route.path,
            query: {
              uc: depositPath,
            }
          });
        } else {
          router.push({
            path: depositPath,
          });
        }
      }
    else if (route.name === "telegram-deposit") {
      const selectedPaymentMethod = computed(() =>
          _.first(paymentBanks.value.AvailableBanksForDeposit)
        );
        const isTransferMethodDepositEnabled = computed(
          () => paymentBanks.value.IsTransferMethodDepositEnable
        );
        const depositPath = isTransferMethodDepositEnabled.value &&
        selectedPaymentMethod.value &&
        responsePaymentGateway
          ? `/telegram/deposit/payment-bank/${selectedPaymentMethod.value.PaymentMethod}`
          : "/telegram/deposit/bank-transfer";
      router.push({
        path: depositPath,
      });
    }
    } else {
      notificationHelper.notification(response.Message, EnumMessageType.Error);
    }
  };
  const getTransactionBankList = async () => {
    const response = await apis.getTransactionBankList("DepositPage");
    if (response.isSuccessful) {
      normalBanks.value = response.Data;
      isNormalBanksLoading.value = false;
    } else {
      notificationHelper.notification(response.Message, EnumMessageType.Error);
    }
  };
  const getManualCryptoCurrencyTransaction = async () => {
    const response = await apis.getManualCryptoCurrencyTransaction(
      "DepositPage"
    );
    if (response.isSuccessful) {
      manualCryptoCurrency.value = response.Data;
      isManualCryptoCurrency.value =
        response.Data.CompanyCryptocurrencyOptions.length !== 0 &&
        response.Data.PlayerCryptocurrencyOptions.length !== 0;
    } else {
      notificationHelper.notification(response.Message, EnumMessageType.Error);
    }
  };
  const getCustomerSetting = async () => {
    const response = await commonHelper.getCustomerSettingById(
      "IsManualCryptocurrencyDepositEnable"
    );
    cryptoCurrencyEnabled.value = response && response.Value === "Y";
  };
  const getAutoDepositEnabledSetting = async () => {
    const response = await commonHelper.getCustomerSettingById(
      "IsAutoDepositEnable"
    );
    isAutoDepositEnabledSetting.value = response && response.Value === "Y";
  };
  getAutoDepositEnabledSetting();
  const isCustomizeBankNameEnabled = computed(() =>
    _.some(normalBanks.value.AvailablePropertyList, {
      PropertyName: "CustomizeBankName",
    })
  );
  const isManualCryptoCurrencyEnabled = computed(
    () => isManualCryptoCurrency.value && cryptoCurrencyEnabled.value
  );
  const IsAutoDepositEnabled = computed(
    () =>
      normalBanks.value.CompanyBanks?.filter((item) => item.IsAutoDeposit)
        .length > 0 &&
      normalBanks.value.PlayerBankOptions.length &&
      isAutoDepositEnabledSetting.value
  );
  const isNormalBankMethodEnabled = computed(() => {
    const bank = normalBanks.value;
    return (
      bank.IsTransferMethodDepositEnable &&
      bank.CompanyBanks.length > 0 &&
      (bank.PlayerBankOptions.length > 0 ||
        (isCustomizeBankNameEnabled.value &&
          bank.PlayerBankOptions.length === 0))
    );
  });
  const isPaymentBankMethodEnabled = computed(() => {
    const bank = paymentBanks.value;
    return (
      bank.IsTransferMethodDepositEnable &&
      bank.AvailableBanksForDeposit.length > 0 &&
      usingPaymentGateway.value
    );
  });
  const isBankMethodAvailabled = computed(
    () =>
      isPaymentBankMethodEnabled.value ||
      isNormalBankMethodEnabled.value ||
      isManualCryptoCurrencyEnabled.value ||
      IsAutoDepositEnabled.value
  );
  const env = () => useRuntimeConfig().public.CURRENTLY_ENV;
  const isUfun = computed (() => env() === 'production' && store.state.webId === 3);
  getPaymentGatewayBanks();
  getTransactionBankList();
  getManualCryptoCurrencyTransaction();
  getCustomerSetting();
  return {
    isUfun,
    paymentBanks,
    normalBanks,
    manualCryptoCurrency,
    isNormalBankMethodEnabled,
    isPaymentBankMethodEnabled,
    isManualCryptoCurrencyEnabled,
    isLoading,
    IsAutoDepositEnabled,
    isBankMethodAvailabled,
  };
};
