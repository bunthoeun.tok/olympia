/* eslint-disable */
import { ComputedRef, Ref, ref, reactive, computed } from "vue";
import { FormInstance } from "element-plus";
import _ from "lodash";
import {
  IPlayerBanksDto,
  ICompanyBanksDto,
  IPlayerBankListAfterFirstTimeDepositDto,
  IGetTransactionBankListResponse,
} from "@/models/getTransactionBankListResponse";
import commonHelper from "@/utils/commonHelper";
import { ProfileDto } from "@/models/getProfileResponse";
import formHelper from "@/utils/elementUiHelpers/formHelper";
import textHelper from "@/utils/formatHelpers/textFormatHelper";
import { IRequestDepositBankTransfer } from "@/models/requestDepositBanks";
import apis from "@/utils/apis";
import notificationHelper from "@/utils/elementUiHelpers/notificationHelper";
import EnumMessageType from "@/models/enums/enumMessageType";
import EnumApiErrorCode from "@/models/enums/enumApiErrorCode";
import featuresToggleHelper from "@/utils/featuresToggleHelper";
import EnumThemePropertyKey from "@/models/enums/enumThemePropertyKey";
import { ThemePropertiesFromApiDto } from "@/models/companyThemePropertiesRespone";
import numberFormatHelper from "@/utils/formatHelpers/numberFormatHelper";
import { store } from "@/store";
import cookieHelper from "~~/utils/cookieHelper";
import useCloseTelegramWebApp from "@/composables/useCloseTelegramWebApp";

interface IUseNormalBankTransferDeposit {
  selectedCompanyBankId: Ref<number>;
  companyFlowSettings: any;
  isFirstTimeDeposit: Ref<boolean>;
  playerBankOptions: Array<IPlayerBanksDto>;
  CompanyBanks: Array<ICompanyBanksDto>;
  playerBankAfterFirstTimeDeposit: ComputedRef<
    Array<IPlayerBankListAfterFirstTimeDepositDto>
  >;
  normalBankTransferDepositRules: any;
  normalBankTransferDepositFormRef: Ref<FormInstance | undefined>;
  normalBankTransferDepositFormData: any;
  slipUpload: Ref<File[]>;
  isNeedToMultiplyByThousand: ComputedRef<boolean>;
  isProccesing: Ref<boolean>;
  isUploadImage: Ref<boolean>;
  submitDepositBankTransfer: (formEl: FormInstance) => void;
  amountPlaceholder: ComputedRef<string>;
  bankTransferDepositTextInfo: ComputedRef<ThemePropertiesFromApiDto>;
  isFirstTimeBankAccountNameEmpty: ComputedRef<boolean>;
  isFirstTimeBankAccountNumberEmpty: ComputedRef<boolean>;
}
export const useNormalBankTransferDeposit = (
  normalBanks: Ref<IGetTransactionBankListResponse>,
  profile: Ref<ProfileDto>
): IUseNormalBankTransferDeposit => {
  const { $t } = useNuxtApp();
  const router = useRouter();
  const {
    CompanyBanks,
    PlayerBankListAfterFirstTimeDeposit,
    PlayerBankOptions,
    AvailablePropertyList,
    IsDecimalAllow,
  } = normalBanks.value;

  const playerBankOptions = _.sortBy(PlayerBankOptions, [
    (bank) => bank.Priority,
  ]);
  const selectedCompanyBankId = ref(CompanyBanks[0].Id);
  const companyFlowSettings = {
    branchNameSetting: _.find(AvailablePropertyList, {
      PropertyName: "BranchName",
    }),
    companyBankAccountNameSetting: _.find(AvailablePropertyList, {
      PropertyName: "CompanyBankAccountName",
    }),
    slipUploadSetting: _.find(AvailablePropertyList, {
      PropertyName: "SlipUpload",
    }),
    depositTimeSetting: _.find(AvailablePropertyList, {
      PropertyName: "DepositTime",
    }),
    depositReferenceNumberSetting: _.find(AvailablePropertyList, {
      PropertyName: "DepositReferenceNumber",
    }),
    customizeBankNameSetting: _.find(AvailablePropertyList, {
      PropertyName: "CustomizeDepositBank",
    }),
    bsbSetting:  _.find(AvailablePropertyList, {
      PropertyName: "BSB",
    }),
    payIdSetting:  _.find(AvailablePropertyList, {
      PropertyName: "PayID",
    }),
    bankAccountNumberSetting: _.find(AvailablePropertyList, {
      PropertyName: "BankAccountNumber",
    }),
    bankAccountNameSetting: _.find(AvailablePropertyList, {
      PropertyName: "BankAccountName",
    }),
  };
  const isFirstTimeDeposit = ref(
    PlayerBankListAfterFirstTimeDeposit.length === 0
  );
  const playerBankAfterFirstTimeDeposit = computed(() => {
    const banks = PlayerBankListAfterFirstTimeDeposit;
    if (
      !isFirstTimeDeposit.value &&
      banks.findIndex((option) => option.PlayerBankName === "Other") === -1 &&
      companyFlowSettings.customizeBankNameSetting
    ) {
      banks.push({
        PlayerBankName: "Other",
      } as IPlayerBankListAfterFirstTimeDepositDto);
    }
    return banks;
  });
  const { slipUpload, isdisabled } = commonHelper;
  if (companyFlowSettings.customizeBankNameSetting) {
    const newBankOption = {
      Id: -1,
      BankName: "Other",
      Priority: 1000,
    } as IPlayerBanksDto;
    if (!_.find(playerBankOptions, { Id: -1 })) {
      playerBankOptions.push(newBankOption);
    }
  }
  const amount = ref();
  const normalBankTransferDepositFormData = reactive({
    selectedPlayerBankOption: isFirstTimeDeposit.value
      ? playerBankOptions[0].BankName
      : playerBankAfterFirstTimeDeposit.value[0].PlayerBankName,
    customizeBankName: "",
    branchName: "",
    bankAccountName: !isFirstTimeDeposit.value
      ? playerBankAfterFirstTimeDeposit.value[0].PlayerBankAccountName
      : "",
    bankAccountNumber: !isFirstTimeDeposit.value
      ? playerBankAfterFirstTimeDeposit.value[0].PlayerBankAccountNumber
      : "",
    bsb: "",
    payId: "",
    amount,
    slipUpload: computed(() => slipUpload.value[0]?.name ?? ""),
    depositTime: "",
    depositReferenceNumber: "",
    selectedCompanyBankId: computed(() => selectedCompanyBankId.value),
    companyBankAccountName: computed(
      () =>
        CompanyBanks.find((bank) => bank.Id === selectedCompanyBankId.value)
          ?.BankAccountName
    ),
    companyBankAccountNumber: computed(
      () =>
        CompanyBanks.find((bank) => bank.Id === selectedCompanyBankId.value)
          ?.BankAccountNumber
    ),
  });
  const selectedCompanyBank = computed(() =>
    _.find(CompanyBanks, {
      Id: normalBankTransferDepositFormData.selectedCompanyBankId,
    })
  );
  const depositMin = selectedCompanyBank.value?.DepositMin ?? 0;
  const depositMax = selectedCompanyBank.value?.DepositMax ?? 0;
  const userCurrency = computed(() => _.lowerCase(profile.value.Currency));
  const isNeedToMultiplyByThousand = computed(
    () => userCurrency.value === "idr" || userCurrency.value === "vnd"
  );
  const validateAmount = computed(
    () => normalBankTransferDepositFormData.amount
  );
  const validateDepositMin = computed(() => depositMin);
  const validateDepositMax = computed(() => depositMax);
  const isBranchNameSettingRequired = commonHelper.isRequiredField(
    companyFlowSettings,
    "branchNameSetting"
  );
  const isDepositTimeSettingRequired = commonHelper.isRequiredField(
    companyFlowSettings,
    "depositTimeSetting"
  );
  const isDepositReferenceNumberSettingRequired = commonHelper.isRequiredField(
    companyFlowSettings,
    "depositReferenceNumberSetting"
  );
  const isSlipUploadSettingRequired = commonHelper.isRequiredField(
    companyFlowSettings,
    "slipUploadSetting"
  );
  const checkAmount = () => {
    if (
      validateAmount.value < validateDepositMin.value ||
      validateAmount.value > validateDepositMax.value
    ) {
      return `${$t("min")}: ${numberFormatHelper.addAccountingFormat(Number(validateDepositMin.value), 0, false)}
      ${$t("max")}: ${numberFormatHelper.addAccountingFormat(Number(validateDepositMax.value), 0, false)}`;
    }
    if (
      !IsDecimalAllow &&
      /\./.test(`${normalBankTransferDepositFormData.amount}`)
    ) {
      return $t("decimal_point_not_allow");
    }
    return "";
  };
  const isBsbSettingRequired = commonHelper.isRequiredField(
    companyFlowSettings,
    "bsbSetting"
  );
  const isPayIdSettingRequired = commonHelper.isRequiredField(
    companyFlowSettings,
    "payIdSetting"
  );
  const isBankAccountNumberSettingRequired = commonHelper.isRequiredField(
    companyFlowSettings,
    "bankAccountNumberSetting"
  );
  const isBankAccountNameSettingRequired = commonHelper.isRequiredField(
    companyFlowSettings,
    "bankAccountNameSetting"
  );
  const rules = {
    customizeBankName: {
      required:
        companyFlowSettings.customizeBankNameSetting?.PropertyStatus ===
        "Required",
    },
    branchName: { required: isBranchNameSettingRequired },
    bankAccountName: { required: isBankAccountNameSettingRequired },
    bankAccountNumber: { required: isBankAccountNumberSettingRequired },
    amount: { customRule: checkAmount, required: true },
    depositTime: {
      required: isDepositTimeSettingRequired,
      isTriggerByChange: true,
    },
    depositReferenceNumber: {
      required: isDepositReferenceNumberSettingRequired,
    },
    slipUpload: { required: isSlipUploadSettingRequired },
    bsb: { required: isBsbSettingRequired },
    payId: { required: isPayIdSettingRequired },
  };
  const normalBankTransferDepositRules = formHelper.getRules(
    rules,
    normalBankTransferDepositFormData
  );
  const normalBankTransferDepositFormRef = ref<FormInstance>();
  const amountPlaceholder = computed(
    () =>
    `${$t("min")}: ${numberFormatHelper.addAccountingFormat(Number(validateDepositMin.value), 0, false)}
    ${$t("max")}: ${numberFormatHelper.addAccountingFormat(Number(validateDepositMax.value), 0, false)}`
  );
  const isProccesing = ref(false);
  const isUploadImage = ref(isdisabled);
  const route = useRoute();
  const { getThemePropertyFromApi, kwaiMetaTrigger, EnumKwaiMeta } = featuresToggleHelper;
  const proceedDepositBankTranfer = async () => {
    isProccesing.value = true;
    const requestParams = {
      PlayerBankName:
        normalBankTransferDepositFormData.selectedPlayerBankOption,
      PlayerBankAccountName: normalBankTransferDepositFormData.bankAccountName,
      PlayerBankAccountNumber:
        normalBankTransferDepositFormData.bankAccountNumber,
      BankReferenceCode:
        normalBankTransferDepositFormData.depositReferenceNumber,
      BranchName: normalBankTransferDepositFormData.branchName,
      CustomizeBankName: normalBankTransferDepositFormData.customizeBankName,
      BSB: normalBankTransferDepositFormData.bsb,
      PayId: normalBankTransferDepositFormData.payId,
      CompanyBankId: normalBankTransferDepositFormData.selectedCompanyBankId,
      Amount: normalBankTransferDepositFormData.amount,
      DepositTime: textHelper.dateStringTo12HourClock(
        normalBankTransferDepositFormData.depositTime
      ),
      SlipImage: slipUpload.value[0]?.name,
      Domain: commonHelper.getDomain(),
      Url:String(window.location),
      FingerPrint:store.state.visitorId,
      OnlineId: Number(localStorage.getItem("onlineId") ?? 0),
    } as IRequestDepositBankTransfer;
    if (slipUpload.value.length > 0) {
      const uploaded = await apis.uploadFile(
        slipUpload.value,
        "theme/SlipImages"
      );
      if (uploaded.ErrorCode !== EnumApiErrorCode.success) {
        notificationHelper.notification(
          "Failed to Upload Slip Image",
          EnumMessageType.Error
        );
      }
    }
    const response = await apis.proceedDepositBankTransfer(requestParams);
    if (response.isSuccessful) {
      notificationHelper.notification(
        response.ErrorMessageForDisplay,
        EnumMessageType.Success
      );
      // Get the player currency
      const currency = cookieHelper.getCookie("currency");
      kwaiMetaTrigger(EnumKwaiMeta.DEPOSIT, requestParams.Amount, currency);
      const {isSubmitSuccess} = useCloseTelegramWebApp();
      isSubmitSuccess.value = true;
      setTimeout(() => {
        if (route.matched[0].name === "deposit") {
          router.push({
            path: "/transaction-history/deposit",
          });
        }
        if (route.fullPath.includes('uc=/deposit')) {
          router.push({
            path: route.path,
            query: {
              uc: "/transaction-history/deposit",
            }
          });
        }
      }, 1000);
    } else {
      notificationHelper.notification(
        response.ErrorMessageForDisplay,
        EnumMessageType.Error
      );
    }
    isProccesing.value = false;
  };
  const submitDepositBankTransfer = formHelper.getSubmitFunction(
    proceedDepositBankTranfer
  );
  const bankTransferDepositTextInfo = computed(() =>
    getThemePropertyFromApi(
      EnumThemePropertyKey.HtmlId,
      "bank_transfer_deposit_text_info"
    )
  );
  const isFirstTimeBankAccountNameEmpty = computed(() => playerBankAfterFirstTimeDeposit.value[0].PlayerBankAccountName === "");
  const isFirstTimeBankAccountNumberEmpty = computed(() => playerBankAfterFirstTimeDeposit.value[0].PlayerBankAccountNumber === "");
  return {
    selectedCompanyBankId,
    companyFlowSettings,
    isFirstTimeDeposit,
    playerBankOptions,
    CompanyBanks: CompanyBanks.filter((item) => !item.IsAutoDeposit),
    playerBankAfterFirstTimeDeposit,
    normalBankTransferDepositRules,
    normalBankTransferDepositFormRef,
    normalBankTransferDepositFormData,
    slipUpload,
    isNeedToMultiplyByThousand,
    isProccesing,
    isUploadImage,
    submitDepositBankTransfer,
    amountPlaceholder,
    bankTransferDepositTextInfo,
    isFirstTimeBankAccountNameEmpty,
    isFirstTimeBankAccountNumberEmpty
  };
};
