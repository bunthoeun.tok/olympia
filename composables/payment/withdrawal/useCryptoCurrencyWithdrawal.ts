import { reactive, ref, ComputedRef, computed, Ref } from "vue";
import _ from "lodash";
import { FormInstance } from "element-plus";
import { IGetPaymentGatewayBanksResponse } from "@/models/getPaymentGatewayBanksRespone";
import { IRequestTransactionFee } from "@/models/requestTransactionFee";
import { IRequestWithdrawalInternetBank } from "@/models/requestWithdrawalBankTransfer";
import { ProfileDto } from "@/models/getProfileResponse";
import apis from "@/utils/apis";
import formHelper from "@/utils/elementUiHelpers/formHelper";
import notificationHelper from "@/utils/elementUiHelpers/notificationHelper";
import EnumMessageType from "@/models/enums/enumMessageType";
import numberFormatHelper from "@/utils/formatHelpers/numberFormatHelper";
import { IRadio } from "@/models/radiosButton";

interface ICryptoCurrencyWithdrawalFormData {
  walletAddress: string;
  amount: number;
  paymentPassword: string;
}
interface ICryptoCurrencyWithdrawal {
  selectCryptoCurrency: Ref<string>;
  withdrawalMin: Ref<string>;
  withdrawalMax: Ref<string>;
  cryptocurrencyRadios: IRadio;
  refreshTransactionFee: () => Promise<void>;
  TransactionFee: Ref<number>;
  submitWithdrawal: (formEl: FormInstance) => void;
  getRule: any;
  formData: ICryptoCurrencyWithdrawalFormData;
  ruleFormRef: Ref<FormInstance | undefined>;
  isWithdrawProcessing: Ref<boolean>;
  checkedCurrency: ComputedRef<boolean>;
  amountPlaceholder: ComputedRef<string>;
  remainingRollover: ComputedRef<string>;
  eligibleWithdrawal: ComputedRef<string>;
}

export const useCryptoCurrencyWithdrawal = (
  paymentBanks: Ref<IGetPaymentGatewayBanksResponse>,
  profile: Ref<ProfileDto>,
  isPaymentPasswordEnable: Ref<boolean>,
  isWithdrawalLimitEnabled: Ref<boolean>
): ICryptoCurrencyWithdrawal => {
  const { $t } = useNuxtApp();
  const router = useRouter();
  const {
    PlayerPaymentWithdrawalBankInfo,
    AvailableBanksForWithdrawal,
    PaymentMethodsForWithdrawal,
    IsDecimalAllow,
    WithdrawalLimit,
    AvailableWithdrawAmount,
  } = paymentBanks.value;

  const cryptoCurrencyNetwork = ref<string[]>([]);
  const banksForCurrentPaymentMethod = computed(() =>
    AvailableBanksForWithdrawal.filter(
      (bank) => bank.PaymentMethod === "cryptocurrency"
    )
  );
  const uniqueBanksByMethod = computed(() =>
    _.uniqBy(banksForCurrentPaymentMethod.value, (bank) => bank.BankName)
  );
  _.filter(
    _.first(banksForCurrentPaymentMethod.value)?.CryptoCurrencyNetwork,
    (network) => {
      cryptoCurrencyNetwork.value.push(network);
    }
  );

  const firstTimebanks = computed(() => {
    const result = uniqueBanksByMethod.value.map((item) => ({
      text: item.BankName,
      value: item.BankName,
    }));
    return result;
  });

  const notFirstTimebanks = computed(() => {
    const result = [
      {
        text: PlayerPaymentWithdrawalBankInfo.BankName,
        value: PlayerPaymentWithdrawalBankInfo.BankName,
      },
    ];
    return result;
  });
  const isHavePlayerPaymentWithdrawalBankInfo = computed(
    () => PlayerPaymentWithdrawalBankInfo.BankName !== undefined
  );
  const userCurrency = computed(() => _.lowerCase(profile.value.Currency));
  const checkedCurrency = computed(
    () => userCurrency.value === "idr" || userCurrency.value === "vnd"
  );

  const selectCryptoCurrency = ref(cryptoCurrencyNetwork.value[0]);
  const selectedBank = computed(() =>
    isHavePlayerPaymentWithdrawalBankInfo.value
      ? notFirstTimebanks.value[0].value
      : firstTimebanks.value[0].value
  );
  const getSelectedBank = computed(() =>
    AvailableBanksForWithdrawal.find(
      (bank) => bank.BankName === selectedBank.value
    )
  );
  const withdrawalMax = computed(() =>
    getSelectedBank.value !== undefined
      ? getSelectedBank.value.WithdrawalMax
      : 0
  );
  const withdrawalMin = computed(() =>
    getSelectedBank.value !== undefined
      ? getSelectedBank.value.WithdrawalMin
      : 0
  );
  const remainingRollover = computed(() =>
    numberFormatHelper.addAccountingFormat(WithdrawalLimit)
  );
  const eligibleWithdrawal = computed(() =>
    numberFormatHelper.addAccountingFormat(AvailableWithdrawAmount)
  );

  const ruleFormRef = ref<FormInstance>();
  const amount = ref();
  const formData = reactive({
    walletAddress: "",
    amount,
    paymentPassword: "",
  });
  const isUCPopup = computed(()=>route.fullPath.includes('uc='));
  const validateAmount = computed(() => formData.amount);
  const cryptocurrencyRadios = reactive({
    name: "network",
    radios: cryptoCurrencyNetwork.value,
  } as IRadio);
  const TransactionFee = ref(0);
  const refreshTransactionFee = async () => {
    const request = {
      Amount: formData.amount,
      CryptoCurrencyNetwork: selectCryptoCurrency.value,
    } as IRequestTransactionFee;
    const response = await apis.getTransactionFee(request);
    TransactionFee.value = response.Data.TotalFee;
  };
  const paymentMethod = computed(() =>
    PaymentMethodsForWithdrawal.filter(
      (payment) => payment.PaymentMethod.toLowerCase() === "cryptocurrency"
    )
  );
  const route = useRoute();
  const isWithdrawProcessing = ref(false);
  const withdrawal = async () => {
    isWithdrawProcessing.value = true;
    const request = {
      Amount: formData.amount,
      PaymentPassword: formData.paymentPassword,
      BankCode: "NoBankCode",
      PlayerBankId: 0,
      CryptoCurrencyNetwork: selectCryptoCurrency.value,
      PaymentMethod: paymentMethod.value[0].PaymentMethodId,
      ReturnUrl: `${window.location.origin}${isUCPopup.value ? '/?uc=' : ''}/transaction-history/withdrawal`,
      FailedUrl: window.location.href,
      BankName: "No Bank Code",
      PpId: getSelectedBank.value && getSelectedBank.value.PpId ? getSelectedBank.value.PpId : 1,
      WalletAddress: formData.walletAddress,
      PlayerBankAccountName: "CryptoCurrency",
      PlayerBankAccountNumber: "0",
      OnlineId: Number(localStorage.getItem("onlineId") ?? 0),
    } as IRequestWithdrawalInternetBank;
    const response = await apis.withdrawalInternetBank(request);
    if (response.isSuccessful) {
      notificationHelper.notification(
        response.Message,
        EnumMessageType.Success
      );
      const {isSubmitSuccess} = useCloseTelegramWebApp();
      isSubmitSuccess.value = true;
      setTimeout(() => {
        if (route.path === "/withdrawal/payment-bank/cryptocurrency") {
          router.push({
            path: "/transaction-history/withdrawal",
          });
        }
      }, 1000);
    } else {
      notificationHelper.notification(response.ErrorMessageForDisplay, EnumMessageType.Error);
    }
    isWithdrawProcessing.value = false;
  };
  const submitWithdrawal = formHelper.getSubmitFunction(withdrawal);

  const playerCreditBalance = ref(0);
  const getBalance = async () => {
    const response = await apis.getPlayerBalance();
    playerCreditBalance.value = response.Data.BetCredit;
  };
  getBalance();

  const checkBankAmount = () => {
    if (!IsDecimalAllow && /\./.test(`${formData.amount}`)) {
      return $t("decimal_point_not_allow");
    }
    if (validateAmount.value < withdrawalMin.value) {
      return (
        $t("your_withdraw_amount_must_be_bigger_than") + withdrawalMin.value
      );
    }
    if (
      withdrawalMax.value !== 0 &&
      validateAmount.value > withdrawalMax.value
    ) {
      return (
        $t("your_withdraw_amount_must_be_lower_than") + withdrawalMax.value
      );
    }
    if (validateAmount.value > playerCreditBalance.value) {
      return (
        $t("your_withdraw_amount_must_be_lower_than") +
        playerCreditBalance.value
      );
    }
    return "";
  };
  const checkPaymentPassword = () => {
    if (
      isPaymentPasswordEnable.value &&
      !/^[0-9]+$/.test(formData.paymentPassword)
    ) {
      return $t("invalid_password_format");
    }
    if (
      isPaymentPasswordEnable.value &&
      !/^\d{4}$/.test(formData.paymentPassword)
    ) {
      return $t("please_enter_4_digits");
    }
    return "";
  };
  const checkWithdrawalLimit = () => {
    if (
      isWithdrawalLimitEnabled.value &&
      remainingRollover.value !== "0.00" &&
      formData.amount > eligibleWithdrawal.value
    ) {
      return $t("withdrawal_requirement_must_be_zero");
    }
    return "";
  };
  const amountPlaceholder = computed(
    () =>
      `${$t("min")}: ${withdrawalMin.value} ${$t("max")}: ${
        withdrawalMax.value === 0 ? "--" : withdrawalMax.value
      }`
  );
  const rules = {
    amount: { customRule: checkBankAmount, required: true },
    paymentPassword: { customRule: checkPaymentPassword, required: true },
    walletAddress: { required: true },
    withdrawalLimit: { customRule: checkWithdrawalLimit },
  };
  const getRule = formHelper.getRules(rules, formData);
  return {
    selectCryptoCurrency,
    withdrawalMin: ref(
      numberFormatHelper.addAccountingFormat(withdrawalMin.value)
    ),
    withdrawalMax: ref(
      numberFormatHelper.addAccountingFormat(withdrawalMax.value)
    ),
    cryptocurrencyRadios,
    refreshTransactionFee,
    TransactionFee,
    submitWithdrawal,
    getRule,
    formData,
    ruleFormRef,
    isWithdrawProcessing,
    checkedCurrency,
    amountPlaceholder,
    remainingRollover,
    eligibleWithdrawal,
  };
};
