import { reactive, ref, computed, ComputedRef, Ref, watch } from "vue";
import _ from "lodash";
import { FormInstance } from "element-plus";

import {
  IGetTransactionBankListResponse,
  IAvailablePropertyListDto,
  IPlayerBankListAfterFirstTimeDepositDto,
} from "@/models/getTransactionBankListResponse";
import { IGetPaymentGatewayBanksResponse } from "@/models/getPaymentGatewayBanksRespone";
import { GetBalanceResponse } from "@/models/getBalanceResponse";
import apis from "@/utils/apis";
import { IRequestWithdrawalBankTransfer } from "@/models/requestWithdrawalBankTransfer";
import { ProfileDto } from "@/models/getProfileResponse";
import formHelper from "@/utils/elementUiHelpers/formHelper";
import notificationHelper from "@/utils/elementUiHelpers/notificationHelper";
import EnumMessageType from "@/models/enums/enumMessageType";
import numberFormatHelper from "@/utils/formatHelpers/numberFormatHelper";
import featuresToggleHelper from "@/utils/featuresToggleHelper";
import EnumThemePropertyKey from "@/models/enums/enumThemePropertyKey";
import { ThemePropertiesFromApiDto } from "@/models/companyThemePropertiesRespone";
import { store } from "@/store";

interface INormalBankTransferWithdrawal {
  customizeBankName: string;
  branchName: string;
  bankAccountNumber: string;
  bankAccountName: string;
  paymentPasswordValue: string;
  cpfNumber: string;
  bsb: string;
  payId: string;
}

interface INormalBankTransferWithdraw {
  isFirstTimeDeposit: Ref<boolean>;
  playerCreditBalance: ComputedRef<string>;
  withdrawalMin: Ref<string>;
  withdrawalMax: Ref<string>;
  bankRadios: any;
  selectedBank: Ref<string>;
  branchNameSetting: IAvailablePropertyListDto | undefined;
  isBankNameEnabled: ComputedRef<boolean>;
  submitWithdrawalBankTransfer: (formEl: FormInstance) => void;
  bankTransferWithdrawalFormRef: Ref<FormInstance | undefined>;
  bankTransferWithdrawalFormData: INormalBankTransferWithdrawal;
  isWithdrawProcessing: Ref<boolean>;
  bankTransferWithdrawalRules: any;
  checkedCurrency: ComputedRef<boolean>;
  remainingRollover: ComputedRef<string>;
  eligibleWithdrawal: ComputedRef<string>;
  amountPlaceholder: ComputedRef<string>;
  bankTransferWithdrawalTextInfo: ComputedRef<ThemePropertiesFromApiDto>;
  cpfNumberSetting: IAvailablePropertyListDto | undefined;
  bsbSetting: IAvailablePropertyListDto | undefined;
  payIdSetting: IAvailablePropertyListDto | undefined;
  bankAccountNumberSetting : IAvailablePropertyListDto | undefined;
  bankAccountNameSetting : IAvailablePropertyListDto | undefined;
  isFirstTimeBankAccountNameEmpty: ComputedRef<boolean>;
  isFirstTimeBankAccountNumberEmpty: ComputedRef<boolean>;
}

export const useNormalBankTransferWithdrawal = (
  normalBanks: Ref<IGetTransactionBankListResponse>,
  paymentBanks: Ref<IGetPaymentGatewayBanksResponse>,
  profile: Ref<ProfileDto>,
  isPaymentPasswordEnable: Ref<boolean>,
  isWithdrawalLimitEnabled: Ref<boolean>
): INormalBankTransferWithdraw => {
  const router = useRouter();
  const { $t } = useNuxtApp();
  const {
    PlayerBankListAfterFirstTimeDeposit,
    PlayerBankOptions,
    AvailablePropertyList,
    IsDecimalAllow,
  } = normalBanks.value;
  const { whiteListCurrencies } = useCurrencyWhitelist();
  const playerBankOptions = _.sortBy(PlayerBankOptions, [
    (bank) => bank.Priority,
  ]);
  const { WithdrawalLimit, AvailableWithdrawAmount } = paymentBanks.value;
  const isWithdrawProcessing = ref(false);
  const playerBalanceLoading = ref(true);
  const playerBalance = ref<GetBalanceResponse>({} as GetBalanceResponse);
  const getBalance = async () => {
    const response = await apis.getPlayerBalance();
    if (response.isSuccessful) {
      playerBalance.value = response.Data;
    } else {
      // TODO: Error Handle
    }
    playerBalanceLoading.value = false;
  };

  const isFirstTimeDeposit = ref(
    PlayerBankListAfterFirstTimeDeposit.length === 0
  );
  const isCustomizeBankNameRequired = computed(
    () =>
      _.find(AvailablePropertyList, { PropertyName: "CustomizeWithdrawalBank" })
        ?.PropertyStatus === "Required"
  );
  const banksForWithdrawal = computed(() => {
    let banks = [];
    banks = playerBankOptions.map((item) => ({
      PlayerBankName: item.BankName,
      PlayerBankAccountName: "",
      PlayerBankAccountNumber: "",
      BankLogo: item.BankLogo,
    }));
    return banks;
  });

  const bankRadios = computed(() => {
    const banks = isFirstTimeDeposit.value
      ? banksForWithdrawal.value
      : PlayerBankListAfterFirstTimeDeposit;
    if (
      banks.findIndex((option) => option.PlayerBankName === "Other") === -1 &&
      isCustomizeBankNameRequired.value
    ) {
      banks.push({
        PlayerBankName: "Other",
      } as IPlayerBankListAfterFirstTimeDepositDto);
    }
    return banks;
  });

  const userCurrency = computed(() => _.lowerCase(profile.value.Currency));
  const checkedCurrency = computed(
    () => userCurrency.value === "idr" || userCurrency.value === "vnd"
  );
  const selectedBank = ref(bankRadios.value[0].PlayerBankName);
  const branchNameSetting = _.find(AvailablePropertyList, {
    PropertyName: "BranchName",
  });
  const isBranchNameRequired = computed(
    () => branchNameSetting?.PropertyStatus === "Required"
  );
  const isBankNameEnabled = computed(() => selectedBank.value === "Other");
  const playerBankSelected = computed(() => _.find(PlayerBankOptions, {'BankName':selectedBank.value}));
  
  const whiteListedUserCurrency = computed(()=>whiteListCurrencies.value.filter(currency=>currency.Currency.toLowerCase() === userCurrency.value.toLowerCase()));
  const withdrawalMin = computed(() => {
    if (selectedBank.value === "Other"){
      if(whiteListedUserCurrency.value.length){
        return whiteListedUserCurrency.value[0].WithdrawalMin;
      }
    }else if (PlayerBankOptions.length !== 0) {
      return playerBankSelected.value?.WithdrawMin;
    }
    return 0;
  });
  const withdrawalMax = computed(() => {
    if (selectedBank.value === "Other") {
      if (whiteListedUserCurrency.value.length) {
        return whiteListedUserCurrency.value[0].WithdrawalMax;
      }
    } else if (PlayerBankOptions.length !== 0) {
      return playerBankSelected.value?.WithdrawMax;
    }
    return 0;
  });
  const remainingRollover = computed(() =>
    numberFormatHelper.addAccountingFormat(WithdrawalLimit)
  );
  const eligibleWithdrawal = computed(() =>
    numberFormatHelper.addAccountingFormat(AvailableWithdrawAmount)
  );
  const creditBalance = computed(() =>
    playerBalanceLoading.value ? 0 : playerBalance.value.BetCredit
  );
  // Helper function to find status of property and check whether that property is required or not
  function findProperty(propertyName: string) {
    const property = _.find(AvailablePropertyList, {
      PropertyName: propertyName,
    });
    const isRequired = computed(() => property?.PropertyStatus === "Required");
    return { property, isRequired };
  }
  const { property: cpfNumberSetting, isRequired: cpfNumberRequired } = findProperty("CPF");
  const { property: bsbSetting, isRequired: bsbRequired } = findProperty("BSB");
  const { property: payIdSetting, isRequired: payIdRequired } = findProperty("PayID");
  const { property: bankAccountNumberSetting, isRequired: isBankAccountNumberSettingRequired } = findProperty("BankAccountNumber");
  const { property: bankAccountNameSetting, isRequired: isBankAccountNameSettingRequired } = findProperty("BankAccountName");
  const bankTransferWithdrawalFormData = reactive({
    bankAccountName: bankRadios.value[0].PlayerBankAccountName,
    customizeBankName: "",
    branchName: "",
    bankAccountNumber: bankRadios.value[0].PlayerBankAccountNumber,
    amountValue: "",
    paymentPasswordValue: "",
    cpfNumber: "",
    bsb: "",
    payId: "",
  });

  watch(selectedBank, (__, oldValue) => {
    if (!isFirstTimeDeposit.value && oldValue === "Other") {
      bankTransferWithdrawalFormData.bankAccountName =
        bankRadios.value[0].PlayerBankAccountName;
      bankTransferWithdrawalFormData.bankAccountNumber =
        bankRadios.value[0].PlayerBankAccountNumber;
    }
  });
  const validateAmount = computed(() =>
    Number(bankTransferWithdrawalFormData.amountValue)
  );
  const bankTransferWithdrawalFormRef = ref<FormInstance>();
  const checkBankAmount = () => {
    if (
      !IsDecimalAllow &&
      /\./.test(`${bankTransferWithdrawalFormData.amountValue}`)
    ) {
      return $t("decimal_point_not_allow");
    }
    if (
      withdrawalMin.value !== 0 &&
      validateAmount.value < withdrawalMin.value
    ) {
      return (
        $t("your_withdraw_amount_must_be_bigger_than") + withdrawalMin.value
      );
    }
    if (validateAmount.value > creditBalance.value) {
      return (
        $t("your_withdraw_amount_must_be_lower_than") + creditBalance.value
      );
    }
    if (
      withdrawalMin.value !== 0 &&
      validateAmount.value > withdrawalMax.value
    ) {
      return (
        $t("your_withdraw_amount_must_be_lower_than") + withdrawalMax.value
      );
    }
    return "";
  };
  const paymentPasswordError = () => {
    if (
      isPaymentPasswordEnable.value &&
      !/^[0-9]+$/.test(bankTransferWithdrawalFormData.paymentPasswordValue)
    ) {
      return $t("invalid_password_format");
    }

    if (
      isPaymentPasswordEnable.value &&
      !/^\d{4}$/.test(bankTransferWithdrawalFormData.paymentPasswordValue)
    ) {
      return $t("please_enter_4_digits");
    }
    return "";
  };
  const checkWithdrawalLimit = () => {
    if (
      isWithdrawalLimitEnabled.value &&
      remainingRollover.value !== "0.00" &&
      bankTransferWithdrawalFormData.amountValue > eligibleWithdrawal.value
    ) {
      return $t("withdrawal_requirement_must_be_zero");
    }
    return "";
  };
  const amountPlaceholder = computed(
    () =>
      `${$t("min")}: ${withdrawalMin.value} ${$t("max")}: ${
        withdrawalMax.value
      }`
  );
  const bankTransferRules = {
    customizeBankName: { required: isCustomizeBankNameRequired.value },
    branchName: { required: isBranchNameRequired.value },
    amountValue: { customRule: checkBankAmount, required: true },
    paymentPasswordValue: { customRule: paymentPasswordError, required: true },
    bankAccountName: { required: isBankAccountNameSettingRequired.value },
    bankAccountNumber: { required: isBankAccountNumberSettingRequired.value },
    withdrawalLimit: { customRule: checkWithdrawalLimit },
    cpfNumber: { required: cpfNumberRequired.value },
    bsb: { required: bsbRequired.value },
    payId: { required: payIdRequired.value },
  };
  const bankTransferWithdrawalRules = formHelper.getRules(
    bankTransferRules,
    bankTransferWithdrawalFormData
  );
  const route = useRoute();
  const proceedWithdrawalBankTransfer = async () => {
    isWithdrawProcessing.value = true;
    const request: IRequestWithdrawalBankTransfer = {
      PlayerBankName: selectedBank.value,
      PlayerBankAccountName: bankTransferWithdrawalFormData.bankAccountName,
      PlayerBankAccountNumber: bankTransferWithdrawalFormData.bankAccountNumber,
      BranchName: bankTransferWithdrawalFormData.branchName,
      CustomizeBankName: bankTransferWithdrawalFormData.customizeBankName,
      CPFNumber: bankTransferWithdrawalFormData.cpfNumber,
      BSB: bankTransferWithdrawalFormData.bsb,
      PayId: bankTransferWithdrawalFormData.payId,
      Amount: Number(bankTransferWithdrawalFormData.amountValue),
      PaymentPassword: bankTransferWithdrawalFormData.paymentPasswordValue,
      Url: String(window.location),
      FingerPrint: store.state.visitorId,
      OnlineId: Number(localStorage.getItem("onlineId") ?? 0),
      Domain: commonHelper.getDomain(),
      Language: store.state.locale,
    };
    const response = await apis.withdrawalBankTransfer(request);
    if (response.isSuccessful) {
      notificationHelper.notification(
        response.Message,
        EnumMessageType.Success
      );
      const {isSubmitSuccess} = useCloseTelegramWebApp();
      isSubmitSuccess.value = true;
      setTimeout(() => {
        if (route.matched[0].name === "withdrawal") {
          router.push("/transaction-history/withdrawal");
        }
        if (route.fullPath.includes('uc=/withdrawal')) {
          router.push({
            path: route.path,
            query: {
              uc: "/transaction-history/withdrawal",
            }
          });
        }
      }, 1000);
    } else {
      notificationHelper.notification(response.ErrorMessageForDisplay, EnumMessageType.Error);
    }
    isWithdrawProcessing.value = false;
  };
  const submitWithdrawalBankTransfer = formHelper.getSubmitFunction(
    proceedWithdrawalBankTransfer
  );
  getBalance();
  const bankTransferWithdrawalTextInfo = computed(() =>
    featuresToggleHelper.getThemePropertyFromApi(
      EnumThemePropertyKey.HtmlId,
      "bank_transfer_withdrawal_text_info"
    )
  );
  const isFirstTimeBankAccountNameEmpty = computed(() => bankRadios.value[0].PlayerBankAccountName === "");
  const isFirstTimeBankAccountNumberEmpty = computed(() => bankRadios.value[0].PlayerBankAccountNumber === "");
  return {
    isFirstTimeDeposit,
    playerCreditBalance: computed(() =>
      numberFormatHelper.addAccountingFormat(creditBalance.value)
    ),
    withdrawalMin: ref(
      numberFormatHelper.addAccountingFormat(withdrawalMin.value)
    ),
    withdrawalMax: ref(
      numberFormatHelper.addAccountingFormat(withdrawalMax.value)
    ),
    bankRadios,
    selectedBank,
    branchNameSetting,
    isBankNameEnabled,
    submitWithdrawalBankTransfer,
    bankTransferWithdrawalFormRef,
    bankTransferWithdrawalFormData,
    bankTransferWithdrawalRules,
    isWithdrawProcessing,
    checkedCurrency,
    remainingRollover,
    eligibleWithdrawal,
    amountPlaceholder,
    bankTransferWithdrawalTextInfo,
    cpfNumberSetting,
    bsbSetting,
    payIdSetting,
    bankAccountNumberSetting,
    bankAccountNameSetting,
    isFirstTimeBankAccountNameEmpty,
    isFirstTimeBankAccountNumberEmpty
  };
};
