import { reactive, ref, Ref, computed, ComputedRef, watch } from "vue";

import _ from "lodash";
import { FormInstance } from "element-plus";
import { IGetPaymentGatewayBanksResponse } from "@/models/getPaymentGatewayBanksRespone";
import { GetBalanceResponse } from "@/models/getBalanceResponse";
import { IRequestWithdrawalInternetBank } from "@/models/requestWithdrawalBankTransfer";
import { ProfileDto } from "@/models/getProfileResponse";
import apis from "@/utils/apis";
import formHelper from "@/utils/elementUiHelpers/formHelper";
import notificationHelper from "@/utils/elementUiHelpers/notificationHelper";
import EnumMessageType from "@/models/enums/enumMessageType";
import numberFormatHelper from "@/utils/formatHelpers/numberFormatHelper";
import featuresToggleHelper from "@/utils/featuresToggleHelper";
import EnumThemePropertyKey from "@/models/enums/enumThemePropertyKey";
import { ThemePropertiesFromApiDto } from "@/models/companyThemePropertiesRespone";
import commonHelper from "@/utils/commonHelper";

interface IExtraParameter {
  pixnumber: string;
  pixtype: string;
  name: string;
  cpfnumber: string;
  branchcode: string;
}
interface IPixSelectOption {
  value: string;
  label: string;
}
interface IPaymentBankTransferWithDrawalFormData {
  bankAccountName: string;
  customizeBankName: string;
  branchName: string;
  bankAccountNumber: string;
  amountValue: number;
  paymentPassword: string;
  ExtraParameter?: IExtraParameter;
}
interface IPaymentBankTransferWithDrawal {
  bankRadios: any;
  playerCreditBalance: ComputedRef<string>;
  selectedBank: Ref<string>;
  withdrawalMin: ComputedRef<string>;
  withdrawalMax: ComputedRef<string>;
  submitWithdrawal: (formEl: FormInstance) => void;
  getRule: any;
  formData: IPaymentBankTransferWithDrawalFormData;
  ruleFormRef: Ref<FormInstance | undefined>;
  isWithdrawProcessing: Ref<boolean>;
  checkedCurrency: Ref<boolean>;
  remainingRollover: ComputedRef<string>;
  eligibleWithdrawal: ComputedRef<string>;
  isDecimalAllow: Ref<boolean>;
  amountPlaceholder: ComputedRef<string>;
  internetBankingWithdrawalTextInfo: ComputedRef<ThemePropertiesFromApiDto>;
  paymentWithdrawalTextInfo: ComputedRef<ThemePropertiesFromApiDto>;
  isFillInPaymentBank: ComputedRef<boolean>;
  getPlayerBank: Ref<string>;
  isAupay: Ref<boolean>;
  pixTypeOptions: Array<IPixSelectOption>;
  isGtrpay: Ref<boolean>;
  isGlobalPayJPY: Ref<boolean>;
  isHiddenForSpecificBank: () => boolean;
}

export const usePaymentBankTransferWithdrawal = (
  paymentBanks: Ref<IGetPaymentGatewayBanksResponse>,
  profile: Ref<ProfileDto>,
  isPaymentPasswordEnable: Ref<boolean>,
  isWithdrawalLimitEnabled: Ref<boolean>
): IPaymentBankTransferWithDrawal => {
  const { $t } = useNuxtApp();
  const router = useRouter();
  const route = useRoute();
  const RegisterFillInPaymentBank = ref(false);
  const {
    AvailableBanksForWithdrawal,
    PaymentMethodsForWithdrawal,
    WithdrawalLimit,
    IsDecimalAllow,
    AvailableWithdrawAmount,
  } = paymentBanks.value;
  const banksForCurrentPaymentMethod = computed(() =>
    AvailableBanksForWithdrawal.filter(
      (bank) => bank.PaymentMethod === route.params.method || route.query?.uc?.includes(bank.PaymentMethod)
    )
  );
  const uniqueBanksByMethod = computed(() =>
    _.uniqBy(banksForCurrentPaymentMethod.value, (bank) => bank.BankName)
  );
  const isFirstTimebanks = computed(() => {
    const result = uniqueBanksByMethod.value.map((item) => ({
      text: item.BankName,
      value: item.BankName,
    }));
    return result;
  });
  const bankRadios = reactive({
    name: "banks",
    radios: isFirstTimebanks,
  });
  const playerBalanceLoading = ref(true);
  const playerBalance = ref<GetBalanceResponse>({} as GetBalanceResponse);
  const getBalance = async () => {
    const response = await apis.getPlayerBalance();
    if (response.isSuccessful) {
      playerBalance.value = response.Data;
    } else {
      // TODO: Error Handle
    }
    playerBalanceLoading.value = false;
  };
  const userCurrency = computed(() => _.lowerCase(profile.value.Currency));
  const checkedCurrency = computed(
    () => userCurrency.value === "idr" || userCurrency.value === "vnd"
  );
  const selectedBank = ref(
    paymentBanks.value.PlayerPaymentWithdrawalBankInfo.BankName &&
      bankRadios.radios.some(
        (bank) =>
          bank.value ===
          paymentBanks.value.PlayerPaymentWithdrawalBankInfo.BankName
      )
      ? paymentBanks.value.PlayerPaymentWithdrawalBankInfo.BankName
      : bankRadios.radios[0]
        ? bankRadios.radios[0].value
        : ""
  );
  const amountValue = ref();
  const formData = reactive({
    bankAccountName: "PIX",
    customizeBankName: "",
    branchName: "",
    bankAccountNumber: "1234",
    amountValue,
    paymentPassword: "",
    ExtraParameter: { pixtype: "CPF" },
  });
  const isAupay = ref(false);
  // Get the last part of the URL
  const segments = window.location.href.split('/');
  const lastSegment = segments[segments.length - 1];
  // Assign the last URL to the lastUrl ref
  isAupay.value = lastSegment === 'pix';

  const checkDefaultSelectedBank = () => {
    if ( paymentBanks.value.PlayerPaymentWithdrawalBankInfo.BankName  !== undefined &&
      selectedBank.value.toLowerCase() ===
      paymentBanks.value.PlayerPaymentWithdrawalBankInfo.BankName.toLowerCase()
    ) {
      formData.bankAccountName =
        paymentBanks.value.PlayerPaymentWithdrawalBankInfo.BankAccountName;
      formData.bankAccountNumber =
        paymentBanks.value.PlayerPaymentWithdrawalBankInfo.BankAccountNumber;
    } else {
      formData.bankAccountName = "";
      formData.bankAccountNumber = "";
    }
  };
  checkDefaultSelectedBank();
  watch(
    bankRadios,
    () => {
      selectedBank.value = bankRadios.radios[0]
        ? bankRadios.radios[0].value
        : "";
    },
    { deep: true }
  );
  watch(selectedBank, () => {
    checkDefaultSelectedBank();
  });
  // const bankAccountName = ref("");
  // const bankAccountNumber = ref("");
  const getSelectedBank = computed(() =>
    AvailableBanksForWithdrawal.find(
      (bank) => bank.BankName === selectedBank.value && route.fullPath.includes(bank.PaymentMethod)
    )
  );

  // for new provider integration (GTRPAY), we need to enable field CPF in withdrawal page
  const isGtrpay = ref(false);
  // for provider globalpay, we need to enable field BranchCode in withdrawal page
  const isGlobalPayJPY = ref(false);

  watch(
    () => getSelectedBank.value,
    (value) => {
      if (value) {
        isGtrpay.value = value.PpId === 28; // 28 is provider id for GTRPAY
        isGlobalPayJPY.value =
          value.PpId === 32 && userCurrency.value === "jpy"; // 32 is provider id for GLOBALPAY
      }
    },
    { immediate: true }
  );
  const withdrawalMin = computed(() =>
    getSelectedBank.value !== undefined
      ? getSelectedBank.value.WithdrawalMin
      : 0
  );
  const withdrawalMax = computed(() =>
    getSelectedBank.value !== undefined
      ? getSelectedBank.value.WithdrawalMax
      : 0
  );
  const isUCPopup = computed(() => route.fullPath.includes('uc='));
  const paymentMethod = computed(() => {
    let method = route.params.method;
    if (isUCPopup && route.query.uc) {
      const ucUrlParts = String(route.query.uc).split("/").filter(part => part);
      if (ucUrlParts[2]) {
        method = ucUrlParts[2];
      }
    }
    return PaymentMethodsForWithdrawal.filter(
      (payment) => payment.PaymentMethod === method
    )
  });
  const remainingRollover = computed(() =>
    numberFormatHelper.addAccountingFormat(WithdrawalLimit)
  );
  const eligibleWithdrawal = computed(() =>
    numberFormatHelper.addAccountingFormat(AvailableWithdrawAmount)
  );
  const creditBalance = computed(() =>
    playerBalanceLoading.value ? 0 : playerBalance.value.BetCredit
  );

  const ruleFormRef = ref<FormInstance>();
  const validateAmount = computed(() => formData.amountValue);
  const checkBankAmount = () => {
    if (!IsDecimalAllow && /\./.test(`${formData.amountValue}`)) {
      return $t("decimal_point_not_allow");
    }
    if (validateAmount.value < withdrawalMin.value) {
      return (
        $t("your_withdraw_amount_must_be_bigger_than") + withdrawalMin.value
      );
    }
    if (
      withdrawalMax.value !== 0 &&
      validateAmount.value > withdrawalMax.value
    ) {
      return (
        $t("your_withdraw_amount_must_be_lower_than") + withdrawalMax.value
      );
    }
    if (validateAmount.value > creditBalance.value) {
      return (
        $t("your_withdraw_amount_must_be_lower_than") + creditBalance.value
      );
    }
    return "";
  };
  const checkPaymentPassword = () => {
    if (
      isPaymentPasswordEnable.value &&
      !/^[0-9]+$/.test(formData.paymentPassword)
    ) {
      return $t("invalid_password_format");
    }
    if (
      isPaymentPasswordEnable.value &&
      !/^\d{4}$/.test(formData.paymentPassword)
    ) {
      return $t("please_enter_4_digits");
    }
    return "";
  };
  const checkWithdrawalLimit = () => {
    if (
      isWithdrawalLimitEnabled.value &&
      remainingRollover.value !== "0.00" &&
      formData.amountValue > eligibleWithdrawal.value
    ) {
      return $t("withdrawal_requirement_must_be_zero");
    }
    return "";
  };
  const amountPlaceholder = computed(
    () =>
      `${$t("min")} : ${withdrawalMin.value} ${$t("max")}: ${withdrawalMax.value
      }`
  );
  const OPAYID =  35;
  const isHiddenForSpecificBank = () => {
    if (PaymentMethodsForWithdrawal[0].PaymentMethodId === OPAYID) {
      return false;
    }
    return true;
  }
  const rules = {
    amountValue: { customRule: checkBankAmount, required: true },
    paymentPassword: { customRule: checkPaymentPassword, required: true },
    bankAccountName: { required: isHiddenForSpecificBank() },
    bankAccountNumber: { required: isHiddenForSpecificBank() },
    withdrawalLimit: { customRule: checkWithdrawalLimit },
  };
  const getRule = formHelper.getRules(rules, formData);
  const isWithdrawProcessing = ref(false);
  const withdrawal = async () => {
    isWithdrawProcessing.value = true;
    const request = {
      Amount: formData.amountValue,
      PaymentPassword: formData.paymentPassword,
      BankCode: getSelectedBank.value && getSelectedBank.value.BankCode ? getSelectedBank.value.BankCode : selectedBank.value,
      PlayerBankId: 0,
      CryptoCurrencyNetwork: "USDT_TRC20",
      PlayerBankAccountName: formData.bankAccountName,
      PlayerBankAccountNumber: formData.bankAccountNumber,
      PaymentMethod: paymentMethod.value[0].PaymentMethodId,
      ReturnUrl: `${window.location.origin}${isUCPopup.value ? '/?uc=' : ''}/transaction-history/withdrawal`,
      FailedUrl: window.location.href,
      BankName: selectedBank.value,
      PpId: getSelectedBank.value && getSelectedBank.value.PpId ? getSelectedBank.value.PpId : 1,
      WalletAddress: "",
      OnlineId: Number(localStorage.getItem("onlineId") ?? 0),
      ExtraParameter: formData.ExtraParameter,
    } as IRequestWithdrawalInternetBank;
    const response = await apis.withdrawalInternetBank(request);
    if (response.isSuccessful) {
      notificationHelper.notification(
        response.Message,
        EnumMessageType.Success
      );
      const {isSubmitSuccess} = useCloseTelegramWebApp();
      isSubmitSuccess.value = true;
      setTimeout(() => {
        router.push("/transaction-history/withdrawal");
      }, 1000);
    } else {
      notificationHelper.notification(response.ErrorMessageForDisplay, EnumMessageType.Error);
    }
    isWithdrawProcessing.value = false;
  };
  const submitWithdrawal = formHelper.getSubmitFunction(withdrawal);
  getBalance();
  const internetBankingWithdrawalTextInfo = computed(() =>
    featuresToggleHelper.getThemePropertyFromApi(
      EnumThemePropertyKey.HtmlId,
      "internet_banking_withdrawal_text_info"
    )
  );
  const paymentWithdrawalTextInfo = computed(() =>
    featuresToggleHelper.getThemePropertyFromApi(
      EnumThemePropertyKey.HtmlId,
      "payment_withdrawal_text_info"
    )
  );
  const getCustomerSetting = async () => {
    const response = await commonHelper.getCustomerSettingById(
      "RegisterFillInPaymentBank"
    );
    RegisterFillInPaymentBank.value = response && response.Value === "Y";
  };
  getCustomerSetting();
  const isFillInPaymentBank = computed(() => RegisterFillInPaymentBank.value);
  const getPlayerBank = ref(
    paymentBanks.value.PlayerPaymentWithdrawalBankInfo.BankName
  );
  const pixTypeOptions: Array<IPixSelectOption> = [
    {
      value: "CPF",
      label: "CPF",
    },
    {
      value: "CNPJ",
      label: "CNPJ",
    },
    {
      value: "PHONE",
      label: "PHONE",
    },
    {
      value: "EMAIL",
      label: "EMAIL",
    },
    {
      value: "EVP",
      label: "EVP",
    },
  ];

  return {
    bankRadios,
    playerCreditBalance: computed(() =>
      numberFormatHelper.addAccountingFormat(creditBalance.value)
    ),
    selectedBank,
    withdrawalMin: computed(() =>
      numberFormatHelper.addAccountingFormat(withdrawalMin.value)
    ),
    withdrawalMax: computed(() =>
      withdrawalMax.value === 0
        ? "--"
        : numberFormatHelper.addAccountingFormat(withdrawalMax.value)
    ),
    submitWithdrawal,
    getRule,
    formData,
    ruleFormRef,
    isWithdrawProcessing,
    checkedCurrency,
    remainingRollover,
    eligibleWithdrawal,
    isDecimalAllow: ref(IsDecimalAllow),
    amountPlaceholder,
    internetBankingWithdrawalTextInfo,
    paymentWithdrawalTextInfo,
    isFillInPaymentBank,
    getPlayerBank,
    isAupay,
    pixTypeOptions,
    isGtrpay,
    isGlobalPayJPY,
    isHiddenForSpecificBank,
  };
};
