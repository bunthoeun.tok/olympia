import { ref, computed, reactive, Ref, ComputedRef } from "vue";
import _ from "lodash";
import type { FormInstance } from "element-plus";
import { IGetPaymentGatewayBanksResponse, IPaymentMethodsForDepositAndWithdrawal } from "@/models/getPaymentGatewayBanksRespone";
import { IGetTransactionBankListResponse } from "@/models/getTransactionBankListResponse";
import { IRequestSetPaymentPassword } from "@/models/requestWithdrawalBankTransfer";
import apis from "@/utils/apis";
import formHelper from "@/utils/elementUiHelpers/formHelper";
import EnumMessageType from "@/models/enums/enumMessageType";
import notificationHelper from "@/utils/elementUiHelpers/notificationHelper";
import useProfile from "@/composables/useProfile";
import { ProfileDto } from "@/models/getProfileResponse";
import featuresToggleHelper from "@/utils/featuresToggleHelper";
import { store } from "@/store";
import commonHelper from "@/utils/commonHelper";

interface IWithdrawalFormdata {
  paymentPassword: string;
  confirmPaymentPassword: string;
  loginPassword: string;
}
interface IWithdrawal {
  paymentBanks: Ref<IGetPaymentGatewayBanksResponse>;
  normalBanks: Ref<IGetTransactionBankListResponse>;
  isNormalBankMethodValible: ComputedRef<boolean>;
  isPaymentBankMethodValible: ComputedRef<boolean>;
  paymentPasswordLabel: ComputedRef<string>;
  confirmPaymentPasswordLabel: ComputedRef<string>;
  loginPasswordLabel: ComputedRef<string>;
  alertMessage: ComputedRef<string>;
  formData: IWithdrawalFormdata;
  isShowPaymentPasswordDialog: ComputedRef<boolean>;
  submitWithdrawal: (formEl: FormInstance) => void;
  getRule: any;
  ruleFormRef: Ref<FormInstance | undefined>;
  isPaymentPasswordEnable: ComputedRef<boolean>;
  profile: Ref<ProfileDto>;
  isWithdrawalLimitEnabled: ComputedRef<boolean>;
  isLoading: ComputedRef<boolean>;
  isBankMethodAvailabled: ComputedRef<boolean>;
  onCancel: () => void;
  isUfun: ComputedRef<boolean>;
}

export const useWithdrawal = (): IWithdrawal => {
  const router = useRouter();
  const { $t } = useNuxtApp();
  const route = useRoute();
  const { profile } = useProfile();
  const paymentBanks = ref<IGetPaymentGatewayBanksResponse>(
    {} as IGetPaymentGatewayBanksResponse
  );
  const normalBanks = ref<IGetTransactionBankListResponse>(
    {} as IGetTransactionBankListResponse
  );
  const isPaymentBankWithdrawalLoading = ref(true);
  const isNormalBankWithdrawalLoading = ref(true);
  const withdrawalLimitEnabled = ref(false);
  const isCustomerSettingLoading = ref(true);
  const paymentPasswordLabel = computed(() => $t("payment_password"));
  const confirmPaymentPasswordLabel = computed(() =>
    $t("confrim_payment_password")
  );
  const loginPasswordLabel = computed(() => $t("login_password"));
  const alertMessage = computed(() =>
    $t(
      "payment_password_will_use_to_verify_your_identity_for_any_fund_related_operation_for_account_safety_purposes_please_enter_4_digits"
    )
  );
  enum LoginPasswordLength {
    minLength = 6,
    maxLength = 20,
  }
  const getTransactionBankList = async () => {
    const response = await apis.getTransactionBankList("WithdrawalPage");
    if (response.isSuccessful) {
      normalBanks.value = response.Data;
      isNormalBankWithdrawalLoading.value = false;
    } else {
      // TODO: Error Handle
    }
  };
  const getCustomerSetting = () => {
    const response = commonHelper.getCustomerSettingById(
      "IsWithdrawalLimitEnabled"
    );
    withdrawalLimitEnabled.value = response && response.Value === "Y";
    isCustomerSettingLoading.value = false;
  };
  const { isUsingPaymentGateway } = featuresToggleHelper;
  const usingPaymentGateway = ref(false);
  const getPaymentGatewayBanks = async () => {
    const response = await apis.getPaymentGatewayBanks();
    usingPaymentGateway.value = await isUsingPaymentGateway();
    if (response.isSuccessful) {
      paymentBanks.value = response.Data;
       //check if the PaymentMethodsForWithdrawal Display Order are  all 0 so will order by alphabet
       if (_.every(paymentBanks.value.PaymentMethodsForWithdrawal, { DisplayOrder: 0 })) {
        const allZero = _.orderBy(paymentBanks.value.PaymentMethodsForWithdrawal, 'PaymentMethod', 'asc');
        paymentBanks.value.PaymentMethodsForWithdrawal = allZero;
      } else {
        const sortedByASC = _.orderBy(paymentBanks.value.PaymentMethodsForWithdrawal, 'DisplayOrder', ['asc', 'desc']);
        const duplicatesOfZero = _.filter(sortedByASC, { DisplayOrder: 0 });
        if (duplicatesOfZero) {
          const sortedDuplicatesOfZero = _.orderBy(duplicatesOfZero, (payment: IPaymentMethodsForDepositAndWithdrawal) => payment.PaymentMethod.toLowerCase(), 'asc');
          const filteredSortedByASC = _.reject(sortedByASC, { DisplayOrder: 0 });
          const finalSorted = [...sortedDuplicatesOfZero, ...filteredSortedByASC];
          paymentBanks.value.PaymentMethodsForWithdrawal = finalSorted;
        } else {
          paymentBanks.value.PaymentMethodsForWithdrawal = sortedByASC;
        }
      paymentBanks.value.PaymentMethodsForWithdrawal = _.orderBy(response.Data.PaymentMethodsForWithdrawal, 'DisplayOrder', ['asc', 'desc'])
      }
      isPaymentBankWithdrawalLoading.value = false;
      const isUCPopup = route.fullPath.includes('uc=/withdrawal');
      if (route.name === "withdrawal" || route.query.uc === '/withdrawal' || route.query.uc === '/withdrawal/') {
        const selectedPaymentMethod = computed(() =>
          _.first(paymentBanks.value.AvailableBanksForWithdrawal)
        );
        const isTransferMethodWithdrawalEnabled = computed(
          () => paymentBanks.value.IsTransferMethodWithdrawalEnable
        );
        const depositPath =
          isTransferMethodWithdrawalEnabled.value &&
          selectedPaymentMethod.value && !isUfun.value
            ? `/withdrawal/payment-bank/${selectedPaymentMethod.value.PaymentMethod}`
            : "/withdrawal/bank-transfer";
        if (isUCPopup) {
          router.push({
            path: route.path,
            query: {
              uc: depositPath,
            },
          });
        } else {
          router.push({
            path: depositPath,
          });
        }
      }
    else if (route.name === "telegram-withdrawal") {
      const selectedPaymentMethod = computed(() =>
          _.first(paymentBanks.value.AvailableBanksForWithdrawal)
        );
        const isTransferMethodWithdrawalEnabled = computed(
          () => paymentBanks.value.IsTransferMethodWithdrawalEnable
        );
        const depositPath =
          isTransferMethodWithdrawalEnabled.value && selectedPaymentMethod.value
            ? `/telegram/withdrawal/payment-bank/${selectedPaymentMethod.value.PaymentMethod}`
            : "/telegram/withdrawal/bank-transfer";
      router.push({
        path: depositPath,
      });
    }
    } else {
      // TODO: Error Handle
    }
  };
  const isCustomizeBankNameEnabled = computed(() =>
    _.some(normalBanks.value.AvailablePropertyList, {
      PropertyName: "CustomizeBankName",
    })
  );
  const isNormalBankMethodValible = computed(
    () =>
      normalBanks.value.IsTransferMethodWithdrawalEnable &&
      normalBanks.value.CompanyBanks.length > 0 &&
      (normalBanks.value.PlayerBankOptions.length > 0 ||
        (isCustomizeBankNameEnabled.value &&
          normalBanks.value.PlayerBankOptions.length === 0))
  );
  const isPaymentBankMethodValible = computed(() => {
    const bank = paymentBanks.value;
    return (
      bank.IsTransferMethodWithdrawalEnable &&
      paymentBanks.value.PaymentMethodsForWithdrawal.length > 0 &&
      usingPaymentGateway.value
    );
  });
  const isBankMethodAvailabled = computed(
    () => isPaymentBankMethodValible.value || isNormalBankMethodValible.value
  );
  const formData = reactive({
    paymentPassword: "",
    confirmPaymentPassword: "",
    loginPassword: "",
  });
  const withdrawal = async () => {
    const request = {
      PaymentPassword: formData.paymentPassword,
      LoginPassword: formData.loginPassword,
    } as IRequestSetPaymentPassword;
    const response = await apis.setPaymentPassword(request);
    if (response.isSuccessful) {
      notificationHelper.notification(
        response.Message,
        EnumMessageType.Success
      );
      store.commit("updateIsPaymentPasswordSet", true);
      window.location.reload();
    } else {
      notificationHelper.notification(response.Message, EnumMessageType.Error);
    }
  };
  const isWithdrawalLimitEnabled = computed(() => withdrawalLimitEnabled.value);
  const isPaymentPasswordEnable = computed(
    () => paymentBanks.value.IsPaymentPasswordEnable
  );
  const isPaymentPasswordSet = computed(
    () => paymentBanks.value.IsPaymentPasswordSet
  );
  const isShowPaymentPasswordDialog = computed(
    () => isPaymentPasswordEnable.value && !isPaymentPasswordSet.value
  );
  const checkPaymentPassword = () => {
    if (!/^[0-9]+$/.test(formData.paymentPassword)) {
      return $t("invalid_password_format");
    }
    if (!/^\d{4}$/.test(formData.paymentPassword)) {
      return $t("please_enter_4_digits");
    }
    if (!formData.paymentPassword) {
      return $t("payment_password_required");
    }
    return "";
  };
  const checkConfirmPaymentPassword = () => {
    if (!/^[0-9]+$/.test(formData.confirmPaymentPassword)) {
      return $t("invalid_password_format");
    }
    if (!/^\d{4}$/.test(formData.confirmPaymentPassword)) {
      return $t("please_enter_4_digits");
    }
    if (!formData.confirmPaymentPassword) {
      return $t("confirm_payment_password_required");
    }
    if (formData.confirmPaymentPassword !== formData.paymentPassword) {
      return $t("password_does_not_match");
    }
    return "";
  };
  const checkLoginPassword = () => {
    if (
      formData.loginPassword.length < LoginPasswordLength.minLength ||
      formData.loginPassword.length > LoginPasswordLength.maxLength
    ) {
      return $t("passwordlenght");
    }
    if (!/^[A-Za-z0-9]+$/.test(formData.loginPassword)) {
      return $t("password_only_letter_number");
    }
    if (!/[A-Za-z]/.test(formData.loginPassword)) {
      return $t("passwordlater");
    }
    if (!/\d/.test(formData.loginPassword)) {
      return $t("passwordnumber");
    }
    return "";
  };
  const ruleFormRef = ref<FormInstance>();
  const rules = {
    paymentPassword: { customRule: checkPaymentPassword, required: true },
    confirmPaymentPassword: {
      customRule: checkConfirmPaymentPassword,
      required: true,
    },
    loginPassword: { customRule: checkLoginPassword, required: true },
  };
  const getRule = formHelper.getRules(rules, formData);
  const submitWithdrawal = formHelper.getSubmitFunction(withdrawal);
  getPaymentGatewayBanks();
  getTransactionBankList();
  getCustomerSetting();
  const isLoading = computed(
    () =>
      isPaymentBankWithdrawalLoading.value ||
      isNormalBankWithdrawalLoading.value ||
      isCustomerSettingLoading.value
  );
  const onCancel = () => {
    router.push("/");
  };
  const env = () => useRuntimeConfig().public.CURRENTLY_ENV;
  const isUfun = computed(
    () => env() === "production" && store.state.webId === 3
  );
  return {
    paymentBanks,
    normalBanks,
    isNormalBankMethodValible,
    isPaymentBankMethodValible,
    paymentPasswordLabel,
    confirmPaymentPasswordLabel,
    loginPasswordLabel,
    alertMessage,
    formData,
    isShowPaymentPasswordDialog,
    submitWithdrawal,
    getRule,
    ruleFormRef,
    isPaymentPasswordEnable,
    profile,
    isWithdrawalLimitEnabled,
    isLoading,
    isBankMethodAvailabled,
    onCancel,
    isUfun,
  };
};
