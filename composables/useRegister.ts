import {
  computed,
  reactive,
  Ref,
  ref,
  UnwrapRef,
  watch,
  ComputedRef,
} from "vue";
import { FormInstance } from "element-plus";
import _ from "lodash";
import { differenceInSeconds } from "date-fns";
import useCurrencyWhitelist from "./useCurrencyWhitelist";
import { useCaptcha } from "./captcha/useCaptcha";
import apis from "@/utils/apis";
import { IAvailablePropertyListDto } from "@/models/getTransactionBankListResponse";
import { IPlayerBankOption } from "@/models/bank";
import formHelper, { IRule } from "@/utils/elementUiHelpers/formHelper";
import formRulesHelper from "@/utils/elementUiHelpers/formRulesHelper";
import { store } from "@/store";
import cookieHelper from "@/utils/cookieHelper";
import { IRegisterRequest, GetRegisterResponse } from "@/models/register";
import { ICompanyFlowSettingDto } from "@/models/getProfileResponse";
import notificationHelper from "@/utils/elementUiHelpers/notificationHelper";
import EnumMessageType from "@/models/enums/enumMessageType";
import { IThemePropertiesFromApi } from "@/models/companyThemePropertiesRespone";
import featuresToggleHelper from "@/utils/featuresToggleHelper";
import EnumThemePropertyKey from "@/models/enums/enumThemePropertyKey";
import { ICurrencies } from "@/models/login";
import commonHelper from "@/utils/commonHelper";
import { IAllCustomerSettingsData } from "~~/models/getCustomerSettingResponse";

export interface ILandingPageLogin {
  Username: string;
  Password: string;
  LandingPageRedirectUrl?: string;
}

export interface IFormModel {
  Username: string;
  Password: string;
  ConfirmPassword: string;
  Currency: string;
  FullName: string;
  Referral: string;
  Gender: string;
  Email: string;
  DateOfBirth: string;
  Nationality: string;
  Phone: string;
  Mobile: string;
  Line: string;
  PersonalSecurityCode: string;
  FirstName: string;
  LastName: string;
  Bank: string;
  BankAccountNumber: string;
  BankAccountName: string;
  Captcha: string;
  Recaptcha: string;
  AgentName: string;
  CountryCode: string;
  OtpVerification: string;
  PhoneNumberCode: number;
}

interface IUseRegister {
  availableProperties: Ref<Array<IAvailablePropertyListDto>>;
  bankOptions: Ref<Array<IPlayerBankOption>>;
  currencies: Ref<Array<ICurrencies>>;
  formModel: UnwrapRef<IFormModel>;
  formRef: Ref<FormInstance | undefined>;
  formRules: any;
  onRegister: (formEl: FormInstance) => void;
  getCaptchaErrorMsg: (valid: any) => void;
  clientCaptcha: Ref<IThemePropertiesFromApi>;
  reCaptcha: Ref<IThemePropertiesFromApi>;
  reCaptchaSiteKey: string;
  onVerify: (res: string) => void;
  onRecaptchaError: () => void;
  isVerified: Ref<boolean>;
  referralQuery: Ref<string>;
  cashAgentQuery: string;
  isLoading: Ref<boolean>;
  captchaErrorMsg: Ref<string>;
  rules: Record<string, IRule>;
  nextRequestOtpInSeconds: Ref<number>;
  isGettingOtpCode: Ref<boolean>;
  onGetVerificationCode: () => void;
  onTelegramRegister: (formEl: FormInstance) => void;
  onRegisterFromLanding: (formEl: FormInstance) => void;
  landingPageFormRule: ComputedRef<Record<string, IRule>>;
  onToggleIsShowPassword: () => void;
  isShowPassword: Ref<boolean>;
}

enum EnumPropertyStatus {
  Optional = "Optional",
  Required = "Required",
}

export const isOTPEnabled = () => {
  const customerSettings = useState<IAllCustomerSettingsData[]>(
    KEYS.ALL_CUSTOMER_SETTINGS
  );
  return (
    customerSettings.value
      .find((v) => v.Id === "IsOtpVerificationEnable")
      ?.Value?.toLowerCase() === "y"
  );
};

export default function useRegister(): IUseRegister {
  const { $t } = useNuxtApp();
  const isLoading = ref(false);
  const formRef = ref<FormInstance>();
  const route = useRoute();
  const isTelegramRegister = computed(
    () => route.path === "/telegram-register"
  );
  const isLandingPage = computed(() => useState("isLandingPage").value);
  const formModel = reactive<IFormModel>({
    Username: "",
    ConfirmPassword: "",
    Currency: "",
    Password: "",
    FullName: "",
    Referral: "",
    Gender: "",
    Email: "",
    DateOfBirth: "",
    Nationality: "",
    Phone: "",
    Mobile: "",
    Line: "",
    PersonalSecurityCode: "",
    FirstName: "",
    LastName: "",
    Bank: "",
    BankAccountNumber: "",
    BankAccountName: "",
    Captcha: "",
    Recaptcha: "",
    AgentName: "",
    CountryCode: "",
    OtpVerification: "",
    PhoneNumberCode: 0,
  });
  if (
    isTelegramRegister.value &&
    route.query.username !== undefined &&
    route.query.phone !== undefined
  ) {
    formModel.Username = String(route.query.username);
    formModel.Phone = String(route.query.phone);
  }
  const { getThemePropertyFromApi, kwaiMetaTrigger, EnumKwaiMeta } =
    featuresToggleHelper;
  const { captchaErrorMsg, getCaptchaErrorMsg } = useCaptcha(formModel);
  const isMobile = !useIsDesktop();
  const template = isMobile ? "mobile" : "desktop";
  const isVerified = ref(false);
  const landingPageFormRule = computed<Record<string, IRule>>(() =>
    formHelper.getRules(
      {
        Username: {
          required: true,
          customRule: () => formRulesHelper.userNameFormat(formModel.Username),
        },
        Password: {
          required: true,
          customRule: () => formRulesHelper.passwordFormat(formModel.Password),
        },
      },
      formModel
    )
  );
  const onVerify = (res: string) => {
    if (res.length) {
      isVerified.value = true;
      formModel.Recaptcha = res;
    }
  };
  const onRecaptchaError = () => {
    isVerified.value = false;
    formModel.Recaptcha = "";
  };
  const rules: Record<string, IRule> = reactive({
    Username: {
      required: true,
      customRule: () => formRulesHelper.userNameFormat(formModel.Username),
    },
    Password: {
      required: true,
      customRule: () => formRulesHelper.passwordFormat(formModel.Password),
    },
    ConfirmPassword: {
      required: true,
      customRule: () =>
        formRulesHelper.confirmPasswordFormat(
          formModel.Password,
          formModel.ConfirmPassword
        ),
    },
    Currency: { required: true, isTriggerByChange: true },
    FullName: { required: false },
    Referral: { required: true },
    Gender: { required: false, isTriggerByChange: true },
    Email: {
      required: false,
      customRule: () => {
        if (rules.Email.required || formModel.Email.length > 0) {
          return formRulesHelper.emailFormat(formModel.Email);
        }
        return "";
      },
    },
    DateOfBirth: { required: false },
    Nationality: { required: false },
    Phone: {
      required: false,
      customRule: () => {
        return formRulesHelper.phoneValidation(formModel.Phone);
      },
    },
    Mobile: {
      required: false,
      customRule: () => {
        return formRulesHelper.numbersOnly(formModel.Mobile);
      },
    },
    Line: { required: false },
    PersonalSecurityCode: { required: false },
    FirstName: { required: false },
    LastName: { required: false },
    Bank: { required: false, isTriggerByChange: true },
    BankAccountNumber: { required: false },
    BankAccountName: { required: false },
    Captcha: { required: true, customRule: () => captchaErrorMsg.value },
    Recaptcha: { required: true, isTriggerByChange: true },
    OtpVerification: { required: true },
  });
  const formRules = computed(() => formHelper.getRules(rules, formModel));
  const availableProperties = ref<Array<IAvailablePropertyListDto>>([]);
  const currencies = ref<Array<ICurrencies>>([]);
  const getCompanyFlowSettings = async () => {
    if (isLandingPage.value) return;
    const availablePropertyResponse = await apis.getCompanyFlowSetting();
    if (availablePropertyResponse.isSuccessful) {
      availablePropertyResponse.Data.AvailablePropertyList.forEach(
        (property) => {
          rules[property.PropertyName.trim()].required =
            property.PropertyStatus === EnumPropertyStatus.Required;
        }
      );
      availableProperties.value =
        availablePropertyResponse.Data.AvailablePropertyList;
    }
  };
  getCompanyFlowSettings();
  const clientCaptcha = computed(() =>
    getThemePropertyFromApi(EnumThemePropertyKey.HtmlId, "register_captcha_two")
  );
  const reCaptcha = computed(() =>
    getThemePropertyFromApi(EnumThemePropertyKey.HtmlId, "register_captcha")
  );
  const siteKey = computed(() =>
    getThemePropertyFromApi(
      EnumThemePropertyKey.HtmlId,
      "register_captcha_site_key"
    )
  );
  // const reCaptchaSiteKey = computed(() => (siteKey.value.Text !== '' ? siteKey.value.Text : '6Le-cnAgAAAAAO_P5tRNhK38t42pab0traAIP9nv'));
  const reCaptchaSiteKey = siteKey.value.Text;
  const requestUrl = window.top
    ? window.top?.location.search
    : window.location.search;
  const cashAgentParams = new URLSearchParams(requestUrl).get("ca");
  const cashAgentQuery =
    cashAgentParams ?? cookieHelper.getCookie("vue_agent_name") ?? "";
  const referralParams =
    new URLSearchParams(requestUrl).get("referral") ??
    new URLSearchParams(requestUrl).get("ref");
  const referralQuery = ref(
    referralParams ?? cookieHelper.getCookie("vue_referral") ?? ""
  );
  const { whiteListCurrencies } = useCurrencyWhitelist();
  if (referralQuery.value) {
    formModel.Referral = referralQuery.value.toString();
  }
  if (cashAgentQuery) {
    formModel.AgentName = cashAgentQuery.toString();
  }
  const getCurrencyByUsername = async (username: string) => {
    const CACurrencyResponse = await apis.getAccountCurrencyByUsername(
      username
    );
    if (CACurrencyResponse.isSuccessful) {
      const { Currency } = CACurrencyResponse.Data;
      formModel.Currency = Currency.trim() !== "*" ? Currency : "AUD";
      currencies.value = [];
    }
    return CACurrencyResponse.ErrorCode;
  };
  const isAgentDomain = ref(false);
  const getCurrencyByAgentDomain = async () => {
    const CACurrencyResponse = await apis.getAccountCurrencyByDomain();
    if (CACurrencyResponse.isSuccessful) {
      const { Currency } = CACurrencyResponse.Data;
      formModel.Currency = Currency.trim() !== "*" ? Currency : "AUD";
      currencies.value = [];
      isAgentDomain.value = true;
    }
    if (CACurrencyResponse.ErrorCode === 1009) {
      isAgentDomain.value = false;
    }
  };
  if (!isLandingPage.value) {
    getCurrencyByAgentDomain();
    if (cashAgentQuery && !isAgentDomain.value) {
      getCurrencyByUsername(cashAgentQuery);
    } else if (referralQuery.value && !isAgentDomain.value) {
      getCurrencyByUsername(referralQuery.value).then((data) => {
        if (data === 1009) {
          notificationHelper.notification(
            $t("referral_code_is_invalid"),
            EnumMessageType.Error
          );
          cookieHelper.setCookie("vue_referral", "", 600);
          formModel.Referral = "";
          referralQuery.value = "";
          formModel.Currency = String(
            whiteListCurrencies.value.length === 1
              ? whiteListCurrencies.value[0].Currency
              : ""
          );
          currencies.value = whiteListCurrencies.value;
        } else if (data !== 0) {
          notificationHelper.notification(
            `something when wrong! err_${data}`,
            EnumMessageType.Error
          );
        }
      });
    } else {
      formModel.Currency = String(
        whiteListCurrencies.value.length === 1 || isTelegramRegister.value
          ? whiteListCurrencies.value[0].Currency
          : ""
      );
      currencies.value = whiteListCurrencies.value;
    }
  }
  const handleResetBankInformation = (bankOption: IPlayerBankOption) => {
    if (bankOption !== undefined) {
      formModel.Bank = bankOption.Id === 0 ? "" : String(bankOption.Id);
      formModel.BankAccountNumber = "";
      formModel.BankAccountName = "";
    } else {
      formModel.Bank = "";
      formModel.BankAccountNumber = "";
      formModel.BankAccountName = "";
    }
  };
  const bankOptions = ref<Array<IPlayerBankOption>>([]);
  const getAvailableBanks = async () => {
    const { Currency } = formModel;
    if (isLandingPage.value) return;
    const bankResponse = await apis.getRegisterBankList(Currency);
    if (bankResponse.isSuccessful) {
      const { PlayerBankOptions } = bankResponse.Data;
      if (rules.Bank.required) {
        bankOptions.value = PlayerBankOptions;
      } else {
        const optionalOption: IPlayerBankOption = {
          Id: 0,
          BankName: "--",
          Priority: 0,
        };
        bankOptions.value = [optionalOption].concat(PlayerBankOptions);
      }
      handleResetBankInformation(bankOptions.value[0]);
    }
  };
  const ensureValidUrl = (url?: string) => {
    if (!url) return "";
    if (/^(http|https):\/\//i.test(url)) return url;
    const { protocol } = new URL(window.location.origin);
    return `${protocol}//${url}`;
  };
  const getSearchQuery = (params: Record<string, string | number>) => {
    return Object.keys(params)
      .map((key) => `${key}=${params[key]}`)
      .join("&");
  };
  const handleRedirectFromLandingPage = (payload: ILandingPageLogin) => {
    const { LandingPageRedirectUrl, Username, Password } = payload;
    if (!LandingPageRedirectUrl) return;
    const url = new URL(ensureValidUrl(LandingPageRedirectUrl));
    const searchParams = {
      flp: btoa(`${Username}:${Password}`),
    };
    window.location.replace(`${url.origin}?${getSearchQuery(searchParams)}`);
  };
  const isShowPassword = ref(false);
  const onToggleIsShowPassword = () => {
    isShowPassword.value = !isShowPassword.value;
  };
  const register = async (isFromLandingPage = false) => {
    isLoading.value = true;
    const {
      Username,
      Password,
      Currency,
      Referral,
      PhoneNumberCode = 0,
    } = formModel;
    // Check this because when register from landing page, we will not enable OTP even the option is enabled.
    const isOtpEnabled = !isFromLandingPage && isOTPEnabled();
    let CompanyFlowSettings: Array<ICompanyFlowSettingDto> =
      availableProperties.value.map((property) => {
        const propValue = formModel[property.PropertyName as keyof IFormModel];
        let PropertyValue = propValue === "0" ? "" : propValue;
        if (
          isOtpEnabled &&
          property.PropertyName === "Phone" &&
          propValue !== "0"
        ) {
          PropertyValue = getPhoneNumber();
        }
        return {
          PropertyName: property.PropertyName,
          PropertyValue,
        };
      });
    if (cashAgentQuery.length) {
      CompanyFlowSettings.push({
        PropertyName: "AgentName",
        PropertyValue: formModel.AgentName,
      });
    }
    if (isOtpEnabled) {
      CompanyFlowSettings.push({
        PropertyName: "CountryCode",
        PropertyValue: formModel.CountryCode,
      });
      CompanyFlowSettings.push({
        PropertyName: "OtpVerification",
        PropertyValue: formModel.OtpVerification,
      });
    }
    if (isAgentDomain.value) {
      CompanyFlowSettings.push({
        PropertyName: "AgentName",
        PropertyValue: "",
      });
    }
    const url = new URL(window.location.href);
    if (!cashAgentQuery) {
      url.port = "";
    }
    if (isFromLandingPage) {
      CompanyFlowSettings = CompanyFlowSettings.filter(
        (setting) => !!setting.PropertyValue
      );
    }
    const request: IRegisterRequest = {
      Domain: commonHelper.getDomain(),
      Username,
      Password,
      Currency: isFromLandingPage ? "VND" : Currency,
      CompanyFlowSettings,
      Referral,
      Url:
        cashAgentQuery || isAgentDomain.value
          ? commonHelper.getDomain() + window.location.pathname
          : url.toString(),
      Ip: store.state.geography?.Ip,
      LoginCountry: store.state.geography?.CountryCode ?? "",
      Region: store.state.geography?.Region ?? "",
      City: store.state.geography?.City ?? "",
      Platform: template,
      Browser: commonHelper.getUserAgent(),
      Fingerprint: store.state.visitorId,
    };
    if (cashAgentQuery || isAgentDomain.value) {
      request.IsTest = false;
      request.IsWalkIn = true;
    }

    const response =
      cashAgentQuery || isAgentDomain.value
        ? await apis.registerAccountViaCashAgent(request)
        : await apis.registerAccount(request);
    if (response.isSuccessful) {
      const result = new GetRegisterResponse(response.Data);
      notificationHelper.notification(
        response.ErrorMessageForDisplay,
        EnumMessageType.Success
      );
      kwaiMetaTrigger(EnumKwaiMeta.REGISTER);
      if (isFromLandingPage) {
        await handleRedirectFromLandingPage({
          LandingPageRedirectUrl: result.LandingPageRedirectUrl,
          Username: result.Username,
          Password,
        });
        return;
      }
      localStorage.setItem("onlineId", String(result.OnlineId));
      localStorage.setItem("betlisttokenid", String(result.OnlineId));
      store.commit("updateAuth", true);
      store.commit("updateToken", result.Token);
      store.commit("updateCurrency", result.Currency);
      store.commit("updateUsername", result.Username);
      store.commit("updateOnlineId", result.OnlineId);
      store.commit("updateCustomerId", result.CustomerId);
      store.commit("updateIsCashPlayer", result.IsCashPlayer);
      store.commit("updateRegisterDialog", false);
      (<any>window).location = window.location.origin;
    } else {
      notificationHelper.notification(
        response.ErrorMessageForDisplay,
        EnumMessageType.Error
      );
    }
    isLoading.value = false;
  };
  let telegramClose: () => void;
  const importTelegram = async () => {
    try {
      const { useWebApp } = await import("vue-tg");
      const { close } = useWebApp();
      telegramClose = close;
    } catch (error) {
      // window?.history?.go(-1);
    }
  };
  importTelegram();
  const telegramRegister = async () => {
    isLoading.value = true;
    const { Username, Password, Currency, Phone } = formModel;
    const CompanyFlowSettings: Array<ICompanyFlowSettingDto> = [
      {
        PropertyName: "Phone",
        PropertyValue: Phone,
      },
    ];
    const url = new URL(window.location.href);
    url.port = "";
    const request: IRegisterRequest = {
      Domain: commonHelper.getDomain(),
      Username,
      Password,
      Currency,
      CompanyFlowSettings,
      Referral: "",
      Url: url.toString(),
      Ip: store.state.geography?.Ip ?? "Telegram",
      LoginCountry: store.state.geography?.CountryCode ?? "",
      Region: store.state.geography?.Region ?? "",
      City: store.state.geography?.City ?? "",
      Platform: template,
      Browser: commonHelper.getUserAgent(),
      Fingerprint: store.state.visitorId,
    };
    const response = await apis.registerAccount(request);
    if (response.isSuccessful) {
      notificationHelper.notification(
        response.ErrorMessageForDisplay,
        EnumMessageType.Success
      );
      setTimeout(() => {
        if (typeof telegramClose === "function") {
          telegramClose();
        }
      }, 2000);
    } else {
      notificationHelper.notification(
        response.ErrorMessageForDisplay,
        EnumMessageType.Error
      );
    }
    isLoading.value = false;
  };
  const curr = computed(() => formModel.Currency);
  watch([curr, availableProperties], () => {
    if (
      _.find(
        availableProperties.value,
        (item) => item.PropertyName === "Bank"
      ) &&
      formModel.Currency !== ""
    ) {
      getAvailableBanks();
    }
  });
  watch(
    () => formModel.Bank,
    (val) => {
      rules.BankAccountNumber.required = Number(val) !== 0 && val !== "";
      rules.BankAccountName.required = Number(val) !== 0 && val !== "";
    }
  );
  const onRegister = formHelper.getSubmitFunction(register);
  const onRegisterFromLanding = formHelper.getSubmitFunction(() =>
    register(true)
  );
  const onTelegramRegister = formHelper.getSubmitFunction(telegramRegister);
  const nextRequestOtpInSeconds = ref<number>(0);
  let remainingRequestOtpInterval: any;
  const WAITING_TIME_IN_SECONDS = 30;
  const isGettingOtpCode = ref<boolean>(false);
  const setRemainingOtpRequest = () => {
    const date = store.state.requestedOtpDate;
    if (date) {
      const diff = differenceInSeconds(new Date(), new Date(date));
      if (diff > WAITING_TIME_IN_SECONDS) {
        nextRequestOtpInSeconds.value = 0;
        store.commit("updateRequestedOtpDate", "");
        clearInterval(remainingRequestOtpInterval);
      } else {
        nextRequestOtpInSeconds.value = WAITING_TIME_IN_SECONDS;
        clearInterval(remainingRequestOtpInterval);
        nextRequestOtpInSeconds.value -= diff;
        remainingRequestOtpInterval = setInterval(() => {
          nextRequestOtpInSeconds.value -= 1;
          if (nextRequestOtpInSeconds.value <= 0) {
            store.commit("updateRequestedOtpDate", "");
            clearInterval(remainingRequestOtpInterval);
          }
        }, 1000);
      }
    }
  };
  setRemainingOtpRequest();
  const getPhoneNumber = (removeLeadingZero = false) => {
    const { Phone = "", PhoneNumberCode = 0 } = formModel;
    const phoneNumber = String(Phone);
    if (!removeLeadingZero) return `${PhoneNumberCode}${phoneNumber}`;
    return phoneNumber.startsWith("0")
      ? phoneNumber.replace("0", `${PhoneNumberCode}`)
      : `${PhoneNumberCode}${phoneNumber}`;
  };
  const onGetVerificationCode = async () => {
    isGettingOtpCode.value = true;
    try {
      const { Phone = "", CountryCode = "" } = formModel;
      if (nextRequestOtpInSeconds.value > 0 || !Phone || !CountryCode) return;
      const response = await apis.getOtpVerification({
        CountryCode,
        Phone: getPhoneNumber(),
        WebId: store.state.webId,
        Domain: commonHelper.getDomain(),
        Language: store.state.locale,
        IsSettingForApp: false,
      });
      if (response.ErrorCode === 0) {
        nextRequestOtpInSeconds.value = WAITING_TIME_IN_SECONDS;
        const now = new Date();
        notificationHelper.notification(
          $t("verification_code_sent"),
          EnumMessageType.Success
        );
        store.commit("updateRequestedOtpDate", now.toString());
        setRemainingOtpRequest();
      } else {
        notificationHelper.notification(
          response.ErrorMessageForDisplay,
          EnumMessageType.Error
        );
      }
    } catch (error) {
      notificationHelper.notification(
        "Failed to get Code",
        EnumMessageType.Error
      );
    } finally {
      isGettingOtpCode.value = false;
    }
  };
  return {
    availableProperties,
    bankOptions,
    currencies,
    formModel,
    formRef,
    formRules,
    onRegister,
    getCaptchaErrorMsg,
    clientCaptcha,
    reCaptcha,
    reCaptchaSiteKey,
    onVerify,
    onRecaptchaError,
    isVerified,
    referralQuery,
    cashAgentQuery,
    isLoading,
    captchaErrorMsg,
    rules,
    nextRequestOtpInSeconds,
    isGettingOtpCode,
    onGetVerificationCode,
    onTelegramRegister,
    onRegisterFromLanding,
    landingPageFormRule,
    onToggleIsShowPassword,
    isShowPassword,
  };
}
