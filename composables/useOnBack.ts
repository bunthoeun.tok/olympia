import { ref } from "vue";

const currentRoute = ref("");
const previousRoute = ref("");
const useOnBack = () => {
  const setPreviousRoute = (from: string) => {
    previousRoute.value = from;
  };
  const setCurrentRoute = (route: string) => {
    currentRoute.value = route;
  };
  return {
    currentRoute,
    setCurrentRoute,
    setPreviousRoute,
    previousRoute,
  };
};

export default useOnBack;
