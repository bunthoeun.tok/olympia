import { IThemePropertiesFromApi } from "~~/models/companyThemePropertiesRespone";
import { store } from "~~/store";

export const useInjectScript = (
  themeProperties: IThemePropertiesFromApi[],
  globalState?: any
) => {
  const noScript = [];
  const script = [
    {
      type: "text/javascript",
      innerHTML:
        // eslint-disable-next-line max-len
        "String.prototype.toLowerCase||(String.prototype.toLowerCase=function(){return this.toLowerCase()}),String.prototype.replaceAll||(String.prototype.replaceAll=function(){return this.replace()});",
      tagPosition: "bodyClose",
    },
    {
      type: "text/javascript",
      innerHTML:
        // eslint-disable-next-line max-len
        "function setLcWidget() {var widget = document.getElementById('chat-widget-container');if(widget){widget.style.position = 'fixed';widget.style.bottom = '65px';widget.style.right = '20px';}}setTimeout(setLcWidget,4000);" +
        "var Tawk_API=Tawk_API||{};Tawk_API.customStyle={visibility:{mobile:{xOffset:0,yOffset:'65px'}}};",
      tagPosition: "bodyClose",
    },
  ];
  if (!commonHelper.isChinaServer()) {
    script.push({
      type: "text/javascript",
      src: "https://telegram.org/js/telegram-web-app.js",
      tagPosition: "head",
    });
  }
    const gtmIdInternal = globalState?.gtmIdInternal;
    if (gtmIdInternal) {
      script.push({
        type: "text/javascript",
        src: `https://www.googletagmanager.com/gtag/js?id=${gtmIdInternal}`,
        // eslint-disable-next-line max-len
        tagPosition: "head",
      });
      script.push({
        type: "text/javascript",
        // eslint-disable-next-line max-len
        innerHTML: `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','${gtmIdInternal}');`,
        tagPosition: "head",
      });
      noScript.push({
        innerHTML:
          '<iframe src="https://www.googletagmanager.com/ns.html?id=' +
          gtmIdInternal +
          '" height="0" width="0" style="display:none; visibility:hidden"></iframe>',
          tagPosition: "bodyOpen",
        });
    }
  const gmtId = themePropertyHelper.getSingleThemeProperty(
    themeProperties,
    "seo_gtm_containerid"
  );
  if (gmtId && !gmtId.IsHidden && gmtId.Text && gmtId.Text !== "GTM-XXXXXX") {
    script.push({
      type: "text/javascript",
      // eslint-disable-next-line max-len
      innerHTML: `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','${gmtId.Text}');`,
      tagPosition: "head",
    });
    noScript.push({
      innerHTML:
        '<iframe src="https://www.googletagmanager.com/ns.html?id=' +
        gmtId.Text +
        '" height="0" width="0" style="display:none; visibility:hidden"></iframe>',
      tagPosition: "bodyOpen",
    });
  }

  // Get Google Analytic, Meta Pixel events, Tiktok Pixel events, Kwai pixel events Id
  const gaId = themePropertyHelper.getSingleThemeProperty(
    themeProperties,
    "seo_google_analystic_key"
  );
  const metaId = themePropertyHelper.getSingleThemeProperty(
    themeProperties,
    "seo_meta_pixel_event_key"
  );
  const kwaiId = themePropertyHelper.getSingleThemeProperty(
    themeProperties,
    "seo_kwai_pixel_event_key"
  );

  // Inject Google Analytic script
  const googleAnalyticScripts = [
    {
      type: "text/javascript",
      src: `https://www.googletagmanager.com/gtag/js?id=${gaId.Text}`,
      tagPosition: "head",
    },
    {
      type: "text/javascript",
      innerHTML: `window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', '${gaId.Text}');`,
      tagPosition: "head",
    }
  ]
  if (gaId && !gaId.IsHidden && gaId.Text) {
    googleAnalyticScripts.forEach((gaScript: any) => script.push(gaScript));
  }

  // Inject Meta Pixel Script
  const metaScripts = [
    {
      type: "text/javascript",
      innerHTML: `!function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '${metaId.Text}');
        fbq('track', 'PageView');`,
      tagPosition: "head",
    },
    {
      innerHTML:
        `<image src="https://www.facebook.com/tr?id=${metaId.Text}&ev=PageView&noscript=1" height="1" width="1" style="display:none" />`,
      tagPosition: "bodyOpen",
    }
  ]
  if (metaId && !metaId.IsHidden && metaId.Text) {
    script.push(metaScripts[0] as any);
    noScript.push(metaScripts[1] as any);
  }

  // Inject Kwai Script
  const kwaiScripts = [
    {
      type: "text/javascript",
      // eslint-disable-next-line max-len
      innerHTML: `!function(e,t){"object"==typeof exports&&"object"==typeof module?module.exports=t():"function"==typeof define&&define.amd?define([],t):"object"==typeof exports?exports.install=t():e.install=t()}(window,(function(){return function(e){var t={};function n(o){if(t[o])return t[o].exports;var r=t[o]={i:o,l:!1,exports:{}};return e[o].call(r.exports,r,r.exports,n),r.l=!0,r.exports}return n.m=e,n.c=t,n.d=function(e,t,o){n.o(e,t)||Object.defineProperty(e,t,{enumerable:!0,get:o})},n.r=function(e){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(e,"__esModule",{value:!0})},n.t=function(e,t){if(1&t&&(e=n(e)),8&t)return e;if(4&t&&"object"==typeof e&&e&&e.__esModule)return e;var o=Object.create(null);if(n.r(o),Object.defineProperty(o,"default",{enumerable:!0,value:e}),2&t&&"string"!=typeof e)for(var r in e)n.d(o,r,function(t){return e[t]}.bind(null,r));return o},n.n=function(e){var t=e&&e.__esModule?function(){return e.default}:function(){return e};return n.d(t,"a",t),t},n.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},n.p="",n(n.s=0)}([function(e,t,n){"use strict";var o=this&&this.__spreadArray||function(e,t,n){if(n||2===arguments.length)for(var o,r=0,i=t.length;r<i;r++)!o&&r in t||(o||(o=Array.prototype.slice.call(t,0,r)),o[r]=t[r]);return e.concat(o||Array.prototype.slice.call(t))};Object.defineProperty(t,"__esModule",{value:!0});var r=function(e,t,n){var o,i=e.createElement("script");i.type="text/javascript",i.async=!0,i.src=t,n&&(i.onerror=function(){r(e,n)});var a=e.getElementsByTagName("script")[0];null===(o=a.parentNode)||void 0===o||o.insertBefore(i,a)};!function(e,t,n){e.KwaiAnalyticsObject=n;var i=e[n]=e[n]||[];i.methods=["page","track","identify","instances","debug","on","off","once","ready","alias","group","enableCookie","disableCookie"];var a=function(e,t){e[t]=function(){for(var n=[],r=0;r<arguments.length;r++)n[r]=arguments[r];var i=o([t],n,!0);e.push(i)}};i.methods.forEach((function(e){a(i,e)})),i.instance=function(e){var t,n=(null===(t=i._i)||void 0===t?void 0:t[e])||[];return i.methods.forEach((function(e){a(n,e)})),n},i.load=function(e,o){var a="https://s1.kwai.net/kos/s101/nlav11187/pixel/events.js";i._i=i._i||{},i._i[e]=[],i._i[e]._u=a,i._t=i._t||{},i._t[e]=+new Date,i._o=i._o||{},i._o[e]=o||{};var c="?sdkid=".concat(e,"&lib=").concat(n);r(t,a+c,"https://s16-11187.ap4r.com/kos/s101/nlav11187/pixel/events.js"+c)}}(window,document,"kwaiq")}])}));`,
      tagPosition: "head",
    },
    {
      type: "text/javascript",
      innerHTML: `kwaiq.load('${kwaiId.Text}');
      kwaiq.page();
      kwaiq.instance('${kwaiId.Text}').track('contentView');
      `,
      tagPosition: "head",
    },
  ];
  if (kwaiId && !kwaiId.IsHidden && kwaiId.Text) {
    kwaiScripts.forEach((kwaiScript: any) => script.push(kwaiScript));
  }

  const tiktokId = themePropertyHelper.getSingleThemeProperty(
    themeProperties,
    "seo_tiktok_pixel_event_key"
  );
  // Inject Tiktok Scripts
  const tiktokScripts = [
    {
      type: "text/javascript",
      // eslint-disable-next-line max-len
      innerHTML: `!function (w, d, t) {w.TiktokAnalyticsObject=t;var ttq=w[t]=w[t]||[];ttq.methods=["page","track","identify","instances","debug","on","off","once","ready","alias","group","enableCookie","disableCookie"],ttq.setAndDefer=function(t,e){t[e]=function(){t.push([e].concat(Array.prototype.slice.call(arguments,0)))}};for(var i=0;i<ttq.methods.length;i++)ttq.setAndDefer(ttq,ttq.methods[i]);ttq.instance=function(t){for(var e=ttq._i[t]||[],n=0;n<ttq.methods.length;n++)ttq.setAndDefer(e,ttq.methods[n]);return e},ttq.load=function(e,n){var i="https://analytics.tiktok.com/i18n/pixel/events.js";ttq._i=ttq._i||{},ttq._i[e]=[],ttq._i[e]._u=i,ttq._t=ttq._t||{},ttq._t[e]=+new Date,ttq._o=ttq._o||{},ttq._o[e]=n||{};n=document.createElement("script");n.type="text/javascript",n.async=!0,n.src=i+"?sdkid="+e+"&lib="+t;e=document.getElementsByTagName("script")[0];e.parentNode.insertBefore(n,e)};}(window, document, 'ttq')`,
      tagPosition: "head",
    },
    {
      type: "text/javascript",
      innerHTML: `ttq.load('${tiktokId.Text}');
        ttq.instance('${tiktokId.Text}').track("ViewContent");
      `,
      tagPosition: "head",
    }
  ]
  if (tiktokId && !tiktokId.IsHidden && tiktokId.Text) {
    tiktokScripts.forEach((tiktokScript: any) => script.push(tiktokScript));
  }

  // const env = useRuntimeConfig().public.CURRENTLY_ENV;
  // if (env === "staging" || env === "development") {
  //   script.push({
  //     type: "module",
  //     // @ts-ignore
  //     src: "https://unpkg.com/vconsole@latest/dist/vconsole.min.js",
  //     tagPosition: "bodyClose",
  //   });
  //   script.push({
  //     type: "text/javascript",
  //     innerHTML: 'window.addEventListener("load", (event) => { var vConsole = new window.VConsole();});',
  //     tagPosition: "bodyClose",
  //   });
  // }
  const scripts: Record<string, Object> = {
    htmlAttrs: {
      lang: String(store.state.locale),
    },
    // @ts-ignore
    noscript: [...noScript],
    // @ts-ignore
    script: [...script],
  };
  return {
    scripts,
  };
};
