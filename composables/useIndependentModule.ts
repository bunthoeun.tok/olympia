import { store } from "~~/store";

type IndependentGameStyle = "Independent_mobile_game_page_style_1";
type IndependentModule =
  | "Independent_provider_type_lobby_geometry"
  | "Independent_provider_type_lobby_concrete";

interface IndependentProviderType {
  SelectedModule: string;
  SelectedStyle: IndependentModule;
  Position: IndependentGameStyle[];
}

export const useIndependentModule = () => {
  const themeProperties = computed(
    () => store.state.companyThemePropertiesFromApi.ThemeProperties
  );

  const isIndependentModule = useState(KEYS.IS_INDEPENDENT_MODULE);

  const independentModule = computed(() => {
    const module = themeProperties.value.find((v) =>
      v.Text.toLowerCase().includes("independent_provider_type_")
    );
    if (!module?.Text) return;
    let parsedModule: IndependentProviderType | undefined;
    try {
      parsedModule = JSON.parse(module.Text) as IndependentProviderType;
    } catch (error) {
      console.error("Failed to parse JSON:", error);
      // Handle the error appropriately, perhaps set parsedModule to null or a default value
      parsedModule = undefined; // or some default value
    }
    return parsedModule;
  });

  const isGeometryStyle = computed(() => {
    return (
      independentModule.value &&
      independentModule.value.SelectedStyle ===
        "Independent_provider_type_lobby_geometry"
    );
  });

  const isConcreteStyle = computed(() => {
    return (
      independentModule.value &&
      independentModule.value.SelectedStyle ===
        "Independent_provider_type_lobby_concrete"
    );
  });

  return { isIndependentModule, isGeometryStyle, isConcreteStyle };
};
