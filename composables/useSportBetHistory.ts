export const useSportBetHistory = (props: any) => {
  const { t, te } = useI18n();
  const totalGoalNgGgOptions = [
    "01 ng",
    "23 ng",
    "46 ng",
    "7 over ng",
    "01_ng",
    "23_ng",
    "46_ng",
    "7over_ng",
    "01 gg",
    "23 gg",
    "46 gg",
    "7 over gg",
    "7over_gg",
    "01_gg",
    "23_gg",
    "46_gg",
  ];
  const toBeTranslated = [
      "away or under",
      "away and over",
      "draw or under",
      "home or under",
      "x or under",
      "away or over",
      "home or over",
      "x or over",
      "x or both team score",
      "away or both team score",
      "home or both team score",
      "away and under",
      "draw and both",
      "1x2 ggng",
      "total ggng",
      "home and both team score",
      "away and both team score",
      "no draw and both team score",
      "handicap 1x2",
      "both team score",
      "draw no bet",
      "home to score",
      "away to score",
      "home and over",
      "draw and over",
      "home and under",
      "draw and under",
      "draw and both team score",
      "double chance and both team score",
      "multi goals",
      "handicap",
      "draw or over",
      "european handicap"
  ];
  const includeOption = [
    "double chance and both team score",
    "european handicap",
  ];
  const customMarketTypeKeys = ["D/C & O/U"];

  const getSelectedText = () => {
    const { MarketType = '', BetOption = '', HomeTeam, AwayTeam } = props.sportBetDetail;
    const marketType = MarketType?.toLowerCase()?.replace(/first half |second half /g, "");
    const marketText = marketType?.replace(/first half |second half /g, "")?.replaceAll(" ", "_");
    if (toBeTranslated.includes(marketType)) {
      let text = `odds_option_${marketText}`;
      if (includeOption.includes(marketType)) {
        text += '_' + BetOption.replaceAll(" ", "_").toLowerCase();
      }
      if (!te(text)) return BetOption;
      return t(
        text,
        {
          BetOption,
          home: HomeTeam,
          away: AwayTeam,
          point: props.sportBetDetail.HandicapPointForDisplay,
        }
      );
    }
    if (customMarketTypeKeys.includes(MarketType)) {
      return t(
        `odds_option_${BetOption?.replaceAll(" ", "_")?.toLowerCase()}`,
        {
          BetOption,
          home: HomeTeam,
          away: AwayTeam,
          point: props.sportBetDetail.HandicapPointForDisplay,
        }
      );
    }
    if (marketType === '1x2') {
      if (BetOption === "1") {
        return HomeTeam;
      } else if (BetOption === "2") {
        return AwayTeam;
      } else if (BetOption?.toLowerCase() === "x") {
        return t("draw");
      }

      // if (
      //   BetOption.includes("Over") ||
      //   BetOption.includes("Under") ||
      //   ["1X2 & GG/NG"].includes(MarketType)
      // ) {
      //   return BetOption;
      // }

      // return t(
      //   `odds_option_${BetOption?.replaceAll(" ", "_")?.toLowerCase()}`,
      //   {
      //     home: HomeTeam,
      //     away: AwayTeam,
      //   }
      // );
      // Special case for Uppercase DRAW
      if (BetOption?.includes("DRAW")) {
        return BetOption.replace("DRAW", "Draw");
      }

      return BetOption;
    }
    if (totalGoalNgGgOptions.includes(BetOption.toLowerCase())) {
      return t(
        `odds_option_${BetOption?.replaceAll(" ", "_")?.toLowerCase()}`,
        {
          home: HomeTeam,
          away: AwayTeam,
        }
      );
    }
    if (MarketType.includes("Correct Score")) {
      return BetOption.replace(" ", ":");
    }
    const betOption = `${marketText}_${BetOption.replaceAll(" ", "_").toLowerCase()}`
    if (te(betOption)){
      return t(
        betOption,
        {
          BetOption,
          home: HomeTeam,
          away: AwayTeam,
          point: props.sportBetDetail.HandicapPointForDisplay,
        }
      )
    }
    return BetOption;
  };

  return { getSelectedText };
};
