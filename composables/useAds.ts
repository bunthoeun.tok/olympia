import _, { property } from "lodash";
import { format } from "date-fns";
import { IThemePropertiesFromApi, ThemePropertiesFromApiDto } from "~~/models/companyThemePropertiesRespone";
import { store } from "~~/store";
import { KEYS } from "~~/utils/constants";

class AdsHiddenForTheDay {
  public static hiddenAds: string[] = [];
  public static expiresAt: Date = new Date();
  private static instance: AdsHiddenForTheDay;
  private constructor() { }

  static getInstance(): AdsHiddenForTheDay {
    if (AdsHiddenForTheDay.instance) {
      return AdsHiddenForTheDay.instance;
    }
    try {
      const today = new Date();
      const existedState = localStorage.getItem('ads_purgatory');
      if (!existedState) {
        throw new Error('no existing data.');
      }
      const parsedState = JSON.parse(existedState);
      if (!parsedState || !parsedState.expiresAt || !parsedState?.hiddenAds) {
        throw new Error('parsing failed.');
      }
      const parsedExpiresAt = new Date(parsedState?.expiresAt);
      const isExpired = parsedExpiresAt < today;
      if (isExpired) {
        throw new Error('expired.');
      }
      AdsHiddenForTheDay.hiddenAds = parsedState.hiddenAds;
      AdsHiddenForTheDay.expiresAt = parsedExpiresAt;
    } catch (error) {
      AdsHiddenForTheDay.resetLocalStorage();
    } finally {
      return AdsHiddenForTheDay.instance;
    }
  }

  public static hideAds(HtmlId: string) {
    AdsHiddenForTheDay.hiddenAds.push(HtmlId);
    AdsHiddenForTheDay.saveToLocalStorage(AdsHiddenForTheDay.hiddenAds, AdsHiddenForTheDay.endOfDay);
  }
  private static saveToLocalStorage(hiddenAds: string[], expiresAt: Date) {
    localStorage.setItem('ads_purgatory', JSON.stringify({
      hiddenAds: hiddenAds,
      expiresAt: format(expiresAt, "yyyy/MM/dd 23:59:59"),
    }));
  }
  private static get endOfDay(): Date {
    const endOfTheDay = new Date();
    endOfTheDay.setHours(23, 59, 59, 999);
    return endOfTheDay;
  }
  private static resetLocalStorage() {
    AdsHiddenForTheDay.hiddenAds = [];
    AdsHiddenForTheDay.expiresAt = AdsHiddenForTheDay.endOfDay;
    AdsHiddenForTheDay.saveToLocalStorage([], AdsHiddenForTheDay.expiresAt);
  }
}

export const usePopupAds = () => {
  AdsHiddenForTheDay.getInstance();
  const temporaryHiddenAds = ref<string[]>([]);
  const route = useRoute();
  const homePageFromSetting = computed(() => store.state.companyThemePropertiesFromApi.ThemeProperties.find(property => {
    if (property.HtmlId && property.HtmlId === "homepage" && !property.IsHidden) return property;
  }));

  const currentPath = computed(() => {
    if (!(route.path === "/")) return route.path.substring(1);
    else return route.path;
  });
  const redirectRouteName = computed(() => {
    if (
      !homePageFromSetting.value ||
      !homePageFromSetting.value.Text ||
      homePageFromSetting.value.Text.toLocaleLowerCase() === "home"
    )
      return "/";
    else {
      return _.toLower(_.replace(homePageFromSetting.value.Text, " ", "-"));
    }
  });
  const illegalRoute = 'game/';
  const isOnlegalRoute = computed(() => currentPath.value?.includes(redirectRouteName.value) && !currentPath.value?.toLocaleLowerCase().includes(illegalRoute));
  let hasBeenShowPopUPBefore = localStorage.getItem(KEYS.HAS_SHOW_POPUP_BEFORE) !== null;
  if (hasBeenShowPopUPBefore) {
    localStorage.removeItem(KEYS.HAS_SHOW_POPUP_BEFORE);
    if (isOnlegalRoute.value) hasBeenShowPopUPBefore = false;
    else hasBeenShowPopUPBefore = true;
  }
  if (!hasBeenShowPopUPBefore || isOnlegalRoute.value) localStorage.setItem(KEYS.HAS_SHOW_POPUP_BEFORE, "true");

  const popupAdsThemeProperties = computed<ThemePropertiesFromApiDto[]>(() => themePropertyHelper
    .getMultipleThemesProperties(store.state.companyThemePropertiesFromApi.ThemeProperties, 'popup_ads_')
    .filter(property => isOnlegalRoute.value && !hasBeenShowPopUPBefore && property.HtmlId && property.ImagePath && !property.IsHidden
      && !AdsHiddenForTheDay.hiddenAds.includes(property.HtmlId)
      && !temporaryHiddenAds.value.includes(property.HtmlId) && property.HtmlId !== "popup_ads_style")
  );

  const popupAdsThemePropertiesStyle = computed<
    ThemePropertiesFromApiDto | undefined
  >(() => {
    const popupProperty =
      store.state.companyThemePropertiesFromApi.ThemeProperties.find(
        (prop) =>
          prop.HtmlId === "popup_ads_style" ||
          (prop.HtmlId.includes("popup_ads_") &&
            prop.HtmlId !== "popup_ads_style" &&
            !AdsHiddenForTheDay.hiddenAds.includes(prop.HtmlId) &&
            !temporaryHiddenAds.value.includes(prop.HtmlId))
      );
    if (!popupProperty || popupProperty?.IsHidden) {
      return undefined;
    }
    return new ThemePropertiesFromApiDto(popupProperty);
  });
  const { allowCompany } = commonHelper;
  const shouldDisplayAds = ref(popupAdsThemeProperties.value.length > 0);
  if (allowCompany.value && popupAdsThemeProperties.value.length > 0) {
    shouldDisplayAds.value = popupAdsThemePropertiesStyle.value !== undefined;
  }
  const hideAds = (HtmlId: string, isForTheDay: boolean = false) => {
    if (isForTheDay) {
      AdsHiddenForTheDay.hideAds(HtmlId);
    }
    temporaryHiddenAds.value.push(HtmlId);
  }
  watch(() => [popupAdsThemeProperties.value.length, currentPath.value], () => {
    if (isOnlegalRoute.value) shouldDisplayAds.value = true;
    else shouldDisplayAds.value = false;
    if (data.popupAds.length > 0 && popupAdsThemePropertiesStyle.value !== undefined) return;
    shouldDisplayAds.value = false;
  });

  const data = reactive({
    shouldDisplayAds,
    popupAds: popupAdsThemeProperties,
  });
  return { data, hideAds };
}

export const useLobbySidebarAds = () => {
  const { allowCompany } = commonHelper;
  const lobbyAdsLeftSideThemeProperty = computed<ThemePropertiesFromApiDto | undefined>(() => {
    const property = store.state.companyThemePropertiesFromApi.ThemeProperties.find(property => property.HtmlId === 'lobby_ads_left_side');
    if (
      !property ||
      (allowCompany.value ? false : property?.IsHidden) ||
      !property?.Path ||
      property?.Path?.length <= 0
    ) {
      return undefined;
    }
    return new ThemePropertiesFromApiDto(property);
  });

  const lobbyAdsLeftSideThemePropertyStyle = computed<
    ThemePropertiesFromApiDto | undefined
  >(() => {
    const LeftSideproperty =
      store.state.companyThemePropertiesFromApi.ThemeProperties.find(
        (prop) => prop.HtmlId === "lobby_ads_left_side_style"
      );
    if (!LeftSideproperty || LeftSideproperty?.IsHidden) {
      return undefined;
    }
    return new ThemePropertiesFromApiDto(LeftSideproperty);
  });

  const canShowAds = ref(lobbyAdsLeftSideThemeProperty.value !== undefined);
  if (allowCompany.value) {
    canShowAds.value = lobbyAdsLeftSideThemePropertyStyle.value !== undefined;
  }
  const route = useRoute();
  const illegalRoutes = ["/game/"];
  const isOnIllegalRoute = computed(() => {
    return illegalRoutes.some(illegalRoute => route?.path?.toLowerCase()?.includes(illegalRoute));
  });
  const shouldDisplayAds = computed(() => canShowAds.value && !isOnIllegalRoute.value);
  const hideAds = () => {
    canShowAds.value = false;
  }

  return {
    leftAds: lobbyAdsLeftSideThemeProperty,
    shouldDisplayAds,
    hideAds,
  }
}
