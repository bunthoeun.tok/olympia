import _ from "lodash";
import { computed, Ref, ref, watch } from "vue";
import apis from "@/utils/apis";
import { ICurrencyWhiteList } from "@/models/currencyWhiteList";
import { ICurrencies } from "@/models/login";
import { store } from "@/store";
import { appStore } from "~~/store/app";

interface IUseCurrecnyWhitelist {
  whiteListCurrencies: ComputedRef<Array<ICurrencies>>;
  isIpWhitelistEnabled: Ref<boolean>;
}
export default function useCurrencyWhitelist(): IUseCurrecnyWhitelist {
  const ip = computed(() => store.state.geography?.Ip);
  const officeIp = computed(() => store.state.geography.IsOfficeIp);
  const isIpWhitelistEnabled = ref(false);
  const checkIfUserIpIsWhiteListIp = async () => {
      isIpWhitelistEnabled.value = appStore.state.IsIpWhitelistEnabled;
  };
  if (String(ip.value) !== "127.0.0.1") {
    checkIfUserIpIsWhiteListIp();
  }
  watch(ip, () => checkIfUserIpIsWhiteListIp());
  const currencyWhiteList: Ref<ICurrencyWhiteList[]> = ref([]);
  const getCurrencyWhiteList = async () => {
    currencyWhiteList.value = appStore.state.CurrencyWhiteList;
  };
  getCurrencyWhiteList();
  const filterCurrenciesByCountryCode = computed(() =>
    _.filter(currencyWhiteList.value, (item) => {
      const countryCode = computed(() => store.state.geography.CountryCode);
      const isValidWebid = store.state.webId === item?.WebId;
      const formattedCountryCode = _.replace(item?.CountryCode ?? "", " ", "");
      const isValiableForAll = formattedCountryCode === "--";
      const byCountryCode = formattedCountryCode
        .split(",")
        .includes(countryCode.value);
      if (
        item.WebId === 20032 &&
        countryCode.value === "GB" &&
        item.Currency === "USD"
      ) {
        return false;
      }
      return (isValiableForAll || byCountryCode) && isValidWebid;
    })
  );
  const allCurrencies: Ref<Array<ICurrencies>> = computed(()=>appStore.state.PlayerCurrencyList);
  const env = () => useRuntimeConfig().public.CURRENTLY_ENV;
  const whiteListCurrencies = computed(() => {
    if (
      env() === "development" ||
      env() === "staging" ||
      env() === "demo" ||
      isIpWhitelistEnabled.value ||
      officeIp.value
    ) {
      return allCurrencies.value;
    }
    return allCurrencies.value.filter((x) =>
      filterCurrenciesByCountryCode.value.some((y) => y.Currency === x.Currency)
    );
  });
  return {
    whiteListCurrencies,
    isIpWhitelistEnabled,
  };
}
