import { computed, ComputedRef } from "vue";
import EnumEntranceLocation from "@/models/enums/enumEntranceLocation";
import { store } from "~~/store";

interface IUseProductFeature {
  auth: boolean;
  processingUrl(url: string, entrance: string): string;
  selectedThemeName: ComputedRef<string>;
}

const useProductFeature = (): IUseProductFeature => {
  const { state } = store;
  const { auth } = state;
  const selectedThemeName = computed(() => state.selectedThemeName);
  const processingUrl = (
    url: string,
    entrance: string = EnumEntranceLocation.Other
  ): string => {
    if (url.includes("?tab=")) {
      return `${url}&&entrance=${entrance}`;
    }
    return `${url}?entrance=${entrance}`;
  };
  return {
    auth,
    processingUrl,
    selectedThemeName,
  };
};

export default useProductFeature;
