import { computed, ComputedRef, Ref, ref } from "vue";
import apis from "@/utils/apis";
import { GetBalanceResponse } from "@/models/getBalanceResponse";
import { Wallet } from "@/models/common/wallet";
import { store } from "~~/store";

interface IUseBalanceInfo {
  isLoading: Ref<boolean>;
  balanceUser: ComputedRef<string>;
  getBalance: () => void;
  currency: Ref<string>;
  wallets: Ref<Wallet[]>;
  isShowBalance: Ref<boolean>;
  mainBalanceForDisplay: Ref<string>;
  getMultiWallets: () => Promise<void>;
}

export default function useBalanceInfo(isFetchOnInit = true): IUseBalanceInfo {
  const { state, commit } = store;
  const isLoading = ref(false);
  const balance = ref({} as GetBalanceResponse);
  const isShowBalance = ref(false);
  const currency = ref(state.currency);
  const mainBalanceForDisplay = ref<string>("");
  const wallets = ref<Array<Wallet>>([]);
  const balanceUser = computed(() => balance.value.BetCreditWithCommas);
  const getMultiWallets = async () => {
    isLoading.value = true;
    const response = await apis.getMultiWallets();
    if (response.isSuccessful && response.Data) {
      wallets.value = response.Data.Wallets.map((item) => new Wallet(item));
      mainBalanceForDisplay.value = response.Data.mainBalanceForDisplay;
    }
    commit("updateIsSuspended", wallets.value.length === 0);
    isLoading.value = false;
  };
  // This function is no use anymore because we use getMultiWallets() instead only
  const getBalance = async () => {
    isLoading.value = true;
    const response = await apis.getAllWalletsBalance();
    if (response.isSuccessful && response.Data) {
      balance.value = response.Data;
    }
    commit("updateIsSuspended", response.ErrorCode === 1003);
    isLoading.value = false;
    getMultiWallets();
  };

  if (state.auth && isFetchOnInit) {
    getMultiWallets();
    if (!state.isSuspended) {
      setInterval(getMultiWallets, 15000);
    }
  }

  return {
    isLoading,
    balanceUser,
    getBalance,
    currency,
    wallets,
    isShowBalance,
    mainBalanceForDisplay,
    getMultiWallets,
  };
}
