export default defineNuxtPlugin((/* nuxtApp */) => {
  const { $i18n } = useNuxtApp();

  return {
    provide: {
      t: (text: string) => $i18n.t(text),
      te: (text: string) => $i18n.te(text),
    },
  };
});
