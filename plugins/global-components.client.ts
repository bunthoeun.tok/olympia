import VueClientRecaptcha from "vue-client-recaptcha";
import VueLazyload from "vue-lazyload";
import ElementPlus, { ElForm } from "element-plus";
import "swiper/css";
import "swiper/css/effect-fade";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "@/assets/styles/shared.scss";
import "@/assets/styles/theme.scss";

export default defineNuxtPlugin((nuxtApp) => {
  ElForm.props.validateOnRuleChange.default = false;
  nuxtApp.vueApp
    .use(VueLazyload, {
      error: commonHelper.getAssetImage(
        "assets/theme/common/icon/no-image.png"
      ),
    })
    .component("Captcha", VueClientRecaptcha)
    .use(ElementPlus);
});
