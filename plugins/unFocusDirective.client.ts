
export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.vueApp.directive('unFocus', {
    mounted(el, binding, vnode) {
      const inputEl = el.querySelector('.el-input__inner');
      if (inputEl !== undefined || inputEl !== null) {
        inputEl.addEventListener('blur', () => {
          const header = document.getElementById('header');
          const methodOption = document.getElementById('method-option');
          if (header !== null) {
            header.scrollIntoView();
          }
        });
        if (!useIsDesktop()) {
          inputEl.addEventListener('focus', () => {
            inputEl.scrollIntoView({ block: 'center', inline: 'nearest' });
          });
        }
      }
    }
  });
});