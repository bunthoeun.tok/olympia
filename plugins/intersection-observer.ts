export default defineNuxtPlugin((nuxtApp) => {
    nuxtApp.vueApp.directive('intersectionObserver', (el, params) => {
      if (typeof params.value === 'function') {
        const observer = new IntersectionObserver(
          params.value,
          {
            root: el.parentElement,
          },
        )
        observer.observe(el);
      }
    });
  });