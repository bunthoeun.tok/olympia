import { InfoFilled, Download, Share } from "@element-plus/icons-vue";

export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.vueApp
    .component("InfoFilled", InfoFilled)
    .component("Download", Download)
    .component("Share", Share);
});
