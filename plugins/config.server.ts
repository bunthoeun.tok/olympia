import os from "os";
import { KEYS } from "~~/utils/constants";

export default defineNuxtPlugin((_) => {
  const config = useRuntimeConfig();
  const hostname = os.hostname().toLowerCase();
  const endpoint = useState(KEYS.SERVER_NAME);
  const sporturl = useState(KEYS.WL_SPORT);
  const DEMO_SERVERS = ["wl01vm-p-x94"];
  const PROD_SA_SERVERS = ["wl03vm-p-a01"];
  const PROD_SERVERS = [
    "wl01sr-p-c141",
    "wl01sr-p-c142",
    "wl01sr-p-c143",
    "wl01sr-p-c144",
    "wl01sr-p-c145",
    "wl01sr-p-c146",
    "wl01sr-p-c147",
    "wl01sr-p-c148",
    "wl11sr-p-c141",
    "wl11sr-p-c142",
    ...PROD_SA_SERVERS,
  ];

  const currentEnvionment = {
    DEMO: config.public.CURRENTLY_ENV === "demo",
    PROD: config.public.CURRENTLY_ENV === "production",
  };

  const isSaServer = () =>
    PROD_SA_SERVERS.some((serverName) => serverName.toLowerCase() === hostname);

  const getTheRightEndpoint = (array: string[]): string => {
    if (isSaServer()) {
      return config.SA_END_POINT;
    }

    const index = array.findIndex(
      (serverName) => serverName.toLowerCase() === hostname
    );
    if (index < 0) return config.public.END_POINT;
    return config[`END_POINT_${index + 1}`];
  };
  const getSportBaseURL = (array: string[]): string => {
    if (isSaServer()) {
      return config.SA_SPORT_URL;
    }
    const index = array.findIndex(
      (serverName) => serverName.toLowerCase() === hostname
    );
    if (index < 0) return config.public.SPORT_URL;
    return config[`SPORT_URL_${index + 1}`];
  };

  if (currentEnvionment.DEMO) {
    endpoint.value = getTheRightEndpoint(DEMO_SERVERS);
    sporturl.value = getSportBaseURL(DEMO_SERVERS);
  }
  if (currentEnvionment.PROD) {
    endpoint.value = getTheRightEndpoint(PROD_SERVERS);
    sporturl.value = getSportBaseURL(PROD_SERVERS);
  }
  if (!endpoint.value) {
    endpoint.value = config.public.END_POINT;
  }
  if (!sporturl.value) {
    sporturl.value = config.public.SPORT_URL;
  }
  if (!sporturl.value) {
    sporturl.value = config.public.SPORT_URL;
  }
});
