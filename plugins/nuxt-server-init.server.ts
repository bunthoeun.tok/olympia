export default defineNuxtPlugin((/* nuxtApp */) => {
    // @ts-ignore
    declare const global: NodeJS.Global & { document?: any };
    if (typeof global !== 'undefined' && typeof global.document === 'undefined' ) {
        global.document = {};
    }
});