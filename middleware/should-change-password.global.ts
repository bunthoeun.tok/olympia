import { store } from "~~/store";
import { ROUTES } from "~~/utils/constants";

export default defineNuxtRouteMiddleware((to, from) => {
  if (process.server) return;

  if (
    (from.path.includes("/deposit/bank-transfer") && to.path === "/deposit") ||
    (from.path.includes("/deposit/payment-bank") && to.path === "/deposit")
  )
    return navigateTo(from.path);

  if (
    (from.path.includes("/withdrawal/bank-transfer") &&
      to.path === "/withdrawal") ||
    (from.path.includes("/withdrawal/payment-bank") &&
      to.path === "/withdrawal")
  )
    return navigateTo(from.path);

  const isGoingToChangePasswordPage =
    to.path.toLowerCase() === ROUTES.ACCOUNT.PASSWORD;
  const isUCPopup = to.fullPath.includes("uc=");
  const alreadyOnChangePassword = isGoingToChangePasswordPage && isUCPopup;
  const isLoggedInAndPasswordExpired =
    store.state.auth && store.state.isPasswordExpired;
  if (
    isLoggedInAndPasswordExpired &&
    (!isGoingToChangePasswordPage || alreadyOnChangePassword)
  ) {
    return navigateTo(ROUTES.ACCOUNT.PASSWORD);
  }
});
