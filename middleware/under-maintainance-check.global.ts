import { store } from "~~/store";

export default defineNuxtRouteMiddleware((to, __) => {
  if (process.server) return;

  const date = new Date();
  const currentTime = date.setTime(date.getTime());
  const getUm = async () => {
    const response = await apis.checkIsPlayerSiteUm();
    const thirtySecondsFromNow = currentTime + 30000;
    const payload = {
      statusCode: response.ErrorCode,
      expireTime: thirtySecondsFromNow,
    };
    store.commit(STORE_MUTATIONS.UM, payload);
  };

  const isUnderMaintenance = store.state.umData.statusCode === 1013;
  const isGoingToUMPage = to.path === "/um-page";

  if (store.state.umData.expireTime < currentTime) {
    getUm();
  }

  if (isUnderMaintenance && !isGoingToUMPage) {
    return navigateTo("/um-page");
  }
});
