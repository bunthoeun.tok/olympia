import { GetBsiGameListResponse } from "~~/models/bsiGameList";
import { store } from "~~/store";
import commonHelper from "~~/utils/commonHelper";
import appHelper from "~~/utils/appHelper";
import { gameStore } from "~~/store/game";

export default defineNuxtRouteMiddleware((_, __) => {
  return;
  if (process.server) return;
  const now = new Date();
  const resourceExpiresTime = new Date(
    Number(localStorage.getItem(KEYS.RESOURCE_EXPIRES) ?? 0)
  );
  const hasResourcesExpired = now >= resourceExpiresTime;
  if (!hasResourcesExpired) return;

  const nuxtApp = useNuxtApp();
  const isAuth = store.state.auth;
  const domain = commonHelper.getDomain();
  const locale = String(nuxtApp.$i18n.getLocaleCookie());
  const currency = useCookie<string>(KEYS.USER_CURRENCY);

  const updateMenuTabs = async () => {
    const response = await apis.getMenuTab(domain, locale);
    if (!response.isSuccessful) return;
    store.commit(STORE_MUTATIONS.TAB_INFOS, response.Data.TabInfos);
  };
  const updateGameTabProviderList = async () => {
    if (Object.keys(store.state.gameTabProviderList).length > 0) return;
    const response = await apis.getGameTabProviderList(
      domain,
      currency.value,
      locale,
    );
    if (!response.isSuccessful) return;
    store.commit(STORE_MUTATIONS.GAME_TAB_PROVIDER_LIST, response.Data);
  };
  const updateBSIGames = async () => {
    if (isAuth) return;
    const response = await apis.getPlayableGame(
      String(currency.value ?? ""),
      locale
    );
    if (!response.isSuccessful) return;
    const result = new GetBsiGameListResponse(response.Data);
    // store.commit(STORE_MUTATIONS.GAMES, result.SortedGame);
    gameStore.commit(STORE_MUTATIONS.GAMES, result.SortedGame);
  };
  const updateGameProviders = async () => {
    const response = await apis.getGameProviderList(
      domain,
      String(currency.value ?? ""),
      locale,
      isAuth === false
    );
    if (!response.isSuccessful) return;
    store.commit(STORE_MUTATIONS.GAME_PROVIDERS, response.Data);
  };
  const getGameTabProviderListInitial = async () => {
    const response = await apis.getGameTabProviderList(
      domain,
      currency.value,
      locale,
    );
    if (response.isSuccessful) {
      store.commit(STORE_MUTATIONS.GAME_TAB_PROVIDER_LIST, response.Data);
    }
  };
  const initialGame = computed(() => store.state.gameTabProviderList);
  if (initialGame.value.SeamlessProviderList === undefined) {
    getGameTabProviderListInitial();
  }
  const refreshResources = async () => {
    await Promise.allSettled([
      updateMenuTabs(),
      updateBSIGames(),
      updateGameProviders(),
      updateGameTabProviderList(),
    ]).finally(() => {
      appHelper.renewResourceExpiryTime();
    });
  };
  refreshResources();
});
