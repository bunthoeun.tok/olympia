import { store } from "~~/store";
import { handleMultiplePlayerLogin } from "@/composables/useLogin";

export default defineNuxtRouteMiddleware((_, __) => {
  if (process.server) return;
  const config = useRuntimeConfig();
  const isNotInDevelopment = config.public.CURRENTLY_ENV !== "development";
  const onlineId = localStorage.getItem("onlineId") ?? 0;
  const hasMulipleLogins =
    store.state.auth && onlineId !== store.state.onlineId;
  if (isNotInDevelopment && hasMulipleLogins) {
    handleMultiplePlayerLogin();
  }
});
