import _ from "lodash";
import { IThemePropertiesFromApi } from "~~/models/companyThemePropertiesRespone";
import { store } from "~~/store";
export default defineNuxtRouteMiddleware((to, __) => {
  if (process.server) return;
  const stateThemeName = useState<String>(KEYS.THEME_NAME);
  if (stateThemeName.value !== "china") return;
  const defaultHomePage = useState<IThemePropertiesFromApi | undefined>(
    KEYS.HOMEPAGE_REDIRECT
  ).value;
  const skipLogin = useCookie("skipLogin").value
    ? useCookie("skipLogin").value
    : "false";
  const allowRoute = [
    "/login",
    "/telegram-login",
    "/telegram-register",
    "/game/:id",
    "join-now",
    "/register",
    "/agent-cooperation",
    "/deposit",
    "/withdrawal",
    "/wallet",
    ROUTES.ACCOUNT.INFO,
    "/my-promotion",
    "/transaction-history",
    BET_HISTORY_ROUTES.INDEX,
  ];
  if (defaultHomePage?.IsHidden === false) return;
  if (
    skipLogin === "false" &&
    !store.state.auth &&
    !_.includes(allowRoute, _.first(to.matched)?.path)
  ) {
    return navigateTo("/login");
  }
});
