import ApiResponse from "~~/models/apiResponse";
import EnumMessageType from "~~/models/enums/enumMessageType";
import { ICustomerSettingResponse } from "~~/models/getCustomerSettingResponse";
import notificationHelper from "~~/utils/elementUiHelpers/notificationHelper";

export default defineNuxtRouteMiddleware((_, __) => {
  if (process.server) return;
  const { themeColor } = themePropertyHelper;
  if (themeColor.value.Text !== "main_lightblue") return;

  const getCustomerSetting = (): Promise<boolean> => {
    return apis
      .getCustomerSetting("IsRedirectToSportsPage-LightBlueSky")
      .then((res: ApiResponse<ICustomerSettingResponse>) => {
        if (res.isSuccessful) {
          return res.Data.Value === "Y";
        } else {
          notificationHelper.notification(res.Message, EnumMessageType.Error);
          return false;
        }
      })
      .catch((___: unknown) => {
        return false;
      });
  };
  const shouldRedirectToSport = async () => {
    const shouldRedirectFromSetting = await getCustomerSetting();
    const hadBeenRedirectedBefore = Boolean(
      localStorage.getItem(KEYS.ALREADY_REDIRECT_TO_SPORT)
    );
    if (shouldRedirectFromSetting && !hadBeenRedirectedBefore) {
      localStorage.setItem(KEYS.ALREADY_REDIRECT_TO_SPORT, "true");
      return navigateTo("/sports");
    }
  };
  shouldRedirectToSport();
});
