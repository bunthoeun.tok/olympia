import { startsWith } from "lodash";
import { IThemePropertiesFromApi } from "~~/models/companyThemePropertiesRespone";
import { store } from "~~/store";
import { KEYS } from "~~/utils/constants";

export default defineNuxtRouteMiddleware((to, from) => {
  if (process.server) return;
  const route = useRoute();
  const onlineId = route.query.onlineId;
  const token = route.query.token;
  const ssoKey = route.query.ssoKey;

  if (
    (route.name === "telegram-deposit" ||
      route.name === "telegram-withdrawal" ||
      route.name === "telegram-bet-history") &&
      token &&
      onlineId
  ) {
    const token = useCookie(KEYS.AUTH_COOKIE_KEY);
    token.value = route.query.token;
    store.commit(STORE_MUTATIONS.ONLINE_ID, onlineId);
    store.commit(STORE_MUTATIONS.AUTH, true);
    localStorage.setItem('onlineId', onlineId)
    localStorage.setItem('ssoKey', ssoKey)
  }

  const layout = useState<string>(KEYS.LAYOUT).value;
  if (layout?.includes("china") && store.state.auth === false) return;

  const defaultHomePage = useState<IThemePropertiesFromApi | undefined>(
    KEYS.HOMEPAGE_REDIRECT
  ).value;
  const isPeruShopEnabled = useState<boolean>(KEYS.IS_PERU_SHOP_ENABLED);
  const selectedTheme = useState<String>(KEYS.THEME_NAME);

  const fromExternalPage = to.path === "/" && from.path === "/";

  let hasBeenHereBefore =
    sessionStorage.getItem(KEYS.HAS_HOME_REDIRECT) === "true";

  const map: { [key: string]: string } = {
    home: "/",
    sports: "/sports",
    virtualsports: "/virtual-sports",
    livecasino: "/live-casino",
    games: "/games",
    cockfight: "/cockfight",
    poker: "/poker",
    lottery: "/lottery",
    keno: "/keno",
    promotion: "/promotion",
  };

  const key = String(defaultHomePage?.Text ?? "")
    .toLowerCase()
    .replace(/\s/g, "");

  const needRedirectToSportPeru = map[key] !== to.path.toLowerCase();

  if (
    isPeruShopEnabled.value &&
    selectedTheme.value.includes("global") &&
    needRedirectToSportPeru
  ) {
    hasBeenHereBefore = false;
  }

  if (selectedTheme.value.toLowerCase().includes("landing")) {
    hasBeenHereBefore = true;
  }

  if (
    !fromExternalPage ||
    !defaultHomePage ||
    defaultHomePage?.IsHidden ||
    !defaultHomePage?.Text ||
    hasBeenHereBefore
  ) {
    return;
  }

  const isChinaMobile = layout === "mobile-china";
  if (isChinaMobile) {
    const chinaMobileTabMap: { [key: string]: string } = {
      sports: "sport",
      virtualsports: "vSport",
      livecasino: "casino",
      games: "game",
      cockfight: "cockFighting",
      poker: "poker",
      lottery: "lottery",
      keno: "keno",
      promotion: "promotion",
    };
    if (!Object.keys(chinaMobileTabMap).includes(key)) return;
    if (chinaMobileTabMap[key] === "promotion") {
      sessionStorage.setItem(KEYS.HAS_HOME_REDIRECT, "true");
      return navigateTo({
        path: "/promotion",
        query: {
          ...to.query,
        },
      });
    }

    sessionStorage.setItem(KEYS.HAS_HOME_REDIRECT, "true");
    return navigateTo({
      path: "/",
      query: {
        tab: chinaMobileTabMap[key],
        ...to.query,
      },
    });
  }

  if (map[key]) {
    sessionStorage.setItem(KEYS.HAS_HOME_REDIRECT, "true");
    return navigateTo({
      path: map[key],
      query: {
        ...to.query,
      },
    });
  }
});
