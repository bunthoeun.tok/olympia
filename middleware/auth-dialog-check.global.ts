import { store } from "~~/store";
import { STORE_MUTATIONS, KEYS } from "~~/utils/constants";

export default defineNuxtRouteMiddleware((to, _) => {
  return;
  const catchRoutes = ["/login", "/register"];
  const newRoute = to.path.toLowerCase().trim();
  const isInCatchRoutes = catchRoutes.includes(newRoute);
  if (!isInCatchRoutes) return;

  const themes = ["mobile-black", "mobile-main", "mobile-sbo"];
  const shouldUsePopup = themes.includes(String(useState(KEYS.LAYOUT).value));
  if (!shouldUsePopup) return;

  if (newRoute === "/login") {
    store.commit(STORE_MUTATIONS.REGISTER_DIALOG, false);
    store.commit(STORE_MUTATIONS.LOGIN_DIALOG, true);
  }
  if (newRoute === "/register") {
    store.commit(STORE_MUTATIONS.LOGIN_DIALOG, false);
    store.commit(STORE_MUTATIONS.REGISTER_DIALOG, true);
  }

  const router = useRouter();
  const previousRoute = router.options.history.state.back;
  if (!previousRoute) {
    return navigateTo("/");
  } else {
    return abortNavigation();
  }
});
