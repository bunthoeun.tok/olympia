import { store } from "~~/store";

export default defineNuxtRouteMiddleware((_, __) => {
  if (process.server) return;
  const auth = () => store.state.auth;
  if (!auth()) {
    return navigateTo("/login");
  }
});
