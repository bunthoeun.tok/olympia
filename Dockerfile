FROM node:16-alpine as builder
ARG EnvConfig

WORKDIR /app

COPY package*.json ./

RUN export https_proxy=http://10.6.8.2:3128
RUN export http_proxy=http://10.6.8.2:3128

RUN npm set registry https://registry.npmjs.org

RUN npm cache clean --force

RUN export https_proxy=http://10.6.8.2:3128 && npm install

COPY . .
ENV MY_ENV_CONFIG=${EnvConfig}
RUN echo "MY_ENV_CONFIG is set to $MY_ENV_CONFIG"
RUN npm run build:$MY_ENV_CONFIG

RUN rm -rf node_modules

# final stage
FROM --platform=linux/amd64 node:16-alpine

WORKDIR /app

COPY --from=builder /app .

EXPOSE 3000

CMD [ "node", ".output/server/index.mjs" ]