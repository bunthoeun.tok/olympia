import _ from "lodash";
import apis from "@/utils/apis";
import { store } from "@/store";
import {
  IThemePropertiesFromApi,
  ThemePropertiesFromApiDto,
} from "~~/models/companyThemePropertiesRespone";
import { IAllCustomerSettingsData } from "~~/models/getCustomerSettingResponse";

enum EnumKwaiMeta {
  DEPOSIT = "DEPOSIT",
  REGISTER = "REGISTER",
  ADD_TO_CARD = 'ADD_TO_CARD',
}

const checkDecimalAllowed = (e: any, isDecimalAllow: boolean): void => {
  if (!isDecimalAllow && e.key === ".") {
    e.preventDefault();
  }
};

const isUsingPaymentGateway = async (): Promise<boolean> => {
  const response = await apis.getFeaturesToggle();
  if (response.isSuccessful) {
    const { Data } = response;
    const toggle = Data.Toggles[0];
    return toggle.IsUsingPaymentGateway;
  }
  return false;
};
const getThemePropertyFromApi = (
  key: string,
  value: string
): ThemePropertiesFromApiDto => {
  const property = _.find(
    store.state.companyThemePropertiesFromApi.ThemeProperties,
    { [key]: value }
  );
  return new ThemePropertiesFromApiDto(property as IThemePropertiesFromApi);
};
const isCustomerSettingEnabled = (key: string, state: IAllCustomerSettingsData[]): boolean => {
  return (state.find((setting) => setting.Id === key)?.Value)?.toLocaleLowerCase() === 'y';
}


const themeProperties = store.state.companyThemePropertiesFromApi.ThemeProperties;
const tiktokId = themePropertyHelper.getSingleThemeProperty(
  themeProperties,
  "seo_tiktok_pixel_event_key"
);
const kwaiId = themePropertyHelper.getSingleThemeProperty(
  themeProperties,
  "seo_kwai_pixel_event_key"
);
const metaId = themePropertyHelper.getSingleThemeProperty(
  themeProperties,
  "seo_meta_pixel_event_key"
);

const kwaiMetaTrigger = (type: EnumKwaiMeta, amount: number = 0.00, currency: string = "") => {
  if (type === "ADD_TO_CARD" && kwaiId && !kwaiId.IsHidden && kwaiId.Text) {
    kwaiq.instance(`${kwaiId.Text}`).track("addToCart");
  }
  if (type === "DEPOSIT" && kwaiId && !kwaiId.IsHidden && kwaiId.Text) {
    kwaiq.instance(`${kwaiId.Text}`).track("purchase");
  }
  if (type === "DEPOSIT" && metaId && !metaId.IsHidden && metaId.Text) {
    fbq("track", "Purchase", { value: amount, currency: currency });
  }
  if (type === "DEPOSIT" && tiktokId && !tiktokId.IsHidden && tiktokId.Text) {
    ttq.instance(`${tiktokId.Text}`).track("CompletePayment");
  }

  if (type === "REGISTER" && kwaiId && !kwaiId.IsHidden && kwaiId.Text) {
    kwaiq.instance(`${kwaiId.Text}`).track("completeRegistration");
  }
  if (type === "REGISTER" && metaId && !metaId.IsHidden && metaId.Text) {
    fbq("track", "CompleteRegistration");
  }
  if (type === "REGISTER" && tiktokId && !tiktokId.IsHidden && tiktokId.Text) {
    ttq.instance(`${tiktokId.Text}`).track("CompleteRegistration");
  }
};

export default {
  checkDecimalAllowed,
  isUsingPaymentGateway,
  getThemePropertyFromApi,
  isCustomerSettingEnabled,
  kwaiMetaTrigger,
  EnumKwaiMeta,
};
