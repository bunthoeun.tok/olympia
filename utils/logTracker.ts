const catchWindowOnError = (callBackOnEnventTrickered: any) => {
  const onErrorHandler = (event: any) => {
    let msg = event.message;
    if (event.filename) {
      msg += `\\n\\t${event.filename.replace(window.location.origin, '')}`;
      if (event.lineno || event.colno) {
        msg += `:${event.lineno}:${event.colno}`;
      }
    }
    // print error stack info
    const hasStack = !!event.error && !!event.error.stack;
    const stackInfo = (hasStack && event.error.stack.toString()) || '';
    msg += `\\n${stackInfo}`;
    callBackOnEnventTrickered(msg);
  };
  window.removeEventListener('error', onErrorHandler);
  window.addEventListener('error', onErrorHandler);
};
const catchResourceError = (callBackOnEnventTrickered: any) => {
  const resourceErrorHandler = (event: any) => {
    const target = <any>event.target;
    // only catch resources error
    if (['link', 'video', 'script', 'img', 'audio'].indexOf(target.localName) > -1) {
      const src = target.href || target.src || target.currentSrc;
      const msg = `GET <${target.localName}> error: ${src}`;
      callBackOnEnventTrickered(msg);
    }
  };
  window.removeEventListener('error', resourceErrorHandler);
  window.addEventListener('error', resourceErrorHandler, true);
};
const catchUnhandledRejection = (callBackOnEnventTrickered: any) => {
  const rejectionHandler = (event: any) => {
    const error = event && event.reason;
    const errorName = 'Uncaught (in promise) ';
    let args = [errorName, error];
    if (error instanceof Error) {
      // args = [
      //   errorName,
      //   {
      //     name: error.name,
      //     message: error.message,
      //     stack: error.stack,
      //   },
      // ];
      const msg = `[${error.name}][${error.message}] ${error.stack}`
      callBackOnEnventTrickered(msg);
    }
  };
  window.removeEventListener('unhandledrejection', rejectionHandler);
  window.addEventListener('unhandledrejection', rejectionHandler);
};
const error = (callBackOnEnventTrickered: any) => {
  catchWindowOnError(callBackOnEnventTrickered);
  catchUnhandledRejection(callBackOnEnventTrickered);
  // catchResourceError(callBackOnEnventTrickered);
};
export default {
  error,
};
