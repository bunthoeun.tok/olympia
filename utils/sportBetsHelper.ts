import EnumSportBetStatus from '~~/models/enums/enumSportBetStatus';
import EnumOperationStatus from '@/models/enums/enumOperationStatus';

const sportBetsHelper = {
    convertOperationStatusTextColor(status: string) {
        switch (status.toLowerCase()) {
            case EnumOperationStatus.unpaid:
                return 'fc-secondary';
            case EnumOperationStatus.paid:
                return 'fc-hint';
            case EnumOperationStatus.redeemed:
            case EnumOperationStatus.refund:
                return 'fc-success';
            default:
                return '';
        }
    },
    convertOperationStatusTextDisplay(status: string, operatorId: any) {
        if (Number(operatorId)) {
            switch (status.toLowerCase()) {
                case EnumOperationStatus.unpaid:
                    return "Money Uncollected";
                case EnumOperationStatus.paid:
                    return "Money Collected";
                case EnumOperationStatus.redeemed:
                    return "Payout Done";
                case EnumOperationStatus.refund:
                    return "Refunded";
                default:
                    return "";
            }
        }
        return status;
    },
    convertBetStausTextColor(status: string) {
        switch (status.toLowerCase()) {
            case EnumSportBetStatus.running:
            case EnumSportBetStatus.draw:
                return "fc-text";
            case EnumSportBetStatus.won:
            case EnumSportBetStatus.halfwon:
                return "fc-success";
            case EnumSportBetStatus.lost:
            case EnumSportBetStatus.void:
                return "fc-failure";
            case EnumSportBetStatus.halflose:
                return "fc-hint";
            default:
                return "fc-text";
        }
    },
}

export default sportBetsHelper;