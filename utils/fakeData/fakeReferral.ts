import {
  IGetReferralLayerAmountResponse,
  INodeResponse,
  IRedeemRequest,
} from "@/models/referral/getReferral";

const fakeReferralLayerAmount: IGetReferralLayerAmountResponse = {
  ReferralChildrenCount: 10,
  TotalRewardTillYesterday: 4157.23,
  TotalRebateTillYesterday: 100.45,
  TotalReferralAmountTillYesterday: 4957.23,
  Redeemed: 1479,
  WaitingRedeem: 1719.5,
  Redeemable: 958.73,
  IsSuccess: true,
};

const fakeRedeemRequestList: Array<IRedeemRequest> = [
  {
    Id: 118,
    RequestAmount: 121,
    ApprovedAmount: 0,
    Remark: "",
    StatusString: "Waiting",
    RequestDate: "2022-05-03T03:27:02.947",
    ModifiedDate: "2022-05-03T03:27:02.947",
  },
  {
    Id: 117,
    RequestAmount: 121,
    ApprovedAmount: 0,
    Remark: "",
    StatusString: "Waiting",
    RequestDate: "2022-05-03T03:27:02.72",
    ModifiedDate: "2022-05-03T03:27:02.72",
  },
  {
    Id: 118,
    RequestAmount: 121,
    ApprovedAmount: 121,
    Remark: "",
    StatusString: "Approved",
    RequestDate: "2022-05-03T03:27:02.947",
    ModifiedDate: "2022-05-03T03:27:02.947",
  },
  {
    Id: 117,
    RequestAmount: 121,
    ApprovedAmount: 0,
    Remark: "",
    StatusString: "Waiting",
    RequestDate: "2022-05-03T03:27:02.72",
    ModifiedDate: "2022-05-03T03:27:02.72",
  },
  {
    Id: 118,
    RequestAmount: 121,
    ApprovedAmount: 0,
    Remark: "",
    StatusString: "Waiting",
    RequestDate: "2022-05-03T03:27:02.947",
    ModifiedDate: "2022-05-03T03:27:02.947",
  },
  {
    Id: 117,
    RequestAmount: 121,
    ApprovedAmount: 0,
    Remark: "",
    StatusString: "Approved",
    RequestDate: "2022-05-03T03:27:02.72",
    ModifiedDate: "2022-05-03T03:27:02.72",
  },
  {
    Id: 118,
    RequestAmount: 121,
    ApprovedAmount: 0,
    Remark: "",
    StatusString: "Rejected",
    RequestDate: "2022-05-03T03:27:02.947",
    ModifiedDate: "2022-05-03T03:27:02.947",
  },
  {
    Id: 117,
    RequestAmount: 121,
    ApprovedAmount: 0,
    Remark: "test",
    StatusString: "Waiting",
    RequestDate: "2022-05-03T03:27:02.72",
    ModifiedDate: "2022-05-03T03:27:02.72",
  },
  {
    Id: 118,
    RequestAmount: 121,
    ApprovedAmount: 0,
    Remark: "",
    StatusString: "Approved",
    RequestDate: "2022-05-03T03:27:02.947",
    ModifiedDate: "2022-05-03T03:27:02.947",
  },
  {
    Id: 117,
    RequestAmount: 121,
    ApprovedAmount: 0,
    Remark: "",
    StatusString: "Rejected",
    RequestDate: "2022-05-03T03:27:02.72",
    ModifiedDate: "2022-05-03T03:27:02.72",
  },
];

const fakeNodeList: Array<INodeResponse> = [
  {
    CustomerId: 64,
    ParentCustomerId: 64,
    Layer: 0,
    Turnover: 0,
    ThisMonthTurnover: 126900.62,
    LastMonthTurnover: 99800.35,
    NetTurnover: 0,
    ThisMonthNetTurnover: 80000.43,
    LastMonthNetTurnover: 63500.72,
    Currency: "MYR",
    Username: "1TCPlayerMYR",
    IsSuccess: true,
  },
  {
    CustomerId: 65,
    ParentCustomerId: 64,
    Layer: 1,
    Turnover: 0,
    ThisMonthTurnover: 337.75,
    LastMonthTurnover: 498.86,
    NetTurnover: 0,
    ThisMonthNetTurnover: 285.17,
    LastMonthNetTurnover: 322.69,
    Currency: "MYR",
    Username: "1TCPlayerMYR",
    IsSuccess: true,
  },
  {
    CustomerId: 64,
    ParentCustomerId: 10202,
    Layer: 2,
    Turnover: 0,
    ThisMonthTurnover: 0,
    LastMonthTurnover: 0,
    NetTurnover: 0,
    ThisMonthNetTurnover: 0,
    LastMonthNetTurnover: 0,
    Currency: "MYR",
    Username: "1TCPlayerMYR",
    IsSuccess: true,
  },
  {
    CustomerId: 64,
    ParentCustomerId: 65,
    Layer: 2,
    Turnover: 0,
    ThisMonthTurnover: 0,
    LastMonthTurnover: 0,
    NetTurnover: 0,
    ThisMonthNetTurnover: 0,
    LastMonthNetTurnover: 0,
    Currency: "MYR",
    Username: "1TCPlayerMYR",
    IsSuccess: true,
  },
  {
    CustomerId: 654,
    ParentCustomerId: 371,
    Layer: 2,
    Turnover: 0,
    ThisMonthTurnover: 0,
    LastMonthTurnover: 0,
    NetTurnover: 0,
    ThisMonthNetTurnover: 0,
    LastMonthNetTurnover: 0,
    Currency: "MYR",
    Username: "1TCPlayerMYR",
    IsSuccess: true,
  },
  {
    CustomerId: 666,
    ParentCustomerId: 64,
    Layer: 3,
    Turnover: 0,
    ThisMonthTurnover: 0,
    LastMonthTurnover: 0,
    NetTurnover: 0,
    ThisMonthNetTurnover: 0,
    LastMonthNetTurnover: 0,
    Currency: "MYR",
    Username: "1TCPlayerMYR",
    IsSuccess: true,
  },
  {
    CustomerId: 64,
    ParentCustomerId: 10202,
    Layer: 2,
    Turnover: 0,
    ThisMonthTurnover: 0,
    LastMonthTurnover: 0,
    NetTurnover: 0,
    ThisMonthNetTurnover: 0,
    LastMonthNetTurnover: 0,
    Currency: "MYR",
    Username: "1TCPlayerMYR",
    IsSuccess: true,
  },
  {
    CustomerId: 10202,
    ParentCustomerId: 64,
    Layer: 1,
    Turnover: 0,
    ThisMonthTurnover: 0,
    LastMonthTurnover: 0,
    NetTurnover: 0,
    ThisMonthNetTurnover: 0,
    LastMonthNetTurnover: 0,
    Currency: "AUD",
    Username: "12tcplaye****",
    IsSuccess: true,
  },
  {
    CustomerId: 10216,
    ParentCustomerId: 64,
    Layer: 1,
    Turnover: 0,
    ThisMonthTurnover: 0,
    LastMonthTurnover: 0,
    NetTurnover: 0,
    ThisMonthNetTurnover: 0,
    LastMonthNetTurnover: 0,
    Currency: "USD",
    Username: "new****",
    IsSuccess: true,
  },
  {
    CustomerId: 10217,
    ParentCustomerId: 64,
    Layer: 1,
    Turnover: 0,
    ThisMonthTurnover: 0,
    LastMonthTurnover: 0,
    NetTurnover: 0,
    ThisMonthNetTurnover: 0,
    LastMonthNetTurnover: 0,
    Currency: "CAD",
    Username: "newg****",
    IsSuccess: true,
  },
  {
    CustomerId: 371,
    ParentCustomerId: 64,
    Layer: 1,
    Turnover: 0,
    ThisMonthTurnover: 0,
    LastMonthTurnover: 0,
    NetTurnover: 0,
    ThisMonthNetTurnover: 0,
    LastMonthNetTurnover: 0,
    Currency: "AUD",
    Username: "seakhen****",
    IsSuccess: true,
  },
];

export { fakeReferralLayerAmount, fakeRedeemRequestList, fakeNodeList };
