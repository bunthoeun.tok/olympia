const fakeBMD1ThemeProperty = [
  {
    HtmlId: "agent_cooperation",
    ThemeId: 45,
    ThemeName: "Modern_Desktop",
    Type: 0,
    Path: "",
    Height: "",
    Width: "",
    HyperLink: "",
    Text: "",
    ResourceKey: "",
    IsHidden: false,
    IsDefault: true,
    Language: "en",
  },
];

export default fakeBMD1ThemeProperty;
