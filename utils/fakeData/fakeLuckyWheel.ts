const fakeLuckyWheelOptions = [
  {
    Id: 4,
    BonusAmount: 23,
  },
  {
    Id: 44,
    BonusAmount: 432,
  },
  {
    Id: 744,
    BonusAmount: 234,
  },
  {
    Id: 47235,
    BonusAmount: 27,
  },
  {
    Id: 423,
    BonusAmount: 3245,
  },
  {
    Id: 3424,
    BonusAmount: 1234,
  },
  {
    Id: 974,
    BonusAmount: 856,
  },
  {
    Id: 9854,
    BonusAmount: 345,
  },
  {
    Id: 514,
    BonusAmount: 852,
  },
  {
    Id: 889,
    BonusAmount: 7987,
  },
];

export default fakeLuckyWheelOptions;
