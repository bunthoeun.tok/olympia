import { IGameBsi, ISeamlessProviderList } from "@/models/bsiGameList";
import { ITabInfo } from "@/models/IGetPlayableGamesResponse";

const fakeGameList: Array<IGameBsi> = [
  {
    GameProviderName: "Pragmatic Play",
    GameProviderShortName: "PP",
    GameProviderId: 3,
    GameId: 2248,
    Category: 3,
    CategoryName: "GAMES",
    GameType: "Slots",
    GameName: "\\7 Piggies™ 25",
    IsEnabled: true,
    IsHotGame: false,
    Rank: 178,
    IsNewGame: false,
    IsJackpot: false,
    IsMaintainByGameProvider: false,
    IsMaintain: false,
    ForMobile: true,
    ForDesktop: true,
    IsPromotionGame: false,
    BsiPriority: 0,
    AsiPriority: 0,
    GameIconUrl:
      "https://sbo-tw1.prerelease-env.biz/game_pic/rec/160/sc7piggiesai.png",
    EnabledCurrency:
      "AUD,BDT,BND,CAD,CHF,CNY,EUR,GBP,HKD,IDR,INR,JPY,KRW,MMK,MYR,NOK,NPR,NZD,PHP,SEK,THB,USD,VND,ZAR",
    GameCode: null,
    Device: 3,
    APIGameProviderName: "SBOBET",
    APIGameProviderId: 1,
    DisplayOrder: 34,
    IsEnabledByGameProvider: true,
    TabType: "Games",
  },
  {
    GameProviderName: "Pragmatic Play",
    GameProviderShortName: "PP",
    GameProviderId: 3,
    GameId: 2250,
    Category: 3,
    CategoryName: "GAMES",
    GameType: "",
    GameName: "\\Queen of Gold™ 100",
    IsEnabled: true,
    IsHotGame: false,
    Rank: 180,
    IsNewGame: false,
    IsJackpot: false,
    IsMaintainByGameProvider: false,
    IsMaintain: false,
    ForMobile: true,
    ForDesktop: true,
    IsPromotionGame: false,
    BsiPriority: 0,
    AsiPriority: 0,
    GameIconUrl:
      "https://sbo-tw1.prerelease-env.biz/game_pic/rec/160/scqogai.png",
    EnabledCurrency:
      "AUD,BDT,BND,CAD,CHF,CNY,EUR,GBP,HKD,IDR,INR,JPY,KRW,MMK,MYR,NOK,NPR,NZD,PHP,SEK,THB,USD,VND,ZAR",
    GameCode: null,
    Device: 3,
    APIGameProviderName: "SBOBET",
    APIGameProviderId: 1,
    DisplayOrder: 34,
    IsEnabledByGameProvider: true,
    TabType: "Games",
  },
];
const fakeTabInfoList: Array<ITabInfo> = [
  {
    TabType: "Sports",
    Order: 1,
    IsFilter: false,
  },
  {
    TabType: "Virtual Sports",
    Order: 2,
    IsFilter: false,
  },
  {
    TabType: "Live Casino",
    Order: 3,
    IsFilter: false,
  },
  {
    TabType: "Games",
    Order: 4,
    IsFilter: true,
  },
  {
    TabType: "Keno",
    Order: 5,
    IsFilter: false,
  },
];
const fakeSeamlessProviderList: Array<ISeamlessProviderList> = [
  {
    ProviderName: "Pragmatic Play",
    HasNewGame: true,
    HasPromoGame: false,
  },
  {
    ProviderName: "MicroGaming",
    HasNewGame: true,
    HasPromoGame: false,
  },
  {
    ProviderName: "FunkyGames",
    HasNewGame: false,
    HasPromoGame: false,
  },
  {
    ProviderName: "Flow Gaming",
    HasNewGame: true,
    HasPromoGame: false,
  },
  {
    ProviderName: "WorldMatch",
    HasNewGame: false,
    HasPromoGame: false,
  },
  {
    ProviderName: "CQNine",
    HasNewGame: false,
    HasPromoGame: false,
  },
  {
    ProviderName: "Yggdrasil",
    HasNewGame: false,
    HasPromoGame: false,
  },
  {
    ProviderName: "Gamatron",
    HasNewGame: false,
    HasPromoGame: false,
  },
  {
    ProviderName: "RealTimeGaming",
    HasNewGame: false,
    HasPromoGame: false,
  },
  {
    ProviderName: "JokerGaming",
    HasNewGame: false,
    HasPromoGame: false,
  },
  {
    ProviderName: "Red Tiger",
    HasNewGame: false,
    HasPromoGame: false,
  },
  {
    ProviderName: "PGSoft",
    HasNewGame: false,
    HasPromoGame: false,
  },
  {
    ProviderName: "SBO Games (New)",
    HasNewGame: false,
    HasPromoGame: false,
  },
  {
    ProviderName: "SBOBET",
    HasNewGame: false,
    HasPromoGame: false,
  },
  {
    ProviderName: "GiocoPlus",
    HasNewGame: false,
    HasPromoGame: false,
  },
  {
    ProviderName: "Huawei 4D",
    HasNewGame: false,
    HasPromoGame: false,
  },
  {
    ProviderName: "LionKing",
    HasNewGame: true,
    HasPromoGame: false,
  },
];
const fakeSeamlessGameGameTypeList: Array<string> = [
  "Slots",
  "TableGames",
  "FishingArcade",
  "OthersGames",
  "SportsBook",
  "HiLoNumber",
  "Lobby",
];
const fakeSeamlessGameTabList: Array<string> = [
  "Top Games",
  "New Games",
  "Game Type",
  "Provider",
];
export {
  fakeSeamlessGameTabList,
  fakeGameList,
  fakeSeamlessProviderList,
  fakeTabInfoList,
  fakeSeamlessGameGameTypeList,
};
