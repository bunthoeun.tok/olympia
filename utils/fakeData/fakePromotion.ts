/* eslint-disable */
import { IPromotion } from "@/models/promotion/promotion";

const fakePromotion: Array<IPromotion> = [
  {
    Id: 529,
    Title: "キャッシュバック50％",
    Content:
      '<br>※提出手順※<br/>1: 初回入金のご資金が100円未満になった方が対象です。<br/><br/>2: 最下部にある<font color="red">「提出」ボタンをクリック</font>するとプロモーションの申請が完了します。<br/><br/>※ご注意※<br/>1:ご出金をされた方は対象外となります。<br/><br/>2: キャッシュバックのご出金を行うには、キャッシュバック額の6倍以上をベットされた方のみ対象となります。ドローゲームはノーカウントとなります。<br/><br/>3: 最低賭け条件をクリアできなかった場合で、別プロモーション「6％ボーナス」の提出ボタンが反応しない時は、お手数ですがカスタマーサービスまでご連絡ください。<br/><br/>4:キャッシュバックの有効期間は受け取った日から起算し30日となります。この期間を過ぎますと残余キャッシュバック額は失効しますのでご注意ください。',
    PromoCode: "DEPOSITCASHBACK50",
    Remark: null,
    ImagePath: "1163yy_Id_pr1.jpg",
    ProductType: -1,
    GameType: "DEPOSIT_BONUS",
    PromotionTypeId: 2,
    PromotionType: "First Deposit Bonus by Percentage",
    Rollover: 2,
    Percentage: 50,
    Amount: 0,
    MaxAmount: 100000000,
    Status: 1,
    StartDate: "2021-08-01T00:00:00",
    EndDate: "2021-12-31T00:00:00",
    ModifiedBy: "GoldPigpro",
    ModifiedOn: "2021-08-24T07:51:19.4",
    DisplayOrder: 6,
  },
];

export default fakePromotion;
