import { IGetRegisterBankListRequest, IPlayerBankOption } from "@/models/bank";
import { ICurrencyWhiteList } from "@/models/currencyWhiteList";
import { IRegisterResponse } from "@/models/register";
import { IAvailablePropertyListDto } from "@/models/getTransactionBankListResponse";

const currencyWhiteList: Array<ICurrencyWhiteList> = [
  {
    WebId: 1,
    Currency: "AUD",
    CountryCode: "AUD",
  },
  {
    WebId: 1,
    Currency: "CAD",
    CountryCode: "CA",
  },
  {
    WebId: 1,
    Currency: "CHF",
    CountryCode: "CH",
  },
  {
    WebId: 1,
    Currency: "CNY",
    CountryCode: "--",
  },
  {
    WebId: 1,
    Currency: "GBP",
    CountryCode: "--",
  },
  {
    WebId: 1,
    Currency: "HKD",
    CountryCode: "HK,MO,KH",
  },
  {
    WebId: 1,
    Currency: "IDR",
    CountryCode: "IDR",
  },
  {
    WebId: 1,
    Currency: "JPY",
    CountryCode: "JP,MY",
  },
  {
    WebId: 1,
    Currency: "KRW",
    CountryCode: "KR",
  },
  {
    WebId: 1,
    Currency: "MMK",
    CountryCode: "MM",
  },
  {
    WebId: 1,
    Currency: "MYR",
    CountryCode: "US,KH,PH",
  },
];

const availablePropertyList: Array<IAvailablePropertyListDto> = [
  {
    PropertyStatus: "Required",
    PropertyName: "FullName",
    DisplayOrder: 2,
  },
  {
    PropertyStatus: "Optional",
    PropertyName: "Referral",
    DisplayOrder: 1,
  },
  {
    PropertyStatus: "Optional",
    PropertyName: "Gender",
    DisplayOrder: 3,
  },
  {
    PropertyStatus: "Optional",
    PropertyName: "Email",
    DisplayOrder: 4,
  },
  {
    PropertyStatus: "Optional",
    PropertyName: "DateOfBirth",
    DisplayOrder: 5,
  },
  {
    PropertyStatus: "Optional",
    PropertyName: "Nationality",
    DisplayOrder: 6,
  },
  {
    PropertyStatus: "Optional",
    PropertyName: "Phone",
    DisplayOrder: 7,
  },
  {
    PropertyStatus: "Optional",
    PropertyName: "Mobile",
    DisplayOrder: 8,
  },
  {
    PropertyStatus: "Optional",
    PropertyName: "Line",
    DisplayOrder: 9,
  },
  {
    PropertyStatus: "Optional",
    PropertyName: "PersonalSecurityCode",
    DisplayOrder: 10,
  },
  {
    PropertyStatus: "Required",
    PropertyName: "FirstName",
    DisplayOrder: 11,
  },
  {
    PropertyStatus: "Optional",
    PropertyName: "LastName",
    DisplayOrder: 12,
  },
  {
    PropertyStatus: "Optional",
    PropertyName: "Bank",
    DisplayOrder: 13,
  },
  {
    PropertyStatus: "Optional",
    PropertyName: "BankAccountNumber",
    DisplayOrder: 13,
  },
  {
    PropertyStatus: "Optional",
    PropertyName: "BankAccountName",
    DisplayOrder: 13,
  },
];

const playerBankOptionsMYR: Array<IPlayerBankOption> = [
  {
    Id: 12,
    BankName: "DemoBankMYR",
    Priority: 1,
  },
  {
    Id: 5462,
    BankName: "Cryptocurrency_BTC",
    Priority: 0,
  },
];

const playerBankOptionsCNY: Array<IPlayerBankOption> = [
  {
    Id: 13,
    BankName: "DemoBankCNY",
    Priority: 1,
  },
  {
    Id: 5465,
    BankName: "Cryptocurrency_BTC",
    Priority: 0,
  },
];

const playerBankOptions: Array<IPlayerBankOption> = [
  {
    Id: 19,
    BankName: "DemoBankGeneral",
    Priority: 1,
  },
  {
    Id: 5562,
    BankName: "Cryptocurrency_BTC",
    Priority: 0,
  },
];

const CARegisterResponse: IRegisterResponse = {
  Username: "tsavongbcaud",
  LoginName: "tsavongbcaud",
  CustomerId: 1040852,
  ParentId: 9624,
  Currency: "MYR",
  CashBalance: 0,
  CreditBalance: 0,
  IsCashPlayer: true,
  ErrorCode: 0,
  Message: "success",
  Games: [
    {
      GameProviderName: "Pragmatic Play",
      GameProviderShortName: "PP",
      GameProviderId: 3,
      GameId: 2248,
      Category: 3,
      CategoryName: "GAMES",
      GameType: "Slots",
      GameName: '"7 Piggies™ 25',
      IsEnabled: true,
      IsHotGame: false,
      Rank: 178,
      IsNewGame: false,
      IsJackpot: false,
      IsMaintainByGameProvider: false,
      IsMaintain: false,
      ForMobile: true,
      ForDesktop: true,
      IsPromotionGame: false,
      BsiPriority: 0,
      AsiPriority: 0,
      GameIconUrl:
        "https://sbo-tw1.prerelease-env.biz/game_pic/rec/160/sc7piggiesai.png",
      EnabledCurrency:
        "AUD,BDT,BND,CAD,CHF,CNY,EUR,GBP,HKD,IDR,INR,JPY,KRW,MMK,MYR,NOK,NPR,NZD,PHP,SEK,THB,USD,VND,ZAR",
      GameCode: null,
      Device: 3,
      APIGameProviderName: "SBOBET",
      APIGameProviderId: 1,
      DisplayOrder: 34,
      IsEnabledByGameProvider: true,
      TabType: "Games",
    },
  ],
  TabInfos: [
    {
      TabType: "Games",
      Order: 4,
      IsFilter: true,
    },
  ],
  OnlineId: 17096,
  AccountType: 1,
  SeamlessGameGameTypeList: null,
  SeamlessGameTabList: null,
  SeamlessProviderList: null,
  IsSuccess: true,
};

const playerRegisterResponse: IRegisterResponse = {
  Username: "Savongmyr",
  LoginName: "Savongmyr",
  CustomerId: 1040853,
  ParentId: 2,
  Currency: "MYR",
  CashBalance: 0,
  CreditBalance: 0,
  IsCashPlayer: true,
  ErrorCode: 0,
  Message: "success",
  Games: [
    {
      GameProviderName: "Pragmatic Play",
      GameProviderShortName: "PP",
      GameProviderId: 3,
      GameId: 2248,
      Category: 3,
      CategoryName: "GAMES",
      GameType: "Slots",
      GameName: '"7 Piggies™ 25',
      IsEnabled: true,
      IsHotGame: false,
      Rank: 178,
      IsNewGame: false,
      IsJackpot: false,
      IsMaintainByGameProvider: false,
      IsMaintain: false,
      ForMobile: true,
      ForDesktop: true,
      IsPromotionGame: false,
      BsiPriority: 0,
      AsiPriority: 0,
      GameIconUrl:
        "https://sbo-tw1.prerelease-env.biz/game_pic/rec/160/sc7piggiesai.png",
      EnabledCurrency:
        "AUD,BDT,BND,CAD,CHF,CNY,EUR,GBP,HKD,IDR,INR,JPY,KRW,MMK,MYR,NOK,NPR,NZD,PHP,SEK,THB,USD,VND,ZAR",
      GameCode: null,
      Device: 3,
      APIGameProviderName: "SBOBET",
      APIGameProviderId: 1,
      DisplayOrder: 34,
      IsEnabledByGameProvider: true,
      TabType: "Games",
    },
  ],
  TabInfos: [
    {
      TabType: "Games",
      Order: 4,
      IsFilter: true,
    },
  ],
  OnlineId: 17099,
  AccountType: 1,
  SeamlessGameGameTypeList: ["Slots", "TableGames"],
  SeamlessGameTabList: ["Top Games", "New Games", "Game Type", "Provider"],
  SeamlessProviderList: [
    {
      ProviderName: "Pragmatic Play",
      HasNewGame: true,
      HasPromoGame: false,
    },
    {
      ProviderName: "Yggdrasil",
      HasNewGame: false,
      HasPromoGame: false,
    },
  ],
  IsSuccess: true,
};

const getPlayerBankOptions = (
  args: IGetRegisterBankListRequest
): Array<IPlayerBankOption> => {
  if (args.Currency === "CNY") {
    return playerBankOptionsCNY;
  }
  if (args.Currency === "MYR") {
    return playerBankOptionsMYR;
  }
  return playerBankOptions;
};

export {
  availablePropertyList,
  CARegisterResponse,
  currencyWhiteList,
  getPlayerBankOptions,
  playerRegisterResponse,
};
