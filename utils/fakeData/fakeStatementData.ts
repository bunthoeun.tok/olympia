import { IStatementResponse } from "@/models/statement";

const statement: IStatementResponse = {
  StatementList: [
    {
      Date: "2022-05-01T08:36:38.643",
      StatementType: "Open Balance",
      Total: 1077,
      Comm: 0,
      Running_Total: 1077,
    },
    {
      Date: "2022-05-05T00:00:00",
      StatementType: "Transaction",
      Total: 123,
      Comm: 0,
      Running_Total: 1200,
    },
    {
      Date: "2022-05-06T00:00:00",
      StatementType: "Transaction",
      Total: -100,
      Comm: 0,
      Running_Total: 1100,
    },
  ],
  IsSuccess: true,
};

export default statement;
