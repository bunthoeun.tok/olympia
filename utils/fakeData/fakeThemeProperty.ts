import { IThemePropertiesFromApi } from "@/models/companyThemePropertiesRespone";

const fakeThemeProperty: Array<IThemePropertiesFromApi> = [
  {
    HtmlId: "bfm1_about_us_detail_info_6_1",
    ThemeId: 30,
    ThemeName: "Black_Flexible_Mobile_1",
    Type: 0,
    Path: "",
    Height: "",
    Width: "",
    HyperLink: "",
    Text: "Customer Service specialists are available 24/7 to answer all of your queries",
    ResourceKey: "",
    IsHidden: false,
    IsDefault: false,
    Language: "en",
    ImagePath: "",
  },
];
export default fakeThemeProperty;
