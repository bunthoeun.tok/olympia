import { IGetProfileResponse } from "@/models/getProfileResponse";

const fakeProfile: IGetProfileResponse = {
  Information: {
    Username: "test2232myr",
    LoginName: "test2232myr",
    Email: "",
    Phone: "",
    Mobile: "",
    Currency: "MYR",
    Country: "TW",
    Bank: "myr test 13124 12345",
    PaymentBank: "Maybank Lapii Demo Account 12345",
    LastLoginOn: "2022-05-10T05:00:06.493",
    LastTransactionOn: "2022-05-03T04:09:50.733",
    PasswordExpiryOn: "2022-07-04T11:32:47.193",
  },
  ProfileDetail: [
    {
      PropertyName: "Email",
      PropertyValue: "",
    },
    {
      PropertyName: "Gender",
      PropertyValue: "male",
    },
    {
      PropertyName: "Phone",
      PropertyValue: "324",
    },
  ],
  IsSuccess: true,
  ErrorCode: 0,
  ErrorMessage: "",
};

export default fakeProfile;
