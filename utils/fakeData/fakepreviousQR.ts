import { IPreviousQR, IPreviousSession } from "~~/models/sportqr/previousQR";

const fakepreviousQRList: Array<IPreviousSession> = [
  {
    OnlineId: 987654,
    CustomerId: 543210,
    Username: "janedoe456",
    Token:
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c",
    LoginTime: "2023-06-18T12:45:00Z",
    TotalActualStake: 250.75,
  },
  {
    OnlineId: 246810,
    CustomerId: 135792,
    Username: "johndoe789",
    Token:
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c",
    LoginTime: "2023-06-17T09:15:00Z",
    TotalActualStake: 1000.0,
  },
  {
    OnlineId: 369121,
    CustomerId: 246801,
    Username: "janedoe789",
    Token:
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c",
    LoginTime: "2023-06-16T16:30:00Z",
    TotalActualStake: 750.5,
  },
  {
    OnlineId: 135791,
    CustomerId: 369120,
    Username: "johndoe456",
    Token:
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c",
    LoginTime: "2023-06-15T21:00:00Z",
    TotalActualStake: 125.25,
  },
];
const fakePreviousQR: IPreviousQR = {
  PreviousSessions: fakepreviousQRList,
  ErrorCode: 200,
  ErrorMessage: "Success",
  IsSuccess: true,
};

export { fakePreviousQR, fakepreviousQRList };

