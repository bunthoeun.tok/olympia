import * as CryptoJS from 'crypto-js';

const generateCryptoToken = (text: string) => {
    const keyBytes = CryptoJS.enc.Utf8.parse('GJ@#*vj237vh1238vWSDf238vs@!#f^w');
    const encrypted = CryptoJS.AES.encrypt(String(text), keyBytes, { iv: CryptoJS.lib.WordArray.create(0) }).toString();
    return encrypted;
}
export {
    generateCryptoToken,
}