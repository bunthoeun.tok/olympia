import { store } from "@/store";

const getImagePathBankLogo = (fileName: string): string => {
  const cdn = commonHelper.getThemePropertiesCdn();

  const version = store.state.imageVersion;
  const fullImagePath = `${cdn}/storage/bankLogo/WebId-${store.state.webId}/${fileName}?v=${version}`;
  return fullImagePath;
};

export default {
  getImagePathBankLogo,
};
