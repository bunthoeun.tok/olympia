export type CountryList = {
  countryName: string
  countryShortCode: string
  phoneNumberCode: number
}
export const  countryList: CountryList[] = [
  {
      countryName: "United States",
      countryShortCode: "US",
      phoneNumberCode: 1
  },
  {
      countryName: "Russian Federation",
      countryShortCode: "RU",
      phoneNumberCode: 7
  },
  {
      countryName: "Egypt",
      countryShortCode: "EG",
      phoneNumberCode: 20
  },
  {
      countryName: "South Africa",
      countryShortCode: "ZA",
      phoneNumberCode: 27
  },
  {
      countryName: "Greece",
      countryShortCode: "GR",
      phoneNumberCode: 30
  },
  {
      countryName: "Netherlands",
      countryShortCode: "NL",
      phoneNumberCode: 31
  },
  {
      countryName: "Belgium",
      countryShortCode: "BE",
      phoneNumberCode: 32
  },
  {
      countryName: "France",
      countryShortCode: "FR",
      phoneNumberCode: 33
  },
  {
      countryName: "Spain",
      countryShortCode: "ES",
      phoneNumberCode: 34
  },
  {
      countryName: "Hungary",
      countryShortCode: "HU",
      phoneNumberCode: 36
  },
  {
      countryName: "Italy",
      countryShortCode: "IT",
      phoneNumberCode: 39
  },
  {
      countryName: "Romania",
      countryShortCode: "RO",
      phoneNumberCode: 40
  },
  {
      countryName: "Switzerland",
      countryShortCode: "CH",
      phoneNumberCode: 41
  },
  {
      countryName: "Austria",
      countryShortCode: "AT",
      phoneNumberCode: 43
  },
  {
      countryName: "United Kingdom",
      countryShortCode: "GB",
      phoneNumberCode: 44
  },
  {
      countryName: "Denmark",
      countryShortCode: "DK",
      phoneNumberCode: 45
  },
  {
      countryName: "Sweden",
      countryShortCode: "SE",
      phoneNumberCode: 46
  },
  {
      countryName: "Norway",
      countryShortCode: "NO",
      phoneNumberCode: 47,
  },
  {
      countryName: "Poland",
      countryShortCode: "PL",
      phoneNumberCode: 48
  },
  {
      countryName: "Germany",
      countryShortCode: "DE",
      phoneNumberCode: 49
  },
  {
      countryName: "Peru",
      countryShortCode: "PE",
      phoneNumberCode: 51
  },
  {
      countryName: "Mexico",
      countryShortCode: "MX",
      phoneNumberCode: 52
  },
  {
      countryName: "Cuba",
      countryShortCode: "CU",
      phoneNumberCode: 53
  },
  {
      countryName: "Argentina",
      countryShortCode: "AR",
      phoneNumberCode: 54
  },
  {
      countryName: "Brazil",
      countryShortCode: "BR",
      phoneNumberCode: 55
  },
  {
      countryName: "Chile",
      countryShortCode: "CL",
      phoneNumberCode: 56
  },
  {
      countryName: "Colombia",
      countryShortCode: "CO",
      phoneNumberCode: 57
  },
  {
      countryName: "Venezuela, Bolivarian Republic of",
      countryShortCode: "VE",
      phoneNumberCode: 58
  },
  {
      countryName: "Malaysia",
      countryShortCode: "MY",
      phoneNumberCode: 60
  },
  {
      countryName: "Cocos (Keeling) Islands",
      countryShortCode: "CC",
      phoneNumberCode: 61
  },
  {
      countryName: "Indonesia",
      countryShortCode: "ID",
      phoneNumberCode: 62
  },
  {
      countryName: "Philippines",
      countryShortCode: "PH",
      phoneNumberCode: 63
  },
  {
      countryName: "New Zealand",
      countryShortCode: "NZ",
      phoneNumberCode: 64
  },
  {
      countryName: "Singapore",
      countryShortCode: "SG",
      phoneNumberCode: 65
  },
  {
      countryName: "Thailand",
      countryShortCode: "TH",
      phoneNumberCode: 66
  },
  {
      countryName: "Kazakhstan",
      countryShortCode: "KZ",
      phoneNumberCode: 77
  },
  {
      countryName: "Japan",
      countryShortCode: "JP",
      phoneNumberCode: 81
  },
  {
      countryName: "Korea, Republic of",
      countryShortCode: "KR",
      phoneNumberCode: 82
  },
  {
      countryName: "Vietnam",
      countryShortCode: "VN",
      phoneNumberCode: 84
  },
  {
      countryName: "China",
      countryShortCode: "CN",
      phoneNumberCode: 86
  },
  {
      countryName: "Turkey",
      countryShortCode: "TR",
      phoneNumberCode: 90
  },
  {
      countryName: "India",
      countryShortCode: "IN",
      phoneNumberCode: 91
  },
  {
      countryName: "Pakistan",
      countryShortCode: "PK",
      phoneNumberCode: 92
  },
  {
      countryName: "Afghanistan",
      countryShortCode: "AF",
      phoneNumberCode: 93
  },
  {
      countryName: "Sri Lanka",
      countryShortCode: "LK",
      phoneNumberCode: 94
  },
  {
      countryName: "Myanmar",
      countryShortCode: "MM",
      phoneNumberCode: 95
  },
  {
      countryName: "Iran, Islamic Republic of",
      countryShortCode: "IR",
      phoneNumberCode: 98
  },
  {
      countryName: "South Sudan",
      countryShortCode: "SS",
      phoneNumberCode: 211
  },
  {
      countryName: "Morocco",
      countryShortCode: "MA",
      phoneNumberCode: 212
  },
  {
      countryName: "Algeria",
      countryShortCode: "DZ",
      phoneNumberCode: 213
  },
  {
      countryName: "Tunisia",
      countryShortCode: "TN",
      phoneNumberCode: 216
  },
  {
      countryName: "Libya",
      countryShortCode: "LY",
      phoneNumberCode: 218
  },
  {
      countryName: "Gambia, The",
      countryShortCode: "GM",
      phoneNumberCode: 220
  },
  {
      countryName: "Senegal",
      countryShortCode: "SN",
      phoneNumberCode: 221
  },
  {
      countryName: "Mauritania",
      countryShortCode: "MR",
      phoneNumberCode: 222
  },
  {
      countryName: "Mali",
      countryShortCode: "ML",
      phoneNumberCode: 223
  },
  {
      countryName: "Guinea",
      countryShortCode: "GN",
      phoneNumberCode: 224
  },
  {
      countryName: "Côte d'Ivoire, Republic of",
      countryShortCode: "CI",
      phoneNumberCode: 225
  },
  {
      countryName: "Burkina Faso",
      countryShortCode: "BF",
      phoneNumberCode: 226
  },
  {
      countryName: "Niger",
      countryShortCode: "NE",
      phoneNumberCode: 227
  },
  {
      countryName: "Togo",
      countryShortCode: "TG",
      phoneNumberCode: 228
  },
  {
      countryName: "Benin",
      countryShortCode: "BJ",
      phoneNumberCode: 229
  },
  {
      countryName: "Mauritius",
      countryShortCode: "MU",
      phoneNumberCode: 230
  },
  {
      countryName: "Liberia",
      countryShortCode: "LR",
      phoneNumberCode: 231
  },
  {
      countryName: "Sierra Leone",
      countryShortCode: "SL",
      phoneNumberCode: 232
  },
  {
      countryName: "Ghana",
      countryShortCode: "GH",
      phoneNumberCode: 233
  },
  {
      countryName: "Nigeria",
      countryShortCode: "NG",
      phoneNumberCode: 234
  },
  {
      countryName: "Chad",
      countryShortCode: "TD",
      phoneNumberCode: 235
  },
  {
      countryName: "Central African Republic",
      countryShortCode: "CF",
      phoneNumberCode: 236
  },
  {
      countryName: "Cameroon",
      countryShortCode: "CM",
      phoneNumberCode: 237
  },
  {
      countryName: "Cape Verde",
      countryShortCode: "CV",
      phoneNumberCode: 238
  },
  {
      countryName: "Sao Tome and Principe",
      countryShortCode: "ST",
      phoneNumberCode: 239
  },
  {
      countryName: "Equatorial Guinea",
      countryShortCode: "GQ",
      phoneNumberCode: 240
  },
  {
      countryName: "Gabon",
      countryShortCode: "GA",
      phoneNumberCode: 241
  },
  {
      countryName: "Congo, Republic of the (Brazzaville)",
      countryShortCode: "CG",
      phoneNumberCode: 242
  },
  {
      countryName: "Congo, the Democratic Republic of the (Kinshasa)",
      countryShortCode: "CD",
      phoneNumberCode: 243
  },
  {
      countryName: "Angola",
      countryShortCode: "AO",
      phoneNumberCode: 244
  },
  {
      countryName: "Guinea-Bissau",
      countryShortCode: "GW",
      phoneNumberCode: 245
  },
  {
      countryName: "British Indian Ocean Territory",
      countryShortCode: "IO",
      phoneNumberCode: 246
  },
  {
      countryName: "Seychelles",
      countryShortCode: "SC",
      phoneNumberCode: 248
  },
  {
      countryName: "Sudan",
      countryShortCode: "SD",
      phoneNumberCode: 249
  },
  {
      countryName: "Rwanda",
      countryShortCode: "RW",
      phoneNumberCode: 250
  },
  {
      countryName: "Ethiopia",
      countryShortCode: "ET",
      phoneNumberCode: 251
  },
  {
      countryName: "Somalia",
      countryShortCode: "SO",
      phoneNumberCode: 252
  },
  {
      countryName: "Djibouti",
      countryShortCode: "DJ",
      phoneNumberCode: 253
  },
  {
      countryName: "Kenya",
      countryShortCode: "KE",
      phoneNumberCode: 254
  },
  {
      countryName: "Tanzania, United Republic of",
      countryShortCode: "TZ",
      phoneNumberCode: 255
  },
  {
      countryName: "Uganda",
      countryShortCode: "UG",
      phoneNumberCode: 256
  },
  {
      countryName: "Burundi",
      countryShortCode: "BI",
      phoneNumberCode: 257
  },
  {
      countryName: "Mozambique",
      countryShortCode: "MZ",
      phoneNumberCode: 258
  },
  {
      countryName: "Zambia",
      countryShortCode: "ZM",
      phoneNumberCode: 260
  },
  {
      countryName: "Madagascar",
      countryShortCode: "MG",
      phoneNumberCode: 261
  },
  {
      countryName: "Réunion",
      countryShortCode: "RE",
      phoneNumberCode: 262
  },
  {
      countryName: "Zimbabwe",
      countryShortCode: "ZW",
      phoneNumberCode: 263
  },
  {
      countryName: "Namibia",
      countryShortCode: "NA",
      phoneNumberCode: 264
  },
  {
      countryName: "Malawi",
      countryShortCode: "MW",
      phoneNumberCode: 265
  },
  {
      countryName: "Lesotho",
      countryShortCode: "LS",
      phoneNumberCode: 266
  },
  {
      countryName: "Botswana",
      countryShortCode: "BW",
      phoneNumberCode: 267
  },
  {
      countryName: "Swaziland",
      countryShortCode: "SZ",
      phoneNumberCode: 268
  },
  {
      countryName: "Comoros",
      countryShortCode: "KM",
      phoneNumberCode: 269
  },
  {
      countryName: "Saint Helena, Ascension and Tristan da Cunha",
      countryShortCode: "SH",
      phoneNumberCode: 290
  },
  {
      countryName: "Eritrea",
      countryShortCode: "ER",
      phoneNumberCode: 291
  },
  {
      countryName: "Aruba",
      countryShortCode: "AW",
      phoneNumberCode: 297
  },
  {
      countryName: "Faroe Islands",
      countryShortCode: "FO",
      phoneNumberCode: 298
  },
  {
      countryName: "Greenland",
      countryShortCode: "GL",
      phoneNumberCode: 299
  },
  {
      countryName: "Gibraltar",
      countryShortCode: "GI",
      phoneNumberCode: 350
  },
  {
      countryName: "Portugal",
      countryShortCode: "PT",
      phoneNumberCode: 351
  },
  {
      countryName: "Luxembourg",
      countryShortCode: "LU",
      phoneNumberCode: 352
  },
  {
      countryName: "Ireland",
      countryShortCode: "IE",
      phoneNumberCode: 353
  },
  {
      countryName: "Iceland",
      countryShortCode: "IS",
      phoneNumberCode: 354
  },
  {
      countryName: "Albania",
      countryShortCode: "AL",
      phoneNumberCode: 355
  },
  {
      countryName: "Malta",
      countryShortCode: "MT",
      phoneNumberCode: 356
  },
  {
      countryName: "Cyprus",
      countryShortCode: "CY",
      phoneNumberCode: 357
  },
  {
      countryName: "Finland",
      countryShortCode: "FI",
      phoneNumberCode: 358
  },
  {
      countryName: "Bulgaria",
      countryShortCode: "BG",
      phoneNumberCode: 359
  },
  {
      countryName: "Lithuania",
      countryShortCode: "LT",
      phoneNumberCode: 370
  },
  {
      countryName: "Latvia",
      countryShortCode: "LV",
      phoneNumberCode: 371
  },
  {
      countryName: "Estonia",
      countryShortCode: "EE",
      phoneNumberCode: 372
  },
  {
      countryName: "Moldova",
      countryShortCode: "MD",
      phoneNumberCode: 373
  },
  {
      countryName: "Armenia",
      countryShortCode: "AM",
      phoneNumberCode: 374
  },
  {
      countryName: "Belarus",
      countryShortCode: "BY",
      phoneNumberCode: 375
  },
  {
      countryName: "Andorra",
      countryShortCode: "AD",
      phoneNumberCode: 376
  },
  {
      countryName: "Monaco",
      countryShortCode: "MC",
      phoneNumberCode: 377
  },
  {
      countryName: "San Marino",
      countryShortCode: "SM",
      phoneNumberCode: 378
  },
  {
      countryName: "Holy See (Vatican City)",
      countryShortCode: "VA",
      phoneNumberCode: 379
  },
  {
      countryName: "Ukraine",
      countryShortCode: "UA",
      phoneNumberCode: 380
  },
  {
      countryName: "Serbia",
      countryShortCode: "RS",
      phoneNumberCode: 381
  },
  {
      countryName: "Montenegro",
      countryShortCode: "ME",
      phoneNumberCode: 382
  },
  {
      countryName: "Croatia",
      countryShortCode: "HR",
      phoneNumberCode: 385
  },
  {
      countryName: "Slovenia",
      countryShortCode: "SI",
      phoneNumberCode: 386
  },
  {
      countryName: "Bosnia and Herzegovina",
      countryShortCode: "BA",
      phoneNumberCode: 387
  },
  {
      countryName: "Macedonia, Republic of",
      countryShortCode: "MK",
      phoneNumberCode: 389
  },
  {
      countryName: "Czech Republic",
      countryShortCode: "CZ",
      phoneNumberCode: 420
  },
  {
      countryName: "Slovakia",
      countryShortCode: "SK",
      phoneNumberCode: 421
  },
  {
      countryName: "Liechtenstein",
      countryShortCode: "LI",
      phoneNumberCode: 423
  },
  {
      countryName: "South Georgia and South Sandwich Islands",
      countryShortCode: "GS",
      phoneNumberCode: 500
  },
  {
      countryName: "Belize",
      countryShortCode: "BZ",
      phoneNumberCode: 501
  },
  {
      countryName: "Guatemala",
      countryShortCode: "GT",
      phoneNumberCode: 502
  },
  {
      countryName: "El Salvador",
      countryShortCode: "SV",
      phoneNumberCode: 503
  },
  {
      countryName: "Honduras",
      countryShortCode: "HN",
      phoneNumberCode: 504
  },
  {
      countryName: "Nicaragua",
      countryShortCode: "NI",
      phoneNumberCode: 505
  },
  {
      countryName: "Costa Rica",
      countryShortCode: "CR",
      phoneNumberCode: 506
  },
  {
      countryName: "Panama",
      countryShortCode: "PA",
      phoneNumberCode: 507
  },
  {
      countryName: "Saint Pierre and Miquelon",
      countryShortCode: "PM",
      phoneNumberCode: 508
  },
  {
      countryName: "Haiti",
      countryShortCode: "HT",
      phoneNumberCode: 509
  },
  {
      countryName: "Saint Martin",
      countryShortCode: "MF",
      phoneNumberCode: 590
  },
  {
      countryName: "Bolivia",
      countryShortCode: "BO",
      phoneNumberCode: 591
  },
  {
      countryName: "Ecuador",
      countryShortCode: "EC",
      phoneNumberCode: 593
  },
  {
      countryName: "French Guiana",
      countryShortCode: "GF",
      phoneNumberCode: 594
  },
  {
      countryName: "Paraguay",
      countryShortCode: "PY",
      phoneNumberCode: 595
  },
  {
      countryName: "Martinique",
      countryShortCode: "MQ",
      phoneNumberCode: 596
  },
  {
      countryName: "Suriname",
      countryShortCode: "SR",
      phoneNumberCode: 597
  },
  {
      countryName: "Uruguay",
      countryShortCode: "UY",
      phoneNumberCode: 598
  },
  {
      countryName: "Curaçao",
      countryShortCode: "CW",
      phoneNumberCode: 599
  },
  {
      countryName: "Timor-Leste",
      countryShortCode: "TL",
      phoneNumberCode: 670
  },
  {
      countryName: "Norfolk Island",
      countryShortCode: "NF",
      phoneNumberCode: 672
  },
  {
      countryName: "Brunei Darussalam",
      countryShortCode: "BN",
      phoneNumberCode: 673
  },
  {
      countryName: "Nauru",
      countryShortCode: "NR",
      phoneNumberCode: 674
  },
  {
      countryName: "Papua New Guinea",
      countryShortCode: "PG",
      phoneNumberCode: 675
  },
  {
      countryName: "Tonga",
      countryShortCode: "TO",
      phoneNumberCode: 676
  },
  {
      countryName: "Solomon Islands",
      countryShortCode: "SB",
      phoneNumberCode: 677
  },
  {
      countryName: "Vanuatu",
      countryShortCode: "VU",
      phoneNumberCode: 678
  },
  {
      countryName: "Fiji",
      countryShortCode: "FJ",
      phoneNumberCode: 679
  },
  {
      countryName: "Palau",
      countryShortCode: "PW",
      phoneNumberCode: 680
  },
  {
      countryName: "Wallis and Futuna",
      countryShortCode: "WF",
      phoneNumberCode: 681
  },
  {
      countryName: "Cook Islands",
      countryShortCode: "CK",
      phoneNumberCode: 682
  },
  {
      countryName: "Niue",
      countryShortCode: "NU",
      phoneNumberCode: 683
  },
  {
      countryName: "Samoa",
      countryShortCode: "WS",
      phoneNumberCode: 685
  },
  {
      countryName: "Kiribati",
      countryShortCode: "KI",
      phoneNumberCode: 686
  },
  {
      countryName: "New Caledonia",
      countryShortCode: "NC",
      phoneNumberCode: 687
  },
  {
      countryName: "Tuvalu",
      countryShortCode: "TV",
      phoneNumberCode: 688
  },
  {
      countryName: "French Polynesia",
      countryShortCode: "PF",
      phoneNumberCode: 689
  },
  {
      countryName: "Tokelau",
      countryShortCode: "TK",
      phoneNumberCode: 690
  },
  {
      countryName: "Micronesia, Federated States of",
      countryShortCode: "FM",
      phoneNumberCode: 691
  },
  {
      countryName: "Marshall Islands",
      countryShortCode: "MH",
      phoneNumberCode: 692
  },
  {
      countryName: "Korea, Democratic People's Republic of",
      countryShortCode: "KP",
      phoneNumberCode: 850
  },
  {
      countryName: "Hong Kong",
      countryShortCode: "HK",
      phoneNumberCode: 852
  },
  {
      countryName: "Macao",
      countryShortCode: "MO",
      phoneNumberCode: 853
  },
  {
      countryName: "Cambodia",
      countryShortCode: "KH",
      phoneNumberCode: 855
  },
  {
      countryName: "Laos",
      countryShortCode: "LA",
      phoneNumberCode: 856
  },
  {
      countryName: "Pitcairn",
      countryShortCode: "PN",
      phoneNumberCode: 872
  },
  {
      countryName: "Bangladesh",
      countryShortCode: "BD",
      phoneNumberCode: 880
  },
  {
      countryName: "Taiwan",
      countryShortCode: "TW",
      phoneNumberCode: 886
  },
  {
      countryName: "Maldives",
      countryShortCode: "MV",
      phoneNumberCode: 960
  },
  {
      countryName: "Lebanon",
      countryShortCode: "LB",
      phoneNumberCode: 961
  },
  {
      countryName: "Jordan",
      countryShortCode: "JO",
      phoneNumberCode: 962
  },
  {
      countryName: "Syrian Arab Republic",
      countryShortCode: "SY",
      phoneNumberCode: 963
  },
  {
      countryName: "Iraq",
      countryShortCode: "IQ",
      phoneNumberCode: 964
  },
  {
      countryName: "Kuwait",
      countryShortCode: "KW",
      phoneNumberCode: 965
  },
  {
      countryName: "Saudi Arabia",
      countryShortCode: "SA",
      phoneNumberCode: 966
  },
  {
      countryName: "Yemen",
      countryShortCode: "YE",
      phoneNumberCode: 967
  },
  {
      countryName: "Oman",
      countryShortCode: "OM",
      phoneNumberCode: 968
  },
  {
      countryName: "Palestine, State of",
      countryShortCode: "PS",
      phoneNumberCode: 970
  },
  {
      countryName: "United Arab Emirates",
      countryShortCode: "AE",
      phoneNumberCode: 971
  },
  {
      countryName: "Israel",
      countryShortCode: "IL",
      phoneNumberCode: 972
  },
  {
      countryName: "Bahrain",
      countryShortCode: "BH",
      phoneNumberCode: 973
  },
  {
      countryName: "Qatar",
      countryShortCode: "QA",
      phoneNumberCode: 974
  },
  {
      countryName: "Bhutan",
      countryShortCode: "BT",
      phoneNumberCode: 975
  },
  {
      countryName: "Mongolia",
      countryShortCode: "MN",
      phoneNumberCode: 976
  },
  {
      countryName: "Nepal",
      countryShortCode: "NP",
      phoneNumberCode: 977
  },
  {
      countryName: "Tajikistan",
      countryShortCode: "TJ",
      phoneNumberCode: 992
  },
  {
      countryName: "Turkmenistan",
      countryShortCode: "TM",
      phoneNumberCode: 993
  },
  {
      countryName: "Azerbaijan",
      countryShortCode: "AZ",
      phoneNumberCode: 994
  },
  {
      countryName: "Georgia",
      countryShortCode: "GE",
      phoneNumberCode: 995
  },
  {
      countryName: "Kyrgyzstan",
      countryShortCode: "KG",
      phoneNumberCode: 996
  },
  {
      countryName: "Uzbekistan",
      countryShortCode: "UZ",
      phoneNumberCode: 998
  },
  {
      countryName: "Bahamas",
      countryShortCode: "BS",
      phoneNumberCode: 1242
  },
  {
      countryName: "Barbados",
      countryShortCode: "BB",
      phoneNumberCode: 1246
  },
  {
      countryName: "Anguilla",
      countryShortCode: "AI",
      phoneNumberCode: 1264
  },
  {
      countryName: "Antigua and Barbuda",
      countryShortCode: "AG",
      phoneNumberCode: 1268
  },
  {
      countryName: "Virgin Islands, British",
      countryShortCode: "VG",
      phoneNumberCode: 1284
  },
  {
      countryName: "Virgin Islands, U.S.",
      countryShortCode: "VI",
      phoneNumberCode: 1340
  },
  {
      countryName: "Bermuda",
      countryShortCode: "BM",
      phoneNumberCode: 1441
  },
  {
      countryName: "Grenada",
      countryShortCode: "GD",
      phoneNumberCode: 1473
  },
  {
      countryName: "Turks and Caicos Islands",
      countryShortCode: "TC",
      phoneNumberCode: 1649
  },
  {
      countryName: "Montserrat",
      countryShortCode: "MS",
      phoneNumberCode: 1664
  },
  {
      countryName: "Northern Mariana Islands",
      countryShortCode: "MP",
      phoneNumberCode: 1670
  },
  {
      countryName: "Guam",
      countryShortCode: "GU",
      phoneNumberCode: 1671
  },
  {
      countryName: "American Samoa",
      countryShortCode: "AS",
      phoneNumberCode: 1684
  },
  {
      countryName: "Saint Lucia",
      countryShortCode: "LC",
      phoneNumberCode: 1758
  },
  {
      countryName: "Dominica",
      countryShortCode: "DM",
      phoneNumberCode: 1767
  },
  {
      countryName: "Saint Vincent and the Grenadines",
      countryShortCode: "VC",
      phoneNumberCode: 1784
  },
  {
      countryName: "Dominican Republic",
      countryShortCode: "DO",
      phoneNumberCode: 1849
  },
  {
      countryName: "Trinidad and Tobago",
      countryShortCode: "TT",
      phoneNumberCode: 1868
  },
  {
      countryName: "Saint Kitts and Nevis",
      countryShortCode: "KN",
      phoneNumberCode: 1869
  },
  {
      countryName: "Jamaica",
      countryShortCode: "JM",
      phoneNumberCode: 1876
  },
  {
      countryName: "Puerto Rico",
      countryShortCode: "PR",
      phoneNumberCode: 1939
  },
  {
      countryName: "Cayman Islands",
      countryShortCode: "KY",
      phoneNumberCode: 345
  }
]