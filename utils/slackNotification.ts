const axios = require("axios");
const { execSync } = require("child_process");

const isCICD = process.env.CI === "true";
if (!isCICD) {
  const SLACK_WEBHOOK_URL =
    "https://hooks.slack.com/services/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";
  const MESSAGE = {
    username: "Olympia Notify",
    text: `*${execSync("git config --get user.name")
      .toString()
      .trim()}*\nDear team! FYI I will push to *${execSync(
      "git rev-parse --abbrev-ref HEAD"
    )
      .toString()
      .trim()
      .toUpperCase()}* at 1min or 2min later.\nAny concern just let me know here.`,
  };
  axios
    .post(SLACK_WEBHOOK_URL, MESSAGE)
    .then((response) => {
      console.warn(
        "Slack notification sent successful at ",
        response.headers.date
      );
    })
    .catch((error) => {
      console.error("Error sending Slack notification:", error);
    });
}
