import { computed } from "vue";
import jwtDecode, { JwtPayload } from "jwt-decode";
import { KEYS } from "./constants";
import { store } from "@/store";
import {
  ThemePropertiesFromApiDto,
  IThemePropertiesFromApi,
} from "@/models/companyThemePropertiesRespone";
import { EnumSelectedThemeMode } from "~~/models/enums/enumSelectedThemeMode";

const lookupJwtKey = (token: string) => {
  const decode = jwtDecode<JwtPayload>(token);
  const jwtData = Object.entries(decode);
  return jwtData;
};
const getDecodeKeyJWT = (token: string, key: string): string => {
  const decode = jwtDecode<JwtPayload>(token);
  const jwtData = Object.entries(decode);
  let value = "";
  jwtData.forEach((data) => {
    if (String(data[0]).trim() === String(key)) {
      value = String(data[1]);
    }
  });
  return value;
};
const getImagePathProvider = (property: string, folder = ""): string => {
  const encodedPath = encodeURIComponent(property);

  const cdn = commonHelper.getThemePropertiesCdn();
  const version = store.state.imageVersion;
  let fullImagePath = "";
  switch (folder) {
    case "casino":
      fullImagePath = `${cdn}/storage/casino/${encodedPath}?v=${version}`;
      break;
    case "promotion":
      fullImagePath = `${cdn}/storage/promotions/WebId-${store.state.webId}/${encodedPath}?v=${version}`;
      break;
    default:
      fullImagePath = `${cdn}/storage/game/${encodedPath}?v=${version}`;
      break;
  }
  return fullImagePath;
};

const getImagePath = (
  group: ThemePropertiesFromApiDto,
  webId: Number = 0,
  cdn = ""
): string => {
  const version = useState("version").value;
  const encodedPath = encodeURIComponent(group.Path);
  const isDefaultFolderName = group.IsDefault
    ? "default"
    : `customize/WebId-${webId}`;
  return `${cdn}/storage/${isDefaultFolderName}/${group.ThemeName?.toLowerCase()}/${
    encodedPath
  }?v=${version}`;
};

const getSingleThemeProperty = (
  allTheme: Array<IThemePropertiesFromApi>,
  key: string,
  isOnlyEnabled = false
): IThemePropertiesFromApi => {
  try {
    return (
      allTheme.find((item) => {
        let isShow = item.HtmlId.toLowerCase() === key.toLowerCase();
        if (isShow && isOnlyEnabled) {
          isShow = !item.IsHidden;
        }
        return isShow;
      }) ?? ({} as IThemePropertiesFromApi)
    );
  } catch (error) {
    return {} as IThemePropertiesFromApi;
  }
};
const getMultipleThemesProperties = (
  themeProperties: Array<IThemePropertiesFromApi>,
  key: string,
  excepts: Array<string> = [],
  isUseHyperLink = false
): Array<ThemePropertiesFromApiDto> => {
  try {
    if (isUseHyperLink) {
      return themeProperties
        .filter(
          (item) =>
            item.HtmlId.includes(key) &&
            !item.IsHidden &&
            item.HyperLink !== "" &&
            !excepts.includes(item.HtmlId)
        )
        .map((item) => new ThemePropertiesFromApiDto(item));
    }
    return themeProperties.filter(
      (item) =>
        item.HtmlId.includes(key) &&
        !item.IsHidden &&
        !excepts.includes(item.HtmlId)
    ).map((item) => new ThemePropertiesFromApiDto(item));
  } catch (error) {
    return [];
  }
};
const getAllMultipleThemesProperties = (
  themeProperties: Array<IThemePropertiesFromApi>,
  key: string
): Array<ThemePropertiesFromApiDto> => {
  try {
    return themeProperties
      .filter((item) => item.HtmlId.includes(key))
      .map((item) => new ThemePropertiesFromApiDto(item));
  } catch (error) {
    return [];
  }
};
const isGlobal = computed(() => store.state.selectedThemeName === "global");
const themeColor = computed(
  (): IThemePropertiesFromApi =>
    getSingleThemeProperty(
      store.state.companyThemePropertiesFromApi.ThemeProperties,
      isGlobal.value
        ? store.state.selectedThemeMode === EnumSelectedThemeMode.Dark
          ? "theme_color_dark"
          : "theme_color_light"
        : "theme_color"
    )
);
export default {
  themeColor,
  lookupJwtKey,
  getDecodeKeyJWT,
  getImagePath,
  getImagePathProvider,
  getSingleThemeProperty,
  getMultipleThemesProperties,
  getAllMultipleThemesProperties,
};
