import { format } from "date-fns";
import { ref, computed } from "vue";
import _ from "lodash";
import {
  genFileId,
  UploadInstance,
  UploadProps,
  UploadRawFile,
} from "element-plus";

import notificationHelper from "./elementUiHelpers/notificationHelper";
import EnumMessageType from "@/models/enums/enumMessageType";
import { store } from "@/store";
import EnumEntranceLocation from "~~/models/enums/enumEntranceLocation";
import { IAllCustomerSettingsData } from "@/models/getCustomerSettingResponse";
import punycode from 'punycode';
import { TabGameMenu } from "~~/models/gameEntryFilter";

const uploadSlipImageFRef = ref<UploadInstance>();
const slipUpload = ref([] as File[]);
const compressImageSize = (image: File) => {
  const newFile = new File([image], String(Date.now()) + image.name, {
    type: image.type,
  });
  slipUpload.value[0] = newFile;
  const imageSize = image.size;
  const maxSizeToCompress = 1000000;
  if (imageSize > maxSizeToCompress) {
    const img = new Image();
    img.src = window.URL.createObjectURL(image);
    img.onload = () => {
      const canvas = document.createElement("canvas");
      canvas.width = img.width;
      canvas.height = img.height;
      const ctx = canvas.getContext("2d");
      ctx?.drawImage(img, 0, 0);
      canvas.toBlob(
        (blob) => {
          const compressedImage = new File(
            [blob ?? ""],
            String(Date.now()) + image.name,
            {
              type: image.type,
            }
          );
          const container = new DataTransfer();
          container.items.add(compressedImage);
          const file = container.files[0];
          slipUpload.value[0] = file;
        },
        "image/jpeg",
        store.state.isImageLess1MBEnabled ? 0.35 : 1
      );
    };
  }
};
const getFile = (file: File): void => {
  if (file) {
    compressImageSize(file);
  }
};

const handleSlipImageExceed: UploadProps["onExceed"] = (files) => {
  uploadSlipImageFRef.value?.clearFiles();
  const file = files[0] as UploadRawFile;
  file.uid = genFileId();
  uploadSlipImageFRef.value?.handleStart(file);
};
const isdisabled = ref(false);
const getSlipImage: UploadProps["onChange"] = async (file) => {
  isdisabled.value = true;
  const acceptFileType = ["image/png", "image/jpeg", "image/jpg"];
  const currentFileType = file.raw?.type ?? "";
  if (acceptFileType.includes(currentFileType)) {
    await getFile(file.raw as File);
  } else {
    slipUpload.value = [];
    uploadSlipImageFRef.value?.clearFiles();
  }
  isdisabled.value = false;
};
const removeSlipImage: UploadProps["onRemove"] = () => {
  slipUpload.value = [];
};
type TServerList =
  | "default"
  | "41"
  | "42"
  | "43"
  | "44"
  | "45"
  | "46"
  | "47"
  | "48"
  | "wl03vm-p-a01";
const getCurrentServer = (): TServerList | undefined => {
  const {
    END_POINT,
    END_POINT_1,
    END_POINT_2,
    END_POINT_3,
    END_POINT_4,
    END_POINT_5,
    END_POINT_6,
    END_POINT_7,
    END_POINT_8,
    SA_END_POINT,
  } = useRuntimeConfig().public;
  const endpointMapper: Record<string, TServerList> = {
    [END_POINT]: "default",
    [END_POINT_1]: "41",
    [END_POINT_2]: "42",
    [END_POINT_3]: "43",
    [END_POINT_4]: "44",
    [END_POINT_5]: "45",
    [END_POINT_6]: "46",
    [END_POINT_7]: "47",
    [END_POINT_8]: "48",
    [SA_END_POINT]: "a01_sa",
  };
  const enpointUrl = useState<keyof typeof endpointMapper>(KEYS.SERVER_NAME);
  // Return undefined when in Development, Staging, and Demo. The endpointMapper will be only Production enpoints.
  if (!Object.keys(endpointMapper).includes(enpointUrl.value)) return;
  if (!enpointUrl.value) return endpointMapper[END_POINT];
  return endpointMapper[enpointUrl.value];
};
const isChinaServer = () => {
  const enpointUrl = useState(KEYS.SERVER_NAME);
  return (
    String(enpointUrl.value).includes("ex-api3-47") ||
    String(enpointUrl.value).includes("ex-api3-48")
  );
};
const isSaServer = () => {
  const enpointUrl = useState(KEYS.SERVER_NAME);

  return enpointUrl.value.includes("ex-api-yy");
};

const getAssetPath = () => {
  if (isSaServer()) {
    return {
      path: useRuntimeConfig().public.SA_ASSET_IMAGE_CDN,
      version: store.state.imageVersion,
    };
  }
  if (isChinaServer()) {
    return {
      path: useRuntimeConfig().public.CHINA_ASSET_IMAGE_CDN,
      version: store.state.imageVersion,
    };
  }
  return {
    path: useRuntimeConfig().public.ASSET_IMAGE_CDN,
    version: store.state.imageVersion,
  };
};
const getAssetImage = (location: string, version = true) => {
  const { ASSET_IMAGE_CDN, CHINA_ASSET_IMAGE_CDN, SA_ASSET_IMAGE_CDN } = useRuntimeConfig().public;
  let imagePath = isChinaServer() ? CHINA_ASSET_IMAGE_CDN : ASSET_IMAGE_CDN;
  if (isSaServer()) {
    imagePath = SA_ASSET_IMAGE_CDN;
  }
  return `${imagePath}/${location}${
    version ? `?v=${store.state.imageVersion}` : ""
  }`;
};

const getThemePropertiesCdn = () => {
  const {
    THEME_PROPERTY_CDN,
    CHINA_THEME_PROPERTY_CDN,
    SA_THEME_PROPERTY_CDN,
  } = useRuntimeConfig().public;
  if (isChinaServer()) {
    return CHINA_THEME_PROPERTY_CDN;
  }
  if (isSaServer()) {
    return SA_THEME_PROPERTY_CDN;
  }

  return THEME_PROPERTY_CDN;
};

const getAppDownloadCdn = () => {
  const { APP_DOWNLOAD, SA_APP_DOWNLOAD } = useRuntimeConfig().public;

  if (isSaServer()) {
    return SA_APP_DOWNLOAD;
  }
  return APP_DOWNLOAD;
};

const onErrorDefaultImage = (e: any, prefix: string, isFromHome = false) => {
  const routed: string = _.toLower(
    _.replace(window.location.href.split("/")[3], "-", "_")
  );

  const splitUrl = prefix.split("/");
  const startIndex = splitUrl.indexOf("entry") + 1;
  const endIndex = splitUrl.indexOf("/", startIndex);
  const wordAfterEntry = splitUrl.slice(startIndex, endIndex)[0];

  const checked = _.split(prefix, `/${isFromHome ? wordAfterEntry : routed}/`);
  const file = `assets/${_.split(checked[0], "/assets/")[1]}/${
    isFromHome ? wordAfterEntry : routed
  }/default/${_.split(checked[1], "/")[1]}`;
  const fileSplit = file.split("/");
  if (fileSplit[fileSplit.length - 1].includes(".")) {
    // prevent error occuring when file points to a directory;
    e.target.src = getAssetImage(file, false);
  }
};
const isFloatBackgroundEntry = computed((): boolean => {
  const route = useRoute();
  return (
    !_.includes(_.toLower(route.path), "sports") ||
    store.state.selectedThemeName.includes("peru")
  );
});
const isHandleDefaultImageRender = (
  path: string,
  isFromHome = false
): boolean => {
  const currentPageName: string = _.toLower(
    _.replace(window.location.href.split("/")[3], "-", "_")
  );

  const splitUrl = path.split("/");
  const startIndex = splitUrl.indexOf("entry") + 1;
  const endIndex = splitUrl.indexOf("/", startIndex);
  const wordAfterEntry = splitUrl.slice(startIndex, endIndex)[0];

  const checked = _.split(
    path,
    `/${isFromHome ? wordAfterEntry : currentPageName}/`
  );
  if (_.split(checked[0], "/assets/")[1] && _.split(checked[1], "/")[1]) {
    return (
      path.includes("/common/entry/live_casino/") ||
      path.includes("/common/entry/sports/")
    );
  } else {
    return false;
  }
};
interface IDateRange {
  startDate: string | Date;
  endDate: string | Date;
}
const getDateRange = (
  shortcutDate: string,
  dateFormat = "yyyy/MM/dd"
): IDateRange => {
  const startDate = new Date();
  const endDate = new Date();
  let previousStateDateRange = 0;
  let previousEndDateRange = 0;
  switch (shortcutDate) {
    case "yesterday": {
      previousStateDateRange = 1;
      previousEndDateRange = 1;
      break;
    }
    case "past3days": {
      previousStateDateRange = 3;
      previousEndDateRange = 1;
      break;
    }
    case "past7days": {
      previousStateDateRange = 7;
      previousEndDateRange = 1;
      break;
    }
    default: {
      previousStateDateRange = 0;
      previousEndDateRange = 0;
      break;
    }
  }
  startDate.setDate(startDate.getDate() - previousStateDateRange);
  endDate.setDate(endDate.getDate() - previousEndDateRange);
  return {
    startDate: format(startDate, dateFormat),
    endDate: format(endDate, dateFormat),
  };
};

const copyTextToClipBoard = (text: string, showNotification = true): void => {
  const { $t } = useNuxtApp();
  const el = document.createElement("textarea");
  el.value = text;
  document.body.appendChild(el);
  el.select();
  document.execCommand("copy");
  document.body.removeChild(el);
  if (showNotification) {
    notificationHelper.notification(
      `${$t("copied")}: ${text}`,
      EnumMessageType.Info
    );
  }
};
const isRequiredField = (settings: any, propertyName: string) => {
  const setting = _.pickBy(settings, (__, key) => key === propertyName);
  return _.find(setting, { PropertyStatus: "Required" }) !== undefined;
};
interface ITemplate {
  isMobile: boolean;
  template: string;
}
const getTemplate = (): ITemplate => {
  const isMobile = false;
  const template = isMobile ? "mobile" : "desktop";
  return {
    isMobile,
    template,
  };
};
const getGameCategoryByTab = (tab: string): number => {
  const categories: Record<string, number> = {
    SportInitial: 1,
    LiveCasinoInitial: 2,
    HomeInitial: 3,
    GameInitial: 3,
    KenoInitial: 3,
    CockfightInitial: 3,
    VirtualSportInitial: 4,
  };
  return categories[tab];
};

const handlePopupNewWindow = (
  segment: string,
  width: number,
  height: number
): void => {
  const position = {
    x: screen.width / 2 - width / 2,
    y: screen.height / 2 - height / 2,
  };
  const url = `${window.location.origin}/${segment}`;
  window.open(
    url,
    "NewWindowInitial",
    `width=${width},height=${height},left=${position.x},top=${position.y}`
  );
};

const handleImportImage = (themeName = "", path = ""): string => {
  const { template } = getTemplate();
  try {
    return getAssetImage(
      `assets/theme/${themeName}/${template}/images/${path}`
    );
  } catch (e) {
    return getAssetImage("assets/theme/common/icon/no-image.png");
  }
};

const handleImportCommonImage = (path = ""): string => {
  try {
    return getAssetImage(`assets/theme/common/${path}`);
  } catch (e) {
    return getAssetImage("assets/theme/common/icon/no-image.png");
  }
};

const handleHyperlinkRender = (HyperLink: string) => {
  if (HyperLink !== "") {
    window.open(
      HyperLink.includes("http") ? HyperLink : `https://${HyperLink}`,
      "_blank"
    );
  }
};
const getUserAgent = () => navigator.userAgent;
const handleRemoveSpaceSpecialCharFormatted = (value: string): string =>
  _.replace(_.toLower(value), /[^\w${0080}{FFFF}]+/gu, "");

const decodePunycode = (encode: string) => {
  return punycode.toUnicode(encode);
}

const getHostName = (url: string) => {
  let hostname = "";
  if (url.startsWith("http")) {
    const domain = new URL(url);

    hostname = domain.hostname;
  } else {
    const domain = new URL(`https://${url ?? ""}`);
    hostname = domain.hostname;
  }
  const decodedHeader = decodePunycode(hostname ?? "");
  if (decodedHeader) return decodedHeader;
  return hostname;
};

const getDomain = () => {
  if (process.server) {
    const headerHost = useNuxtApp().ssrContext?.event.node.req.headers.host;
    return getHostName(String(headerHost));
  } else {
    return getHostName(window.location.hostname);
  }
};
const onChangeSelect = () => {
  const header = document.getElementById("header");
  if (header !== null) {
    header.scrollIntoView();
  }
};
const onVisibleChange = (e: boolean) => {
  if (!e) {
    const header = document.getElementById("header");
    if (header !== null) {
      header.scrollIntoView();
    }
  }
};
/**
 * calculate minutes from now in milliseconds
 * @param duration in minutes
 * @returns time value in milliseconds
 */
const getMinutesFromNow = (duration = 1) => {
  const currentTime = new Date().getTime();
  return currentTime + 1000 * 60 * duration;
};
const toggleLoginRegisterDialog = (toggleLogin = true) => {
  if (toggleLogin) {
    store.commit("updateLoginDialog", !store.state.showLoginDialog);
    store.commit("updateRegisterDialog", false);
  } else {
    store.commit("updateRegisterDialog", !store.state.showRegisterDialog);
    store.commit("updateLoginDialog", false);
  }
};
const handleGameEntryOpenWindow = (
  gameId: number,
  width: number,
  height: number,
  entranceLocation = EnumEntranceLocation.Other,
  viewAsDesktop = false
) => {
  const position = reactive({
    x: screen.width / 2 - width / 2,
    y: screen.height / 2 - height / 2,
  });
  window.open(
    `/game/${gameId}?location=${entranceLocation}&pop${
      viewAsDesktop ? "&DesktopView" : ""
    }`,
    "_blank",
    `width=${width},height=${height},left=${position.x},top=${position.y}`
  );
};
const onClickPlayGame = (
  GameId: number,
  auth: boolean,
  tab: string,
  entrance: string
) => {
  const router = useRouter();
  if (auth) {
    if (GameId === 74)
      handleGameEntryOpenWindow(
        GameId,
        1200,
        850,
        (entrance = EnumEntranceLocation.Other)
      );
    else router.push(`/Game/${GameId}?tab=${tab}&&entrance=${entrance}`);
  } else {
    toggleLoginRegisterDialog();
  }
};

const onInputValue = (value: any, vModel: any, property: any) => {
  const regex = /[^0-9.]+/g;
  let cleanedValue = value.replace(regex, "");
  const dotCount = (cleanedValue.match(/\./g) || []).length;
  if (dotCount > 1) {
    // Remove all dots except the first one
    cleanedValue = cleanedValue.replace(/\./g, (match: any, index: any) =>
      index === cleanedValue.indexOf(".") ? "." : ""
    );
  }
  vModel[property] = cleanedValue;
};
const getCustomerSettingById = (Id: string) => {
  try {
    return _.find(
      store.state.allCustomerSettings,
      (setting: IAllCustomerSettingsData) => setting.Id === Id
    );
  } catch (error) {
    console.warn(`cusetting id ${Id} : `, error);
  }
};

const env = () => useRuntimeConfig().public.CURRENTLY_ENV;
const isMain = computed(() =>
  /^(?!.*kr).*main.*$/.test(store.state.selectedThemeName)
);
const isUseFlexibleModule = computed(() => {
  const customerSettings = useState<IAllCustomerSettingsData[]>(
    KEYS.ALL_CUSTOMER_SETTINGS
  );

  const prop = customerSettings.value.find(
    (v) => v.Id === "isUseFlexibleModule"
  );

  if (!prop || prop.Value.toLowerCase() === "n") return false;
  return true;
});

/**
 * For Customize Content. Currently return static true first, The old logic:
 * const isAllowCompany = computed(() =>
 *  ["staging", "development", "demo"].includes(env()) ||
 *   (env() === "production" && store.state.webId === 2)
 * );
 */
const isAllowCompany = computed(() => {
  return true;
});

// Preventing missing old module for specific theme when apply flexible module.
const exceptTheme = ["kr_mobile", "bk8_mobile", "cool88_mobile"];
const isExceptionTheme = computed(() =>
  exceptTheme.includes(store.state.themeName?.toLowerCase())
);
const allowCompany = computed(() => {
  if (isExceptionTheme.value) return false;
  const _isMain =
    isMain.value && !store.state.themeName?.toLowerCase().includes("kr");
  return _isMain && isUseFlexibleModule.value;
});
const isAllowEligibleWithdrawal = computed(() => store.state.webId !== 20082);
const isBhinbhin = computed(() =>
  store.state.webId === 20099 && store.state.locale === "id_ID" ? "_suffix" : ""
);
const getMultiplesOf100InRange = (start: number, end: number) => {
  if (start > end) {
    [start, end] = [end, start];
  }

  const firstMultiple = Math.ceil(start / 100) * 100;
  const lastMultiple = Math.floor(end / 100) * 100;

  // Generate the multiples of 100 within the range
  let result = [];
  for (let i = firstMultiple; i <= lastMultiple; i += 100) {
    result.push(i);
  }

  return result;
};

const isIndependentGameFilterPage = () => {
  const route = useRoute();
  const isIndependentLobby = useState(KEYS.IS_INDEPENDENT_MODULE);
  const isHasFilter = route.params.filter;
  const isHasSearch = route.query.f;

  if (!isIndependentLobby.value) return false;

  return isHasFilter || isHasSearch;
};
export default {
  slipUpload,
  getFile,
  uploadSlipImageFRef,
  handleSlipImageExceed,
  getSlipImage,
  removeSlipImage,
  getDateRange,
  copyTextToClipBoard,
  isRequiredField,
  getTemplate,
  getGameCategoryByTab,
  isdisabled,
  handlePopupNewWindow,
  handleImportImage,
  handleImportCommonImage,
  getUserAgent,
  getAssetPath,
  getAssetImage,
  onErrorDefaultImage,
  isFloatBackgroundEntry,
  isHandleDefaultImageRender,
  handleRemoveSpaceSpecialCharFormatted,
  handleHyperlinkRender,
  getDomain,
  onChangeSelect,
  onVisibleChange,
  getMinutesFromNow,
  toggleLoginRegisterDialog,
  onClickPlayGame,
  onInputValue,
  getCustomerSettingById,
  allowCompany,
  isAllowCompany,
  isChinaServer,
  isAllowEligibleWithdrawal,
  isBhinbhin,
  isExceptionTheme,
  getMultiplesOf100InRange,
  isIndependentGameFilterPage,
  isMain,
  getThemePropertiesCdn,
  getAppDownloadCdn,
  getCurrentServer,
};
