import { ElNotification, NotificationParams } from "element-plus";
import enumMessageStatus from "@/models/enums/enumMessageType";

const notification = (
  message: string,
  type: enumMessageStatus,
  title?: string,
  customClass?: string,
  appendTo?: string,
  showClose = true
): void => {
  ElNotification({
    title,
    message,
    type,
    duration: 3000,
    customClass,
    appendTo,
    showClose,
  });
};

const success = (
  message: string,
  params: Omit<NotificationParams, "type" | "message"> = {}
): void => {
  ElNotification({
    message,
    duration: 3000,
    ...params,
    type: "success",
  });
};

const error = (
  message: string,
  params: Omit<NotificationParams, "type" | "message"> = {}
): void => {
  ElNotification({
    message,
    duration: 3000,
    ...params,
    type: "error",
  });
};

export default {
  notification,
  success,
  error,
};
