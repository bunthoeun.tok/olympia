enum inputLength {
  minLength = 6,
  maxLength = 20,
}

const userNameFormat = (username: string): string => {
  const { $t } = useNuxtApp();
  if (
    username.length < inputLength.minLength ||
    username.length > inputLength.maxLength
  ) {
    return $t(
      "must_be_in_6-20_characters_use_only_letters_(a-z)_and_numbers_(0-9)_username_is_not_case_sensitive"
    );
  }
  if (!/[A-Za-z]/.test(username)) {
    return $t("username_must_be_contain_letter");
  }
  if (!/^[\d\w]+$/.test(username)) {
    return $t("username_must_not_contain_with_space_or_symbol");
  }
  return "";
};

const passwordFormat = (password: string, isLogin: boolean = false): string => {
  const { $t } = useNuxtApp();
  if (
    password.length < inputLength.minLength ||
    password.length > inputLength.maxLength
  ) {
    return isLogin ? $t("passwordlenght"): $t(
      "must be_in_6-20_characters_use_both_letters_(a-z)_and_numbers_(0-9)_password_is_case_sensitive"
    );
  }
  if (!/^[A-Za-z0-9]+$/.test(password)) {
    return $t("password_only_letter_number");
  }
  if (!/[A-Za-z]/.test(password)) {
    return isLogin ? $t("passwordlater"): $t("password_must_contain_with_letter");
  }
  if (!/\d/.test(password)) {
    return isLogin ? $t("passwordnumber"): $t("password_must_contain_with_number");
  }
  return "";
};

const confirmPasswordFormat = (
  password: string,
  confirmPassword: string
): string => {
  const { $t } = useNuxtApp();
  if (password !== confirmPassword) {
    return $t("password_does_not_match");
  }
  return "";
};

const emailFormat = (email: string): string => {
  const { $t } = useNuxtApp();
  if (
    !/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
      email
    )
  ) {
    return $t("must_be_valid_email");
  }
  return "";
};

const phoneValidation = (number: string): string => {
  if (isOTPEnabled() && !number) return "required";
  if (Number.isNaN(Number(number))) {
    return "should be numbers only";
  }
  return "";
}

const numbersOnly = (number: string): string => {
  const { $t } = useNuxtApp();
  if (Number.isNaN(Number(number))) {
    return $t("should be numbers only");
  }
  return "";
}

export default {
  passwordFormat,
  confirmPasswordFormat,
  userNameFormat,
  emailFormat,
  numbersOnly,
  phoneValidation,
};
