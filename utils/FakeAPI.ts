/* eslint-disable */
import { IGetGameProviderListResponse } from "@/models/IGetPlayableGamesResponse";
import { IApiResponse } from "@/models/apiResponse";
import { IAxiosPromise, IAxiosResponse } from "@/models/axiosPromise";
import {
  IGetRegisterBankListRequest,
  IGetRegisterBankListResponse,
} from "@/models/bank";
import {
  IGetAsiGameListResponse,
  IGetBsiGameListResponse,
} from "@/models/bsiGameList";
import {
  IGetLanguageSettingResponse,
  ILanguage,
} from "@/models/common/language";
import { IGetMultiWalletBalaceResponse, IWallet } from "@/models/common/wallet";
import { ICompanyFlowSettingResponse } from "@/models/companyFlowSetting";
import {
  GetThemePropertiesRequestDto,
  ICompanyThemePropertiesFromApi
} from "@/models/companyThemePropertiesRespone";
import {
  IGetAccountCurrencyByUsernameResponse,
  IGetCurrencyWhiteListResponse,
} from "@/models/currencyWhiteList";
import EnumApiErrorCode from "@/models/enums/enumApiErrorCode";
import EnumMessageType from "@/models/enums/enumMessageType";
import { IGameProvider, IGameProviders } from "@/models/gameProvider";
import IGetBetListRequest from "@/models/getBetListRequest";
import { IGetBetListResponse } from "@/models/getBetListResponse";
import IGetBetPayloadRequest from "@/models/getBetPayloadRequest";
import IGetBetPayloadResponse from "@/models/getBetPayloadResponse";
import IGetPlayerPreference from "@/models/getPlayerPreference";
import {
  GetTransactionHistoryRequest,
  GetTransactionHistoryResponse,
  IProceedTransactionRequest,
} from "@/models/getTransactionHistoryResponse";
import IGetPromotionApplyListRequest from "@/models/promotion/getPromotionApplyListRequest";
import IGetPromotionApplyListResponse from "@/models/promotion/getPromotionApplyListResponse";
import {
  IGetPromotionResponse, IPromotionApplyReqeust
} from "@/models/promotion/promotion";
import {
  IGetRedeemReferralResponse,
  IGetReferralDiagramRequest,
  IGetReferralDiagramResponse,
  IGetReferralLayerAmountResponse,
  IRedeemReferralAmountRequest
} from "@/models/referral/getReferral";
import { IRegisterRequest, IRegisterResponse } from "@/models/register";
import IGetSportBetRequest from "@/models/sportbet/getSportBetRequest";
import { IGetSportBetResponse } from "@/models/sportbet/getSportBetResponse";
import { ISportBet } from "@/models/sportbet/sportbet";
import { IStatementRequest, IStatementResponse } from "@/models/statement";
import {
  IRequestBetList,
  IResponseBetDetail,
  IResponseBetList, IResponseSportBets
} from "@/models/theme/lite/PureLite";
import {
  ILoginByMatchIdRequest,
  ILoginByMatchIdResponse,
  IRecommendMatch,
  IRecommendMatchResponse
} from "@/models/theme/popular_match/popularMatch";
import { IPreviousQR } from "~~/models/sportqr/previousQR";
import { IPreviousQRReq } from "~~/models/sportqr/previousQRReq";
import commonHelper from "./commonHelper";
import fakeBMD1ThemeProperty from "./fakeData/fakeBMD1ThemeProperty";
import {
  fakeGameList,
  fakeSeamlessGameGameTypeList,
  fakeSeamlessGameTabList,
  fakeSeamlessProviderList,
  fakeTabInfoList,
} from "./fakeData/fakeBsiGameList";
import fakePromotion from "./fakeData/fakePromotion";
import {
  fakeNodeList,
  fakeRedeemRequestList,
  fakeReferralLayerAmount,
} from "./fakeData/fakeReferral";
import {
  CARegisterResponse,
  availablePropertyList,
  currencyWhiteList,
  getPlayerBankOptions,
  playerRegisterResponse,
} from "./fakeData/fakeRegisterData";
import statement from "./fakeData/fakeStatementData";
import { fakepreviousQRList } from "./fakeData/fakepreviousQR";
import { IBaseResponse } from "~~/models/baseResponse";
import {
  IGetLuckyWheelOptionsRequest,
  IGetLuckyWheelOptionsResponse,
  IGetLuckyWheelSpinResultRequest,
  IGetLuckyWheelSpinResultResponse,
} from "../models/lucky-wheel/luckyWheel";
import fakeLuckyWheel from "./fakeData/fakeLuckyWheel";

const { getAssetImage } = commonHelper;
// const HomeTeam = getAssetImage(
//   "assets/theme/common/popular_match/teams/AccringtonStanley.png"
// );
// const AwayTeam = getAssetImage(
//   "assets/theme/common/popular_match/teams/Exeter_City.png"
// );
export default {
  getGameBetList(
    request: IGetBetListRequest
  ): IAxiosPromise<IGetBetListResponse> {
    const fakeData: IGetBetListResponse = {
      SeamlessGameBets: [
        {
          RefNo: "FunkyGames_1885_Funky_fkg_200001559353",
          OrderTime: "2022-04-25T08:31:18.27",
          Stake: 20,
          Turnover: 20,
          NetTurnover: 20,
          Status: "won",
          Currency: "IDR",
          WinLost: 5.8,
          PlayerCommission: 0,
          GameType: "FunkyGames",
          OrderDetail: "",
          IsShowBetDetailButton: true,
          MjpCommission: 0,
          MjpWinlose: 0,
          IsShowMjp: false,
          GameProviderName: "FUNKY",
          GameProviderId: 16,
          GameProviderType: 3,
          MaxPage: 1,
          RowNumber: 1,
        },
      ],
      TotalWinLose: 30.8,
      TotalCommission: 0,
      IsSuccess: true,
      ErrorCode: 0,
      ErrorMessage: "",
    };
    const apiResponse: IAxiosResponse<IGetBetListResponse> = {
      data: {
        Data: fakeData,
        ErrorCode: 0,
        Message: "sting",
      },
    };
    if (request) {
      apiResponse.data.Message = "success";
    }
    return new Promise((resolve) => resolve(apiResponse));
  },
  getCockfightingBetList(
    request: IGetBetListRequest
  ): IAxiosPromise<IGetBetListResponse> {
    const fakeData: IGetBetListResponse = {
      SeamlessGameBets: [
        {
          RefNo: "jay_fight_gg_test_001",
          OrderTime: "2022-04-25T08:31:18.27",
          Stake: 20,
          Turnover: 20,
          NetTurnover: 20,
          Status: "won",
          Currency: "IDR",
          WinLost: 5.8,
          PlayerCommission: 0,
          GameType: "Sv388Cockfighting",
          OrderDetail: "",
          IsShowBetDetailButton: false,
          MjpCommission: 0,
          MjpWinlose: 0,
          IsShowMjp: false,
          GameProviderName: "SV388",
          GameProviderId: 46,
          GameProviderType: 6,
          MaxPage: 1,
          RowNumber: 1,
        },
      ],
      TotalWinLose: 5.8,
      TotalCommission: 0,
      IsSuccess: true,
      ErrorCode: 0,
      ErrorMessage: "",
    };
    const apiResponse: IAxiosResponse<IGetBetListResponse> = {
      data: {
        Data: fakeData,
        ErrorCode: 0,
        Message: "sting",
      },
    };
    if (request) {
      apiResponse.data.Message = "success";
    }
    return new Promise((resolve) => resolve(apiResponse));
  },
  getLotteryBetList(
    request: IGetBetListRequest
  ): IAxiosPromise<IGetBetListResponse> {
    const fakeData: IGetBetListResponse = {
      SeamlessGameBets: [
        {
          RefNo: "TCGaming_1_483670_LC11416816FZ87WCB",
          OrderTime: "2022-09-27T08:46:19.643",
          Stake: 1,
          Turnover: 0,
          NetTurnover: 0,
          Status: "running",
          Currency: "TMP",
          WinLost: 0,
          PlayerCommission: 0,
          GameType: null,
          OrderDetail: null,
          IsShowBetDetailButton: false,
          MjpCommission: 0,
          MjpWinlose: 0,
          IsShowMjp: false,
          GameProviderName: "TCG",
          GameProviderId: 1012,
          GameProviderType: 5,
          MaxPage: 1,
          RowNumber: 1,
        },
      ],
      TotalWinLose: 5.8,
      TotalCommission: 0,
      IsSuccess: true,
      ErrorCode: 0,
      ErrorMessage: "",
    };
    const apiResponse: IAxiosResponse<IGetBetListResponse> = {
      data: {
        Data: fakeData,
        ErrorCode: 0,
        Message: "sting",
      },
    };
    if (request) {
      apiResponse.data.Message = "success";
    }
    return new Promise((resolve) => resolve(apiResponse));
  },
  getCasinoBetList(
    request: IGetBetListRequest
  ): IAxiosPromise<IGetBetListResponse> {
    const fakeData: IGetBetListResponse = {
      SeamlessGameBets: [
        {
          RefNo: "SBO_LIVE_CASINO_Draw_FakeData_20220425064939",
          OrderTime: "2022-04-25T00:00:00",
          Stake: 10,
          Turnover: 10,
          NetTurnover: 0,
          Status: "draw",
          Currency: "IDR",
          WinLost: 0,
          PlayerCommission: 0,
          GameType: "SBO Live Casino",
          OrderDetail: "",
          IsShowBetDetailButton: true,
          MjpCommission: 0,
          MjpWinlose: 0,
          IsShowMjp: false,
          GameProviderName: "SBO",
          GameProviderId: -1,
          GameProviderType: 2,
          MaxPage: 1,
          RowNumber: 1,
        },
      ],
      TotalWinLose: 70,
      TotalCommission: 0,
      IsSuccess: true,
      ErrorCode: 0,
      ErrorMessage: "",
    };
    const apiResponse: IAxiosResponse<IGetBetListResponse> = {
      data: {
        Data: fakeData,
        ErrorCode: 0,
        Message: "sting",
      },
    };
    if (request) {
      apiResponse.data.Message = "success";
    }
    return new Promise((resolve) => resolve(apiResponse));
  },
  getSportsBetList(
    request: IGetSportBetRequest
  ): IAxiosPromise<IGetSportBetResponse> {
    const fakeSportData: Array<ISportBet> = [
      {
        RefNo: "SabaSeamlessWallet_1885_4252700_384_U",
        MainBetSportsType: "Three Picture",
        OrderTime: "2022-04-25T08:17:56.463",
        MainBetOdds: 2,
        Stake: 4,
        ActualStake: 4,
        PlayerCommission: 0,
        Winlose: -4,
        Status: "lose",
        OddsStyle: "Euro",
        IsLiveBet: false,
        Currency: "IDR",
        IsShowBetDetailButton: true,
        IsResettled: false,
        RollbackTime: "0001-01-01T00:00:00",
        ResettleTime: "0001-01-01T00:00:00",
        BetDetails: [
          {
            BetOption: "banker",
            SubBetOdds: 2,
            HandicapPoint: 0,
            SportType: "Three Picture",
            MarketType: "result",
            SubBetStatus: "Lose",
            BetType: 0,
            Match: " vs ",
            League: "",
            WinLoseDate: "2022-04-25T00:00:00",
            LiveScore: "0 : 0",
            HalfTimeScore: "",
            FullTimeScore: "",
            CustomizedBetType: "",
            KickOffTime: "2022-04-25T00:00:00",
            HomeTeam: "",
            AwayTeam: "",
            Favourite: " ",
          },
        ],
        VoidReason: "",
        GameProviderId: 44,
        GameProviderType: 1,
        GameProviderName: "SABA",
        DisplayType: 2,
        MaxPage: 2,
        RowNumber: 1,
      },
    ];
    // if (!request.IsGetFromAllProviders) {
    //   fakeSportData = fakeSportData.filter((bet) => bet.GameProviderName === request.GameProviderId);
    // }
    const response: IGetSportBetResponse = {
      SportBets: fakeSportData,
      TotalWinLose: 0,
      TotalCommission: 0,
      IsSuccess: true,
    };
    const apiResponse: IApiResponse<IGetSportBetResponse> = {
      Data: response,
      ErrorCode: 0,
      Message: "Success",
    };
    const axiosResponse: IAxiosResponse<IGetSportBetResponse> = {
      data: apiResponse,
    };
    return new Promise((resolve) => resolve(axiosResponse));
  },
  getVirtualSportsBetList(
    request: IGetSportBetRequest
  ): IAxiosPromise<IGetSportBetResponse> {
    const fakeVirtualSportData: Array<ISportBet> = [
      {
        RefNo: "SBO_VIRTUAL_SPORTS_Draw_FakeData_20220425064944",
        MainBetSportsType: "VirtualFootballMobile",
        OrderTime: "2022-04-25T00:00:00",
        MainBetOdds: 4,
        Stake: 10,
        ActualStake: 10,
        PlayerCommission: 0,
        Winlose: 0,
        Status: "draw",
        OddsStyle: "Malay",
        IsLiveBet: false,
        Currency: "IDR",
        IsShowBetDetailButton: false,
        IsResettled: false,
        RollbackTime: "0001-01-01T00:00:00",
        ResettleTime: "0001-01-01T00:00:00",
        BetDetails: [
          {
            BetOption: "Over",
            SubBetOdds: 4,
            HandicapPoint: 2.5,
            SportType: "",
            MarketType: "OverUnder",
            SubBetStatus: "draw",
            BetType: 3,
            Match: "VL Ankara -vs- VL Madrid",
            League: "",
            WinLoseDate: "2022-04-25T00:00:00",
            LiveScore: "0",
            HalfTimeScore: "0:1",
            FullTimeScore: "0:1",
            CustomizedBetType: "",
            KickOffTime: "1900-01-01T00:00:00",
            HomeTeam: "",
            AwayTeam: "",
            Favourite: " ",
          },
        ],
        VoidReason: "",
        GameProviderName: "SBO",
        GameProviderId: -1,
        GameProviderType: 4,
        MaxPage: 1,
        RowNumber: 1,
      },
    ];
    const response: IGetSportBetResponse = {
      SportBets: fakeVirtualSportData,
      TotalWinLose: -900,
      TotalCommission: 2072,
      IsSuccess: true,
    };
    const apiResponse: IApiResponse<IGetSportBetResponse> = {
      Data: response,
      ErrorCode: 0,
      Message: "Success",
    };
    const axiosResponse: IAxiosResponse<IGetSportBetResponse> = {
      data: apiResponse,
    };
    return new Promise((resolve) => resolve(axiosResponse));
  },
  getPromotionApplyList(
    request: IGetPromotionApplyListRequest
  ): IAxiosPromise<IGetPromotionApplyListResponse> {
    const promotionApplyListResponse: IGetPromotionApplyListResponse = {
      PromotionApplyList: [],
      IsSuccess: true,
    };
    const apiResponse: IApiResponse<IGetPromotionApplyListResponse> = {
      Data: promotionApplyListResponse,
      ErrorCode: EnumApiErrorCode.success,
      Message: "",
    };
    const axiosResponse: IAxiosResponse<IGetPromotionApplyListResponse> = {
      data: apiResponse,
    };
    if (request) {
      apiResponse.Message = "success";
    }
    return new Promise((resolve) => resolve(axiosResponse));
  },
  getGamesProviders(): IAxiosPromise<IGameProviders> {
    const gameProviders: Array<IGameProvider> = [
      {
        GameProviderId: 35,
        GameProviderName: "PGSoft",
      },
      {
        GameProviderId: 2,
        GameProviderName: "CQNine",
      },
      {
        GameProviderId: 3,
        GameProviderName: "Pragmatic Play",
      },
      {
        GameProviderId: 8,
        GameProviderName: "SBO Games (New)",
      },
      {
        GameProviderId: 10,
        GameProviderName: "JokerGaming",
      },
      {
        GameProviderId: 11,
        GameProviderName: "RealTimeGaming",
      },
      {
        GameProviderId: 13,
        GameProviderName: "WorldMatch",
      },
      {
        GameProviderId: 14,
        GameProviderName: "(SBO) Slots",
      },
      {
        GameProviderId: 16,
        GameProviderName: "FunkyGames",
      },
      {
        GameProviderId: 22,
        GameProviderName: "Yggdrasil",
      },
      {
        GameProviderId: 36,
        GameProviderName: "Flow Gaming",
      },
      {
        GameProviderId: 39,
        GameProviderName: "Red Tiger",
      },
      {
        GameProviderId: 40,
        GameProviderName: "Huawei 4D",
      },
      {
        GameProviderId: 41,
        GameProviderName: "Gamatron",
      },
      {
        GameProviderId: 18,
        GameProviderName: "IdnPoker",
      },
      {
        GameProviderId: 29,
        GameProviderName: "MicroGaming",
      },
      {
        GameProviderId: 50,
        GameProviderName: "LionKing",
      },
      {
        GameProviderId: 1009,
        GameProviderName: "Unknown",
      },
      {
        GameProviderId: 1010,
        GameProviderName: "Unknown",
      },
      {
        GameProviderId: 47,
        GameProviderName: "GiocoPlus",
      },
      {
        GameProviderId: -1,
        GameProviderName: "SBO",
      },
    ];
    const apiResponse: IAxiosResponse<IGameProviders> = {
      data: {
        Data: {
          GameProviders: gameProviders,
        },
        ErrorCode: 0,
        Message: "sting",
      },
    };
    return new Promise((resolve) => resolve(apiResponse));
  },
  getCasinoProviders(): IAxiosPromise<IGameProviders> {
    const gameProviders: Array<IGameProvider> = [
      {
        GameProviderId: 20,
        GameProviderName: "Evolution Gaming",
      },
      {
        GameProviderId: 0,
        GameProviderName: "WAN MEI",
      },
      {
        GameProviderId: 5,
        GameProviderName: "BigGaming",
      },
      {
        GameProviderId: 7,
        GameProviderName: "SexyGaming",
      },
      {
        GameProviderId: 12,
        GameProviderName: "IONLC",
      },
      {
        GameProviderId: 17,
        GameProviderName: "IDN Live",
      },
      {
        GameProviderId: 19,
        GameProviderName: "SaGaming",
      },
      {
        GameProviderId: 28,
        GameProviderName: "ALLBET",
      },
      {
        GameProviderId: 33,
        GameProviderName: "Green Dragon",
      },
      {
        GameProviderId: 37,
        GameProviderName: "RedTiger Casino",
      },
      {
        GameProviderId: 38,
        GameProviderName: "Pragmatic Play Casino",
      },
      {
        GameProviderId: -1,
        GameProviderName: "SBO",
      },
    ];
    const apiResponse: IAxiosResponse<IGameProviders> = {
      data: {
        Data: {
          GameProviders: gameProviders,
        },
        ErrorCode: 0,
        Message: "sting",
      },
    };
    return new Promise((resolve) => resolve(apiResponse));
  },
  getSportProvider(): IAxiosPromise<IGameProviders> {
    const sportProvider: Array<IGameProvider> = [
      {
        GameProviderName: "SBO",
        GameProviderId: -1,
      },
    ];
    const apiResponse: IAxiosResponse<IGameProviders> = {
      data: {
        Data: {
          GameProviders: sportProvider,
        },
        ErrorCode: 0,
        Message: "success",
      },
    };
    return new Promise((resolve) => resolve(apiResponse));
  },
  getVirtualSportProvider(): IAxiosPromise<IGameProviders> {
    const sportProvider: Array<IGameProvider> = [
      {
        GameProviderName: "SBO",
        GameProviderId: -1,
      },
    ];
    const apiResponse: IAxiosResponse<IGameProviders> = {
      data: {
        Data: {
          GameProviders: sportProvider,
        },
        ErrorCode: 0,
        Message: "success",
      },
    };
    return new Promise((resolve) => resolve(apiResponse));
  },
  getBetPayLoad(
    request: IGetBetPayloadRequest
  ): IAxiosPromise<IGetBetPayloadResponse> {
    const gameBetPayload: IGetBetPayloadResponse = {
      Url: "https://uat-wlkeno.93connect.com/mybet.php?mybet==QXZilXbNp2a45keVdXTzc3dmRUW61UMONEV6VFNNh0d1IGWslmWYFVP",
      IsSuccess: false,
    };
    const apiResponse: IAxiosResponse<IGetBetPayloadResponse> = {
      data: {
        Data: gameBetPayload,
        ErrorCode: 0,
        Message: "string",
      },
    };
    if (request) {
      apiResponse.data.Message = "success";
    }
    return new Promise((resolve) => resolve(apiResponse));
  },
  getTransactionHistory(
    params: GetTransactionHistoryRequest
  ): IAxiosPromise<GetTransactionHistoryResponse> {
    const transactions: GetTransactionHistoryResponse = {
      Transactions: [
        {
          TransactionNo: "4055",
          RequestedTime: "2022-03-25T04:15:56.19",
          PlayerBankName: "DemoBankMYR",
          BranchName: "demo",
          PlayerBankAccountNumber: "1111",
          PlayerBankAccountName: "seakheng",
          BeforeBalance: 2019861.62,
          AfterBalance: 2019871.62,
          Currency: "MYR",
          RequestedAmount: 10,
          VerifiedAmount: 0,
          CompanyBankName: "1WINGMYR",
          CompanyBankAccountName: "1MYR",
          CompanyBankAccountNumber: "9090887264",
          Email: "1TCPlayerMYR@gmail.com",
          Type: "Deposit",
          Status: "Waiting",
          PlayerRemark: "",
          Remark: "",
          IsPayment: false,
          ReferenceNumber: "1234",
          ModifiedBy: "1TCPlayerMYR",
          IsUsingBank: true,
          SlipImage: "64_20220325041555.jpg",
          DepositTime: "2022-03-25T00:00:00",
          MaxPage: 55,
          RowNumber: 1,
          DisplayTransactionNo: "BankTransfer_4055",
          IsSuccess: true,
        },
      ],
      IsSuccess: true,
    };

    const apiResponse: IApiResponse<GetTransactionHistoryResponse> = {
      Data: transactions,
      ErrorCode: EnumApiErrorCode.success,
      Message: EnumMessageType.Success,
    };

    const axiosResponse: IAxiosResponse<GetTransactionHistoryResponse> = {
      data: apiResponse,
    };
    return new Promise((resolve) => resolve(axiosResponse));
  },
  proceedTransaction(
    params: IProceedTransactionRequest
  ): IAxiosPromise<GetTransactionHistoryResponse> {
    const transactions: GetTransactionHistoryResponse = {
      Transactions: [
        {
          TransactionNo: "4055",
          RequestedTime: "2022-03-25T04:15:56.19",
          PlayerBankName: "DemoBankMYR",
          BranchName: "demo",
          PlayerBankAccountNumber: "1111",
          PlayerBankAccountName: "seakheng",
          BeforeBalance: 2019861.62,
          AfterBalance: 2019871.62,
          Currency: "MYR",
          RequestedAmount: 10,
          VerifiedAmount: 0,
          CompanyBankName: "1WINGMYR",
          CompanyBankAccountName: "1MYR",
          CompanyBankAccountNumber: "9090887264",
          Email: "1TCPlayerMYR@gmail.com",
          Type: "Deposit",
          Status: "Waiting",
          PlayerRemark: "",
          Remark: "",
          IsPayment: false,
          ReferenceNumber: "1234",
          ModifiedBy: "1TCPlayerMYR",
          IsUsingBank: true,
          SlipImage: "64_20220325041555.jpg",
          DepositTime: "2022-03-25T00:00:00",
          MaxPage: 55,
          RowNumber: 1,
          DisplayTransactionNo: "BankTransfer_4055",
          IsSuccess: true,
        },
      ],
      IsSuccess: true,
    };

    const apiResponse: IApiResponse<GetTransactionHistoryResponse> = {
      Data: transactions,
      ErrorCode: EnumApiErrorCode.success,
      Message: EnumMessageType.Success,
    };

    const axiosResponse: IAxiosResponse<GetTransactionHistoryResponse> = {
      data: apiResponse,
    };

    return new Promise((resolve) => resolve(axiosResponse));
  },
  getCurrencyWhiteList(): IAxiosPromise<IGetCurrencyWhiteListResponse> {
    const currencyWhiteListResponse: IGetCurrencyWhiteListResponse = {
      CurrencyWhiteList: currencyWhiteList,
      IsSuccess: true,
    };
    const apiResponse: IApiResponse<IGetCurrencyWhiteListResponse> = {
      Data: currencyWhiteListResponse,
      ErrorCode: 0,
      Message: "Success",
    };
    const axiosResponse: IAxiosResponse<IGetCurrencyWhiteListResponse> = {
      data: apiResponse,
    };
    return new Promise((resolve) => resolve(axiosResponse));
  },
  getCompanyFlowSetting(): IAxiosPromise<ICompanyFlowSettingResponse> {
    const availablePropertyListResponse: ICompanyFlowSettingResponse = {
      AvailablePropertyList: availablePropertyList,
      IsSuccess: true,
    };
    const apiResponse: IApiResponse<ICompanyFlowSettingResponse> = {
      Data: availablePropertyListResponse,
      ErrorCode: 0,
      Message: "Success",
    };
    const axiosResponse: IAxiosResponse<ICompanyFlowSettingResponse> = {
      data: apiResponse,
    };
    return new Promise((resolve) => resolve(axiosResponse));
  },
  getAccountCurrencyByUsername(): IAxiosPromise<IGetAccountCurrencyByUsernameResponse> {
    const apiResponse: IApiResponse<IGetAccountCurrencyByUsernameResponse> = {
      Data: {
        Currency: "AUD",
        IsSuccess: true,
      },
      ErrorCode: 0,
      Message: "Success",
    };
    const axiosResponse: IAxiosResponse<IGetAccountCurrencyByUsernameResponse> =
    {
      data: apiResponse,
    };
    return new Promise((resolve) => resolve(axiosResponse));
  },
  getRegisterBankList(
    args: IGetRegisterBankListRequest
  ): IAxiosPromise<IGetRegisterBankListResponse> {
    const playerBankOptionsResponse: IGetRegisterBankListResponse = {
      PlayerBankOptions: getPlayerBankOptions(args),
      IsSuccess: true,
    };
    const apiResponse: IApiResponse<IGetRegisterBankListResponse> = {
      Data: playerBankOptionsResponse,
      ErrorCode: 0,
      Message: "Success",
    };
    const axiosResponse: IAxiosResponse<IGetRegisterBankListResponse> = {
      data: apiResponse,
    };
    return new Promise((resolve) => resolve(axiosResponse));
  },
  register(args: IRegisterRequest): IAxiosPromise<IRegisterResponse> {
    const apiResponse: IApiResponse<IRegisterResponse> = {
      Data: args.IsTest ? CARegisterResponse : playerRegisterResponse,
      ErrorCode: 0,
      Message: "Success",
    };
    const axiosResponse: IAxiosResponse<IRegisterResponse> = {
      data: apiResponse,
    };
    return new Promise((resolve) => resolve(axiosResponse));
  },
  getGameProviderList(): IAxiosPromise<IGetGameProviderListResponse> {
    const fakeGameProvider: IGetGameProviderListResponse = {
      Games: [
        {
          GameProviderName: "Saba Sports",
          // GameProviderShortName: 'SABA',
          GameProviderId: 44,
          GameId: 581,
          Category: 1,
          // CategoryAtGameProvider: 9,
          // GameIdAtGameProvider: 0,
          CategoryName: "SPORTS",
          GameType: "SportsBook",
          GameName: "Saba Sports LOBBY",
          EnglishGameName: "Saba Sports LOBBY",
          IsEnabled: true,
          IsHotGame: false,
          // Rank: 1,
          IsNewGame: false,
          // IsJackpot: false,
          IsMaintainByGameProvider: false,
          IsMaintain: false,
          ForMobile: true,
          ForDesktop: true,
          IsPromotionGame: false,
          BsiPriority: 0,
          AsiPriority: 0,
          GameIconUrl:
            "//gp-luckybet888.wecname.com//images/sportsbooks/SportsBookImgSaba-en.png",
          EnabledCurrency: "BDT,CNY,IDR,INR,JPY,KRW,MYR,NPR,THB,USD,VND",
          GameCode: null,
          // Device: 3,
          APIGameProviderName: "SBOBET",
          APIGameProviderId: 1,
          DisplayOrder: 4,
          IsEnabledByGameProvider: true,
          TabType: "Sports",
        },
      ],
      TabInfos: [
        {
          TabType: "Sports",
          Order: 1,
          IsFilter: false,
        },
        {
          TabType: "Virtual Sports",
          Order: 2,
          IsFilter: false,
        },
        {
          TabType: "Live Casino",
          Order: 3,
          IsFilter: false,
        },
        {
          TabType: "Games",
          Order: 4,
          IsFilter: true,
        },
        {
          TabType: "Keno",
          Order: 5,
          IsFilter: false,
        },
        {
          TabType: "Cock Fight",
          Order: 6,
          IsFilter: false,
        },
      ],
      IsSuccess: true,
    };
    const apiResponse: IApiResponse<IGetGameProviderListResponse> = {
      Data: fakeGameProvider,
      ErrorCode: EnumApiErrorCode.success,
      Message: EnumMessageType.Success,
    };
    const axiosResponse: IAxiosResponse<IGetGameProviderListResponse> = {
      data: apiResponse,
    };
    return new Promise((resolve) => resolve(axiosResponse));
  },
  getLanguageSetting(): IAxiosPromise<IGetLanguageSettingResponse> {
    const fakeLanguageSetting: Array<ILanguage> = [
      {
        ISO: "en",
        LanguageLocalName: "English",
        Status: true,
        ModifiedBy: "1Company",
        ModifiedOn: "2022-04-20T23:12:54.283",
      },
      {
        ISO: "cn_MY",
        LanguageLocalName: "Bahasa Melayu",
        Status: false,
        ModifiedBy: "1Company",
        ModifiedOn: "2022-04-20T06:27:07.347",
      },
      {
        ISO: "km_KH",
        LanguageLocalName: "ភាសាខ្មែរ",
        Status: false,
        ModifiedBy: "1Company",
        ModifiedOn: "2022-04-20T06:27:07.347",
      },
      {
        ISO: "id_ID",
        LanguageLocalName: "Indonesian",
        Status: true,
        ModifiedBy: "1Company",
        ModifiedOn: "2022-04-06T23:57:55.633",
      },
      {
        ISO: "ja_JP",
        LanguageLocalName: "日本語",
        Status: true,
        ModifiedBy: "1Company",
        ModifiedOn: "2022-04-06T23:57:55.633",
      },
      {
        ISO: "ko_KR",
        LanguageLocalName: "한국어",
        Status: true,
        ModifiedBy: "1Company",
        ModifiedOn: "2022-04-06T23:57:55.633",
      },
      {
        ISO: "th_TH",
        LanguageLocalName: "ภาษาไทย",
        Status: false,
        ModifiedBy: "1Company",
        ModifiedOn: "2022-04-21T11:23:04.72",
      },
      {
        ISO: "vi_VN",
        LanguageLocalName: "Tiếng Việt",
        Status: true,
        ModifiedBy: "1Company",
        ModifiedOn: "2022-04-06T23:57:55.633",
      },
      {
        ISO: "zh_CN",
        LanguageLocalName: "简体中文",
        Status: true,
        ModifiedBy: "1Company",
        ModifiedOn: "2022-04-06T23:57:55.633",
      },
      {
        ISO: "my_MM",
        LanguageLocalName: "Burmes",
        Status: true,
        ModifiedBy: "1Company",
        ModifiedOn: "2022-04-06T23:57:55.633",
      },
    ];
    const fakeLanguageResponse: IGetLanguageSettingResponse = {
      Language: fakeLanguageSetting,
      IsSuccess: true,
    };
    const apiResponse: IApiResponse<IGetLanguageSettingResponse> = {
      Data: fakeLanguageResponse,
      ErrorCode: EnumApiErrorCode.success,
      Message: EnumMessageType.Success,
    };
    const axiosResponse: IAxiosResponse<IGetLanguageSettingResponse> = {
      data: apiResponse,
    };
    return new Promise((resolve) => resolve(axiosResponse));
  },
  getPlayerPreferenceForDisplay(): IAxiosPromise<IGetPlayerPreference> {
    const fakeLanguageResponse: IGetPlayerPreference = {
      TimeZone: {
        IsUsingLocalTime: true,
        IsHidden: true,
      },
      ErrorCode: 0,
      ErrorMessage: "success",
    };
    const apiResponse: IApiResponse<IGetPlayerPreference> = {
      Data: fakeLanguageResponse,
      ErrorCode: EnumApiErrorCode.success,
      Message: EnumMessageType.Success,
    };
    const axiosResponse: IAxiosResponse<IGetPlayerPreference> = {
      data: apiResponse,
    };
    return new Promise((resolve) => resolve(axiosResponse));
  },
  getMultiWallet(): IAxiosPromise<IGetMultiWalletBalaceResponse> {
    const wallets: Array<IWallet> = [
      {
        WalletName: "Main Wallet",
        BetCredit: 123415123.12415,
        Category: 0,
      },
      {
        WalletName: "Games Wallet",
        BetCredit: 123123.12415,
        Category: 0,
      },
      {
        WalletName: "Sport Wallet",
        BetCredit: 123123.12415,
        Category: 0,
      },
    ];
    const fakeDataWallet: IGetMultiWalletBalaceResponse = {
      Wallets: wallets,
      IsSuccess: true,
    };
    const apiResponse: IApiResponse<IGetMultiWalletBalaceResponse> = {
      Data: fakeDataWallet,
      ErrorCode: EnumApiErrorCode.success,
      Message: EnumMessageType.Success,
    };
    const axiosResponse: IAxiosResponse<IGetMultiWalletBalaceResponse> = {
      data: apiResponse,
    };
    return new Promise((resolve) => resolve(axiosResponse));
  },
  checkHaveReferralEvent(): IAxiosPromise<null> {
    const apiResponse: IApiResponse<null> = {
      Data: null,
      ErrorCode: EnumApiErrorCode.success,
      Message: EnumMessageType.Success,
    };
    const axiosResponse: IAxiosResponse<null> = {
      data: apiResponse,
    };
    return new Promise((resolve) => resolve(axiosResponse));
  },
  getWithdrawalFromGameProvider(): IAxiosPromise<null> {
    const apiResponse: IApiResponse<null> = {
      Data: null,
      ErrorCode: EnumApiErrorCode.success,
      Message: EnumMessageType.Success,
    };
    const axiosResponse: IAxiosResponse<null> = {
      data: apiResponse,
    };
    return new Promise((resolve) => resolve(axiosResponse));
  },
  getBsiGameList(): IAxiosPromise<IGetBsiGameListResponse> {
    const bsiGameList: IGetBsiGameListResponse = {
      Games: fakeGameList,
      TabInfos: fakeTabInfoList,
      SeamlessGameGameTypeList: fakeSeamlessGameGameTypeList,
      SeamlessProviderList: fakeSeamlessProviderList,
      SeamlessGameTabList: fakeSeamlessGameTabList,
      IsSuccess: true,
    };
    const apiResponse: IApiResponse<IGetBsiGameListResponse> = {
      Data: bsiGameList,
      ErrorCode: EnumApiErrorCode.success,
      Message: EnumMessageType.Success,
    };
    const axiosResponse: IAxiosResponse<IGetBsiGameListResponse> = {
      data: apiResponse,
    };
    return new Promise((resolve) => resolve(axiosResponse));
  },
  getPlayableGame(): IAxiosPromise<IGetBsiGameListResponse> {
    const bsiGameList: IGetBsiGameListResponse = {
      Games: fakeGameList,
      TabInfos: fakeTabInfoList,
      SeamlessGameGameTypeList: fakeSeamlessGameGameTypeList,
      SeamlessProviderList: fakeSeamlessProviderList,
      SeamlessGameTabList: fakeSeamlessGameTabList,
      IsSuccess: true,
    };
    const apiResponse: IApiResponse<IGetBsiGameListResponse> = {
      Data: bsiGameList,
      ErrorCode: EnumApiErrorCode.success,
      Message: EnumMessageType.Success,
    };
    const axiosResponse: IAxiosResponse<IGetBsiGameListResponse> = {
      data: apiResponse,
    };
    return new Promise((resolve) => resolve(axiosResponse));
  },
  getAsiGameList(): IAxiosPromise<IGetAsiGameListResponse> {
    const fakeResponse: IGetAsiGameListResponse = {
      Games: fakeGameList,
      IsSuccess: true,
    };
    const apiResponse: IApiResponse<IGetAsiGameListResponse> = {
      Data: fakeResponse,
      ErrorCode: EnumApiErrorCode.success,
      Message: EnumMessageType.Success,
    };
    const axiosResponse: IAxiosResponse<IGetAsiGameListResponse> = {
      data: apiResponse,
    };
    return new Promise((resolve) => resolve(axiosResponse));
  },

  getPreviousQR(request: IPreviousQRReq): IAxiosPromise<IPreviousQR> {
    const fakeResponse: IPreviousQR = {
      PreviousSessions: fakepreviousQRList,
      ErrorCode: 200,
      ErrorMessage: 'Success',
      IsSuccess: true,
    };

    const apiResponse: IAxiosResponse<IPreviousQR> = {
      data: {
        Data: fakeResponse,
        ErrorCode: 0,
        Message: "sting",
      },
    };
    if (request) {
      apiResponse.data.Message = "success";
    }
    return new Promise((resolve) => resolve(apiResponse));
  },
  getReferralLayerAmount(): IAxiosPromise<IGetReferralLayerAmountResponse> {
    const response: IGetReferralLayerAmountResponse = fakeReferralLayerAmount;
    const apiResponse: IApiResponse<IGetReferralLayerAmountResponse> = {
      Data: response,
      ErrorCode: EnumApiErrorCode.success,
      Message: EnumMessageType.Success,
    };
    const axiosResponse: IAxiosResponse<IGetReferralLayerAmountResponse> = {
      data: apiResponse,
    };
    return new Promise((resolve) => resolve(axiosResponse));
  },
  getRedeemReferralRequest(): IAxiosPromise<IGetRedeemReferralResponse> {
    const response: IGetRedeemReferralResponse = {
      RedeemRequests: fakeRedeemRequestList,
      IsSuccess: true,
    };
    const apiResponse: IApiResponse<IGetRedeemReferralResponse> = {
      Data: response,
      ErrorCode: EnumApiErrorCode.success,
      Message: EnumMessageType.Success,
    };
    const axiosResponse: IAxiosResponse<IGetRedeemReferralResponse> = {
      data: apiResponse,
    };
    return new Promise((resolve) => resolve(axiosResponse));
  },
  redeemReferralAmount(
    request: IRedeemReferralAmountRequest
  ): IAxiosPromise<IGetRedeemReferralResponse> {
    const response: IGetRedeemReferralResponse = {
      RedeemRequests: fakeRedeemRequestList,
      IsSuccess: true,
    };
    const apiResponse: IApiResponse<IGetRedeemReferralResponse> = {
      Data: response,
      ErrorCode: EnumApiErrorCode.success,
      Message: EnumMessageType.Success,
    };
    const axiosResponse: IAxiosResponse<IGetRedeemReferralResponse> = {
      data: apiResponse,
    };
    return new Promise((resolve) => resolve(axiosResponse));
  },
  getReferralDiagram(
    request: IGetReferralDiagramRequest
  ): IAxiosPromise<IGetReferralDiagramResponse> {
    const response: IGetReferralDiagramResponse = {
      NodesList: fakeNodeList,
      IsSuccess: true,
    };
    const apiResponse: IApiResponse<IGetReferralDiagramResponse> = {
      Data: response,
      ErrorCode: EnumApiErrorCode.success,
      Message: EnumMessageType.Success,
    };
    const axiosResponse: IAxiosResponse<IGetReferralDiagramResponse> = {
      data: apiResponse,
    };
    return new Promise((resolve) => resolve(axiosResponse));
  },
  getStatement(args: IStatementRequest): IAxiosPromise<IStatementResponse> {
    const apiResponse: IApiResponse<IStatementResponse> = {
      Data: statement,
      ErrorCode: 0,
      Message: "Success",
    };
    const axiosResponse: IAxiosResponse<IStatementResponse> = {
      data: apiResponse,
    };
    return new Promise((resolve) => resolve(axiosResponse));
  },
  getPromotions(): IAxiosPromise<IGetPromotionResponse> {
    const response: IGetPromotionResponse = {
      Promotions: fakePromotion,
      IsSuccess: true,
    };
    const apiResponse: IApiResponse<IGetPromotionResponse> = {
      Data: response,
      ErrorCode: 0,
      Message: "Success",
    };
    const axiosResponse: IAxiosResponse<IGetPromotionResponse> = {
      data: apiResponse,
    };
    return new Promise((resolve) => resolve(axiosResponse));
  },
  postApplyPromotion(request: IPromotionApplyReqeust): IAxiosPromise<null> {
    const apiResponse: IApiResponse<null> = {
      Data: null,
      ErrorCode: 0,
      Message: "Success",
    };
    const axiosResponse: IAxiosResponse<null> = {
      data: apiResponse,
    };
    return new Promise((resolve) => resolve(axiosResponse));
  },
  recommendMatches(): IAxiosPromise<IRecommendMatchResponse> {
    const popularMatch: IRecommendMatch = {
      LeagueName: "e-Football F22 Elite Club Friendly",
      MatchId: 1,
      HomeName: "Accrington Stanley",
      AwayName: "Exeter City",
      KickOffTime: "08/26 21:00",
      GameTime: "21:00",
      HomeScore: 4,
      AwayScore: 2,
      HomeTeamIconUrl: '',
      AwayTeamIconUrl: '',
      Handicap: {
        HomeOdds: 1,
        AwayOdds: 1,
        Point: 1,
      },
      OverUnder: {
        HomeOdds: 1,
        AwayOdds: 1,
        Point: 1,
      },
    };
    const apiResponse: IApiResponse<IRecommendMatchResponse> = {
      Data: {
        RecommendMatches: [
          popularMatch,
          popularMatch,
          popularMatch,
          popularMatch,
          popularMatch,
        ],
      },
      ErrorCode: 0,
      Message: "Success",
    };
    const axiosResponse: IAxiosResponse<IRecommendMatchResponse> = {
      data: apiResponse,
    };
    return new Promise((resolve) => resolve(axiosResponse));
  },
  loginByMatchId(
    request: ILoginByMatchIdRequest
  ): IAxiosPromise<ILoginByMatchIdResponse> {
    const apiResponse: IApiResponse<ILoginByMatchIdResponse> = {
      Data: {
        Url: "https://www.youtube.com/",
      },
      ErrorCode: 0,
      Message: "Success",
    };
    const axiosResponse: IAxiosResponse<ILoginByMatchIdResponse> = {
      data: apiResponse,
    };
    return new Promise((resolve) => resolve(axiosResponse));
  },

  getPureLiteBitList(
    request: IRequestBetList
  ): IAxiosPromise<IResponseBetList> {
    const betDetails: Array<IResponseBetDetail> = [
      {
        BetOption: "Lazio",
        SubBetOdds: -0.93,
        HandicapPoint: 2,
        SportType: "Basketball",
        MarketType: "First Half Hdp",
        SubBetStatus: "string",
        BetType: 7,
        Match: "Lazio vs AS Roma",
        League: "ITALY SERIE A",
        WinLoseDate: new Date("2021-01-15T03:57:09.893"),
        LiveScore: "0:0",
        HalfTimeScore: "0:0",
        FullTimeScore: "0:0",
        CustomizedBetType: "",
        KickOffTime: new Date("2021-01-15T15:45:00"),
        HomeTeam: "Lazio",
        AwayTeam: "AS Roma",
        Favourite: "d",
      },
    ];
    const sportBets: Array<IResponseSportBets> = [
      {
        RefNo: 121121,
        Token: "string token",
        IsRedeem: true,
        RedeemTime: new Date("2021-01-15T03:57:09.893"),
        PrintCount: 0,
        MainBetSportsType: "Football",
        OrderTime: new Date("2021-01-15T03:57:09.893"),
        MainBetOdds: -0.96,
        Stake: 12,
        ActualStake: 15.22,
        PlayerCommission: 0,
        Winlose: 2,
        Status: "running",
        OddsStyle: "M",
        IsLiveBet: false,
        Currency: "MYR",
        IsShowBetDetailButton: false,
        IsResettled: false,
        RollbackTime: new Date("2021-01-15T03:57:09.893"),
        ResettleTime: new Date("2021-01-15T03:57:09.893"),
        BetDetails: betDetails,
        VoidReason: null,
        GameProviderId: 1,
        MaxPage: 1,
        RowNumber: 1,
      },
    ];
    const betList: IResponseBetList = {
      SportBets: sportBets,
      TotalWinLose: 0,
      TotalCommission: 0,
      IsSuccess: true,
    };
    const apiResponse: IApiResponse<IResponseBetList> = {
      Data: betList,
      ErrorCode: 0,
      Message: "Success",
    };
    const axiosResponse: IAxiosResponse<IResponseBetList> = {
      data: apiResponse,
    };
    return new Promise((resolve) => resolve(axiosResponse));
  },
  getBMD1ThemeProperty(
    request: GetThemePropertiesRequestDto
  ): Promise<IApiResponse<IBaseResponse>> {
    const properties = {
      ThemeProperties: fakeBMD1ThemeProperty,
      ErrorCode: 0,
      ErrorMessage: "Success",
      IsSuccess: true,
    };
    const apiResponse: IApiResponse<ICompanyThemePropertiesFromApi> = {
      Data: properties as ICompanyThemePropertiesFromApi,
      ErrorCode: 0,
      Message: "Success",
    };
    return new Promise((resolve) => resolve(apiResponse));
  },
  getLuckyWheelOptions(
    request: IGetLuckyWheelOptionsRequest
  ): Promise<IApiResponse<IGetLuckyWheelOptionsResponse>> {
    const fakedata = {
      LuckyWheelOptions: fakeLuckyWheel,
    };
    const apiResponse: IApiResponse<any> = {
      Data: fakedata,
      ErrorCode: 0,
      Message: "Success",
    };
    return new Promise((resolve) => resolve(apiResponse));
  },
};
