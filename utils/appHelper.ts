import apis from "./apis";
import { KEYS } from "./constants";
import { store } from "~~/store";

const _getCurrentTime = () => new Date().getTime();

const _saveAuthRefreshToStorage = (duration: number): void => {
  const expiresTime = _getCurrentTime() + duration;
  localStorage.setItem(KEYS.AUTH_REFRESH, String(expiresTime));
};
const clearTokenRefresh = (): void => {
  localStorage.removeItem(KEYS.AUTH_REFRESH);
  localStorage.removeItem(KEYS.AUTH_REFRESH_INTERVAL_ID);
};
const _getAuthRefreshFromStorage = (): number => {
  const expires = Number(localStorage.getItem(KEYS.AUTH_REFRESH));
  if (Number.isNaN(expires)) return 0;
  return expires;
};
const _hasAuthExpired = (): boolean => {
  return _getCurrentTime() > _getAuthRefreshFromStorage();
};
const _getRemainingDuration = (): number => {
  return _getAuthRefreshFromStorage() - _getCurrentTime();
};
const _createTokenRefreshInterval = (duration: number): NodeJS.Timer => {
  const authInterval = setInterval(async () => {
    const auth = () => store.state.auth;
    if (auth()) {
      await apis.renewToken();
    }
  }, duration);
  localStorage.setItem(KEYS.AUTH_REFRESH_INTERVAL_ID, String(authInterval));
  return authInterval;
};

const renewTokenRefresh = () => {
  const intervalId = localStorage.getItem(KEYS.AUTH_REFRESH_INTERVAL_ID);
  if (intervalId) {
    // if we don't clear the interval,
    // there will be duplicates of renew token requests.
    clearInterval(intervalId);
  }
  clearTokenRefresh();
  _saveAuthRefreshToStorage(VALUES.AUTH_REFRESH_TIME);
  _createTokenRefreshInterval(_getRemainingDuration());
};

const initTokenRefresh = () => {
  const authRefreshDuration = _hasAuthExpired()
    ? VALUES.AUTH_REFRESH_TIME
    : _getRemainingDuration();
  _createTokenRefreshInterval(authRefreshDuration);
};
const renewResourceExpiryTime = () => {
  const expiresTime = commonHelper.getMinutesFromNow(
    VALUES.RESOURCE_EXPIRES_TIME
  );
  localStorage.setItem(KEYS.RESOURCE_EXPIRES, String(expiresTime));
};
const clearExpiryTime = () => {
  localStorage.removeItem(KEYS.RESOURCE_EXPIRES);
};
export default {
  initTokenRefresh,
  renewTokenRefresh,
  clearTokenRefresh,
  renewResourceExpiryTime,
  clearExpiryTime,
};
