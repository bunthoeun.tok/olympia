import {
  ICompanyThemeOption,
  GetThemePropertiesRequestDto,
  ICompanyThemePropertiesFromApi,
  ThemePropertiesFromApiDto,
} from "../models/companyThemePropertiesRespone";
import {
  IUpdateTelegramSsoTokenRequest,
  IUpdateTelegramSsoTokenResponse,
} from "../models/UpdateTelegramSsoToken";
import apiCalling from "./apiCalling";
import {
  GameProviderList,
  IGetGameProviderListResponse,
  IGetTopGameListResponse,
} from "@/models/IGetPlayableGamesResponse";
import { IFormAgentCooperation } from "@/models/agentCooperation";
import ApiResponse, { IApiResponse } from "@/models/apiResponse";
import { IRequestResetPassword } from "@/models/auth/resetPassword";
import { IGetRegisterBankListResponse } from "@/models/bank";
import {
  IBaseResponse,
  IBaseResponsePromotion,
  INullDataResponse,
} from "@/models/baseResponse";
import {
  GetBsiGameListResponse,
  IGameTypeProviderList,
  IGetMenuTabResponse,
} from "@/models/bsiGameList";
import { IGetLanguageSettingResponse } from "@/models/common/language";
import {
  GetMultiWalletBalaceResponse,
  IGetMultiWalletBalaceResponse,
} from "@/models/common/wallet";
import {
  CompanyFlowSettingResponse,
  ICompanyFlowSettingResponse,
} from "@/models/companyFlowSetting";
import {
  IGetAccountCurrencyByUsernameResponse,
  IGetCurrencyWhiteListResponse,
} from "@/models/currencyWhiteList";
import EnumDisplayTimeOption from "@/models/enums/enumDisplayTimeOption";
import { IFeaturesToggleRespone } from "@/models/featuresToggleRespone";
import { IGameProviders } from "@/models/gameProvider";
import { IGetGameProviderLoginResponse } from "@/models/gameProviderLogin";
import IGetGeography from "@/models/geoIpResponse";
import { GetBalanceResponse } from "@/models/getBalanceResponse";
import IGetBetListRequest from "@/models/getBetListRequest";
import {
  GetBetListResponse,
  IGetBetListResponse,
} from "@/models/getBetListResponse";
import IGetBetPayloadRequest from "@/models/getBetPayloadRequest";
import IGetBetPayloadResponse from "@/models/getBetPayloadResponse";
import {
  IAllCustomerSettingResponse,
  ICustomerSettingResponse,
} from "@/models/getCustomerSettingResponse";
import {
  IGetManualCryptoCurrencyTransactionResponse,
  IRequestManualCryptoCurrencyDeposit,
} from "@/models/getManualCryptoCurrencyTransactionResponse";
import { GetPaymentGatewayBanksResponse } from "@/models/getPaymentGatewayBanksRespone";
import IGetPlayerPreference from "@/models/getPlayerPreference";
import IGetPreferenceForSettingsResponse from "@/models/getPreferenceForSettingsResponse";
import { GetProfileResponse } from "@/models/getProfileResponse";
import { IGetTransactionBankListResponse } from "@/models/getTransactionBankListResponse";
import { IGetTransactionFeeResponse } from "@/models/getTransactionFeeResponse";
import {
  GetTransactionHistoryRequest,
  GetTransactionHistoryResponse,
  IProceedTransactionRequest,
} from "@/models/getTransactionHistoryResponse";
import {
  GetLoginResponse,
  IGetCurrencies,
  IGetWhiteListedIps,
  IRequestLogin,
  IRequestPlayerIsOnline,
} from "@/models/login";
import IGetPromotionApplyListRequest from "@/models/promotion/getPromotionApplyListRequest";
import IGetPromotionApplyListResponse, {
  GetPromotionApplyListResponse,
} from "@/models/promotion/getPromotionApplyListResponse";
import {
  GetPromotionResponse,
  IGetPromotionResponse,
  IPromotionApplyReqeust,
  IRequestPromotion,
} from "@/models/promotion/promotion";
import {
  GetRedeemReferralResponse,
  GetReferralLayerAmountResponse,
  IGetRedeemReferralResponse,
  IGetReferralDiagramRequest,
  IGetReferralDiagramResponse,
  IRedeemReferralAmountRequest,
} from "@/models/referral/getReferral";
import {
  GetRegisterResponse,
  IRegisterRequest,
  IGetOtpVerificaitonRequest,
  IGetAccountCurrencyByDomainResponse,
} from "@/models/register";
import {
  IGetDepositInternetBankReponse,
  IRequestAutoDepositBankTransfer,
  IRequestDepositBankTransfer,
  IRequestDepositInternetBank,
} from "@/models/requestDepositBanks";
import { IRequestTransactionFee } from "@/models/requestTransactionFee";
import {
  IGetWithdrawalInternetBankReponse,
  IRequestSetPaymentPassword,
  IRequestWithdrawalBankTransfer,
  IRequestWithdrawalInternetBank,
} from "@/models/requestWithdrawalBankTransfer";
import IGetSportBetRequest from "@/models/sportbet/getSportBetRequest";
import {
  GetSportBetResponse,
  IGetSportBetResponse,
} from "@/models/sportbet/getSportBetResponse";
import {
  IStatementRequest,
  IStatementResponse,
  StatementResponse,
} from "@/models/statement";
import {
  ILoginByMatchIdRequest,
  ILoginByMatchIdResponse,
  IRecommendMatch,
  IRecommendMatchResponse,
  RecommendMatch,
} from "@/models/theme/popular_match/popularMatch";
import { IWebId } from "@/models/webId";
import { IVersionResponse } from "~~/models/imageVersion";
import { IPreviousQR } from "~~/models/sportqr/previousQR";
import { IPreviousQRReq } from "~~/models/sportqr/previousQRReq";
import {
  IRequestBetList,
  IResponseBetList,
} from "@/models/theme/lite/PureLite";
import {
  AllSportBetsResponse,
  IAllSportBetsRequest,
} from "~~/models/sportqr/redeemSingleBetDetail";
import {
  IPayTicketsRequest,
  IRedeemRequest,
  IRedeemResponse,
} from "~~/models/sportqr/redeemTicket";

import {
  GetPlayerRedeemHistoryResponse,
  GetPlayerRedeemRequest,
  IGetApplyRedeemRequest,
  IGetPlayerRedeemHistoryRequest,
  ReferralV2GeneralInformation,
} from "~~/models/referral/referralV2";
import { IApplyPromotionRequest } from "~~/models/promotion/promotionApplyList";
import {
  ILoginToSportsRequest,
  ILoginToSportsRespone,
} from "~~/sports/models/loginToSport";
import { IQrCodePayout } from "~~/models/sportqr/qrCodePayout";
import { IPlayerSearchGameRequest } from "~~/models/playerSearchGame";
import { ICustomerInfoRequest } from "~~/models/telegramCustomerInfo";
import { ILoginTelegram } from "~~/models/telegramLogin";
import {
  IGetMessageResponse,
  IGetSiteMessageRequest,
  IUpdateMessageStatus,
} from "~~/models/message";
import {
  IGetLuckyWheelOptionsRequest,
  IGetLuckyWheelOptionsResponse,
  IGetLuckyWheelSpinResultRequest,
  IGetLuckyWheelSpinResultResponse,
  IGetAvailableLuckyWheelTickets,
  IGetAvailableLuckyWheelTicketsResponse,
} from "../models/lucky-wheel/luckyWheel";
import { store } from "~/store";
import { IUpdatePlayerPreferenceRequest } from "@/models/account/preference";

export type ServerPromise<T = any> = Promise<IApiResponse<T>>;

const getResponse = (response: ServerPromise) =>
  response.then((value) => new ApiResponse(value));
export default {
  getGeography(): Promise<ApiResponse<IGetGeography>> {
    const response = apiCalling.callGetGeography();
    return getResponse(response);
  },
  getGeographyByIp(Ip: string): Promise<ApiResponse<IGetGeography>> {
    const response = apiCalling.callGetGeographyByIp(Ip);
    return getResponse(response);
  },
  getProfileInformation(): Promise<ApiResponse<GetProfileResponse>> {
    const response = getResponse(apiCalling.callGetProfileInformation());

    return response.then((value) => {
      value.Data = new GetProfileResponse(value.Data);
      return value;
    });
  },
  getAllWalletsBalance(): Promise<ApiResponse<GetBalanceResponse>> {
    const response = getResponse(apiCalling.callGetAllWalletsBalance());

    return response.then((value) => {
      value.Data = new GetBalanceResponse(value.Data);
      return value;
    });
  },
  changePassword(
    oldPassword: string,
    newPassword: string,
    OnlineId: number
  ): Promise<ApiResponse<IBaseResponse>> {
    const response = apiCalling.callChangePassword(
      oldPassword,
      newPassword,
      OnlineId
    );
    return getResponse(response);
  },
  changePaymentPassword(
    oldPassword: string,
    newPassword: string,
    onlineId: number
  ): Promise<ApiResponse<IBaseResponse>> {
    const response = apiCalling.callChangePaymentPassword(
      oldPassword,
      newPassword,
      onlineId
    );
    return getResponse(response);
  },
  changeLoginName(name: string): Promise<ApiResponse<IBaseResponse>> {
    const response = apiCalling.callChangeLoginName(name);
    return getResponse(response);
  },
  getPreferenceSetting(): Promise<
    ApiResponse<IGetPreferenceForSettingsResponse>
  > {
    const response = apiCalling.callGetPreferenceSetting();
    return getResponse(response);
  },
  updatePlayerPreference(
    UpdatePlayerPreferenceRequest: IUpdatePlayerPreferenceRequest
  ): Promise<ApiResponse<IBaseResponse>> {
    const response = apiCalling.callUpdatePlayerPreference(
      UpdatePlayerPreferenceRequest
    );
    return getResponse(response);
  },
  async renewToken(): Promise<void> {
    const response = await apiCalling.callRenewToken();
    const authCookie = useCookie(KEYS.AUTH_COOKIE_KEY, {
      expires: new Date(commonHelper.getMinutesFromNow(25)),
    });
    authCookie.value = response.Data.Token;
    appHelper.renewTokenRefresh();
  },
  getTransactionBankList(
    RequestPageType: string
  ): Promise<ApiResponse<IGetTransactionBankListResponse>> {
    const response = apiCalling.callGetTransactionBankList(RequestPageType);
    return getResponse(response);
  },
  getPaymentGatewayBanks(): Promise<
    ApiResponse<GetPaymentGatewayBanksResponse>
  > {
    const response = getResponse(apiCalling.callgetPaymentGatewayBanks());
    return response.then((value) => {
      value.Data = new GetPaymentGatewayBanksResponse(value.Data);
      return value;
    });
  },
  proceedDepositBankTransfer(
    params: IRequestDepositBankTransfer
  ): Promise<ApiResponse<IBaseResponse>> {
    const response = apiCalling.callProceedDepositBankTransfer(params);
    return getResponse(response);
  },

  proceedDepositInternetBank(
    params: IRequestDepositInternetBank
  ): Promise<ApiResponse<IGetDepositInternetBankReponse>> {
    const response = apiCalling.callProceedDepositInternetBank(params);
    return getResponse(response);
  },
  proceedAutoDeposit(
    param: IRequestAutoDepositBankTransfer
  ): Promise<ApiResponse<IBaseResponse>> {
    const response = apiCalling.callProceedAutoDepositBankTransfer(param);
    return getResponse(response);
  },
  uploadFile(
    files: File[],
    folder: string
  ): Promise<ApiResponse<IBaseResponse>> {
    const form = new FormData();
    files.forEach((file) => {
      form.append("file", file);
    });
    form.append("folder", folder);
    const response = apiCalling.callUploadFile(form);
    return getResponse(response);
  },
  getFeaturesToggle(): Promise<ApiResponse<IFeaturesToggleRespone>> {
    const response = apiCalling.callGetFeaturesToggle();
    return getResponse(response);
  },
  withdrawalBankTransfer(
    params: IRequestWithdrawalBankTransfer
  ): Promise<ApiResponse<IBaseResponse>> {
    const response = apiCalling.callWithdrawalBankTransfer(params);
    return getResponse(response);
  },
  getTransactionFee(
    params: IRequestTransactionFee
  ): Promise<ApiResponse<IGetTransactionFeeResponse>> {
    const response = apiCalling.callGetTransactionFee(params);
    return getResponse(response);
  },
  withdrawalInternetBank(
    params: IRequestWithdrawalInternetBank
  ): Promise<ApiResponse<IGetWithdrawalInternetBankReponse>> {
    const response = apiCalling.callWithdrawalInternetBank(params);
    return getResponse(response);
  },
  setPaymentPassword(
    params: IRequestSetPaymentPassword
  ): Promise<ApiResponse<IBaseResponse>> {
    const response = apiCalling.callSetPaymentPassword(params);
    return getResponse(response);
  },
  getTransactionHistory(
    params: GetTransactionHistoryRequest
  ): Promise<ApiResponse<GetTransactionHistoryResponse>> {
    const response = getResponse(apiCalling.callGetTransactionHistory(params));
    return response.then((value) => {
      if (value.Data) {
        value.Data = new GetTransactionHistoryResponse(value.Data);
      }
      return value;
    });
  },
  proceedTransaction(
    params: IProceedTransactionRequest
  ): Promise<ApiResponse<INullDataResponse>> {
    const response = apiCalling.callProceedTransaction(params);
    return getResponse(response);
  },
  getCustomerSetting(
    RequestId: string
  ): Promise<ApiResponse<ICustomerSettingResponse>> {
    const response = apiCalling.callGetCustomerSetting(RequestId);
    return getResponse(response);
  },
  getManualCryptoCurrencyTransaction(
    RequestPageType: string
  ): Promise<ApiResponse<IGetManualCryptoCurrencyTransactionResponse>> {
    const response =
      apiCalling.callGetManualCryptoCurrencyTransaction(RequestPageType);
    return getResponse(response);
  },
  proceedManualCryptoCurrencyDeposit(
    params: IRequestManualCryptoCurrencyDeposit
  ): Promise<ApiResponse<IBaseResponse>> {
    const response = apiCalling.callProceedManualCryptoCurrencyDeposit(params);
    return getResponse(response);
  },
  getThemeProperties(
    params: GetThemePropertiesRequestDto,
    webId: Number,
    cdn: string
  ): Promise<ApiResponse<ICompanyThemePropertiesFromApi>> {
    const response = apiCalling.callGetThemeProperties(params);
    return getResponse(response).then(
      (value: ApiResponse<ICompanyThemePropertiesFromApi>) => {
        if (value.ErrorCode === 0) {
          value.Data.ThemeProperties = value.Data.ThemeProperties.map(
            (themeProperty: ThemePropertiesFromApiDto) =>
              new ThemePropertiesFromApiDto(themeProperty, webId, cdn)
          );
        } else {
          value.Data.ThemeProperties = [];
        }
        return value;
      }
    );
  },
  getPromotionApplyList(
    params: IGetPromotionApplyListRequest
  ): Promise<ApiResponse<IGetPromotionApplyListResponse>> {
    const response = getResponse(apiCalling.callGetPromotionApplyList(params));
    return response.then((value) => {
      value.Data = new GetPromotionApplyListResponse(value.Data);
      return value;
    });
  },
  getGamesBetList(
    params: IGetBetListRequest
  ): Promise<ApiResponse<IGetBetListResponse>> {
    const response = apiCalling.callGetGameBetList(params);
    return getResponse(response).then((value) => {
      if (value.Data) {
        value.Data = new GetBetListResponse(value.Data);
      }
      return value;
    });
  },
  getCasinoBetList(
    params: IGetBetListRequest
  ): Promise<ApiResponse<IGetBetListResponse>> {
    const response = apiCalling.callGetCasinoBetList(params);
    return getResponse(response).then((value) => {
      if (value.Data) {
        value.Data = new GetBetListResponse(value.Data);
      }
      return value;
    });
  },
  getCockfightingBetList(
    params: IGetBetListRequest
  ): Promise<ApiResponse<IGetBetListResponse>> {
    const response = apiCalling.callGetCockfightingBetList(params);
    return getResponse(response).then((value) => {
      if (value.Data) {
        value.Data = new GetBetListResponse(value.Data);
      }
      return value;
    });
  },
  getPokerBetList(
    params: IGetBetListRequest
  ): Promise<ApiResponse<IGetBetListResponse>> {
    const response = apiCalling.callGetPokerBetList(params);
    return getResponse(response).then((value) => {
      if (value.Data) {
        value.Data = new GetBetListResponse(value.Data);
      }
      return value;
    });
  },
  getSportsbetList(
    params: IGetSportBetRequest
  ): Promise<ApiResponse<IGetSportBetResponse>> {
    const response = apiCalling.callGetSportsBetList(params);
    return getResponse(response).then((value) => {
      if (value.Data) {
        value.Data = new GetSportBetResponse(value.Data);
      }
      return value;
    });
  },
  getVirtualSportsbetList(
    params: IGetSportBetRequest
  ): Promise<ApiResponse<IGetSportBetResponse>> {
    const response = apiCalling.callGetVirtualSportsBetList(params);
    return getResponse(response).then((value) => {
      if (value.Data) {
        value.Data = new GetSportBetResponse(value.Data);
      }
      return value;
    });
  },
  getGamesProvider(): Promise<ApiResponse<IGameProviders>> {
    const response = getResponse(apiCalling.callGetGamesProviders());
    return response;
  },
  getCasinoProvider(): Promise<ApiResponse<IGameProviders>> {
    const response = getResponse(apiCalling.callGetCasinoProviders());
    return response;
  },
  getSportProvider(): Promise<ApiResponse<IGameProviders>> {
    const response = getResponse(apiCalling.callGetSportProvider());
    return response;
  },
  getVirtualSportProvider(): Promise<ApiResponse<IGameProviders>> {
    const response = getResponse(apiCalling.callGetVirtualSportProvider());
    return response;
  },
  getBetPayload(
    params: IGetBetPayloadRequest
  ): Promise<ApiResponse<IGetBetPayloadResponse>> {
    const response = getResponse(apiCalling.callGetBetPayload(params));
    return response;
  },
  getCurrencyWhiteList(): Promise<ApiResponse<IGetCurrencyWhiteListResponse>> {
    const response = getResponse(apiCalling.callGetCurrencyWhiteList());
    return response;
  },
  getCompanyFlowSetting(): Promise<ApiResponse<ICompanyFlowSettingResponse>> {
    const response = getResponse(apiCalling.callGetCompanyFlowSetting());
    return response.then((value) => {
      value.Data = new CompanyFlowSettingResponse(value.Data);
      return value;
    });
  },
  getAccountCurrencyByUsername(
    username: string
  ): Promise<ApiResponse<IGetAccountCurrencyByUsernameResponse>> {
    const response = getResponse(
      apiCalling.callGetAccountCurrencyByUsername(username)
    );
    return response;
  },
  getAccountCurrencyByDomain(): Promise<
    ApiResponse<IGetAccountCurrencyByDomainResponse>
  > {
    const response = getResponse(apiCalling.callGetAccountCurrencyByDomain());
    return response;
  },
  getRegisterBankList(
    currency: string
  ): Promise<ApiResponse<IGetRegisterBankListResponse>> {
    const response = getResponse(apiCalling.callGetRegisterBankList(currency));
    return response;
  },
  registerAccountViaCashAgent(
    args: IRegisterRequest
  ): Promise<ApiResponse<GetRegisterResponse>> {
    const response = getResponse(
      apiCalling.registerAccountViaCashAgent(args)
    ).then((res: ApiResponse<GetRegisterResponse>) => {
      if (res.isSuccessful) {
        const authCookie = useCookie(KEYS.AUTH_COOKIE_KEY, {
          expires: new Date(commonHelper.getMinutesFromNow(25)),
        });
        authCookie.value = String(res.Data.Token);
        useCookie(KEYS.USER_CURRENCY).value = String(res.Data.Currency);
        sessionStorage.removeItem(KEYS.HAS_HOME_REDIRECT);
        appHelper.renewTokenRefresh();
      }
      return res;
    });
    return response;
  },
  registerAccount(
    args: IRegisterRequest
  ): Promise<ApiResponse<GetRegisterResponse>> {
    const response = getResponse(apiCalling.registerAccount(args)).then(
      (res: ApiResponse<GetRegisterResponse>) => {
        if (res.isSuccessful) {
          const authCookie = useCookie(KEYS.AUTH_COOKIE_KEY, {
            expires: new Date(commonHelper.getMinutesFromNow(25)),
          });
          authCookie.value = String(res.Data.Token);
          useCookie(KEYS.USER_CURRENCY).value = String(res.Data.Currency);
          sessionStorage.removeItem(KEYS.HAS_HOME_REDIRECT);
          appHelper.renewTokenRefresh();
        }
        return res;
      }
    );
    return response;
  },
  getOtpVerification(
    req: IGetOtpVerificaitonRequest
  ): Promise<ApiResponse<undefined>> {
    const response = apiCalling.callGetOtpVerification(req);
    return getResponse(response);
  },
  getGameProviderList(
    domain: string,
    currency: string,
    language = "en",
    isBsi = false
  ): Promise<ApiResponse<IGetGameProviderListResponse>> {
    const response = getResponse(
      apiCalling.callGetGameProviderList(domain, currency, language, isBsi)
    );
    return response.then((gameProviders: any) => {
      gameProviders.Data = new GameProviderList(gameProviders.Data);
      return gameProviders;
    });
  },
  getJackpotGameList(): Promise<ApiResponse<IGetGameProviderListResponse>> {
    const response = getResponse(apiCalling.callGetJackpotGameList());
    return response.then((gameLists: any) => {
      gameLists.Data = new GameProviderList(gameLists.Data);
      return gameLists;
    });
  },
  getHotGameList(): Promise<ApiResponse<IGetTopGameListResponse>> {
    const response = getResponse(apiCalling.callGetHotGameList());
    return response;
  },
  getNewGameList(): Promise<ApiResponse<IGetGameProviderListResponse>> {
    const response = getResponse(apiCalling.callGetNewGameList());
    return response.then((gameLists: any) => {
      gameLists.Data = new GameProviderList(gameLists.Data);
      return gameLists;
    });
  },
  getGameListByProvider(
    provider: string
  ): Promise<ApiResponse<IGetGameProviderListResponse>> {
    const response = getResponse(apiCalling.callGetGameProvider(provider));
    return response.then((gameLists: any) => {
      gameLists.Data = new GameProviderList(gameLists.Data);
      return gameLists;
    });
  },
  getGameListByGameType(
    gameType: string
  ): Promise<ApiResponse<IGetGameProviderListResponse>> {
    const response = getResponse(
      apiCalling.callGetGameListByGameType(gameType)
    );
    return response.then((gameLists: any) => {
      gameLists.Data = new GameProviderList(gameLists.Data);
      return gameLists;
    });
  },
  getGameTabProviderList(
    Domain: string,
    Currency = "",
    Language = "en",
    isMobileTheme = false
  ): Promise<ApiResponse<IGameTypeProviderList>> {
    const response = getResponse(
      apiCalling.callGetGameTabProviderList(
        Domain,
        Currency,
        Language,
        isMobileTheme
      )
    );
    return response;
  },
  getMenuTab(
    domain: string,
    lang: string,
    isAuth = false,
    token = ""
  ): Promise<ApiResponse<IGetMenuTabResponse>> {
    const response = getResponse(
      apiCalling.callGetMenuTab(domain, lang, isAuth, token)
    );
    return response;
  },
  getLanguageSetting(
    domain: string
  ): Promise<ApiResponse<IGetLanguageSettingResponse>> {
    const response = getResponse(apiCalling.callGetLanguagesSetting(domain));
    return response;
  },
  getLanguageSettingBsi(
    domain: string
  ): Promise<ApiResponse<IGetLanguageSettingResponse>> {
    const response = getResponse(apiCalling.callGetLanguagesSettingBsi(domain));
    return response;
  },
  getPlayerPreferenceForDisplay(): Promise<ApiResponse<IGetPlayerPreference>> {
    const response = getResponse(apiCalling.getPlayerPreferenceForDisplay());
    return response;
  },
  getMultiWallets(): Promise<ApiResponse<GetMultiWalletBalaceResponse>> {
    const response = getResponse(apiCalling.callGetMultiWallet());
    return response.then((value) => {
      value.Data = new GetMultiWalletBalaceResponse(value.Data);
      return value;
    });
  },
  getCheckHaveReferralPromotion(token: string): Promise<ApiResponse<null>> {
    const response = getResponse(
      apiCalling.callCheckHaveReferralPromotion(token)
    );
    return response;
  },
  // getBsiGameList(): Promise<ApiResponse<GetBsiGameListResponse>> {
  //   const response = getResponse(apiCalling.CallGetBsiGameList());
  //   return response.then((value) => {
  //     value.Data = new GetBsiGameListResponse(value.Data);
  //     return value;
  //   });
  // },
  getPlayableGame(
    currency: string,
    lang: string
  ): Promise<ApiResponse<GetBsiGameListResponse>> {
    const response = getResponse(
      apiCalling.callGetPlayableGame(currency, lang)
    );
    return response.then((value) => {
      value.Data = new GetBsiGameListResponse(value.Data);
      return value;
    });
  },
  getWebIdByDomainName(domain: string): Promise<ApiResponse<IWebId>> {
    const response = getResponse(apiCalling.callGetWebIdByDomainName(domain));
    return response;
  },
  getLogin(params: IRequestLogin): Promise<ApiResponse<GetLoginResponse>> {
    const response = apiCalling.callLogin(params);
    return getResponse(response).then(
      (value: ApiResponse<GetLoginResponse>) => {
        if (value.isSuccessful) {
          const authCookie = useCookie(KEYS.AUTH_COOKIE_KEY, {
            expires: new Date(commonHelper.getMinutesFromNow(25)),
          });
          authCookie.value = value.Data.Token;
          useCookie(KEYS.USER_CURRENCY).value = value.Data.Currency;
          sessionStorage.removeItem(KEYS.HAS_HOME_REDIRECT);
          appHelper.renewTokenRefresh();
          // cookieHelper.setCookie("atoken", value.Data.Token, 60);
        }
        value.Data = new GetLoginResponse(value.Data);
        return value;
      }
    );
  },
  getLogout(): void {
    apiCalling.logout();
  },
  getGameProviderLogin(
    GameId: number,
    EntranceLocation: string,
    TabSelected: string = ""
  ): Promise<ApiResponse<IGetGameProviderLoginResponse>> {
    const isMapping = ["sport", "sports"].includes(TabSelected) && store.state.locale === "pt_BR";
    const response = getResponse(
      apiCalling.callGameProviderLogin(GameId, EntranceLocation, useDevice().isDesktop, isMapping ? "pt_PT" : "")
    );
    return response;
  },
  getReferralV2GetPlayerRedeemHistory(
    params: IGetPlayerRedeemHistoryRequest
  ): Promise<ApiResponse<GetPlayerRedeemHistoryResponse>> {
    const response = getResponse(
      apiCalling.callReferralV2GetPlayerRedeemHistory(params)
    );
    return response.then((res) => {
      if (res.isSuccessful) {
        res.Data = new GetPlayerRedeemHistoryResponse(res.Data);
      } else {
        res.Data = new GetPlayerRedeemHistoryResponse({
          RedeemRequests: [],
        });
      }
      return res;
    });
  },
  getReferralV2GetGeneralInformation(): Promise<
    ApiResponse<ReferralV2GeneralInformation>
  > {
    const response = getResponse(
      apiCalling.callReferralV2GetGeneralInformation()
    );
    return response.then((res) => {
      if (res.isSuccessful) {
        res.Data = new ReferralV2GeneralInformation(res.Data);
      } else {
        const now = new Date();
        res.Data = new ReferralV2GeneralInformation({
          StartDate: now.toISOString(),
          EndDate: now.toISOString(),
          IsHaveReferralEvent: false,
          Content: "",
        });
      }
      return res;
    });
  },
  getReferralV2GetApplyRedeem(
    params: IGetApplyRedeemRequest
  ): Promise<ApiResponse<IBaseResponse>> {
    return getResponse(apiCalling.callReferralV2GetApplyRedeem(params));
  },
  getReferralV2PlayerRedeemRequest(): Promise<
    ApiResponse<GetPlayerRedeemRequest>
  > {
    const response = getResponse(
      apiCalling.callReferralV2GetPlayerRedeemRequest()
    );
    return response.then((res) => {
      if (res.isSuccessful) {
        res.Data = new GetPlayerRedeemRequest(res.Data);
      } else {
        res.Data = new GetPlayerRedeemRequest({
          CompanyPromotionId: 0,
          ReferralCount: 0,
          EffectiveReferralCount: 0,
          Redeemable: 0,
          ReferralMemberList: [],
        });
      }
      return res;
    });
  },
  getReferralLayerAmount(): Promise<
    ApiResponse<GetReferralLayerAmountResponse>
  > {
    const response = getResponse(apiCalling.callGetReferralLayerAmount());
    return response.then((res) => {
      res.Data = new GetReferralLayerAmountResponse(res.Data);
      return res;
    });
  },
  getRedeemReferralRequest(): Promise<ApiResponse<GetRedeemReferralResponse>> {
    const response = getResponse(apiCalling.callGetRedeemReferralRequest());
    return response.then((res) => {
      res.Data = new GetRedeemReferralResponse(res.Data);
      return res;
    });
  },
  getRedeemReferralAmountCallRedeemReferralAmount(
    request: IRedeemReferralAmountRequest
  ): Promise<ApiResponse<IGetRedeemReferralResponse>> {
    const response = getResponse(apiCalling.callRedeemReferralAmount(request));
    return response;
  },
  getReferralDiagram(
    request: IGetReferralDiagramRequest
  ): Promise<ApiResponse<IGetReferralDiagramResponse>> {
    const response = getResponse(apiCalling.callGetReferralDiagram(request));
    return response;
  },
  getStatement(
    args: IStatementRequest
  ): Promise<ApiResponse<IStatementResponse>> {
    const response = apiCalling.callGetStatement(args);
    return getResponse(response).then((value) => {
      value.Data = new StatementResponse(value.Data);
      return value;
    });
  },
  getWithdrawalFromGameProvider(): Promise<ApiResponse<null>> {
    const response = getResponse(
      apiCalling.callGetWithdrawalFromGameProvider()
    );
    return response;
  },
  getWhiteListedIpEnabled(
    IpAddress?: string,
    domain: string
  ): Promise<ApiResponse<IGetWhiteListedIps>> {
    const response = getResponse(
      apiCalling.callGetWhiteListedIpEnabled(IpAddress, domain)
    );
    return response;
  },
  getCurrencies(domain: string): Promise<ApiResponse<IGetCurrencies>> {
    const response = getResponse(apiCalling.callGetCurrencies(domain));
    return response;
  },
  getPlayerIsOnline(
    params: IRequestPlayerIsOnline
  ): Promise<ApiResponse<IBaseResponse>> {
    const response = getResponse(apiCalling.callGetPlayerIsOnline(params));
    return response;
  },
  getPromotions(
    params: IRequestPromotion
  ): Promise<ApiResponse<IGetPromotionResponse>> {
    const response = getResponse(apiCalling.callGetPromotions(params));
    return response.then((res) => {
      res.Data = new GetPromotionResponse(res.Data);
      return res;
    });
  },
  applyPromotion(
    params: IApplyPromotionRequest
  ): Promise<ApiResponse<IBaseResponsePromotion>> {
    const response = getResponse(apiCalling.callApplyPromotion(params));
    return response;
  },
  getLotteryBetList(
    params: IGetBetListRequest
  ): Promise<ApiResponse<IGetBetListResponse>> {
    const response = apiCalling.callGetLotteryBetList(params);
    return getResponse(response).then((value) => {
      value.Data = new GetBetListResponse(value.Data);
      return value;
    });
  },
  getCockfightProvider(): Promise<ApiResponse<IGameProviders>> {
    const response = getResponse(apiCalling.callGetCockfightProviders());
    return response;
  },
  getPokerProvider(): Promise<ApiResponse<IGameProviders>> {
    const response = getResponse(apiCalling.callGetPokerProviders());
    return response;
  },
  getLotteryProvider(): Promise<ApiResponse<IGameProviders>> {
    const response = getResponse(apiCalling.callGetLotteryProviders());
    return response;
  },

  getCreatePromotionDetails(): Promise<
    ApiResponse<IGetCreatePromotionDetailsResponse>
  > {
    const response = getResponse(apiCalling.callGetCreatePromotionDetails());
    return response;
  },

  createAgentCooperation(
    request: IFormAgentCooperation
  ): Promise<ApiResponse<IGetCreatePromotionDetailsResponse>> {
    const response = getResponse(apiCalling.callAgentCooperation(request));
    return response;
  },
  checkIsPlayerSiteUm(): Promise<ApiResponse<IBaseResponse>> {
    const response = getResponse(apiCalling.callCheckIsPlayerSite());
    return response;
  },
  recommendMatches(): Promise<ApiResponse<IRecommendMatchResponse>> {
    const response = getResponse(apiCalling.callRecommendMatches());
    return response.then((res) => {
      res.Data.RecommendMatches = res.Data.RecommendMatches.map(
        (item: IRecommendMatch) => new RecommendMatch(item)
      );
      return res;
    });
  },
  loginByMatchId(
    request: ILoginByMatchIdRequest
  ): Promise<ApiResponse<ILoginByMatchIdResponse>> {
    const response = getResponse(apiCalling.callLoginByMatchId(request));
    return response;
  },
  getBetList(request: IRequestBetList): Promise<ApiResponse<IResponseBetList>> {
    const response = getResponse(apiCalling.callPureLiteBetList(request));
    return response;
  },
  getResetPassword(
    request: IRequestResetPassword
  ): Promise<ApiResponse<INullDataResponse>> {
    const response = getResponse(apiCalling.callResetPassword(request));
    return response;
  },

  getPlayerBalance(): Promise<ApiResponse<GetBalanceResponse>> {
    const response = getResponse(apiCalling.callGetPlayerBalance());

    return response.then((value) => {
      return value;
    });
  },

  getVersion(): Promise<ApiResponse<IVersionResponse>> {
    const response = getResponse(apiCalling.callGetVersion());
    return response;
  },
  getPayoutQrCode(): Promise<ApiResponse<IQrCodePayout>> {
    const response = getResponse(apiCalling.callPayoutQrCode());
    return response;
  },
  getAllSportBets(
    req: IAllSportBetsRequest
  ): Promise<ApiResponse<AllSportBetsResponse>> {
    const response = getResponse(apiCalling.callScanQrCode(req));
    return response.then((value) => {
      value.Data = new AllSportBetsResponse(value.Data);
      return value;
    });
  },
  payTickets(req: IPayTicketsRequest): Promise<ApiResponse<IRedeemResponse>> {
    const response = getResponse(apiCalling.callPayTickets(req));
    return response;
  },
  redeemTicket(req: IRedeemRequest): Promise<ApiResponse<IRedeemResponse>> {
    const response = getResponse(apiCalling.callRedeemTicket(req));
    return response;
  },
  getPreviousQR(req: IPreviousQRReq): Promise<ApiResponse<IPreviousQR>> {
    const response = getResponse(apiCalling.callGetPreviousQR(req));
    return response;
  },
  getAllCustomerSettings(
    domain: string
  ): Promise<ApiResponse<IAllCustomerSettingResponse>> {
    const response = getResponse(apiCalling.callGetAllCustomerSettings(domain));
    return response;
  },
  loginToSportApi(
    req: ILoginToSportsRequest
  ): Promise<ApiResponse<ILoginToSportsRespone>> {
    const response = apiCalling.loginToSportApi(req);
    return getResponse(response);
  },
  getPlayerSearchGame(
    payload: IPlayerSearchGameRequest
  ): Promise<ApiResponse<undefined>> {
    const response = apiCalling.callPlayerSearchGame(payload);
    return getResponse(response);
  },
  updateCustomerInformation(
    payload: ICustomerInfoRequest
  ): Promise<ApiResponse<undefined>> {
    const response = apiCalling.callUpdateCustomerInfo(payload);
    return getResponse(response);
  },
  loginTelgram(payload: ILoginTelegram): Promise<ApiResponse<string>> {
    const response = apiCalling.callLoginTelegram(payload);
    return getResponse(response);
  },
  getCompanyThemeOptions(
    webId: number
  ): Promise<ApiResponse<ICompanyThemeOption[]>> {
    const response = apiCalling.callCompanyThemeOptions(webId);
    return getResponse(response);
  },
  getTelegramBotInformation(url: string) {
    return apiCalling.getTelegramBotInformation(url);
  },
  updateTelegramSsoToken(
    req: IUpdateTelegramSsoTokenRequest
  ): Promise<ApiResponse<IUpdateTelegramSsoTokenResponse>> {
    const response = apiCalling.callUpdateTelegramSsoToken(req);
    return getResponse(response);
  },
  getMemberMessage(
    req: IGetSiteMessageRequest
  ): Promise<ApiResponse<IGetMessageResponse>> {
    const response = apiCalling.callGetSiteMessages(req);
    return getResponse(response);
  },
  updateMemberMessage(
    req: IUpdateMessageStatus
  ): Promise<ApiResponse<IBaseResponse>> {
    const response = apiCalling.callUpdateSiteMessages(req);
    return getResponse(response);
  },
  getLuckyWheelOptions(
    req: IGetLuckyWheelOptionsRequest
  ): Promise<ApiResponse<IGetLuckyWheelOptionsResponse>> {
    const response = apiCalling.callGetLuckyWheelOptions(req);
    return getResponse(response);
  },
  getLuckyWheelResult(
    req: IGetLuckyWheelSpinResultRequest
  ): Promise<ApiResponse<IGetLuckyWheelSpinResultResponse>> {
    const response = apiCalling.callGetLuckyWheelResult(req);
    return getResponse(response);
  },
  getAvailableLuckyWheelTicket(): Promise<
    ApiResponse<IGetAvailableLuckyWheelTicketsResponse>
  > {
    const response = apiCalling.callGetAvailableLuckyWheelTicket();
    return getResponse(response);
  },
};
