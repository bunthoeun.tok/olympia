import {
  ICompanyThemeOption,
  GetThemePropertiesRequestDto,
} from "../models/companyThemePropertiesRespone";
import {
  IUpdateTelegramSsoTokenRequest,
  IUpdateTelegramSsoTokenResponse,
} from "../models/UpdateTelegramSsoToken";
import {
  IGetGameProviderListResponse,
  IGetTopGameListResponse,
} from "@/models/IGetPlayableGamesResponse";
import { IRequestResetPassword } from "@/models/auth/resetPassword";
import {
  IGetRegisterBankListRequest,
  IGetRegisterBankListResponse,
} from "@/models/bank";
import { IBaseResponse, INullDataResponse } from "@/models/baseResponse";
import {
  IGameTypeProviderList,
  IGetBsiGameListResponse,
  IGetMenuTabResponse,
} from "@/models/bsiGameList";
import { IGetLanguageSettingResponse } from "@/models/common/language";
import { IGetMultiWalletBalaceResponse } from "@/models/common/wallet";
import {
  ICompanyFlowSettingRequest,
  ICompanyFlowSettingResponse,
} from "@/models/companyFlowSetting";
import {
  IGetAccountCurrencyByUsernameRequest,
  IGetAccountCurrencyByUsernameResponse,
  IGetCurrencyWhiteListResponse,
} from "@/models/currencyWhiteList";
import EnumDisplayTimeOption from "@/models/enums/enumDisplayTimeOption";
import { IFeaturesToggleRespone } from "@/models/featuresToggleRespone";
import { IGameProviders } from "@/models/gameProvider";
import {
  IGetGameProviderLoginResponse,
  IRequestGameProviderLogin,
} from "@/models/gameProviderLogin";
import IGetGeography from "@/models/geoIpResponse";
import { GetBalanceResponse } from "@/models/getBalanceResponse";
import IGetBetListRequest from "@/models/getBetListRequest";
import { IGetBetListResponse } from "@/models/getBetListResponse";
import IGetBetPayloadRequest from "@/models/getBetPayloadRequest";
import IGetBetPayloadResponse from "@/models/getBetPayloadResponse";
import {
  IAllCustomerSettingResponse,
  ICustomerSettingResponse,
} from "@/models/getCustomerSettingResponse";
import {
  IGetManualCryptoCurrencyTransactionResponse,
  IRequestManualCryptoCurrencyDeposit,
} from "@/models/getManualCryptoCurrencyTransactionResponse";
import { IGetPaymentGatewayBanksResponse } from "@/models/getPaymentGatewayBanksRespone";
import IGetPlayerPreference from "@/models/getPlayerPreference";
import IGetPreferenceForSettingsResponse from "@/models/getPreferenceForSettingsResponse";
import { IGetProfileResponse } from "@/models/getProfileResponse";
import { IGetTransactionBankListResponse } from "@/models/getTransactionBankListResponse";
import { IGetTransactionFeeResponse } from "@/models/getTransactionFeeResponse";
import {
  GetTransactionHistoryRequest,
  GetTransactionHistoryResponse,
  IProceedTransactionRequest,
} from "@/models/getTransactionHistoryResponse";
import {
  GetLoginResponse,
  IGetCurrencies,
  IGetWhiteListedIps,
  IRequestLogin,
  IRequestPlayerIsOnline,
} from "@/models/login";
import IGetPromotionApplyListRequest from "@/models/promotion/getPromotionApplyListRequest";
import IGetPromotionApplyListResponse from "@/models/promotion/getPromotionApplyListResponse";
import {
  IGetPromotionResponse,
  IPromotionApplyReqeust,
  IRequestPromotion,
} from "@/models/promotion/promotion";
import {
  IGetRedeemReferralResponse,
  IGetReferralDiagramRequest,
  IGetReferralDiagramResponse,
  IGetReferralLayerAmountResponse,
  IRedeemReferralAmountRequest,
} from "@/models/referral/getReferral";
import {
  IRegisterRequest,
  IRegisterResponse,
  IGetOtpVerificaitonRequest,
  IGetAccountCurrencyByDomainResponse,
} from "@/models/register";
import { IRenewTokenResponse } from "@/models/renewTokenResponse";
import {
  IRequestAutoDepositBankTransfer,
  IRequestDepositBankTransfer,
  IRequestDepositInternetBank,
} from "@/models/requestDepositBanks";
import { IRequestTransactionFee } from "@/models/requestTransactionFee";
import {
  IRequestSetPaymentPassword,
  IRequestWithdrawalBankTransfer,
  IRequestWithdrawalInternetBank,
} from "@/models/requestWithdrawalBankTransfer";
import IGetSportBetRequest from "@/models/sportbet/getSportBetRequest";
import { IGetSportBetResponse } from "@/models/sportbet/getSportBetResponse";
import { IStatementRequest, IStatementResponse } from "@/models/statement";
import {
  IRequestBetList,
  IResponseBetList,
} from "@/models/theme/lite/PureLite";
import {
  ILoginByMatchIdRequest,
  ILoginByMatchIdResponse,
  IRecommendMatchResponse,
} from "@/models/theme/popular_match/popularMatch";
import { IWebId } from "@/models/webId";
import { store } from "@/store";
import commonHelper from "@/utils/commonHelper";
import { IFormAgentCooperation } from "~~/models/agentCooperation";
import { IApiResponse } from "~~/models/apiResponse";
import { IVersionResponse } from "~~/models/imageVersion";
import {
  IAllSportBetsRequest,
  IAllSportBetsResponse,
} from "~~/models/sportqr/redeemSingleBetDetail";
import {
  IPayTicketsRequest,
  IRedeemRequest,
  IRedeemResponse,
} from "~~/models/sportqr/redeemTicket";
import { IPreviousQR } from "~~/models/sportqr/previousQR";
import { IPreviousQRReq } from "~~/models/sportqr/previousQRReq";
import {
  IGetApplyRedeemRequest,
  IGetGeneralInformationResponse,
  IGetPlayerRedeemHistoryRequest,
  IGetPlayerRedeemHistoryResponse,
  IGetPlayerRedeemRequest,
} from "~~/models/referral/referralV2";
import {
  ILoginToSportsRequest,
  ILoginToSportsRespone,
} from "~~/sports/models/loginToSport";
// import FakeAPI from "./FakeAPI";
import { IApplyPromotionRequest } from "~~/models/promotion/promotionApplyList";
import { IQrCodePayout } from "~~/models/sportqr/qrCodePayout";
import { IPlayerSearchGameRequest } from "~~/models/playerSearchGame";
import { ICustomerInfoRequest } from "~~/models/telegramCustomerInfo";
import { ILoginTelegram } from "~~/models/telegramLogin";
import {
  IGetMessageResponse,
  IGetSiteMessageRequest,
  IUpdateMessageStatus,
} from "~~/models/message";
import {
  IGetLuckyWheelOptionsRequest,
  IGetLuckyWheelOptionsResponse,
  IGetLuckyWheelSpinResultRequest,
  IGetLuckyWheelSpinResultResponse,
  IGetAvailableLuckyWheelTickets,
  IGetAvailableLuckyWheelTicketsResponse,
} from "../models/lucky-wheel/luckyWheel";
import { IUpdatePlayerPreferenceRequest } from "~~/models/account/preference";

export type FetchParams<T = any> = T;

const removeDuplicateSlash = (url: string) =>
  url.replace(/(https?:\/\/.*?)\/{2,}/, (_, group) => group + "/");

export default {
  getEndPoint(): string {
    if (process.dev) {
      const config = useRuntimeConfig();
      return config.public.END_POINT;
    }
    if (process.server) {
      return "http://localhost:10000/api";
    } else {
      const endpoint = String(useState(KEYS.SERVER_NAME).value);
      return endpoint;
    }
  },
  bsiGet<T = any>(url: string) {
    const endpoint = this.getEndPoint();

    return $fetch<T>(removeDuplicateSlash(endpoint + url), {
      headers: {
        "Accept-Encoding": "*",
      },
    });
  },
  bsiPost<T = any>(url: string, params: FetchParams = {}) {
    const endpoint = this.getEndPoint();

    return $fetch<T>(removeDuplicateSlash(endpoint + url), {
      method: "post",
      headers: {
        "Accept-Encoding": "*",
      },
      body: JSON.stringify(params),
    });
  },
  getOutSider<T = any>(url: string) {
    return $fetch<T>(url, {
      method: "get",
      headers: {
        "Accept-Encoding": "*",
      },
      onRequest: async (_) => {},
    });
  },
  get<T = any>(url: string, manualToken = "") {
    let token = () => useCookie(KEYS.AUTH_COOKIE_KEY).value;
    if (manualToken && manualToken.length > 0) {
      token = () => manualToken;
    }
    const endpoint = this.getEndPoint();
    return $fetch<T>(removeDuplicateSlash(endpoint + url), {
      method: "get",
      headers: {
        "Accept-Encoding": "*",
        Authorization: `Bearer ${token()}`,
        // Authorization: `Bearer ${() => useCookie(KEYS.AUTH_COOKIE_KEY).value}`,
      },
      onRequest: async (_) => {},
      onResponse: (ctx) => {
        if (ctx.response.status === 403) {
          this.logout();
        }
      },
    });
  },
  post<T = any>(url: string, params: FetchParams = {}, manualToken = "") {
    let token = () => useCookie(KEYS.AUTH_COOKIE_KEY).value;
    if (manualToken && manualToken.length > 0) {
      token = () => manualToken;
    }
    const endpoint = this.getEndPoint();
    return $fetch<T>(removeDuplicateSlash(endpoint + url), {
      method: "post",
      headers: {
        "Accept-Encoding": "*",
        Authorization: `Bearer ${token()}`,
      },
      body: JSON.stringify(params),
      onRequest: async (_) => {},
      onResponse: (ctx) => {
        if (ctx.response.status === 403) {
          this.logout();
        }
      },
    });
  },

  logout() {
    if (process.server) return;
    cookieHelper.removeCookie(KEYS.AUTH_COOKIE_KEY);
    cookieHelper.removeCookie(KEYS.WEB_VIEW_AUTH_COOKIE_KEY);
    cookieHelper.removeCookie(KEYS.USER_CURRENCY);
    sessionStorage.removeItem(KEYS.HAS_HOME_REDIRECT);
    appHelper.clearExpiryTime();
    appHelper.clearTokenRefresh();
    localStorage.removeItem("vuex");
    const onlineId = Number(
      localStorage.getItem("onlineId") ?? localStorage.getItem("betlisttokenid")
    );
    if (onlineId) {
      this.callLogout(onlineId).then(() => {
        localStorage.removeItem("onlineId");
        if (!store.state.selectedThemeName.includes("peru"))
          localStorage.removeItem("betlisttokenid");
        window.location.href = "/";
      });
    } else {
      localStorage.removeItem("onlineId");
      window.location.href = "/";
    }
  },
  callGetGeography(): Promise<IApiResponse<IGetGeography>> {
    return this.bsiGet("/player/v2/get-geography");
  },
  callGetGeographyByIp(Ip: string): Promise<IApiResponse<IGetGeography>> {
    return this.bsiPost("/player/v2/get-geography-by-ip", {
      Ip,
    });
  },
  callGetProfileInformation(): Promise<IApiResponse<IGetProfileResponse>> {
    return this.post("/player/v2/get-profile", {});
  },
  callGetPlayerBalance(): Promise<IApiResponse<GetBalanceResponse>> {
    return this.post("/player/v2/get-player-balance", {});
  },
  callGetAllWalletsBalance(): Promise<IApiResponse<GetBalanceResponse>> {
    return this.post("/player/v2/get-all-balance", {});
  },
  callChangePassword(
    oldPassword: string,
    newPassword: string,
    onlineId: number
  ): Promise<IApiResponse<IBaseResponse>> {
    return this.post("/player/v2/update-password", {
      OldPassword: oldPassword,
      NewPassword: newPassword,
      OnlineId: onlineId,
    });
  },
  callChangePaymentPassword(
    oldPassword: string,
    newPassword: string,
    onlineId: number
  ): Promise<IApiResponse<IBaseResponse>> {
    return this.post("/player/v2/payment/update-password", {
      OldPassword: oldPassword,
      NewPassword: newPassword,
      OnlineId: onlineId,
    });
  },
  callChangeLoginName(name: string): Promise<IApiResponse<IBaseResponse>> {
    return this.post("/common/v2/update-login-name", {
      NewLoginName: name,
    });
  },
  callGetPreferenceSetting(): Promise<
    IApiResponse<IGetPreferenceForSettingsResponse>
  > {
    return this.post("/player/v2/get-preference", {});
  },
  callUpdatePlayerPreference(
    UpdatePlayerPreferenceRequest: IUpdatePlayerPreferenceRequest
  ): Promise<IApiResponse<IBaseResponse>> {
    return this.post(
      "/player/v2/update-preference",
      UpdatePlayerPreferenceRequest
    );
  },
  callRenewToken(): Promise<IApiResponse<IRenewTokenResponse>> {
    return this.post("/common/v2/renew-token", {
      Token: useCookie(KEYS.AUTH_COOKIE_KEY).value,
    });
  },
  callGetTransactionBankList(
    RequestPageType: string
  ): Promise<IApiResponse<IGetTransactionBankListResponse>> {
    return this.post("/player/v2/data/get-bank-list-for-transaction", {
      Language: store.state.locale,
      RequestPageType,
      Domain: commonHelper.getDomain(),
    });
  },
  callgetPaymentGatewayBanks(): Promise<
    IApiResponse<IGetPaymentGatewayBanksResponse>
  > {
    return this.post("/payment-gateway/v2/request-banks", {});
  },
  callProceedDepositBankTransfer(
    params: IRequestDepositBankTransfer
  ): Promise<IApiResponse<IBaseResponse>> {
    return this.post("/player/v2/transaction/raise-deposit", {
      ...params,
      Language: store.state.locale,
    });
  },
  callProceedDepositInternetBank(
    params: IRequestDepositInternetBank
  ): Promise<IApiResponse<IBaseResponse>> {
    return this.post("/payment-gateway/v2/raise-deposit", params);
  },
  callProceedAutoDepositBankTransfer(
    request: IRequestAutoDepositBankTransfer
  ): Promise<IApiResponse<IBaseResponse>> {
    return this.post("/player/v2/transaction/raise-deposit", {
      ...request,
      Language: store.state.locale,
    });
  },
  callUploadFile(params: FormData): Promise<IApiResponse<IBaseResponse>> {
    const token = () => useCookie(KEYS.AUTH_COOKIE_KEY).value;
    const endPoint = this.getEndPoint();

    return fetch(removeDuplicateSlash(endPoint + "/common/v2/upload-file"), {
      method: "POST",
      mode: "cors",
      headers: {
        Authorization: `Bearer ${token()}`,
      },
      body: params,
    })
      .then(async (d) => await d.json())
      .then((d) => d);
    // return this.post("/common/v2/upload-file", params);
  },
  callGetFeaturesToggle(): Promise<IApiResponse<IFeaturesToggleRespone>> {
    return this.post("/common/v2/get-all-feature-toggle", {});
  },
  callWithdrawalBankTransfer(
    params: IRequestWithdrawalBankTransfer
  ): Promise<IApiResponse<IBaseResponse>> {
    return this.post("/player/v2/transaction/request-withdrawal", params);
  },
  callWithdrawalInternetBank(
    params: IRequestWithdrawalInternetBank
  ): Promise<IApiResponse<IBaseResponse>> {
    return this.post("/payment-gateway/v2/raise-withdrawal", params);
  },
  callGetTransactionFee(
    params: IRequestTransactionFee
  ): Promise<IApiResponse<IGetTransactionFeeResponse>> {
    return this.post(
      "/payment-gateway/v2/payment-provider/get-gcx-withdrawal-fee",
      params
    );
  },
  callSetPaymentPassword(
    params: IRequestSetPaymentPassword
  ): Promise<IApiResponse<IBaseResponse>> {
    return this.post("/player/v2/set-payment-password", params);
  },
  callGetTransactionHistory(
    params: GetTransactionHistoryRequest
  ): Promise<IApiResponse<GetTransactionHistoryResponse>> {
    return this.post("/player/v2/transaction/get-player-transactions", params);
    // return FakeAPI.getTransactionHistory(params);
  },
  callProceedTransaction(
    params: IProceedTransactionRequest
  ): Promise<IApiResponse<INullDataResponse>> {
    return this.post("/common/v2/transaction/proceed-transaction", params);
    // return FakeAPI.proceedTransaction(params);
  },
  callGetCustomerSetting(
    RequestId: string
  ): Promise<IApiResponse<ICustomerSettingResponse>> {
    return this.bsiPost("/appsettings/player/v2/get-customer-settings", {
      Id: RequestId,
      Domain: commonHelper.getDomain(),
    });
  },
  callGetAllCustomerSettings(
    domain: string
  ): Promise<IApiResponse<IAllCustomerSettingResponse>> {
    return this.bsiPost(
      "/appsettings/player/v2/get-all-customer-settings-by-webId",
      {
        Domain: domain,
      }
    );
  },
  callGetManualCryptoCurrencyTransaction(
    RequestPageType: string
  ): Promise<IApiResponse<IGetManualCryptoCurrencyTransactionResponse>> {
    return this.post(
      "/player/v2/data/get-player-manual-cryptocurrency-transaction",
      {
        RequestPageType,
        Domain: commonHelper.getDomain(),
      }
    );
  },
  callProceedManualCryptoCurrencyDeposit(
    params: IRequestManualCryptoCurrencyDeposit
  ): Promise<IApiResponse<IBaseResponse>> {
    return this.post(
      "/player/v2/transaction/raise-manual-cryptocurrency-deposit",
      params
    );
  },
  callGetThemeProperties(
    params: GetThemePropertiesRequestDto
  ): Promise<IApiResponse<IBaseResponse>> {
    return this.bsiPost("/theme/v2/get-theme-properties", params);
    // return FakeAPI.getBMD1ThemeProperty(params);
  },
  callGetPromotionApplyList(
    params: IGetPromotionApplyListRequest
  ): Promise<IApiResponse<IGetPromotionApplyListResponse>> {
    return this.post("/common/v2/promotion/get-apply-list", params);
    // return FakeAPI.getPromotionApplyList(params);
  },
  callGetGameBetList(
    params: IGetBetListRequest
  ): Promise<IApiResponse<IGetBetListResponse>> {
    return this.post("/game-provider/v2/get-games-bet-list", params);
    // return FakeAPI.getGameBetList(params);
  },
  callGetCasinoBetList(
    params: IGetBetListRequest
  ): Promise<IApiResponse<IGetBetListResponse>> {
    return this.post("/game-provider/v2/get-casino-bet-list", params);
    // return FakeAPI.getCasinoBetList(params);
  },
  callGetCockfightingBetList(
    params: IGetBetListRequest
  ): Promise<IApiResponse<IGetBetListResponse>> {
    return this.post("/game-provider/v2/get-cock-fighting-bet-list", params);
    // return FakeAPI.getCockfightingBetList(params);
  },
  callGetPokerBetList(
    params: IGetBetListRequest
  ): Promise<IApiResponse<IGetBetListResponse>> {
    return this.post("/game-provider/v2/get-poker-bet-list", params);
    // return FakeAPI.getCockfightingBetList(params);
  },
  callGetSportsBetList(
    params: IGetSportBetRequest
  ): Promise<IApiResponse<IGetSportBetResponse>> {
    return this.post("/game-provider/v2/get-sports-bet-list", params);
    // return FakeAPI.getSportsBetList(params);
  },
  callGetVirtualSportsBetList(
    params: IGetSportBetRequest
  ): Promise<IApiResponse<IGetSportBetResponse>> {
    return this.post("/game-provider/v2/get-virtual-sports-bet-list", params);
    // return FakeAPI.getVirtualSportsBetList(params);
  },
  callGetGamesProviders(): Promise<IApiResponse<IGameProviders>> {
    return this.post("/game-provider/v2/get-games-providers", {});
    // return FakeAPI.getGamesProviders();
  },
  callGetCasinoProviders(): Promise<IApiResponse<IGameProviders>> {
    return this.post("/game-provider/v2/get-casino-providers", {});
    // return FakeAPI.getCasinoProviders();
  },
  callGetSportProvider(): Promise<IApiResponse<IGameProviders>> {
    return this.post("/game-provider/v2/get-sport-providers", {});
    // return FakeAPI.getSportProvider();
  },
  callGetVirtualSportProvider(): Promise<IApiResponse<IGameProviders>> {
    return this.post("/game-provider/v2/get-virtual-sport-providers", {});
    // return FakeAPI.getVirtualSportProvider();
  },
  callGetBetPayload(
    params: IGetBetPayloadRequest
  ): Promise<IApiResponse<IGetBetPayloadResponse>> {
    return this.post("/game-provider/v2/bet-payload", params);
    // return FakeAPI.getBetPayLoad(params);
  },
  callGetCurrencyWhiteList(): Promise<
    IApiResponse<IGetCurrencyWhiteListResponse>
  > {
    return this.bsiPost("/player/get-currency-white-list-by-domain", {
      Domain: commonHelper.getDomain(),
    });
    // return FakeAPI.getCurrencyWhiteList();
  },
  callGetCompanyFlowSetting(): Promise<
    IApiResponse<ICompanyFlowSettingResponse>
  > {
    const request: ICompanyFlowSettingRequest = {
      Language: store.state.locale,
      Type: "RegisterPage",
      Domain: commonHelper.getDomain(),
      IsSettingForApp: false,
    };
    return this.bsiPost("/player/v2/get-company-flow-setting", request);
    // return FakeAPI.getCompanyFlowSetting(args);
  },
  callGetAccountCurrencyByUsername(
    username: string
  ): Promise<IApiResponse<IGetAccountCurrencyByUsernameResponse>> {
    const arg: IGetAccountCurrencyByUsernameRequest = {
      Domain: commonHelper.getDomain(),
      Username: username,
    };
    return this.bsiPost("/cash-agent/v2/get-account-currency-by-username", arg);
    // return FakeAPI.getAccountCurrencyByUsername(args);
  },
  callGetRegisterBankList(
    currency = ""
  ): Promise<IApiResponse<IGetRegisterBankListResponse>> {
    const request: IGetRegisterBankListRequest = {
      Domain: commonHelper.getDomain(),
      Currency: currency ?? "",
    };
    return this.bsiPost("/player/v2/get-register-bank-list", request);
    // return FakeAPI.getRegisterBankList(args);
  },
  registerAccountViaCashAgent(
    args: IRegisterRequest
  ): Promise<IApiResponse<IRegisterResponse>> {
    return this.bsiPost("/cash-agent/v2/create-player", args);
    // return FakeAPI.register(args);
  },
  registerAccount(
    params: IRegisterRequest
  ): Promise<IApiResponse<IRegisterResponse>> {
    return this.bsiPost("/player/v2/register/join-now", params);
    // return FakeAPI.register(params);
  },
  callGetAccountCurrencyByDomain(): Promise<
    IApiResponse<IGetAccountCurrencyByDomainResponse>
  > {
    return this.bsiPost("/cash-agent/v2/get-account-currency-by-domain", {
      Domain: commonHelper.getDomain(),
    });
  },
  callGetGameProviderList(
    Domain: string,
    Currency = "",
    Language = "en",
    IsBsi = false
  ): Promise<IApiResponse<IGetGameProviderListResponse>> {
    return this.bsiPost("/player/v2/get-game-provider-list", {
      Domain,
      Currency: Currency ?? "",
      Language,
      IsBsi,
    });
  },
  callGetJackpotGameList(): Promise<
    IApiResponse<IGetGameProviderListResponse>
  > {
    return this.bsiPost("/player/get-special-selection-game-list", {
      Domain: commonHelper.getDomain(),
      Currency: store.state.currency,
      Lang: store.state.locale,
      IsBsi: !store.state.auth,
    });
  },
  callGetHotGameList(): Promise<IApiResponse<IGetTopGameListResponse>> {
    return this.bsiPost("/player/v2/get-hot-game-list", {
      Domain: commonHelper.getDomain(),
      Currency: store.state.currency,
      Lang: store.state.locale,
      IsBsi: !store.state.auth,
    });
  },
  callGetNewGameList(): Promise<IApiResponse<IGetGameProviderListResponse>> {
    return this.bsiPost("/player/v2/get-new-game-list", {
      Domain: commonHelper.getDomain(),
      Currency: store.state.currency,
      Lang: store.state.locale,
      IsBsi: !store.state.auth,
    });
  },
  callGetGameProvider(
    provider: string
  ): Promise<IApiResponse<IGetGameProviderListResponse>> {
    return this.bsiPost("/player/v2/get-game-list-by-provider", {
      Domain: commonHelper.getDomain(),
      Provider: provider,
      Currency: store.state.currency,
      Lang: store.state.locale,
      IsBsi: !store.state.auth,
    });
  },
  callGetGameListByGameType(
    gameType: string
  ): Promise<IApiResponse<IGetGameProviderListResponse>> {
    return this.bsiPost("/player/v2/get-game-list-by-game-type", {
      Domain: commonHelper.getDomain(),
      Category: gameType,
      Currency: store.state.currency,
      Lang: store.state.locale,
      IsBsi: !store.state.auth,
    });
  },
  callGetGameTabProviderList(
    Domain: string,
    Currency = "",
    Language = "en",
    isMobileTheme: boolean
  ): Promise<IApiResponse<IGameTypeProviderList>> {
    return this.bsiPost("/player/v2/get-game-tab-provider-list", {
      Domain,
      Currency,
      Lang: Language,
      IsFromMobile: isMobileTheme,
    });
  },
  callGetMenuTab(
    Domain: string,
    Lang: string,
    isAuth = false,
    token = ""
  ): Promise<IApiResponse<IGetMenuTabResponse>> {
    const request = {
      Domain,
      Lang,
    };
    let response;
    if (isAuth) {
      response = this.post("/player/v2/get-player-game-tab", request, token);
    } else {
      response = this.bsiPost("/player/v2/get-bsi-game-tab", request);
    }
    return response;
  },
  callGetLanguagesSetting(
    domain: string
  ): Promise<IApiResponse<IGetLanguageSettingResponse>> {
    return this.bsiPost("/appsettings/v2/get-language-setting", {
      Domain: domain,
    });
  },
  callGetLanguagesSettingBsi(
    domain: string
  ): Promise<IApiResponse<IGetLanguageSettingResponse>> {
    return this.bsiPost("/appsettings/player/v2/get-language-setting", {
      Domain: domain,
    });
  },
  callGetMultiWallet(): Promise<IApiResponse<IGetMultiWalletBalaceResponse>> {
    return this.post("/player/v2/get-all-wallet-balance", {});
  },
  callCheckHaveReferralPromotion(token: string): Promise<IApiResponse<null>> {
    return this.post("/player/v2/check-referral-event", {}, token);
  },
  getPlayerPreferenceForDisplay(): Promise<IApiResponse<IGetPlayerPreference>> {
    return this.bsiPost("/theme/v2/get-preference-for-display", {
      Username: store.state.username,
      Language: store.state.locale,
      Domain: commonHelper.getDomain(),
    });
  },
  callGetPlayableGame(
    Currency = "",
    Lang: string
  ): Promise<IApiResponse<IGetBsiGameListResponse>> {
    return this.bsiPost("/player/v2/get-playable-games", {
      Domain: commonHelper.getDomain(),
      Currency: Currency ?? "",
      Lang,
    });
  },
  callGetWebIdByDomainName(domain: string): Promise<IApiResponse<IWebId>> {
    return this.bsiPost("/player/v2/get-webId-by-domain", {
      Domain: domain,
    });
  },
  callLogin(params: IRequestLogin): Promise<IApiResponse<GetLoginResponse>> {
    return this.bsiPost("/player/v2/login", params);
  },
  callGameProviderLogin(
    GameId: number,
    EntranceLocation: string,
    isSelectDesktopView = false,
    specifyLanguage: string = ""
  ): Promise<IApiResponse<IGetGameProviderLoginResponse>> {
    const specifySelected = specifyLanguage === "" ? store.state.locale : specifyLanguage;
    const params: IRequestGameProviderLogin = {
      GameId,
      Ip: store.state.geography?.Ip,
      FromUrl: window.location.origin,
      IsFromMobile: Boolean(!useDevice().isDesktop && !isSelectDesktopView ? 1 : 0),
      EntranceLocation,
      Lang: specifySelected,
      OnlineId: Number(localStorage.getItem("onlineId") ?? 0),
    };
    return this.post("/game-provider/v2/player-login", params);
  },
  callReferralV2GetPlayerRedeemHistory(
    params: IGetPlayerRedeemHistoryRequest
  ): Promise<IApiResponse<IGetPlayerRedeemHistoryResponse>> {
    return this.post("/referral/v2/player/get-redeem-history", params);
  },
  callReferralV2GetGeneralInformation(): Promise<
    IApiResponse<IGetGeneralInformationResponse>
  > {
    if (store.state.auth) {
      return this.post("/referral/v2/player/get-referral-info", {
        Currency: store.state.currency,
        Language: store?.state?.locale ?? "en",
        Domain: commonHelper.getDomain(),
      });
    }
    return this.post("/referral/player/get-referral-info", {
      Currency: "",
      Language: store?.state?.locale ?? "en",
      Domain: commonHelper.getDomain(),
    });
  },
  callReferralV2GetApplyRedeem(
    params: IGetApplyRedeemRequest
  ): Promise<IApiResponse<IBaseResponse>> {
    return this.post("/referral/v2/player/redeem", params);
  },
  callReferralV2GetPlayerRedeemRequest(): Promise<
    IApiResponse<IGetPlayerRedeemRequest>
  > {
    return this.post("/referral/v2/player/get-redeemable-info", {});
  },
  callGetReferralLayerAmount(): Promise<
    IApiResponse<IGetReferralLayerAmountResponse>
  > {
    return this.post("/player/v2/referral/get-layer-amount", {});
  },
  callGetRedeemReferralRequest(): Promise<
    IApiResponse<IGetRedeemReferralResponse>
  > {
    return this.post("/player/v2/referral/get-redeem-requests", {});
  },
  callRedeemReferralAmount(
    request: IRedeemReferralAmountRequest
  ): Promise<IApiResponse<IGetRedeemReferralResponse>> {
    return this.post("/player/v2/referral/raise-redeem", request);
  },
  callGetReferralDiagram(
    request: IGetReferralDiagramRequest
  ): Promise<IApiResponse<IGetReferralDiagramResponse>> {
    return this.post("/player/v2/referral/get-diagram", request);
  },
  callGetStatement(
    args: IStatementRequest
  ): Promise<IApiResponse<IStatementResponse>> {
    return this.post("/player/v2/statement/get-winlose", args);
  },
  callGetWithdrawalFromGameProvider(): Promise<IApiResponse<null>> {
    return this.post("/game-provider/v2/withdrawal-all", {});
  },
  callGetWhiteListedIpEnabled(
    IpAddress = store.state.geography?.Ip,
    domain: string
  ): Promise<IApiResponse<IGetWhiteListedIps>> {
    const payload = {
      Domain: domain,
      IpAddress,
    };
    return this.bsiPost("/common/v2/restriction/is-whitelisted-ip", payload);
  },
  callGetCurrencies(domain: string): Promise<IApiResponse<IGetCurrencies>> {
    return this.bsiPost("/player/v2/get-player-currency-list", {
      Domain: domain,
    });
  },
  callGetPlayerIsOnline(
    params: IRequestPlayerIsOnline
  ): Promise<IApiResponse<IBaseResponse>> {
    return this.post("/player/v2/is-online", params);
  },
  callGetPromotions(
    params: IRequestPromotion
  ): Promise<IApiResponse<IGetPromotionResponse>> {
    return this.bsiPost("/player/v2/get-promotions", {
      ...params,
      Status: "",
      Language: store.state.locale,
      IsBSI: !store.state.auth,
    });
  },
  callApplyPromotion(
    params: IApplyPromotionRequest
  ): Promise<IApiResponse<IBaseResponse>> {
    const request: IPromotionApplyReqeust = {
      ...params,
      Ip: store.state.geography?.Ip,
    };
    return this.post("/player/v2/apply-promotion", request);
  },
  callGetLotteryBetList(
    params: IGetBetListRequest
  ): Promise<IApiResponse<IGetBetListResponse>> {
    return this.post("/game-provider/v2/get-lottery-bet-list", params);
  },
  callGetCockfightProviders(): Promise<IApiResponse<IGameProviders>> {
    return this.post("/game-provider/v2/get-cockfighting-providers", {});
  },
  callGetPokerProviders(): Promise<IApiResponse<IGameProviders>> {
    return this.post("/game-provider/v2/get-poker-providers", {});
  },
  callGetLotteryProviders(): Promise<IApiResponse<IGameProviders>> {
    return this.post("/game-provider/v2/get-lottery-providers", {});
  },
  callGetCreatePromotionDetails(): Promise<
    IApiResponse<IGetCreatePromotionDetailsResponse>
  > {
    return this.post("/player/v2/promotion/get-create-promotion-details", {
      Domain: commonHelper.getDomain(),
    });
  },

  callAgentCooperation(
    request: IFormAgentCooperation
  ): Promise<IApiResponse<IGetCreatePromotionDetailsResponse>> {
    return this.bsiPost("/cash-agent/v2/request-become-to-cash-agent", {
      ...request,
      Domain: commonHelper.getDomain(),
    });
  },
  callCheckIsPlayerSite(): Promise<IApiResponse<IBaseResponse>> {
    return this.bsiPost("/player/v2/check-is-playersite-um", {
      Domain: commonHelper.getDomain(),
    });
  },
  callRecommendMatches(): Promise<IApiResponse<IRecommendMatchResponse>> {
    return this.bsiPost("/player/game/v2/sportsbook/recommend-matches", {
      Language: store.state.locale,
    });
  },
  callLoginByMatchId(
    request: ILoginByMatchIdRequest
  ): Promise<IApiResponse<ILoginByMatchIdResponse>> {
    return this.post("/player/game/v2/sportsbook/login-by-matchid", request);
  },
  callPureLiteBetList(
    request: IRequestBetList
  ): Promise<IApiResponse<IResponseBetList>> {
    return this.post("/game-provider/v2/get-redeem-history", request);
  },
  callResetPassword(
    param: IRequestResetPassword
  ): Promise<IApiResponse<INullDataResponse>> {
    return this.bsiPost("/player/v2/forgot-password", param);
  },
  callGetVersion(): Promise<IApiResponse<IVersionResponse>> {
    return this.bsiGet("/player/v2/get-version");
  },
  callPayoutQrCode(): Promise<IApiResponse<IQrCodePayout>> {
    return this.post("/peru/v2/generate-payout-qrcode", {});
  },
  callScanQrCode(
    req: IAllSportBetsRequest
  ): Promise<IApiResponse<IAllSportBetsResponse>> {
    return this.bsiPost("/peru/v2/scan-qrcode", req);
  },
  callPayTickets(
    req: IPayTicketsRequest
  ): Promise<IApiResponse<IRedeemResponse>> {
    return this.bsiPost("/peru/pay-ticket", req);
  },
  callRedeemTicket(
    req: IRedeemRequest
  ): Promise<IApiResponse<IRedeemResponse>> {
    return this.bsiPost("/game-provider/redeem-ticket", req);
  },
  callGetPreviousQR(req: IPreviousQRReq): Promise<IApiResponse<IPreviousQR>> {
    return this.bsiPost("/peru/v2/found-previous-bets", req);
    // return FakeAPI.getPreviousQR(req);
  },
  callLogout(OnlineId: number): Promise<IApiResponse<IBaseResponse>> {
    return this.post("/player/logout", {
      OnlineId,
    });
  },
  loginToSportApi(
    req: ILoginToSportsRequest
  ): Promise<IApiResponse<ILoginToSportsRespone>> {
    return this.post("/game-provider/login-to-sports-api", req);
  },
  callPlayerSearchGame(
    payload: IPlayerSearchGameRequest
  ): Promise<IApiResponse<undefined>> {
    return this.bsiPost("/player/v2/player-search-game", payload);
  },
  callGetOtpVerification(
    req: IGetOtpVerificaitonRequest
  ): Promise<IApiResponse<undefined>> {
    return this.bsiPost("/player/v2/get-otp", req);
  },
  callUpdateCustomerInfo(
    payload: ICustomerInfoRequest
  ): Promise<IApiResponse<undefined>> {
    return this.post("/telegram-bet-bot/v2/update-customer-info", payload);
  },
  callLoginTelegram(payload: ILoginTelegram): Promise<IApiResponse<string>> {
    return this.bsiPost("/telegram-bet-bot/v2/login-game-provider", payload);
  },
  callGetDistillFP(token: string): Promise<IApiResponse<any>> {
    return this.bsiPost(
      "/player/DecryptCookie",
      {
        Cookie: token,
        SimulateId: 0,
        OperatorId: 0,
        OnlineId: 0,
      },
      false
    );
  },
  callCompanyThemeOptions(
    WebId: number
  ): Promise<IApiResponse<ICompanyThemeOption[]>> {
    return this.bsiPost("/theme/v2/get-company-theme-options", {
      WebId,
    });
  },
  getTelegramBotInformation(
    url: string
  ): Promise<IApiResponse<ICompanyThemeOption[]>> {
    return this.getOutSider(url);
  },
  callUpdateTelegramSsoToken(
    payload: IUpdateTelegramSsoTokenRequest
  ): Promise<IApiResponse<IUpdateTelegramSsoTokenResponse>> {
    return this.post("/telegram-bet-bot/v2/update-telegram-sso-token", payload);
  },
  callGetSiteMessages(
    payload: IGetSiteMessageRequest
  ): Promise<IApiResponse<IGetMessageResponse>> {
    return this.post("/player/v2/get-site-messages", payload);
  },
  callUpdateSiteMessages(
    payload: IUpdateMessageStatus
  ): Promise<IApiResponse<IBaseResponse>> {
    return this.post("/player/v2/update-site-message-status", payload);
  },
  callGetLuckyWheelOptions(
    payload: IGetLuckyWheelOptionsRequest
  ): Promise<IApiResponse<IGetLuckyWheelOptionsResponse>> {
    // return FakeAPI.getLuckyWheelOptions(payload);
    return this.post("/player/get-lucky-wheel-options", payload);
  },
  callGetLuckyWheelResult(
    payload: IGetLuckyWheelSpinResultRequest
  ): Promise<IApiResponse<IGetLuckyWheelSpinResultResponse>> {
    return this.post("/player/v2/get-lucky-Wheel-spin-result", payload);
  },
  callGetAvailableLuckyWheelTicket(): Promise<
    IApiResponse<IGetAvailableLuckyWheelTicketsResponse>
  > {
    return this.post("/player/v2/get-available-lucky-wheel-tickets", {});
  },
};
