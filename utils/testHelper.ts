const waitRAF = () => new Promise((resolve) => requestAnimationFrame(resolve));
export default {
  waitRAF,
};
