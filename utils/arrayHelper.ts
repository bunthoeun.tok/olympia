const arrayHelper = {
    upsert(array: any, item: any, test: string) {
      const i = array.findIndex((_item:any) => _item[test] === item[test]);
      if (i > -1) array[i] = item;
      else array.push(item);
    }
}

export default arrayHelper;