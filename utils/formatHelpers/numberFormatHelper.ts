import _ from "lodash";

const numberFormatHelper = {
  floorTo2Digit(number: number): number {
    return _.floor(number, 2);
  },
  addAccountingFormat(number = 0, prefix = 2, round = true): string {
    if (number) {
      const temp = String(number).split(".");
      const rounded = `${temp[0]}.${
        temp[1] === undefined ? "" : temp[1].substring(0, prefix)
      }`;

      const string = (round ? number : Number(rounded)).toFixed(prefix);
      const [integer, decimal] = string.split(".");
      const newInteger = integer.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      return [newInteger, decimal].filter((x) => x).join(".");
    }
    if (prefix === 0) {
      return "0";
    }
    return "0.00";
  },
  addAccountingFormatWithoutRoundUp(number: number, decimalPlaces = 2): string {
    if (number === 0) return `0.${'0'.repeat(decimalPlaces)}`;
    const numberParts = String(number).split(".");
    numberParts[0] = numberParts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    if (numberParts.length === 1) {
      return numberParts[0] + '.' + '0'.repeat(decimalPlaces);
    }
    const formatted = numberParts[0] + '.' + numberParts[1].substring(0, decimalPlaces).padEnd(decimalPlaces, '0');
    return formatted;
  },
};

export default numberFormatHelper;
