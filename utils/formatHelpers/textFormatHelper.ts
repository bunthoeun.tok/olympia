import { IGame } from "@/models/IGetPlayableGamesResponse";
import { GameBsi, IGameBsi } from "@/models/bsiGameList";
import { format } from "date-fns";
import _ from "lodash";
import { ref } from "vue";

const textHelper = {
  convertCamelCaseToSnakeCase(camelCaseString: string): string {
    return camelCaseString
      .split(/(?=[A-Z])/)
      .join("_")
      .toLowerCase();
  },
  removeAllSpace(stringWithSpace: string): string {
    return stringWithSpace.replace(/ /g, "");
  },
  dateStringTo12HourClock(
    dateString: string,
    isDisplaySecon?: boolean
  ): string {
    let result = "";
    if (dateString) {
      const date = new Date(dateString);
      result = isDisplaySecon
        ? format(date, "yyyy/MM/dd h:mm:ss a")
        : format(date, "yyyy/MM/dd h:mm a");
    }
    return result;
  },
  dateStringTo24HourClock(dateString: string): string {
    let result = "";
    if (dateString) {
      const date = new Date(dateString);
      result = format(date, "yyyy/MM/dd HH:mm");
    }
    return result;
  },
  getDate(dateString: string, isAddDay = false): string {
    const date = new Date(dateString);
    return isAddDay
      ? format(date, "yyyy/MM/dd EEEE")
      : format(date, "yyyy/MM/dd");
  },
  get12HourTime(dateString: string): string {
    const date = new Date(dateString);
    return format(date, "h:mm a");
  },
  getDateFormatDisplay(start: string, end: string): string {
    const startDate = new Date(start);
    const endDate = new Date(end);
    const sameMonth = format(startDate, "MMM") === format(endDate, "MMM");
    const sameYear = format(startDate, "yyyy") === format(endDate, "yyyy");
    let dateDisplay = `${format(startDate, "MMM, dd/yyyy")} - ${format(
      endDate,
      "MMM, dd/yyyy"
    )}`;
    if (sameMonth && sameYear) {
      dateDisplay = `${format(startDate, "MMM, dd")} - ${format(
        endDate,
        "dd"
      )}`;
    } else if (!sameMonth && sameYear) {
      dateDisplay = `${format(startDate, "dd/MMM")} - ${format(
        endDate,
        "dd/MMM, yyyy"
      )}`;
    }
    return dateDisplay;
  },
  getGmtMinusFourDate(
    originalDate: string,
    isStatementDate = false,
    isSetHour = true
  ): string {
    const dateTime = new Date(originalDate);
    const offSetFromLocalToUtc = dateTime.getTimezoneOffset();
    const offSetFromUtcToMinusFour = 60 * 4;
    const newGMTMinus4Date = isStatementDate
      ? new Date(dateTime.getTime()).toString()
      : new Date(
        dateTime.getTime() +
        (offSetFromLocalToUtc - offSetFromUtcToMinusFour) * 60000
      ).toString();
    const date = isSetHour
      ? new Date(newGMTMinus4Date).setHours(0, 0, 0, 0)
      : new Date(newGMTMinus4Date);
    return isStatementDate
      ? format(date, "yyyy-MM-dd HH:mm:ss")
      : format(date, "yyyy/MM/dd HH:mm:ss");
  },
  getDateTimeByGMT(originalDate: string, gmt: number): string {
    const dateTime = new Date(originalDate);
    const offSetFromLocalToUtc = dateTime.getTimezoneOffset();
    const offSetFromUtcToMinusFour = 60 * gmt;
    const newGMTMinus4Date = new Date(
      dateTime.getTime() +
      (offSetFromLocalToUtc - offSetFromUtcToMinusFour) * 60000
    ).toString();
    return `${this.dateStringTo12HourClock(newGMTMinus4Date, true)} GMT${gmt < 0 ? "+" : "-"
      }${-1 * gmt}`;
  },
  addDefaultValue(text: string | undefined | null): string {
    const defaultValue = "--";
    let result = text ?? defaultValue;
    result = result === "" ? defaultValue : result;
    return result;
  },
  getMonthRange(dateString: string): string {
    const date = new Date(dateString);
    return format(date, "MM/dd");
  },
  convertMiliSecToTime(miliSec: number): string {
    if (miliSec <= 0) {
      return "00:00";
    }
    let num = miliSec;
    const hour = Math.floor(num / 3.6e6);
    num %= 3.6e6;
    const minute = Math.floor(num / 60000);
    num %= 60000;
    const second = Math.floor(num / 1000);
    return `${hour > 0 ? `${hour > 9 ? hour : `0${hour}`}:` : ""}${minute > 0 ? `${minute > 9 ? minute : `0${minute}`}:` : ""
      }${second > 9 ? second : `0${second}`}`;
  },
  getDateTimeFromGMTMinusFour(originalDate: string): string {
    const currentGMT = _.first(
      _.split(new Date().toTimeString().slice(9), " (", 1)
    );
    const date = _.replace(
      new Date(originalDate).toString(),
      `${currentGMT}`,
      "GMT-0400"
    );
    const dateTime = new Date(date);
    return dateTime.toString();
  },
  convertString(
    param: string,
    removeParenthesis = false,
    replaceValue = "_"
  ): string {
    try {
      const converted = param
        .trim()
        .toLowerCase()
        .replace(/(\s)+/g, replaceValue);
      if (!removeParenthesis) return converted;
      return converted.replace(/([()])/g, "");
    } catch (error) {
      return "";
    }
  },
  checkMultiple(games: Array<GameBsi> | Array<IGameBsi> | Array<IGame>) {
    const providerCount = ref({} as any);
    games.map((item) => {
      // @ts-ignore
      providerCount.value[item.GameProviderName] = providerCount.value[
        item.GameProviderName
      ]
        ? providerCount.value[item.GameProviderName] + 1
        : 1;
      return item;
    });
    return providerCount.value;
  },
  sortedGames(games: IGameBsi[] | GameBsi[] | IGame[]) {
    return _.sortBy(games, [
      (item) => {
        if (item.TabType?.toLowerCase() === "sports" || item.Category === 1) {
          const objectFilter: Record<string, number> = {
            "SBO SPORT": 1,
            "SBO LIVE": 2,
            "AFB SPORT": 3,
            "AFBMP SPORT": 4,
            "SABA SPORT": 5,
          };
          const found = Object.keys(objectFilter).find((temp) =>
            _.toUpper(item.EnglishGameName).includes(temp)
          );
          return found === undefined ? 999 : objectFilter[found];
        }
        return item.AsiPriority;
      },
      (item) => {
        if (item.TabType?.toLowerCase() === "sports" || item.Category === 1) {
          return item.EnglishGameName;
        }
        return item.DisplayOrder;
      },
    ]);
  },
};

export default textHelper;
