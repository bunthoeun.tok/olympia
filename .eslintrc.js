module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
  },
  parser: "vue-eslint-parser",
  parserOptions: {
    parser: "@typescript-eslint/parser",
  },
  extends: ["@nuxtjs/eslint-config-typescript", "plugin:prettier/recommended"],
  plugins: [],
  rules: {
    "max-classes-per-file": "off",
    "max-len": ["error", { code: 200 }],
    "no-console": ["warn", { allow: ["warn", "error"] }],
    "no-debugger": process.env.NODE_ENV === "production" ? "warn" : "off",
    "no-shadow": "off",
    "no-param-reassign": [2, { props: false }],
    "linebreak-style": "off",
    "lines-between-class-members": "off",
    "vue/no-unused-components": "warn",
    "@typescript-eslint/ban-ts-comment": "off",
    "vue/multi-word-component-names": "off",
    "@typescript-eslint/no-shadow": ["error"],
    "prettier/prettier": [
      "error",
      {
        endOfLine: "auto",
      },
    ],
  },
};
